var http = require('http');
//var orm = require("orm");

httpServer = http.createServer(function(req, res){
	res.end('Hello World\n');
	console.log('Un utilisateur a affiché la page');
});

var oUsers = {};
var aGirls = [{}];
var aBoys = [{}];
var isConnected = false;
var aUtilisateurChatBox = [];

httpServer.listen(1337);


/*
orm.connect("mysql://rm:BseZQM3C@212.227.96.81/rev_beta", function (err, db) {
  if (err) throw err;

    var Person = db.define("user", {
        username      	: String,
        email      		: String
    }, {
        methods: {
            fullName: function () {
                return this.email + ' ' + this.username;
            }
        },
        validations: {
            age: orm.enforce.ranges.number(18, undefined, "under-age")
        }
    });

    Person.find({ username: "Youssef" }, function (err, people) {
        // SQL: "SELECT * FROM person WHERE surname = 'Doe'"

        console.log("People found: %d", people.length);
        console.log("First person: %s, email %d", people[0].fullName(), people[0].email);

        people[0].age = 16;
        people[0].save(function (err) {
            // err.msg = "under-age";
        });
    });
});*/








var io = require('socket.io').listen(httpServer);

io.sockets.on('connection', function(socket){
	var me = false;
	//console.log('ARRIVE 1');
	socket.on('newuser', function(data, callback)
	{
		// On cree le tableau du user connecter pour le remplir
		

		//console.log('ARRIVE 2');
		/*
			username : USER_ACTUALUSERNAME,
			sexe 	 : USER_SEXE,
	        ville    : USER_VILLE,
	        age      : USER_AGE,
	        img      : USER_IMG
		*/
		if ( data.username in oUsers )
		{
			//isConnected = true;
			//console.log('ARRIVE 3');
			//callback(false);
		}
		else
		{
			socket.username = data.username;
			socket.sexe 	= data.sexe;
			oUsers[socket.username] = socket;

			if ( data.sexe == "femme" )
			{
				socket.join('girls');
				socket.broadcast.to('boys').emit('user online', data);
				//aGirls.push(new Object({ username: data.username, ville: data.ville, age: data.age, img: data.img }));
			}
			else
			{
				socket.join('boys');
				socket.broadcast.to('girls').emit('user online', data);
				//aBoys.push(new Object({ username: data.username, ville: data.ville, age: data.age, img: data.img }));
			}
		}
	});

	socket.on('send message', function(data, callback){

		var sMessage = data.message;
		var sReceiver = data.receiver;
		var sSender = data.sender;
		var iLu = data.lu;
		if ( sReceiver in oUsers )
		{
			oUsers[sReceiver].emit('transmit message', { 
				message: sMessage, 
				sender: sSender, 
				receiver: sReceiver,
				lu : iLu
			});
		}
		else
		{
			callback('Mauvaise transmission');
		}
	});

	socket.on('disconnect', function(data){
		/*isConnected = false;
		setTimeout(function(){
			if ( !isConnected )
			{*/
				if (!socket.username) return;
				delete oUsers[socket.username];
				if ( socket.sexe == "femme" ) socket.broadcast.to('boys').emit('user offline', socket.username);
				else socket.broadcast.to('girls').emit('user offline', socket.username);
		/*	}
		}, 5000);*/
	});
});

