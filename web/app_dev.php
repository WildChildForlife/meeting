<?php

use Symfony\Component\HttpFoundation\Request;

// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
umask(0002);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.

// CAFE ESSE : 41.250.38.227
// NEODATE : 196.12.239.50
// MARC MAISON : 41.250.48.110
// MARC BUREAU : 41.251.180.24 / 41.141.127.176 / 196.217.173.110
// CHEZ MOI : 41.251.116.206 / 41.250.87.61
// ABDEL : 105.157.42.140

// Segafredo 196.206.161.252
// Kasdal 196.206.197.100

/*if (//isset($_SERVER['HTTP_CLIENT_IP'])
    //|| isset($_SERVER['HTTP_X_FORWARDED_FOR']) ||
     !in_array(@$_SERVER['REMOTE_ADDR'], array('196.12.239.50', '196.206.161.252', '196.206.197.100', '41.141.121.168'))
) {
    header('HTTP/1.0 403 Forbidden');
    exit(utf8_decode('Vous n\'êtes pas autorisé à accéder à cette page.'));
}*/

date_default_timezone_set("UTC");

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
