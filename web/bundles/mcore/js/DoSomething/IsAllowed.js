$(document).ready(function(){
	/*
		CHATBOX : ENVOYER MESSAGE
	*/
	$(document).on('keypress', '.ChatBoxSendMessage', function(e)
	{
		if ( e.which == 13 )
		{
			e.preventDefault();

			if ( $(this).hasClass('impossible') ) return false;

			// Sauvegarde de l'ajout actuel
			var $this = $(this);
			var $sUsername = $this.parents('.chatbox').find('.entete .username').text();

			// Capture et sanitize du message
			var $sMessage = $this.find('.chatbox_textmessage').val();
			$sMessage = $sMessage.replace(/\s\s+/g, ' ');
			if ( $sMessage.length == 0 || $sMessage === " " ) return false;

			// Implantation du message envoyé
			$this.parents('.chatbox').find('.chatbox_content .mCSB_container').append(
				 appendToChatBox(ACTUALUSERNAME, '', ACTUALUSERNAME, $sMessage)
			);

			// Mise en place des émoticones
			$(".chatbox_content." + $sUsername + " .chatbox_message:last-child").emotions(PATH_EMOTICONES);
			
			// Vidage du champ
			$this.find('.chatbox_textmessage').val('');
			// Scroll en bas
			$(".chatbox_content." + $sUsername).mCustomScrollbar("update");
			$(".chatbox_content." + $sUsername).mCustomScrollbar("scrollTo","bottom");
			
			// Début requete AJAX
			$.ajaxq('sendMessageChat', {
				url: URL_NOUVEAUMESSAGE,
				type: "POST",
				data: 
				{ 
					username: $sUsername, 
					message: $sMessage,
					type : 'chat' 
				}
			// Quand la requete retourne une réponse
			}).done(function( data ) {
			    if ( console && console.log ) 
			    {	
			    	// Si la requete retourne un succés
			    	if ( data.split(':')[0] === "success" )
			    	{
			    		socket.emit('send message', { 
			    			message : $sMessage, 
			    			receiver : $sUsername, 
			    			sender: ACTUALUSERNAME,
			    			lu : data.split(':')[2] 
			    		}, function(data){
			    			console.log(data);
			    		});
			    		$(".chatbox_content." + $sUsername).mCustomScrollbar("scrollTo","bottom");
			    		console.log('luuu '+data.split(':')[2]);
			    	}
			    	else alert("Un problème est survenu (Problème données)");
			    }
			}).fail(function( data ){
				alert("Un problème est survenu (Problème serveur)");
			});
		}
		
	});
});