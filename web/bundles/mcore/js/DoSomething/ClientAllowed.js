    var socket = io.connect('http://212.227.96.81:1337', {
                          'reconnect'                   : true,
                          'reconnection delay'          : 500,
                          'max reconnection attempts'   : 10,
                          'sync disconnect on unload'   : false,
                          'auto connect'                : true
                        });
    socket.heartbeatTimeout = 20000;

    socket.emit('newuser', {
        username        : USER_ACTUALUSERNAME,
        idencoded       : USER_IDENCODED,
        tockenencoded   : USER_TOCKENENCODED,
        sexe            : USER_SEXE,
        ville           : USER_VILLE,
        age             : USER_AGE,
        img             : USER_IMG,
        bloques         : USER_BLOQUE,
        albums          : USER_ALBUM,
        urlprofil       : USER_URLPROFIL,
        role            : USER_ROLE
    });

    socket.on('error', function(){
      socket.socket.reconnect();
    });

    socket.on('user online', function(data){
        if ( $('.chat_content ul').find('.username:Contains("' + data.username + '")').length === 0 )
        {
            if ( data.role === 'ROLE_ADMIN' ) return true;
            var $sAlbum = ( $.inArray(USER_IDENCODED, data.albums ) !== -1 ) ? "yes" : "no";
            if ($.inArray(data.idencoded, USER_BLOQUE) == -1 && $.inArray(USER_IDENCODED, data.bloques) == -1 ) 
            {
                var $sStatus = '';
                if ( !$('.enligne_chat').hasClass('status') ) $sStatus = "style='display:none;'";
                $('.chat_content ul').prepend('\
                    <li class="membres_enligne" ' + $sStatus + '>\
                        <a href="' + data.urlprofil + '"  >\
                            <img src="' + data.img + '" alt="' + data.username + '">\
                            <span class="username">' + data.username + '</span> - ' + data.age + ' ans \
                            <span class="ville">' + data.ville + '</span>\
                        </a>\
                        <a href="#talkTo" rel=\'{ "iSenderID":"' + data.idencoded + '", "sSenderTocken":"' + data.tockenencoded + '", "username":"' + data.username + '", "ville":"' + data.ville + '", "age":"' + data.age + '", "alreadyInContact":"' + $sAlbum + '" }\' title="Discuter avec ' + data.username + '" class="text-indent talk_to tooltip">Talk to</a>\
                    </li>\
                ');
            }
        }
    });

    socket.on('user offline', function(data){
        $('.chat_content').find('.username:Contains(' + data + ')').parents('li').remove();
        //console.log('Utilisateur : ' + data.username + ' offline, supprimé');
    });
    
    socket.on('transmit message', function(data){
        //console.log(data);
        var $bPageMessage = 0;
        var $sUsername = data.sender;
        var $sMessage = data.message;
        var $iLu = data.lu;
        var $sImageProfilUserSender = $('.chatbox_content.' + $sUsername).parents('.chatbox').find('.imageSenderHidden').text();

        $bPageMessage = 2;
        // console.log('client '+$iLu);
        /*
            AUCUNE CONVERSATION EN COURS
            DECLENCHEMENT DES NOTIFICATIONS DE DEMANDE DE CHAT
        */

        if ($iLu == 0 )
        {
            console.log("aucune conversation en cours");
            $bPageMessage = 2.1;
            addDemandeChat(URL_DEMANDECHAT, $sUsername);
            document.title= $sUsername +' dit ...';
        }
        /*
            UNE CONVERSATION EN COURS
            MISE EN PLACE DU MESSAGE
        */
        else if ($iLu == 1) 
        {
            document.title= $sUsername +' dit ...';
            $bPageMessage = 2.2; 
            // SI LA BOX DE CHAT EST REDUITE ON LUI MET UNE SURBRILLANCE
            if ( $('.chatbox_content.' + $sUsername).parents('li.chatbox').hasClass('reduced') )
            {
                $('.chatbox_content.' + $sUsername).parents('li.chatbox').addClass('new');
            }
            $('.chatbox_content.' + $sUsername + ' .mCSB_container').append(
                appendToChatBox(ACTUALUSERNAME, $sImageProfilUserSender, $sUsername, $sMessage)
            );

            // Mise en place des émoticones
            $(".chatbox_content." + $sUsername + " .chatbox_message:last-child").emotions(PATH_EMOTICONES);

            $('.chatbox_content.' + $sUsername).mCustomScrollbar('update');
            $(".chatbox_content." + $sUsername).mCustomScrollbar("scrollTo","bottom");
              $.ajax({
                url: URL_UPDATEDEMANDECHAT,
                type: "POST",
                data : 'sUsername='+ $('.chatbox_textmessage').parents('li').find('span.username').text(),
                success:function(result)
                {
                    console.log(result)
                }
            });
        } 
    });

  