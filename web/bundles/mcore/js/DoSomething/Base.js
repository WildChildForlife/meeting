$(document).ready(function(){
	$(window).bind('beforeunload', function(){ 
        $('.chatbox_container').css({ display:'none' });
        $(".chatbox_content").mCustomScrollbar("destroy");
        $.jStorage.set(ENCODED_ID, $('.chatbox_list').html());
        $.jStorage.set(ENCODED_ID + 'active', $('.chat_menu .status').attr('class').split(" ")[0]);
    });

    var $oChatListBackup = $.jStorage.get(ENCODED_ID);
    var $sActiveChatState = $.jStorage.get(ENCODED_ID + 'active');
    
    $('.chat_menu .' + $sActiveChatState + ' a').trigger('click');

    console.log('.chat_menu .' + $sActiveChatState + ' a')
    console.log($('.chat_menu .' + $sActiveChatState + ' a').trigger('click'))

    $('.chatbox_list').html($oChatListBackup);
    $(".chatbox_content").mCustomScrollbar({
        theme:"dark-thick",
        scrollInertia: 100,
        autoHideScrollbar: true,
        advanced: {
            updateOnContentResize: true, 
            autoScrollOnFocus: true
        }
    });
    $('.chatbox_content').each(function(index)
    {
        $(this).mCustomScrollbar("scrollTo","bottom");
    });

    $('.list_messages a span.message_sample, .list_messages .message').emotions(PATH_EMOTICONES);    
                
    updateRegroupementChat();

    $('.chatbox_content').each(function(index)
    {
        $(this).mCustomScrollbar("scrollTo","bottom");
    });

    $.fn.powerTip.defaults.placement = 'n';
    $('.tooltip').powerTip({
        placement: 'n'
    });

     var Clicked = false;
    $(document).on('submit', '#auth', function(){
        $aPaths = [PATH_DOCUMENTS_CGU, PATH_DOCUMENTS_CHARTEQUALITE];
        popUpCreate('', 'valider_conditions', $aPaths);
        return ( !Clicked ) ? false : true;
    });
    $(document).on('click', '.accepter_conditions', function(){
        Clicked = true;
        $("#auth").submit();
    });

    $(document).on('click', '.decliner_conditions', function(){
        deletePopup();
    });
    $(document).on('click', '.nous_contacter_bouton a, a.declenche_contact', function(e){
        e.preventDefault();
        popUpCreate(URL_SENDCONTACTFORM, 'nous_contacter');
    });
    $(document).on('click', '.oublie_auth', function(e){
        e.preventDefault();
        $this = $(this);
        if ( $.trim($('.username_auth').val()).length <= 5 ) 
        {
            popMeOnError(Array('Oublie de mot de passe', 'Merci de saisir votre pseudo/email avant de ne procéder à la récupération de votre mot de passe'));
        }
        else
        {
            $('.usernameOrEmail').val($.trim($('.username_auth').val()));
            $('.usernameOrEmailForm').submit();
        }
    });
    $(document).on('click', '.redirectA, .redirectA a', function(e) 
    {
        e.preventDefault();
        $oHtml = '\
            <p style="text-align: left;padding-left: 30px;width: 434px;font-size: 12px;color: rgb(80, 76, 76);line-height: 18px;">Cette fonctionnalité est réservée aux membres "Premium". Pour y accéder, veuillez vous rendre à la page "Abonnement du site".</p>\
            <div class="bouttons" style="margin-top: 30px;" >\
                <button class="button close_popup" style="width: 100px;margin-left: -30px;">Plus tard</button>\
                <a class="button" href="' + URL_PROFILABONNEMENT + '" style="margin-left: 60px;" >Voir les offres</a>\
            </div>\
        ';
        popUpCreate($oHtml, 'redirect_abonnement');
    })
    
    $.each($('option:contains("Autres")'), function(index, value){
        $oParent = $(this).parents('select');
        $(this).exchangePositionWith($oParent.children('option:last-child'));
    });

});