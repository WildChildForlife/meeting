function searchOnChatByUsernameExt(force) 
{
	searchOnChatByUsername(force, URL_AJAXCHATGETCONTACTS);
}
$(document).ready(function(){
	/*
		MENU CHAT : EN LIGNE
	*/
	$(document).on('click', '.enligne_chat', function(e){
		e.preventDefault();
		$('.membres_contacts').remove();
		$('.filtres_contacts').hide();
		$('.membres_enligne').slideDown();							

		$(".chat_menu .status").removeClass("status");
		$(".enligne_chat").addClass("status");
	});
	/*
		MENU CHAT : MES CONTACTS
	*/
	$(document).on('click', '.mescontacts_chat', function(e){
		e.preventDefault();
		$('.overlay_chat').removeClass('hide');
		$(".chat_content").mCustomScrollbar("disable");
		$.ajax({
			url: URL_AJAXCHATGETCONTACTS,
			type: "POST",
			data: {
				bContacts: 'yes'
			}
		// Quand la requete retourne une réponse
		}).done(function( data ) {
			$('.membres_contacts').remove();
			$oData = $.parseJSON(data);
			console.log($oData);
			$('.membres_enligne').slideUp();
			$('.filtres_contacts').remove();

			$.each( $oData, function( $iKey, $oValue ) 
	    	{
				appendToChatList($oValue, $iKey, 'membres_contacts');
			});
			$('.overlay_chat').addClass('hide');
			$(".chat_content").mCustomScrollbar("update");

			$(".chat_menu .status").removeClass("status");
			$(".mescontacts_chat").addClass("status");
		}).fail(function( data ){
			alert("Un problème est survenu");
		});
	});
	/*
		MENU CHAT : FILTRE PERSONNALISE
	*/
	$(document).on('click', '.filtrer_button', function(e){
		e.preventDefault();
		$('.filtre_box').slideUp();

		$iAgeDebut = $('#filtre_ageDebut').find('option:selected').val();
		$iAgeFin = $('#filtre_ageFin').find('option:selected').val();
		$iVille = $('#filtre_ville').find('option:selected').val();

		if ( !$.isNumeric($iAgeDebut) && !$.isNumeric($iAgeFin) && !$.isNumeric($iVille) )
		{
			popMeOnError(Array('Recherche', 'Vous n\'avez selectionné aucun critère de recherche à filtrer'));
			return false;
		}

		$('.overlay_chat').removeClass('hide');
		$(".chat_content").mCustomScrollbar("disable");
		$.ajax({
			url: URL_AJAXCHATGETCONTACTS,
			type: "POST",
			data: {
				iAgeDebut: $iAgeDebut,
				iAgeFin: $iAgeFin,
				iVille: $iVille
			}
		// Quand la requete retourne une réponse
		}).done(function( data ) {
			$('.filtres_contacts').remove();

			$oData = $.parseJSON(data);
			$('.membres_enligne').slideUp();
			$('.membres_contacts').remove();

			$.each( $oData, function( $iKey, $oValue ) 
	    	{
				appendToChatList($oValue, $iKey, 'filtres_contacts');
			});
			$('.overlay_chat').addClass('hide');
			$(".chat_content").mCustomScrollbar("update");

			$(".chat_menu .status").removeClass("status");
			$(".filtre_chat").addClass("status");


		}).fail(function( data ){
			alert("Un problème est survenu");
		});
	});
	/*
		MENU CHAT : ACTUALISATION
	*/
	$(document).on('click', '.chat_refresh', function(e){
		e.preventDefault();
		$('.chat_menu .status a').click();
	});
	/*
		DISCUTER EN CHAT
	*/
	$(document).on('click', '.chat_content .talk_to', function(e){
		e.preventDefault();

		var $oData = $.parseJSON($(this).attr('rel'));
		createChatBox( URL_GETMESSAGES, 
					   ACTUALUSERNAME, 
					   $oData.username, 
					   $oData.iSenderID, 
					   $oData.sSenderTocken, 
					   PATH_EMOTICONES,
					   $oData.alreadyInContact
					  );
	});
	/*
		MENU BOX CHAT : AJOUTER AUX CONTACTS
	*/
	$(document).on('click', '.chat_box_submenu .chatbox_ajoutcontact a', function(e)
	{
		e.preventDefault();
		var $this = $(this);
		ajaxMe(e, $this, URL_SETNEWCONTACT, "ajout_contact", "", 'add', function(){
			$this.parents('.chatbox_ajoutcontact').slideUp();
		});
	});
	/*
		MENU BOX CHAT : BLOQUER CONTACT
	*/
	$(document).on('click', '.chat_box_submenu .chatbox_bloquer a', function(e)
	{
		e.preventDefault();

		var $this = $(this);
		var confirm = window.confirm("Une fois bloqué, ce membre ne pourra plus vous contacter d'aucune manière sur notre site. Vous pourrez toujours le débloquer en accédant à la page de votre compte.");
		if ( confirm ) 
		{
			ajaxMe(e, $this, URL_BLOQUERCONTACT, "bloquer", "", "bloquer", function(){
				// Fermeture de la box après bloquage
				$this.parents('.chatbox').find('.chatbox_closeit').click();
				// Suppression du contact de la liste de chat
				$('.chat_content ul li > a[title="' + $this.attr('title') + '"]').parent('li').slideUp(400, function(){
					$(this).remove();
				});
			});
		}
	});

	/*
		MENU BOX CHAT : SIGNALER CONTACT
	*/
	$(document).on('click', '.chat_box_submenu .chatbox_signaler a', function(e)
	{
		e.preventDefault();
		var $this = $(this);

		$('.signaler_content span.username_signaler').text($this.attr('title'));

		popUpCreate($(".signaler_content").html(), 'signaler', PATH_IMAGES);
	});

	/*
		MENU BOX CHAT : ALBUM PERSO
	*/
	$(document).on('click', '.chat_box_submenu .chatbox_albumperso a', function(e)
	{
		e.preventDefault();
		var $this = $(this);

		ajaxMe(e, $this, URL_ALLOWALBUM, "allow_album", "", 'add', function(){
			$this.parents('.chatbox_albumperso').slideUp();
			popMeOnSuccess('Vous avez autorisé votre album perso spécialement pour ce membre.');
		});
	});

	/*
		MENU BOX CHAT : RESTREINDRE ALBUM PERSO
	*/
	$(document).on('click', '.chatbox .chatbox_closeit', function(e)
	{
		e.preventDefault();
		var $this = $(this);
		$sUsernameElement = $this.parents('.chatbox').find('.chat_box_submenu .chatbox_albumperso a');
		$sUsername = $this.parents('.chatbox').find('span.username').text();
		deletefrominchatbox($sUsername,ACTUALUSERNAME,URL_DELETEFROMCHATBOX);
		if ( $this.parents('.chatbox').find('.chatbox_albumperso').css('display') === 'none' )
		{
			ajaxMe(e, $sUsernameElement, URL_ALLOWALBUM, "allow_album", "", 'delete', function(){});
		}
	});

	/*
		ENVOYER UN MESSAGE DE SIGNALEMENT
	*/
	$(document).on('click', '.popup .corps.signaler_membre button.envoyer_message_bouton', function(e)
	{
		e.preventDefault();
		// Sauvegarde de l'ajout actuel
		var $this = $(this);
		$sMessage = $('.dialog .ecrire_message_signaler').val();
		var $bStopMe = false;
		if ( $sMessage.length < 50 )
		{
			popMeOnError(Array('Signalement', 'Vous devez saisir en moins 50 caractères pour pouvoir signaler.'));
			$bStopMe = true;
		}
		else if ( $sMessage.length > 400 )
		{
			popMeOnError(Array('Signalement', 'Vous avez dépassé le maximum de 400 caractères pour pouvoir signaler.'));
			$bStopMe = true;
		}

		if ( $bStopMe )
		{
			return false;
		} 
			
		// Mise en place du loader
		$this.parents('.corps').prepend("<img src='" + PATH_PRELOADER + "' class='preloader'/>")
		$('.ecrire_message_signaler').attr('disabled', 'disabled');
		// Début requete AJAX
		$.ajax({
			url: URL_SIGNALERCONTACT,
			type: "POST",
			data: { 
				username: $('.dialog span.username_signaler').text(), 
				message: $sMessage,
				typeOP : 'add' 
			}
		// Quand la requete retourne une réponse
		}).done(function( data ) {
			// Disparition du loader
			$(".loader").css({ display:"none" });
		    if ( console && console.log ) 
		    {	
		    	// Si la requete retourne un succés
		    	if ( data === "success" )
		    	{
		    		$('.ecrire_message_signaler').removeAttr('disabled');
		    		deletePopup();
		    		popMeOnSuccess('Membre signalé avec succés');
		    	}
		    	else alert("Un problème est survenu");
		    }
		}).fail(function( data ){
			alert("Un problème est survenu (Problème serveur)");
		});
	});

	// Affichage des émoticones
	$(document).on('click', '.chatbox .liste_emo li', function(){
		$oInput = $(this).parents('.chatbox').find('.chatbox_textmessage');
		if ( $oInput.val().substr($oInput.val().length - 1) === " " || $oInput.val().length === 0 )
		{
			$oInput.val($oInput.val() + $(this).find('img').attr('alt'));
		}
		else
		{
			$oInput.val($oInput.val() + ' ' + $(this).find('img').attr('alt'));
		}

		$(this).parents('.liste_emo').slideUp();
	});

	// Affichage des demandes de chat en attente
	$(document).on('click', '.chat_container .chat h3.new', function(e)
	{
		// Début requete AJAX
	    $.ajaxq('DemandesDeChatMessages', {
	        url: URL_DEMANDECHATMESSAGES,
	        type: "POST"
	    // Quand la requete retourne une réponse
	    }).done(function( data ) 
	    {
	    	$oData = $.parseJSON(data);
	    	console.log($oData);
	        if ( console && console.log )
	        {
	        	$sHtml = '';
	        	$.each( $oData, function( $iKey, $oValue ) 
		    	{
					$sHtml += appendToDemandeChat($oValue);
				});
				popUpCreate($sHtml, 'demandeChat');
				$(".dialog.demandeChatBox h3 .nb").text($(".chat_container .chat h3 span").text());
				$('.content_demande .message_demandeChat').emotions(PATH_EMOTICONES);   
				$(".popup .dialog.demandeChatBox .corps").mCustomScrollbar({
					theme:"dark-thick",
					scrollInertia: 100,
					autoHideScrollbar: true,
					advanced: {
						updateOnContentResize: true, 
						autoScrollOnFocus: true
					}
				});	
	        }
	    }).fail(function( data ){
	        alert("Un problème est survenu (Problème serveur)");
	    });	
		
	});

	// En acceptant ou refusant une demande
	$(document).on('click', '.demandeChatBox .buttons button', function(e){
		$this = $(this);
		
		// Début requete AJAX
	    $.ajaxq('UpdateDemandeChatMessage', {
	        url: URL_UPDATEDEMANDECHAT,
	        type: "POST",
	        data : {
	        	sUsername : $this.parents('li').find('.username').text()
	        }
	    // Quand la requete retourne une réponse
	    }).done(function( data ) {
	    	//$oData = $.parseJSON(data);
	        if ( console && console.log )
	        {
	        	$iNbNotif = data;
        		$this.parents('li').slideUp(500, function()
        		{
        			if ( $this.hasClass('accepter_demande') )
	        		{
	        			$.ajaxq('getContactByUsername', {
							url: URL_AJAXCHATGETCONTACTS,
							type: "POST",
							data: {
								sUsername: $this.parents('li').find('.username').text()
							}
						// Quand la requete retourne une réponse
						}).done(function( data ) {

							$oData = $.parseJSON(data);

							$.each( $oData, function( $iKey, $oValue ) 
					    	{
					    		createChatBox(URL_GETMESSAGES, 
										  ACTUALUSERNAME, 
										  $oValue.username, 
										  $oValue.idEncoded, 
										  $oValue.salt, 
										  PATH_EMOTICONES,
										  $oValue.alreadyInContact
										  );
					    		updateDemandeChat($iNbNotif);
					    		deletePopup();
							});
						}).fail(function( data ){
							alert("Un problème est survenu");
						});
	        		}
	        		else
	        		{
	        			updateDemandeChat($iNbNotif);
						deletePopup();
	        		}
        		});
	        }
	    }).fail(function( data ){
	        alert("Un problème est survenu (Problème serveur)");
	    });	
		
	});

	/*
		En cliquant sur le bouton pour chatter avec une personne hors chatlist
	*/
	$(document).on('click', 'li.item1.chatter a, .evenement .talk_to', function(e)
	{
		e.preventDefault();
		if ( $('.body_container.evenement').length !== 0 )
			$sUsername = $(this).attr('rel');
		else 
			$sUsername = $(this).attr('title');
		

		if ( $('.chatbox_list .username:contains("' + $sUsername + '")').length !== 0 ) return false;

		$.ajaxq('getContactByUsername', {
			url: URL_AJAXCHATGETCONTACTS,
			type: "POST",
			data: {
				sUsername: $sUsername
			}
		// Quand la requete retourne une réponse
		}).done(function( data ) {
			$oData = $.parseJSON(data);
			if ( $oData.length === 0 )
			{
				var $aError = Array();
				$aError[0] = 'Tchat';
				$aError[1] = 'Le membre avec qui vous désirez tchatter n\'est pas en ligne.';
				popMeOnError($aError);
			}
			//console.log($oData);
			$.each( $oData, function( $iKey, $oValue ) 
	    	{
	    		createChatBox(URL_GETMESSAGES, 
						  ACTUALUSERNAME, 
						  $oValue.username, 
						  $oValue.idEncoded, 
						  $oValue.salt, 
						  PATH_EMOTICONES,
						  $oValue.alreadyInContact
						  );
			});
		}).fail(function( data ){
			alert("Un problème est survenu");
		});
	});

	$('.chatbox_textmessage').on('focus',function(){
		$this = $(this);
		$.ajax({
	        url: URL_UPDATEDEMANDECHAT,
	        type: "POST",
	        data : 'sUsername='+ $this.parents('li').find('span.username').text(),
	        success:function(result)
	        {
	        	console.log(result)
	        }
	    });
	});
	updateRegroupementChat();

	
	if ( $('.chatbox .chatbox_content').length != 0)
	{
		var usernames = [];

		$("ul.chatbox_list li span.username").each(function() { usernames.push($(this).text()) });

		
		for (i=0 ; i<usernames.length; i++)
		{
		lastmessage = $('.chatbox_content.' + usernames[i]).parents('li').find(".received:last-child").attr('id');
		// console.log(lastmessage+'  '+usernames[i]);
		getLastesmessagechat(usernames[i],ACTUALUSERNAME,lastmessage,URL_GETLASTMESSAGECHAT);	
		}
		
		
	}


});
