$(document).ready(function(){
    $(document).on('click', '.popup button.envoyer_message_bouton', function(e)
    {
        e.preventDefault();
        if ( $(this).parents('.corps').hasClass('signaler_membre') ) return false;
        // Sauvegarde de l'ajout actuel
        var $this = $(this);
        $sUsername = $.trim($('.popup .dialog .username').text());
        // Mise en place du loader
        $this.parents('.corps').prepend("<img src='" + PATH_PRELOADER + "' class='preloader'/>")
        $('.ecrire_message').attr('disabled', 'disabled');
        // Début requete AJAX
        $.ajax({
            url: URL_NOUVEAUMESSAGE,
            type: "POST",
            data: 
            { 
                username: $sUsername, 
                message: $('.ecrire_message').val() 
            }
        // Quand la requete retourne une réponse
        }).done(function( data ) {
            // Disparition du loader
            $(".loader").css({ display:"none" });
            if ( console && console.log ) 
            {   
                // Si la requete retourne un succés
                if ( data.split(':')[0] === "success" )
                {
                    deletePopup();
                    popMeOnSuccess('Message envoyé avec succés');
                }
                else alert("Un problème est survenu au niveau du serveur");
            }
        }).fail(function( data ){
            alert("Un problème est survenu");
        });
    });
});