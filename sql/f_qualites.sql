-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 30 Décembre 2013 à 12:05
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `meeting`
--

-- --------------------------------------------------------

--
-- Structure de la table `f_qualites`
--

CREATE TABLE IF NOT EXISTS `f_qualites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `homme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `femme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Contenu de la table `f_qualites`
--

INSERT INTO `f_qualites` (`id`, `homme`, `femme`) VALUES
(1, 'Ambitieux', 'Ambitieuse'),
(2, 'Sensible', 'Sensible'),
(3, 'Charmeur', 'Charmeuse'),
(4, 'Sérieux', 'Sérieuse'),
(5, 'Curieux', 'Curieuse'),
(6, 'Sincère', 'Sincère'),
(7, 'Créatif', 'Créative'),
(8, 'Sociable', 'Sociable'),
(9, 'Drôle', 'Drôle'),
(10, 'Souriant', 'Souriante'),
(11, 'Dynamique', 'Dynamique'),
(12, 'Sportif', 'Sportive'),
(13, 'Élégant', 'Élégante'),
(14, 'Tolérant', 'Tolérante'),
(15, 'Fidèle', 'Fidèle'),
(16, 'Calme', 'Calme'),
(17, 'Généreux', 'Généreuse'),
(18, 'Tendre', 'Tendre'),
(19, 'Honnête', 'Honnête'),
(20, 'Travailleur', 'Travailleuse');
