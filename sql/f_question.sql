-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 04 Mars 2014 à 21:32
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `rm`
--

-- --------------------------------------------------------

--
-- Structure de la table `f_question`
--

CREATE TABLE IF NOT EXISTS `f_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `reponse` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Contenu de la table `f_question`
--

INSERT INTO `f_question` (`id`, `question`, `reponse`) VALUES
(1, 'a:2:{i:0;s:43:"Êtes-vous plutôt rêveur ou pragmatique ?";i:1;s:44:"Êtes-vous plutôt rêveuse ou pragmatique ?";}', 'a:2:{i:0;a:2:{i:0;s:7:"Rêveur";i:1;s:11:"Pragmatique";}i:1;a:1:{i:0;s:8:"Rêveuse";}}'),
(2, 'a:2:{i:0;s:58:"En public, êtes-vous plutôt du genre timide ou avenant ?";i:1;s:59:"En public, êtes-vous plutôt du genre timide ou avenante ?";}', 'a:2:{i:0;a:2:{i:0;s:6:"Timide";i:1;s:7:"Avenant";}i:1;a:2:{i:0;s:6:"Timide";i:1;s:8:"Avenante";}}'),
(3, 'a:2:{i:0;s:40:"Etes-vous plutôt patient ou impatient ?";i:1;s:42:"Etes-vous plutôt patiente ou impatiente ?";}', 'a:2:{i:0;a:2:{i:0;s:7:"Patient";i:1;s:9:"Impatient";}i:1;a:2:{i:0;s:8:"Patiente";i:1;s:10:"Impatiente";}}'),
(4, 'a:1:{i:0;s:83:"Selon vous, le physique est-il un élément fondamental pour entamer une relation ?";}', 's:0:"";'),
(5, 'a:2:{i:0;s:60:"Pensez-vous être perfectionniste et attentif aux détails ?";i:1;s:61:"Pensez-vous être perfectionniste et attentive aux détails ?";}', 's:0:"";'),
(6, 'a:1:{i:0;s:59:"Vous prenez vos décisions avec votre coeur ou votre tête?";}', 'a:1:{i:0;a:2:{i:0;s:5:"Coeur";i:1;s:5:"Tête";}}'),
(7, 'a:1:{i:0;s:42:"Préférez vous la vie rurale ou urbaine ?";}', 'a:1:{i:0;a:2:{i:0;s:6:"Rurale";i:1;s:7:"Urbaine";}}'),
(8, 'a:2:{i:0;s:43:"Etes-vous plutôt aventurier ou casanier  ?";i:1;s:46:"Etes-vous plutôt aventurière ou casanière ?";}', 'a:2:{i:0;a:2:{i:0;s:10:"Aventurier";i:1;s:8:"Casanier";}i:1;a:2:{i:0;s:12:"Aventurière";i:1;s:10:"Casanière";}}'),
(9, 'a:1:{i:0;s:64:"Préférez-vous partir en vacances à la montagne ou à la mer ?";}', 'a:1:{i:0;a:2:{i:0;s:8:"Montagne";i:1;s:3:"Mer";}}'),
(10, 'a:1:{i:0;s:37:"La politique vous intéresse-t-elle ?";}', 's:0:"";'),
(11, 'a:1:{i:0;s:59:"Êtes-vous sensible à la protection de l’environnement ?";}', 's:0:"";'),
(12, 'a:2:{i:0;s:46:"Préférez-vous travailler seul ou en groupe ?";i:1;s:47:"Préférez-vous travailler seule ou en groupe ?";}', 'a:2:{i:0;a:2:{i:0;s:4:"Seul";i:1;s:9:"En groupe";}i:1;a:1:{i:0;s:5:"Seule";}}}'),
(13, 'a:1:{i:0;s:51:"Pratiquez-vous une activité physique régulière ?";}', 's:0:"";'),
(14, 'a:1:{i:0;s:39:"Jouez-vous d''un instrument de musique ?";}', 's:0:"";'),
(15, 'a:1:{i:0;s:37:"Aimez-vous les animaux de compagnie ?";}', 's:0:"";');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
