-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 30 Décembre 2013 à 12:04
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `meeting`
--

-- --------------------------------------------------------

--
-- Structure de la table `f_centresinteret`
--

CREATE TABLE IF NOT EXISTS `f_centresinteret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C2F862A7BCF5E72D` (`categorie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=132 ;

--
-- Contenu de la table `f_centresinteret`
--

INSERT INTO `f_centresinteret` (`id`, `categorie_id`, `nom`) VALUES
(1, 1, 'Alternative'),
(2, 1, 'Chaâbi'),
(3, 1, 'Classique'),
(4, 1, 'Electro'),
(5, 1, 'Latino'),
(6, 1, 'Rock'),
(7, 1, 'Folk / Blues'),
(8, 1, 'Funk'),
(9, 1, 'Gospel'),
(10, 1, 'Hard Rock'),
(11, 1, 'Hip-Hop'),
(12, 1, 'Jazz'),
(13, 1, 'Mezoued'),
(14, 1, 'Opéra'),
(15, 1, 'Orientale'),
(16, 1, 'Pop'),
(17, 1, 'Raï'),
(18, 1, 'Reggae'),
(19, 1, 'Variété'),
(20, 1, 'Autres'),
(21, 2, 'Action'),
(22, 2, 'Animation'),
(23, 2, 'Aventure'),
(24, 2, 'Bollywood'),
(25, 2, 'Classique'),
(26, 2, 'Combat'),
(27, 2, 'Comédie'),
(28, 2, 'Dessin Animé'),
(29, 2, 'Drame'),
(30, 2, 'Horreur'),
(31, 2, 'Espionnage'),
(32, 2, 'Fantastique'),
(33, 2, 'Guerre'),
(34, 2, 'Historique'),
(35, 2, 'Musical'),
(36, 2, 'Péplum'),
(37, 2, 'Policier'),
(38, 2, 'Romantique'),
(39, 2, 'Sciences Fiction'),
(40, 2, 'Séries TV'),
(41, 2, 'Thriller'),
(42, 2, 'Western'),
(43, 2, 'Autres'),
(44, 3, 'Africaine'),
(45, 3, 'Chinoise'),
(46, 3, 'De Maman'),
(47, 3, 'Fast Food'),
(48, 3, 'Française'),
(49, 3, 'Gastronomique'),
(50, 3, 'Grecque'),
(51, 3, 'Italienne'),
(52, 3, 'Indienne'),
(53, 3, 'Japonaise'),
(54, 3, 'Libanaise'),
(55, 3, 'Marocaine'),
(56, 3, 'Mediterranéenne'),
(57, 3, 'Mexicaine'),
(58, 3, 'Orientale'),
(59, 3, 'Thaïlandaise'),
(60, 3, 'Turque'),
(61, 4, 'Automobile'),
(62, 4, 'Ballades'),
(63, 4, 'Bricolage'),
(64, 4, 'Caritatif'),
(65, 4, 'Cinéma'),
(66, 4, 'Couture'),
(67, 4, 'Cuisine'),
(68, 4, 'Danse'),
(69, 4, 'Décoration'),
(70, 4, 'Dessin'),
(71, 4, 'Expos Art'),
(72, 4, 'Fitness'),
(73, 4, 'Informatique'),
(74, 4, 'Internet'),
(75, 4, 'Jardinage'),
(76, 4, 'Jeux Video'),
(77, 4, 'Lecture'),
(78, 4, 'Musique'),
(79, 4, 'Peinture'),
(80, 4, 'Photo'),
(81, 4, 'Poker/Carte'),
(82, 4, 'Restaurants'),
(83, 4, 'Shopping'),
(84, 4, 'Sport'),
(85, 4, 'Télévision'),
(86, 4, 'Théâtre'),
(87, 4, 'Voyage'),
(88, 4, 'Autres'),
(89, 5, 'After Work'),
(90, 5, 'Bar/Pub'),
(91, 5, 'Café'),
(92, 5, 'Cinéma'),
(93, 5, 'Concert'),
(94, 5, 'Discothèque'),
(95, 5, 'Event Sport'),
(96, 5, 'En Famille'),
(97, 5, 'Entre Amis'),
(98, 5, 'Karaoké'),
(99, 5, 'Lecture'),
(100, 5, 'Opéra'),
(101, 5, 'Pleine Nature'),
(102, 5, 'Shopping'),
(103, 5, 'Restau'),
(104, 5, 'Autres'),
(105, 6, '2M TV'),
(106, 6, 'Al Jazeera'),
(107, 6, 'Arté'),
(108, 6, 'Aswat'),
(109, 6, 'Radio 2M'),
(110, 6, 'Atlantic '),
(111, 6, 'Canal +'),
(112, 6, 'Cap Radio'),
(113, 6, 'Casa FM'),
(114, 6, 'France TV'),
(115, 6, 'Luxe Radio'),
(116, 6, 'Med Radio'),
(117, 6, 'MFM'),
(118, 6, 'Al Idaa'),
(119, 6, 'Hit Radio'),
(120, 6, 'M6'),
(121, 6, 'Chada FM'),
(122, 6, 'Chaîne Inter'),
(123, 6, 'Medi 1 Radio'),
(124, 6, 'Medi 1 TV'),
(125, 6, 'Radio Plus'),
(126, 6, 'Radio Sawa'),
(127, 6, 'Radio Mars'),
(128, 6, 'RTM'),
(129, 6, 'Medina FM'),
(130, 6, 'MFJ Radio'),
(131, 6, 'TF1');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `f_centresinteret`
--
ALTER TABLE `f_centresinteret`
  ADD CONSTRAINT `FK_825DEB21BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categoriecentresinteret` (`id`);
