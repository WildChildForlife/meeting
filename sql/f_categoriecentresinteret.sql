-- phpMyAdmin SQL Dump
-- version 4.0.3
-- http://www.phpmyadmin.net
--
-- Client: 127.0.0.1
-- Généré le: Lun 11 Novembre 2013 à 01:20
-- Version du serveur: 5.6.11-log
-- Version de PHP: 5.5.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `meeting`
--

-- --------------------------------------------------------

--
-- Structure de la table `categoriecentresinteret`
--

CREATE TABLE IF NOT EXISTS `f_categoriecentresinteret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `categoriecentresinteret`
--

INSERT INTO `f_categoriecentresinteret` (`id`, `nom`, `question`) VALUES
(1, 'Musique', 'Mes artistes préférés'),
(2, 'Cinéma', 'Mes films préférés'),
(3, 'Cuisine', 'Mes restaurants préféré'),
(4, 'Loisirs', 'Mes Loisirs préféré'),
(5, 'Sorties', 'Mes Sorties préféré'),
(6, 'Medias TV & Radios', 'Mes émission préféré');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
