$(document).ready(function(){
	/*
		CHATBOX : ENVOYER MESSAGE
	*/
	$(document).on('keypress', '.ChatBoxSendMessage', function(e)
	{
		if ( e.which == 13 )
		{
			e.preventDefault();

			if ( $(this).hasClass('impossible') ) return false;

			// Sauvegarde de l'ajout actuel
			var $this = $(this);
			var $sUsername = $this.parents('.chatbox').find('.entete .username').text();

			var $sImageSender = $this.parents('.chatbox').find('.imageSenderHidden').text();

			var $sMessage = 'Pour tchater avec ' + $sUsername + ' et communiquer en illimité sur Rencontres & Vous, <a href="' + URL_ABONNEMENT + '" >Devenez  membre Premium !</a>' ;
			
			var $sTempID = new Date().getTime();
			$sTempID = parseInt($sTempID) + (1 + Math.floor(Math.random() * 6));

			$this.parents('.chatbox').find('.chatbox_content .mCSB_container').append(
				 appendToChatBox(ACTUALUSERNAME, '', ACTUALUSERNAME, $sMessage, $sTempID)
			);

			$(".chatbox_content." + $sUsername + " .chatbox_message").emotions(PATH_EMOTICONES);
			
			$this.find('.chatbox_textmessage').val('');
			$(".chatbox_content." + $sUsername).mCustomScrollbar("update");
			$(".chatbox_content." + $sUsername).mCustomScrollbar("scrollTo","bottom");
						
			$(".chatbox_content." + $sUsername).mCustomScrollbar("scrollTo","bottom");
			
		}
		
	});
});