/* Création du popUp pour croper */
function popUpCreate (html, type, imagesPath) 
{
	if ( $(".popup").length !== 0 ) return false;

	html = typeof html !== 'undefined' ? html : "";
	imagesPath = typeof imagesPath !== 'undefined' ? imagesPath : "";

	var PopMe = '';
	if ( type === 'crop' )
	{
		PopMe = "\
		<div class='popup' >" + html + "\
			<aside class='footer'> \
				<h3>Rognage de la photo de profil :</h3> \
				<p>Veuillez selectionner la partie que vous voulez rogner.</p> \
				<button class='valider_crop'>Valider</button> \
				<button class='annuler_crop'>Annuler</button> \
			</aside>\
		</div>";
	}
	else if ( type === 'valider_conditions' )
	{
		PopMe = "\
		<div class='popup' >" + html + "\
			<a class='logo' href='/' title='Rencontres &amp; Vous - Entre Célibataires Sérieux au Maroc'>Rencontres &amp; Vous - Entre Célibataires Sérieux au Maroc</a> \
			<div class='validation'> \
				<p>Je certifie être majeur(e), avoir lu et accepté l'ensemble des <a href='" + imagesPath[0] + "' target='_blank' >CGUV</a> ainsi que la <a href='" + imagesPath[1] + "' target='_blank'>charte qualité</a> du site 'Rencontres &amp; Vous'.</p> \
				<button class='decliner_conditions'>Décliner les conditions</button> \
				<button class='accepter_conditions'>Accepter les conditions</button> \
			</div>\
		</div>";
	}
	else if ( type === 'voir_album' )
	{
		PopMe = "<div class='popup' ><button class='close_popup'>Fermer</button>" + html + "</div>";
	}
	else if ( type === 'envoyer_message' )
	{
		PopMe = "\
		<div class='popup' >\
			<div class='dialog' >\
				<h3>Envoyer un message à <span class='username'>" + html + "</span><span class='close_popup' >X</span></h3>\
				<div class='corps' >\
					<textarea class='ecrire_message'></textarea>\
					<div class='declenche_emo'></div>\
					<button class='close_popup button'>Annuler</button>\
					<button class='button envoyer_message_bouton'>Envoyer le message</button>\
					<div class='liste_emo' >\
						<ul>\
							<li><img alt='o:)' title='o:)' src='" + imagesPath + "emoticons/angel.gif' /></li>\
							<li><img alt='o.O' title='o.O' src='" + imagesPath + "emoticons/confused.gif' /></li>\
							<li><img alt='3:)' title='3:)' src='" + imagesPath + "emoticons/devil.gif' /></li>\
							<li><img alt=':O' title=':O' src='" + imagesPath + "emoticons/gasp.gif' /></li>\
							<li><img alt=':D' title=':D' src='" + imagesPath + "emoticons/grin.gif' /></li>\
							<li><img alt='<3' title='<3' src='" + imagesPath + "emoticons/heart.gif' /></li>\
							<li><img alt=':*' title=':*' src='" + imagesPath + "emoticons/kiss.gif' /></li>\
							<li><img alt=':)' title=':)' src='" + imagesPath + "emoticons/smile.gif' /></li>\
							<li><img alt='8|' title='8|' src='" + imagesPath + "emoticons/sunglasses.gif' /></li>\
							<li><img alt=':/' title=':/' src='" + imagesPath + "emoticons/unsure.gif' /></li>\
							<li><img alt=';)' title=';)' src='" + imagesPath + "emoticons/wink.gif' /></li>\
							<li><img alt=':3' title=':3' src='" + imagesPath + "emoticons/colonthree.gif' /></li>\
							<li><img alt=\":'(\" title=\":'(\" src='" + imagesPath + "emoticons/cry.gif' /></li>\
							<li><img alt=':(' title=':(' src='" + imagesPath + "emoticons/frown.gif' /></li>\
							<li><img alt='8)' title='8)' src='" + imagesPath + "emoticons/glasses.gif' /></li>\
							<li><img alt='>:(' title='>:(' src='" + imagesPath + "emoticons/grumpy.gif' /></li>\
							<li><img alt='^_^' title='^_^' src='" + imagesPath + "emoticons/kiki.gif' /></li>\
							<li><img alt=':v' title=':v' src='" + imagesPath + "emoticons/pacman.gif' /></li>\
							<li><img alt='-_-' title='-_-' src='" + imagesPath + "emoticons/squint.gif' /></li>\
							<li><img alt=':p' title=':p' src='" + imagesPath + "emoticons/tongue.gif' /></li>\
							<li><img alt='>:O' title='>:O' src='" + imagesPath + "emoticons/upset.gif' /></li>\
						</ul>\
					</div>\
				</div>\
			</div>\
		</div>";
	}
	else if ( type === 'edit_pass' )
	{
		PopMe = "\
		<div class='popup' >\
			<div class='dialog' >\
				<h3>Modifier votre mot de passe<span class='close_popup' >X</span></h3></h3>\
				<div class='corps' >\
					<p class='notice'>Votre mot de passe doit contenir de 8 à 16 caractères et doit se composer de chiffres et lettres.</p>\
					" + html + "\
				</div>\
			</div>\
		</div>";
	}
	else if ( type === 'unsubscribe' )
	{
		PopMe = "\
		<div class='popup' >\
			<div class='dialog' >\
				<h3>Me désinscrire du site<span class='close_popup' >X</span></h3></h3>\
				<div class='corps' >\
					<p class='notice'>Pour vous désinscrire de notre site, remplissez les champs suivants.</p>\
					<p class='notice'>La désinscription est définitive et l'ensemble des informations relatives à votre compte seront supprimées.</p>\
					" + html + "\
				</div>\
			</div>\
		</div>";
	}
	else if ( type === 'agenda' )
	{
		PopMe = "\
		<div class='popup' >\
			<div class='dialog' >\
				" + html  + "\
			</div>\
		</div>";
	}
	else if ( type === 'signaler' )
	{
		PopMe = "\
		<div class='popup' >\
			<div class='dialog' >\
				" + html + "\
			</div>\
		</div>";
	}
	else if ( type === 'redirect_abonnement' )
	{
		PopMe = "\
		<div class='popup redirect_abonnement' >\
			<div class='dialog' >\
				<h3>Devenez Membre Premium</h3>\
				<div class='corps' >\
					" + html + "\
				</div>\
			</div>\
		</div>";
	}
	else if ( type === 'demandeChat' )
	{
		PopMe = "\
		<div class='popup' >\
			<div class='dialog demandeChatBox' >\
				<h3>Vous avez <span class='nb'>3</span> nouvelles demandes de Tchat<span class='close_popup' >X</span></h3></h3>\
				<div class='corps' >\
					<ul>" + html + "</ul>\
				</div>\
			</div>\
		</div>";
	}
	else if ( type === 'nous_contacter' )
	{
		PopMe = "\
		<div class='popup' >\
			<div class='dialog' >\
				<h3>Contacter l'équipe Rencontres &amp; Vous<span class='close_popup' >X</span></h3></h3>\
				<div class='corps' >\
					<p class='notice'>Veuillez renseigner les champs ci-dessous, le service concerné vous répondra rapidement.</p>\
					<form class='form_contact' method='POST' action='" + html + "' >\
						<div class='block_field'>\
			    			<span class='circle'></span>\
			    			<label for='form_contact_service'>Service concerné* :</label>\
			    			<select id='form_contact_service' name='service' required >\
			    				<option value='' ></option>\
			    				<option value='clientele' >Clientèle</option>\
			    				<option value='technique' >Technique</option>\
			    				<option value='presse' >Presse</option>\
			    				<option value='recrutement' >Recrutement</option>\
			    				<option value='temoignages' >Témoignages</option>\
			    				<option value='autre' >Autre</option>\
			    			</select>\
			    		</div>\
			    		<div class='block_field'>\
			    			<span class='circle' ></span>\
			    			<label for='form_contact_nomprenom' >Nom et prénom* :</label>\
			    			<input id='form_contact_nomprenom' name='nomprenom' type='text' required />\
			    		</div>\
			    		<div class='block_field'>\
			    			<span class='circle'></span>\
			    			<label for='form_contact_societe'>Société :</label>\
			    			<input id='form_contact_societe' name='societe' type='text' />\
			    		</div>\
			    		<div class='block_field'>\
			    			<span class='circle'></span>\
			    			<label for='form_contact_email'>Email* :</label>\
			    			<input id='form_contact_email' name='email' type='email' required />\
			    		</div>\
			    		<div class='block_field'>\
			    			<span class='circle'></span>\
			    			<label for='form_contact_tel'>Téléphone :</label>\
			    			<input id='form_contact_tel' name='tel' type='text' />\
			    		</div>\
			    		<div class='block_field'>\
			    			<span class='circle'></span>\
			    			<label for='form_contact_objet'>Objet du message* :</label>\
			    			<input id='form_contact_objet' name='objet' type='text' required />\
			    		</div>\
			    		<div class='block_field'>\
			    			<span class='circle'></span>\
			    			<label for='form_contact_message'>Message* :</label>\
			    			<textarea id='form_contact_message' name='message' ></textarea>\
			    		</div>\
			    		<input type='submit' value='Envoyer' name='contacter' class='button' />\
					</form>\
				</div>\
			</div>\
		</div>";
	}
	$("body").prepend(PopMe);
	//$("body").scrollTop();
	if (type === 'voir_album')
	{
		$('.popup .album').galleryView({
	 		transition_speed : 500,
	 		transition_interval : 3000,
	 		panel_scale : 'fit',
	 		pan_images : true,
	 		autoplay : true
	 		/*filmstrip_position : 'right',
	 		panel_width : 300,
	 		panel_height : 300*/
	 	});
	}
	$(".popup").css({ top: $(document).scrollTop() });
	$("body").css({ overflow:'hidden' });
}
function deletePopup () 
{
	if ( $(".popup").length !== 0 )
	{
		$(".popup").remove();
		$("body").css({ overflow:'auto' });
	}
}	
function ajaxMe (Event, Element, root, className, preloader, operation, onSuccess) 
{
	Event.preventDefault();
	operation = typeof operation !== 'undefined' ? operation : "add";
	className = typeof className !== 'undefined' ? className : "";
	// Sauvegarde de l'ajout actuel
	var $this = Element;
	// Cacher le coeur (image)
	$this.children('img').addClass('hidden');
	// Mise en place du loader
	if ( preloader !== "" ) $this.prepend("<img src='" + preloader + "' class='loader'/>")
	// Début requete AJAX

	$.ajax({
		url: root,
		type: "POST",
		data: { username: $this.attr('title'), typeOP: operation }
	// Quand la requete retourne une réponse
	}).done(function( data ) {
		// Disparition du loader
		$(".loader").css({ display:"none" });
	    if ( console && console.log ) 
	    {
	    	// Si la requete retourne un succés
	    	if ( data === "success" )
	    	{
	    		if ( typeof onSuccess === "function" ){ onSuccess(); }
	    		else
	    		{
		    		// Réapparition du coeur
		    		$this.children('img').removeClass('hidden');
		    		// Dégradation de l'opacité du coeur
		    		$this.css({ opacity: 0.2, cursor: 'default' });
		    		// Désactivation de reclique sur le coeur
		    		$this.parent('li').removeClass(className);
		    	}
		    	switch(className)
		    	{
		    		case 'coup_coeur'    : 	popMeOnSuccess('Coup de coeur envoyé');
		    								break;
		    		case 'ajout_contact' : 	popMeOnSuccess('Contact ajouté avec succés');
		    								break;
		    		case 'bloquer'		 :  popMeOnSuccess('Membre bloqué avec succés');
		    								break;

		    	}
	    	}
	    	else if(data == 'permissionDenied')
	    	{
	    		$oHtml = '\
            <p style="text-align: left;padding-left: 30px;width: 434px;font-size: 12px;color: rgb(80, 76, 76);line-height: 18px;">Cette fonctionnalité est réservée aux membres "Premium". Pour y accéder, veuillez vous rendre à la page "Abonnement du site".</p>\
            <div class="bouttons" style="margin-top: 30px;" >\
                <button class="button close_popup" style="width: 100px;margin-left: -30px;">Plus tard</button>\
                <a class="button" href="' + URL_PROFILABONNEMENT + '" style="margin-left: 60px;" >Voir les offres</a>\
            </div>\
        	';
        	popUpCreate($oHtml, 'redirect_abonnement');
	    			// alert('dazdza');
	    	}

	    	else alert("Un problème est survenu");
	    }
	}).fail(function( data ){
		alert("Un problème est survenu au niveau du serveur");
	});
}
function appendToMessageConv (sType, oObject, sUsername, sImage, sMessage, iMessageId, sSexe, sTime, sOrientation) 
{
	sTime = typeof sTime !== 'undefined' ? sTime : "Il y a 0 minutes";
	sOrientation = typeof sOrientation !== 'undefined' ? sOrientation : ".last";

	var sSame = ($(oObject).find('li.last .username').text() === sUsername) ? ' same' : '';
	$(oObject).find('li.last').removeClass('last');
	var ToAppend = '\
	<li class="last' + sSame + '">\
		<div class="left">\
			<img src="' + sImage + '" alt="femme1">\
			<span class="connectivite enligne"></span>\
			<a href="#"><span class="username ' + sSexe + '">' + sUsername + '</span></a>\
			<span class="message">' + sMessage + '</span>\
		</div>\
		<div class="right">\
			<div class="time_container">\
				<span class="date_envoi">' + sTime + '</span>\
				<span class="checkbox"><input type="checkbox" name="m_corebundle_usertype[delete_message][' + sType + '][]" value="' + iMessageId + '"></span>\
			</div>\
		</div>\
	</li>';
	if ( sOrientation === '.last' )
	{
		$(oObject).append(ToAppend);
		$(".messages.conversation_detail .list_messages").mCustomScrollbar("update");
		$(".messages.conversation_detail .list_messages").mCustomScrollbar("scrollTo", sOrientation);
	}		
	else $(oObject).prepend(ToAppend); 
		

}
function appendToChatList($oValue, $iKey, $sClass) 
{
	var ToAppend = '\
	<li class="' + $sClass + '">\
		<a href="' + $oValue.sUrl + '" title="' + $oValue.username +'" >\
			<img src="' + $oValue.image + '" alt="' + $oValue.username +'">\
			<span class="username">' + $oValue.username +'</span> - ' + $oValue.age + ' ans \
			<span class="ville">' + $oValue.ville + '</span>\
		</a>\
		<a href="#talkTo" rel=\'{ "iSenderID":"' + $oValue.idEncoded + '", "sSenderTocken":"' + $oValue.salt +'", "username":"' + $oValue.username + '", "ville":"' + $oValue.ville + '", "age":"' + $oValue.age + '", "alreadyInContact":"yes" }\' title="' + $oValue.username +'" class="text-indent talk_to">Talk to</a>\
	</li>';

	$('.chat_content ul').append(ToAppend);
}


function popMeOnSuccess (text_output, autoHide) 
{
	autoHide = typeof autoHide !== 'undefined' ? autoHide : 3000;
	var n = noty({
        text        : text_output,
        type        : 'success',
        dismissQueue: true,
        layout      : 'top',
        theme       : 'defaultTheme',
        maxVisible  : 1,
        animation: {
	        open: {height: 'toggle'},
	        close: {height: 'toggle'},
	        easing: 'swing',
	        speed: 500 // opening & closing animation speed
	    },
	    timeout: autoHide,
    });
    $('#noty_top_layout_container li').css({ float:'none', color:'white', borderColor:'white' });
    $('#noty_top_layout_container li span').css({ fontSize:'14px', letterSpacing:'1px', fontFamily:'Arial' });

    if ( $('body').hasClass('Homme') ) $('#noty_top_layout_container li').css({ backgroundColor:'#0090bb' });
    else if ( $('body').hasClass('Femme') ) $('#noty_top_layout_container li').css({ backgroundColor:'#c21570' });
}
function popMeOnError (text_output) {
	var $sText = text_output;
	if ( typeof text_output === 'object' )
	{
		$aArray = text_output;
		$sText = '';
		$.each($aArray, function( $iKey, $oValue ) 
    	{
    		if ( ( $iKey % 2 ) === 0 )
    		{
    			$sText += '<b>' + $oValue + ' : </b> ';
    			return true;
    		}
    		else 
    		{
    			$sText += $oValue;	
    			var n = noty({
			        text        : $sText,
			        type        : 'error',
			        dismissQueue: true,
			        layout      : 'topCenter',
			        theme       : 'defaultTheme',
			        maxVisible  : 1,
			        maxVisible  : 10,
			        animation: {
				        open: {height: 'toggle'},
				        close: {height: 'toggle'},
				        easing: 'swing',
				        speed: 500 // opening & closing animation speed
				    },
				    timeout: 5000,
			    });
    		} 
    		$sText = '';
		});
		$('.noty_type_error').css({ backgroundColor:'#ca344d' });
		$('.noty_type_error .noty_text').css({ fontFamily:'Arial' });
	}
}
function mapMe (Element, City, Zoom)
{
	City = typeof City !== 'undefined' ? City : "Casablanca";
	Zoom = typeof Zoom !== 'undefined' ? Zoom : 10;
	Element = typeof Element !== 'undefined' ? Element : ".map_container .map";
	$(Element).gmap3(
	{
	    marker:{
	    	address: City + " Morocco"
	    },
	    map:{
	    	options:
	    	{
	    		zoom: Zoom
	      	}
	    }
	});	
}
function createChatBox($sUrl, $sActual, $sUsername, $iSenderID, $sSenderTocken, $sPathEmo, $alreadyInContact)
{
	if ( $('.chatbox_list .username:Contains("' + $sUsername + '")').length !== 0 ) return true;

	// Mise en place du loader
	$oBubble = $('.chat_content img[alt="' + $sUsername + '"]').parents('li').children('.talk_to');
	$oBubble.parent('li').append("<img src='" + PATH_PRELOADER + "' class='preloader_bulle'/>");
	$oBubble.css({ display:'none' });

	$.ajaxq('getMessages', {
		url: $sUrl,
		type: "POST",
		data: { 
			iSenderID: $iSenderID, 
			sSenderTocken: $sSenderTocken,
			nullable: 'yes',
			iPage : 1
		}
	}).done(function( sData ) {
		// Récupération du JSON
		$oData = $.parseJSON(sData);		
		console.log($oData);
		// Récupération de l'image de l'utilisateur
		var $sImageUserSender = $oData.imageSender;

		// Construction du shéma de base
		var html = '\
			<li class="chatbox open">\
		        <div class="entete">\
		        	<a href="/dashboard/membre/' + $sUsername + '" title="' + $sUsername + '" >\
		            	<span class="connectivite enligne"></span>\
		            	<span class="username">' + $sUsername + '</span>\
		            </a>\
		            <ul class="chat_box_menu">\
		                <li class="chatbox_params tooltip" data-powertip="Options">\
		                    <ul class="chat_box_submenu">';
		// Si le contact fait déjà partie des contacts du membres, on ne met pas l'outil d'ajout
		html += ( $alreadyInContact === 'no' ) ? 
								'<li class="chatbox_ajoutcontact">\
									<a href="#" title="' + $sUsername + '">Ajouter à mes contacts</a>\
								</li>' : '';
		// Affichage des autres outils
		if(USER_ROLE == "ROLE_FREE")
		{
			html += '<li class="redirectA"><a href="#" title="' + $sUsername + '">Partager mon album perso</a></li>';
		}
		if(USER_ROLE == "ROLE_USER")
		{
		html += 				'<li class="chatbox_albumperso"><a href="#" title="' + $sUsername + '">Partager mon album perso</a></li>';
		}
		html +=                	'<li class="chatbox_signaler"><a href="#" title="' + $sUsername + '">Signaler cette conversation</a></li>\
		                		<li class="chatbox_bloquer"><a href="#" title="' + $sUsername + '">Bloquer ce profil</a></li>\
		            		</ul>\
        				</li>\
				        <li class="chatbox_reduceit tooltip" data-powertip="Réduire"><a href="#" title=""></a></li>\
				        <li class="chatbox_closeit tooltip" data-powertip="Fermer"><a href="#" title=""></a></li>\
				    </ul>\
    			</div>\
    			<div class="hidden imageSenderHidden">' + $sImageUserSender + '</div>\
    			<div class="chatbox_content ' + $sUsername + '">';
		var $sLastID = '';
	    if ( console && console.log )
	    {
	    	// Si il y a déjà des messages entre les deux membres
	    	if ( typeof($oData[0]) != "undefined" )
	    	{
	    		// On boucle sur la liste des messages à implémenter
		    	$.each( $oData, function( $iKey, $oValue ) 
		    	{
		    		// Si aucun utilisateur n'est retourné, on sort de la boucle
		    		if (typeof($oValue.username) == "undefined") return true;

		    		// Implémentation des messages
					html += appendToChatBox($sActual, $sImageUserSender, $oValue.username, $oValue.Message, $oValue.id, $oValue.dateEnvoi);
					$sLastID = $oValue.id;
				});
			}
			// Implémentation de l'input de la box
			html += '</div>\
				        <div class="chatbox_input">\
				            <form action="#" method="POST" class="ChatBoxSendMessage">\
				            	<textarea type="text" name="chatbox_textmessage" class="chatbox_textmessage" ></textarea>\
				            </form>\
				            <span class="emo"></span>\
				            <div class="liste_emo" >\
							<ul>\
								<li><img alt="o:)" title="o:)" src="' + $sPathEmo + 'angel.gif" /></li>\
								<li><img alt="o.O" title="o.O" src="' + $sPathEmo + 'confused.gif" /></li>\
								<li><img alt="3:)" title="3:)" src="' + $sPathEmo + 'devil.gif" /></li>\
								<li><img alt=":O" title=":O" src="' + $sPathEmo + 'gasp.gif" /></li>\
								<li><img alt=":D" title=":D" src="' + $sPathEmo + 'grin.gif" /></li>\
								<li><img alt="<3" title="<3" src="' + $sPathEmo + 'heart.gif" /></li>\
								<li><img alt=":*" title=":*" src="' + $sPathEmo + 'kiss.gif" /></li>\
								<li><img alt=":)" title=":)" src="' + $sPathEmo + 'smile.gif" /></li>\
								<li><img alt="8|" title="8|" src="' + $sPathEmo + 'sunglasses.gif" /></li>\
								<li><img alt=":/" title=":/" src="' + $sPathEmo + 'unsure.gif" /></li>\
								<li><img alt=";)" title=";)" src="' + $sPathEmo + 'wink.gif" /></li>\
								<li><img alt=":3" title=":3" src="' + $sPathEmo + 'colonthree.gif" /></li>\
								<li><img alt=":\'(" title=":\'(" src="' + $sPathEmo + 'cry.gif" /></li>\
								<li><img alt=":(" title=":(" src="' + $sPathEmo + 'frown.gif" /></li>\
								<li><img alt="8)" title="8)" src="' + $sPathEmo + 'glasses.gif" /></li>\
								<li><img alt=">:(" title=">:(" src="' + $sPathEmo + 'grumpy.gif" /></li>\
								<li><img alt="^_^" title="^_^" src="' + $sPathEmo + 'kiki.gif" /></li>\
								<li><img alt=":v" title=":v" src="' + $sPathEmo + 'pacman.gif" /></li>\
								<li><img alt="-_-" title="-_-" src="' + $sPathEmo + 'squint.gif" /></li>\
								<li><img alt=":p" title=":p" src="' + $sPathEmo + 'tongue.gif" /></li>\
								<li><img alt=">:O" title=">:O" src="' + $sPathEmo + 'upset.gif" /></li>\
							</ul>\
						</div>\
				        </div>\
				    </li>';
					

			$('.chatbox_list').prepend(html);
			preventMoreThanXOpenChatBox();
			updateRegroupementChat();

			$(".chatbox_content." + $sUsername).mCustomScrollbar({
				theme:"dark-thick",
				scrollInertia: 100,
				autoHideScrollbar: true,
				advanced: {
					updateOnContentResize: true, 
					autoScrollOnFocus: true
				}
			});

			$(".chatbox_content." + $sUsername + " .chatbox_message").emotions($sPathEmo);
			$(".chatbox_content." + $sUsername).mCustomScrollbar("scrollTo", "#" + $sLastID);
			$(".chatbox_content." + $sUsername).parents('.chatbox').find('.tooltip').powerTip({
                    placement: 'n'
            });

			if ( $('.preloader_bulle').length !== 0 )
			{
	            // Disparition du loader et réaffichage de la bulle
				$oBubble.css({ display:'block' });
				$oBubble.parent('li').children('.preloader_bulle').remove();
			}
	    }
	}).fail(function( data ){
		alert("Un problème est survenu (Problème serveur)");
	});

	setTimeout(function(){
		if ( $('.preloader_bulle').length !== 0 )
		{
			// Disparition du loader et réaffichage de la bulle
			$oBubble.css({ display:'block' });
			$oBubble.parent('li').children('.preloader_bulle').remove();
		}
	}, 5000);
}

function appendToChatBox ($sActual, $sImageUserSender, $sUsername, $sMessage, $sID, $sTime) 
{
	$sTime = typeof $sTime !== 'undefined' ? $sTime : "Il y a quelques secondes";
	$sTime = typeof $sTime === 'string' ? $sTime : "Il y a quelques secondes";

	$sID = typeof $sID !== 'undefined' ? $sID : "m" + (Math.random() * (9999 - 2764) + 2764);
	if ( $sUsername === $sActual )
	{
		$sStatus = 'sended';
		$sImage = '';
	}
	else
	{
		$sStatus = 'received';
		$sImage = '<img src="' + $sImageUserSender +'" alt="' + $sUsername + '" />';
	}

	return '<div class="chatbox_message ' + $sStatus + '" id="' + $sID + '">\
				<p class="time" ><span>' + $sTime + '</span></p>\
                <span class="image">' + $sImage + '</span>\
                <div class="bubble">\
                    <div class="tail"></div>\
                    <p>' + $sMessage + '</p>\
                </div>\
            </div>';

	
}
(function($){
  $.fn.shuffle = function() {
    return this.each(function(){
      var items = $(this).children();
      return (items.length) 
        ? $(this).html($.shuffle(items)) 
        : this;
    });
  }
	
  $.shuffle = function(arr) {
    for(
      var j, x, i = arr.length; i; 
      j = parseInt(Math.random() * i), 
      x = arr[--i], arr[i] = arr[j], arr[j] = x
    );
    return arr;
  }	
})(jQuery);
$.fn.exchangePositionWith = function(selector) {
    var other = $(selector);
    this.after(other.clone());
    other.after(this).remove();
};

function updateRegroupementChat () 
{
	/* Affichage et disparition du bloc de regroupement */
	if ( $(".chatbox_list .chatbox").length < 4 )
	{
		$('.regroupement').css({ display:'none' });
	}
	else
	{
		$('.regroupement').css({ display:'block' });
		$('.chatbox_list .regroupement_container').exchangePositionWith('.chatbox_list .chatbox:eq(3)');

		$('.chatbox_list .regroupement_container').nextAll().css({ display:'none' });
		$('.chatbox_list .regroupement_container').prevAll().css({ display:'block' });

		$('.regroupement .nb_conversation').text($(".chatbox_list .chatbox").length - 3);

		$('.regroupement ul li').remove();

		// Mise à jour des élements regroupés
		$.each( $('.chatbox_list .chatbox:hidden'), function( $iKey, $oValue ) 
		{
			$('.regroupement ul').prepend('<li class="group">' + $(this).find('.username').text() + '</li>');
		});
	}
}

function preventMoreThanXOpenChatBox ($iNB) 
{
	/*$iNB = typeof $iNB !== 'undefined' ? $iNB : 3;
	if ( $('.chatbox.open').length > $iNB ) $('.chatbox_list .chatbox.open:eq(2)').nextAll().find('.chatbox_reduceit').click();*/
}

function checkDemandeChat () 
{
	if ( parseInt($('.chat_container .chat h3 span').text()) > 0 )
	{
		$('.chat_container .chat h3').addClass('new');
		var iCount = 0;
        var $oInterval = setInterval(function()
        {
            ++iCount;
            if ( iCount === 10 )
            {
                $('.chat_container .chat h3 p').fadeIn(300);
                stopInterval($oInterval);
                return false;
            }
            $('.chat_container .chat h3 p').fadeIn(200);
            $('.chat_container .chat h3 p').fadeOut(300);
        }, 500);
	}
	else $('.chat_container .chat h3').removeClass('new');
}

function stopInterval (intervalName) 
{
	clearInterval(intervalName);
}
function appendToDemandeChat ($oObj) 
{
	$sEnligne = ( $oObj.enLigne === 1 ) ? 'enligne' : 'horsligne';
	$sMessage = ( $oObj.Message.length > 120 ) ? $oObj.Message + '...' : $oObj.Message;
	return 	"<li class='n_" + $oObj.notification + "'>\
				<div class='left_demande'>\
					<div class='image_demande'>\
						<img alt='test' src='" + $oObj.image + "' />\
					</div>\
					<div class='content_demande'>\
						<div class='first_line' >\
							<span class='connectivite " + $sEnligne + "'></span>\
							<span class='username' >" + $oObj.username + "</span> - " + $oObj.age + " - " + $oObj.ville + "\
						</div>\
						<p class='message_demandeChat'>" + $sMessage + "</p>\
						<div class='buttons' >\
							<button class='button accepter_demande' >Accepter</button>\
							<button class='button refuser_demande' >Ignorer</button>\
						</div>\
					</div>\
				</div>\
				<div class='right_demande'>\
					<p>" + $oObj.dateEnvoi + "</p>\
				</div>\
			</li>";
}

function addDemandeChat ($sUrl, $sUsername) 
{
	// Début requete AJAX
    $.ajaxq('DemandesDeChat', {
        url: $sUrl,
        type: "POST",
        data: { username : $sUsername }
    // Quand la requete retourne une réponse
    }).done(function( data ) {
        if ( console && console.log )
        {
			$('.chat_container .chat h3 span').text(parseInt(data));
			checkDemandeChat();
        }
    }).fail(function( data ){
        alert("Un problème est survenu (Problème serveur)");
    });	
}
function updateDemandeChat ($iNbNotif) 
{
/*	// Début requete AJAX
    $.ajaxq('DemandesDeChat', {
        url: $sUrl,
        type: "POST"
    // Quand la requete retourne une réponse
    }).done(function( data ) {
        if ( console && console.log )
        {*/
        	console.log($iNbNotif);
			$('.chat_container .chat h3 span').text(parseInt($iNbNotif));
			checkDemandeChat();
        //}
    /*}).fail(function( data ){
        alert("Un problème est survenu (Problème serveur)");
    });	*/
}

function searchOnChatByUsername(force, sUrl) 
{
	var $existingString = $("#Recherche_chat").val();
	if (!force && $existingString.length < 3) return; //wasn't enter, not > 2 char

	/*$('.overlay_chat').removeClass('hide');*/
	$(".chat_content").mCustomScrollbar("disable");

	$.ajaxq('getContactByUsername', {
		url: sUrl,
		type: "POST",
		data: {
			sUsername: $existingString
		}
	// Quand la requete retourne une réponse
	}).done(function( data ) {
		/*$('.filtres_contacts, .membres_username').remove();*/

		$oData = $.parseJSON(data);

		/*$('.membres_enligne, .membres_contacts').slideUp();
		$('.membres_contacts').remove();*/

		$.each( $oData, function( $iKey, $oValue ) 
    	{
			appendToChatList($oValue, $iKey, 'membres_username');
		});

		$(".chat_content ul").find(".username:not(:Contains(" + $.trim($("#Recherche_chat").val()) + "))").parent().parent().stop().slideUp();
		$(".chat_content ul").find(".username:Contains(" + $.trim($("#Recherche_chat").val()) + ")").parent().parent().stop().slideDown();

		/*$('.overlay_chat').addClass('hide');*/
		$(".chat_content").mCustomScrollbar("update");

		$(".chat_menu .status").removeClass("status");


	}).fail(function( data ){
		alert("Un problème est survenu");
	});
}

function refreshSuggestUsers () 
{
	if ( $('.TempSuggest ul li').length === 0 ) return false;
	$('.sky-carousel').remove();
	$('.TempSuggest ul').shuffle();
	$('.TempSuggest ul').shuffle();
	$sHtml = '<div class="sky-carousel"><div class="sky-carousel-wrapper"><ul class="sky-carousel-container">';
					
	$.each( $('.TempSuggest ul li'), function( $iKey, $oValue ) 
	{
		if ( $iKey >= 12 ) return false;
		$sHtml += '<li>' + $(this).html() + '</li>';
	});
	$sHtml += '</ul></div></div>';
	$('.suggestions_profil').append($sHtml);
	/* Carousel du Dashboard */
	setTimeout($('.sky-carousel').carousel({
		itemWidth: 110,
		itemHeight: 150,
		distance: 10,
		selectedItemDistance: 10,
		selectedItemZoomFactor: 1,
		unselectedItemZoomFactor: 0.5,
		unselectedItemAlpha: 0.9,
		motionStartDistance: 200,
		topMargin: 140,
		gradientStartPoint: 0.35,
		gradientOverlayColor: "#ffffff",
		gradientOverlaySize: 190,
		selectByClick: false,
		loop: true,
		preload:true,
		showPreloader:true,
		topMargin:25,
		slideSpeed:0.30
	}), 1000);
}
