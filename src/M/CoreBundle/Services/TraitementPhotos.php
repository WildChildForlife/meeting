<?php
namespace M\CoreBundle\Services;

use M\CoreBundle\Entity\Images;


class TraitementPhotos
{

    function __construct($oContainer)
    {
        $this->oManager     = $oContainer->get("doctrine")->getManager();
        $this->oSession     = $oContainer->get("session");
        $this->oRequest     = $oContainer->get("request");
        $this->oUser        = (is_object($oContainer->get("security.context")->getToken())) ? 
                                    $oContainer->get("security.context")->getToken()->getUser() : false;
    }

    private $oManager;
    private $oUser;
    private $oSession;
    private $oRequest;
    private $sType;

    public function TraitementPhotos($aRequest, $sType = 'Profil')
    {
        if (!$this->oSession->has('ImagesProfil') && !$this->oSession->has('ImagesAlbum')) return false;
        
        $this->sType = $sType;
        $this->setAlbumStatus($aRequest);
        $aRequest = $this->buildSimilarArray($aRequest);
        $aRequest = $this->detectStatus($aRequest);
        $aRequest = $this->filterArray($aRequest);
        /* Si la page est rechargée sans changements */
        if (!$aRequest) return false;
        $this->workOnBD($aRequest);
        return true;
    }

    public function setAlbumStatus($aRequest)
    {
        if ( $this->sType !== 'Album' || !$aRequest->get('status_album') ) return false;
        switch ($aRequest->get('status_album')) 
        {
            case 'prive':  if ($this->oUser->getStatusAlbum()) 
                           {
                                $this->oUser->setStatusAlbum(0);
                                $this->oManager->persist($this->oUser);
                                $this->oManager->flush();
                           }
                           break;
            case 'public': if (!$this->oUser->getStatusAlbum()) 
                           {
                                $this->oUser->setStatusAlbum(1);
                                $this->oManager->persist($this->oUser);
                                $this->oManager->flush();
                           }
                           break;
        }
        
    }

    public function ProfilDefaultSelect($aRequest)
    {
        if (!$aRequest->get('profil_default')) return false;

        $_oImageDefault = $this->oManager->getRepository('MCoreBundle:Images')->findOneBy(array(
            'user' => $this->oUser->getId(), 
            'profile_default' => 1
        ));

        if(is_object($_oImageDefault))
        {
            $_oImageDefault->setProfileDefault(0);
            $this->oManager->persist($_oImageDefault);
        }


        $_oImage = $this->oManager->getRepository('MCoreBundle:Images')->find($aRequest->get('profil_default'));
        $_oImage->setProfileDefault(1);
        $this->oManager->persist($_oImage);

        $this->oManager->flush();

        return true;
    }

    public function buildSimilarArray($aRequest)
    {
        $aImages = array();
        $iCount = 0;
        foreach ($aRequest as $iKey => $sValue) 
        {
            if (preg_match("/DOC_/", $iKey)) 
            {
                $_id = explode("_", $iKey);
                $aImages[$iCount]['id'] = $_id[1];
                $aImages[$iCount]['image'] = $sValue;
                $iCount++;
            }
            else if (preg_match("/ADD_/", $iKey) && $sValue !== "") 
            {
                $aImages[$iCount]['image'] = $sValue;
                $aImages[$iCount]['status'] = 'added';
                $iCount++;
            }
            
        }
        return $aImages;
    }

    public function workOnBD($aRequest)
    {
        foreach ($aRequest as $iKey => $sValue) 
        {
            switch ($sValue['status']) 
            {
                case 'deleted': $_oImage = $this->oManager->getRepository('MCoreBundle:Images')->find($sValue['id']);
                                $this->oManager->remove($_oImage);
                                break;
                case 'updated': $_oImage = $this->oManager->getRepository('MCoreBundle:Images')->find($sValue['id']);
                                $_oImage->setImage($sValue['image']);
                                $_oImage->setModerer(0);

                                $this->oManager->persist($_oImage);
                                break;
                case 'added'  : $_oImage = new Images();
                                $_oImage->setImage($sValue['image']);
                                $_oImage->setUser($this->oUser);
                                $bType = ($this->sType === 'Profil') ? 1 : 0;
                                $_oImage->setProfile($bType);
                                $_oImage->setProfileDefault(0);
                                $_oImage->setModerer(0);
                                $this->oManager->persist($_oImage);
                                break;
            }
        }

        $this->oManager->flush();
        return true;
    }

    public function filterArray($aRequest)
    {
        $aRequestFiltred = $aRequest;
        foreach ($aRequest as $iKey => $sValue) 
        {
            if ( !isset($sValue['status'])) return false;
            if ( isset($sValue['status']) && $sValue['status'] === 'initial' ) unset($aRequestFiltred[$iKey]);
        }

        return $aRequestFiltred;
    }

    

    public function detectStatus($aRequest)
    {
        $oOnSession = ($this->sType === 'Profil') ? $this->oSession->get('ImagesProfil') : $this->oSession->get('ImagesAlbum');

        $aRequestStatus = $aRequest;
        /*echo '<pre>', print_r($oOnSession, true), '</pre>';
        echo '<pre>', print_r($aRequest, true), '</pre>';
        die();*/
        foreach ($aRequest as $iKey => $sValue) 
        {
            /*echo '<pre>$oOnSession[$iKey]) : ', (isset($oOnSession[$iKey])), '</pre>';
            echo '<pre>$sValue["id"] : ', $sValue["id"], '</pre>';
            echo '<pre>$oOnSession[$iKey]["id"] : ' , $oOnSession[$iKey]['id'], '</pre>';*/
            if (isset($oOnSession[$iKey]) && $sValue['id'] == $oOnSession[$iKey]['id'])
            {
                /* DELETED */
                if ( $sValue['image'] === "" && $oOnSession[$iKey]['image'] !== "" )  $aRequestStatus[$iKey]['status'] = 'deleted';
                /* UPDATED */
                else if ( $sValue['image'] !== $oOnSession[$iKey]['image'] ) $aRequestStatus[$iKey]['status'] = 'updated';
                else $aRequestStatus[$iKey]['status'] = 'initial';
            }
        }

        return $aRequestStatus;

        /*echo '<pre>', print_r($aRequestStatus, true), '</pre>';

            die();
        echo '<pre>', print_r($aRequest, true), '</pre>';
        echo '<pre>', print_r($aImageIDs, true), '</pre>';
        echo '<pre>', print_r($this->oSession->get('ImagesProfil'), true), '</pre>';*/
    }

   

}


