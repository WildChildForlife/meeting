<?php
namespace M\CoreBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Registry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class MergeMe
{

	protected $oContainer;
    protected $oSession;
    protected $oObjA;
    protected $oObjB;

	public function __construct(Registry $oContainer)
    {
    	$this->setContainer($oContainer);
        $this->setSession($this->getContainer()->get('session'));
    }

    public function MergeIt($oObjA, $oObjB)
    {
        $this->oObjA = $oObjA;
        $this->oObjB = $oObjB;
    }

    public function __get($sAttr) 
    {
        if ($this->oObjA->$sAttr) 
        {
            return $this->oObjA->$sAttr;
        } 
        else 
        {
            return $this->oObjB->$sAttr;
        }
    }

    public function setContainer(Registry $oContainer)
    {
        $this->oContainer = $oContainer;
    }
    public function getContainer()
    {
        return $this->oContainer;
    }

    public function setSession($oSession)
    {
        $this->oSession = $oSession;
    }
    public function getSession()
    {
        return $this->oSession;
    }

	
}

?>