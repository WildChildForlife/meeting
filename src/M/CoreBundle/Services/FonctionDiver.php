<?php
namespace M\CoreBundle\Services;


class FonctionDiver
{

    function __construct($oContainer)
    {
        $this->oManager     = $oContainer->get("doctrine")->getManager();
        $this->oSession     = $oContainer->get("session");
        $this->oRequest     = $oContainer->get("request");
        $this->oRouter     = $oContainer->get("router");
    }

    private $oManager;
    private $oSession;
    private $oRequest;
    private $oRouter;

    public function CentresInteret($Categorie,$Centresinteret,$CIselected,$CIreponse = array())
    {   

        if (!is_array($Categorie) || !is_array($Centresinteret) || !is_object($CIselected) || !is_array($CIreponse) ) return false; 

        $_aFormCentresInteret = array();
        foreach($Categorie as $key => $cat)
        {   
            $_aFormCentresInteret[$key]['categorie']= $cat->getId();
            $_aFormCentresInteret[$key]['nom']= $cat->getNom();
            $_aFormCentresInteret[$key]['question']= $cat->getQuestion();
            $_aFormCentresInteret[$key]['reponse']= '';
            $_aFormCentresInteret[$key]['donnees'] = '';
        
        }
        $iKey = 0;
        foreach($_aFormCentresInteret as $keyFCI => $valeur)
        {
            foreach ($Centresinteret as $key => $centre) 
            {
                if ($valeur['categorie'] === $centre['categorie'])
                {

                    $_aFormCentresInteret[$keyFCI]['donnees'][$iKey]['id'] = $Centresinteret[$key]['id'];
                    $_aFormCentresInteret[$keyFCI]['donnees'][$iKey]['nom'] = $Centresinteret[$key]['nom'];
                    $_aFormCentresInteret[$keyFCI]['donnees'][$iKey]['selected'] = '';   
                    foreach ($CIselected as $_value) 
                    {
                        if ($_value->getId() === $Centresinteret[$key]['id'])
                        {
                            $_aFormCentresInteret[$keyFCI]['donnees'][$iKey]['selected'] = true;
                            break;
                        }
                        else $_aFormCentresInteret[$keyFCI]['donnees'][$iKey]['selected'] = false;
                    } 
                    $iKey++;

                }
            }
            $iKey = 0;
        }  

        foreach($CIreponse as $key => $reponse)
        {   

          $_aFormCentresInteret[$key]['reponse']= $reponse;
           
        }
        return $_aFormCentresInteret;
        
    }

    public function Questions($bSexe,$aQuestions,$aReponseSelected)
    { 

      if (!is_bool($bSexe) || !is_array($aQuestions) || !is_array($aReponseSelected)) return false; 
       $_aFormQuestions = array();
       $aReponse        = array();
      
        // On génére le formulaire en fonction du sexe de l'utilisateur
        if ($bSexe == false) 
        { 
            //boucle sur les questions qui sont remplis dans la bdd

            foreach($aQuestions as $key => $donnees)
            {   
                
              $aQuestion = $donnees->getQuestion();   
              $_aFormQuestions[$key]['question'] = $aQuestion[0];
              $_aFormQuestions[$key]['id'] = $donnees->getId(); 
              $_aFormQuestions[$key]['selected'] = '';
              $aReponse = $donnees->getReponse(); 
              // Si il n 'y a pas de réponse automatiquement on mais oui et non sinon il peut y avoir des réponses différente ' 
              
              $_aFormQuestions[$key]['choix1'] = (!isset($aReponse[0][0])) ? "Oui" : $aReponse[0][0] ;
              $_aFormQuestions[$key]['choix2'] = (!isset($aReponse[0][1])) ? "Non" : $aReponse[0][1] ;
            }  

      
        }
        // Cette partie permet de généré le question relative au femme ainsi que les réponse au feminin 
        else
        {   
            //boucle sur les questions qui sont remplis dans la bdd
            foreach($aQuestions as $key => $donnees)
            {  

                $aQuestion = $donnees->getQuestion(); 

                $_aFormQuestions[$key]['id']= $donnees->getId();
                $_aFormQuestions[$key]['selected'] = '';

                //si le champs question est vide on récupére celui au masculin car il correspond au méme que celui au féminin
                $_aFormQuestions[$key]['question'] = (!isset($aQuestion[1])) ? $aQuestion[0] : $aQuestion[1];
                $aReponse = $donnees->getReponse(); 

                //si le champs reponse est vide on récupére celui au masculin car il correspond au méme que celui au féminin

                if (!isset($aReponse[0]) && !isset($aReponse[1]))
                {
                    $_aFormQuestions[$key]['choix1'] = "Oui";
                    $_aFormQuestions[$key]['choix2'] = "Non";
                }
                else
                {
                    $_aFormQuestions[$key]['choix1'] = (isset($aReponse[1][0])) ? $aReponse[1][0] : $aReponse[0][0];
                    $_aFormQuestions[$key]['choix2'] = (isset($aReponse[1][1])) ? $aReponse[1][1] : $aReponse[0][1];
                }
            }
        }

        foreach ($aReponseSelected as $key => $_value) 
        { 
            foreach ($_aFormQuestions as $ikey => $value) 
            { 
                if ($key == $_aFormQuestions[$ikey]['id'])
                {
                    $_aFormQuestions[$ikey]['selected'] = $_value;
                }
            } 
        }             
        return $_aFormQuestions;
       
    }
    public function setEnligneUserToResult($aResult)
    {
        if (!is_array($aResult)) return $aResult; 
        $dDelay = new \DateTime();
        $dDelay->setTimestamp(strtotime('10 minutes ago'));

        foreach ($aResult as $key => $value) 
        {   
             $aResult[$key]['enLigne'] = ($aResult[$key]['lastActivity'] > $dDelay ) ? 1 : 0 ;
                                 
        }
        return $aResult;
    }
    public function setSigneToResult($aResult)
    {
        if (!is_array($aResult)) return $aResult; 
        foreach ($aResult as $key => $value) 
        {   
            $aResult[$key]['signe'] = $this->signeAstrologique($aResult[$key]['datenaissance']);
                                 
        }

        return $aResult;
    }
    //Fonctiona déplacé dans le cas ou elle sera utilisé par d'autre service !!!
    public function signeAstrologique($datenaissance)
    {
        if (empty($datenaissance)) return $datenaissance;
        
            $zodiac[356] = "Capricorne";
            $zodiac[326] = "Sagitaire";
            $zodiac[296] = "Scorpion";
            $zodiac[266] = "Balance";
            $zodiac[235] = "Vierge";
            $zodiac[203] = "Lion";
            $zodiac[172] = "Cancer";
            $zodiac[140] = "Gémaux";
            $zodiac[111] = "Taureau";
            $zodiac[78]  = "Bélier";
            $zodiac[51]  = "Poissons";
            $zodiac[20]  = "Verseau";
            $zodiac[0]   = "Capricorne";
            if (!$datenaissance) $datenaissance = time();
            $dayOfTheYear = date("z",$datenaissance);
            $isLeapYear = date("L",$datenaissance);
            if ($isLeapYear && ($dayOfTheYear > 59)) $dayOfTheYear = $dayOfTheYear - 1;
            foreach($zodiac as $day => $sign) if ($dayOfTheYear > $day) break;

            return $sign;

    }
    public function setDateTimeToString($aResult)
    {
        if (!is_array($aResult)) return $aResult; 
        $dYesterday = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d')-1, date('y')));
        $dToday = date('Y-m-d') ;

        foreach ($aResult as $key => $value) 
        {   
           $dDateNotif = $aResult[$key]['dateNotification'];
          
            switch ($dDateNotif ->format('Y-m-d'))
            {
                case $dToday:
                    $aResult[$key]['dateNotification'] = "Aujourd'hui ".$dDateNotif->format('à H:i');
                break;
                case $dYesterday:
                    $aResult[$key]['dateNotification'] = "Hier ".$dDateNotif->format('à H:i');
                break;
                default:
                    $aResult[$key]['dateNotification'] =   "Le ". $dDateNotif ->format('d F Y à H:i');
            }

           
        }

        return $aResult;
    }
    public function setDateTimeToStringFromUser($aDate)
    {
        if (!is_array($aDate)) return $aDate; 
        $dYesterday = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d')-1, date('y')));
        $dToday = date('Y-m-d') ;
        
        foreach ($aDate as $key => $value) 
        {   
            switch ($aDate[$key]->format('Y-m-d')):
            case $dToday:
                 $aDate[$key] = "Aujourd'hui ".$aDate[$key]->format('à H:i');
                break;
            case $dYesterday:
                 $aDate[$key] = "Hier ".$aDate[$key]->format('à H:i');
                break;
            default:
                $aDate[$key] =   "Le ".$aDate[$key]->format('d F Y à H:i');
            endswitch;
        }
        return $aDate;
    }

    public function filtreContact($oRequest)
    {

        if (!is_array($oRequest)) return $oRequest; 
        //On crée les parametres de la recherche en fonction des champs remplis
        
        //On supprime les clé inutile _token 
        unset($oRequest['_token']);
        $oParam = array();
       
        $sQuery = "" ; //On prépare la variable de la requéte
        //Cette partie de requete a été ajouté dans une variable pour évité la redondance elle permet de transformé le timestamp en  age 
        $sTimediff = "TimeDIFF(YEAR,DATEFORMAT(DATEADD('1970-01-01 00:00:00',INTERVAL (u.datenaissance) SECOND),'%Y%-%m%-%d'),:today)";

        foreach ($oRequest as $key => $value) 
        {   
            //On supprime les clés vide
            if(!empty($value) )  $oParam[$key] = $oRequest[$key];
        }


                /////// ICi nous traiterons les champs qui font appelle a des tables étrangére : ville
        if (!empty($oParam['ville']))  
        {   
            // on génére la requéte sql
            $sQuery = $sQuery." AND v.id = :ville" ;
            //On détruit cette clé car elle a été traité préalablement
        
        }  

        //////// FIn tables clés étrangére

        // Dans le cas ou l'age est pas soumis
        if (!empty($oParam['ageDebut']) || !empty($oParam['ageFin']))
        {  
            
           //On supprime le paramétre age de fin qui ne sera pas utilisé
           if (empty($oParam['ageFin']))
            {
                unset($oParam['ageFin']);
                $sQuery =  $sQuery."  AND $sTimediff  = :ageDebut";

            }
            else
            {   // Sinon on faire une recherche par intervall
                $sQuery =  $sQuery." AND $sTimediff  >= :ageDebut  AND  $sTimediff <= :ageFin ";
            }

            $oParam['today'] = date('Y-m-d');
        }

        // Dans le cas ou Enligne est soumis
        if (!empty($oParam['EnLigne']))
        {   
            if ($oParam['EnLigne'] == 1 )
            {     
                $dDelay = new \DateTime();
                $dDelay->setTimestamp(strtotime('10 minutes ago'));
                
                $oParam['EnLigne'] = $dDelay;
                $sQuery =  $sQuery." AND u.lastActivity > :EnLigne ";  

            }
        }

                // Dans le cas ou Enligne est soumis
        if (!empty($oParam['0']))
        {   

                $dDelay = new \DateTime();
                $dDelay->setTimestamp(strtotime('10 minutes ago'));
                $sQuery =  $sQuery." AND u.username LIKE :lettre ";  
                $oParam['lettre']=$oParam['0']."%";
                unset($oParam['0']);
            
        }

        $Resultat['query'] = $sQuery;
        $Resultat['param'] = $oParam;

        return $Resultat;
    }    

    public function setDateTimeToStringFromMessage($aDate)
    {
        if (!is_array($aDate)) return $aDate; 
        $dYesterday = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d')-1, date('y')));
        $dToday = date('Y-m-d') ;
        
        foreach ($aDate as $key => $value) 
        {   
            switch ($aDate[$key]['dateEnvoi']->format('Y-m-d')):
            case $dToday:
                $res = $aDate[$key]['dateEnvoi']->diff(new \DateTime())->h;
                 
                if ($res < 1 ) 
                {
                    /*
                    $nbHeures = $aDate[$key]['dateEnvoi']->diff(new \DateTime())->i;
                    $tmp = ( $nbHeures > 1 ) ? $nbHeures . ' Heures' : $nbHeures . ' Heure';
                    */
                    $nbMinutes = $aDate[$key]['dateEnvoi']->diff(new \DateTime())->i;
                    $tmp = ( $nbMinutes > 1 ) ? $nbMinutes . ' Minutes' : $nbMinutes . ' Minute';
                }
                else
                {
                    $nbHeures = $aDate[$key]['dateEnvoi']->diff(new \DateTime())->h;
                    $tmp = ( $nbHeures > 1 ) ? $nbHeures . ' Heures' : $nbHeures . ' Heure';
                }       
                 

                $aDate[$key]['dateEnvoi'] = "Il y a ". $tmp;
                break;
            case $dYesterday:
                 $aDate[$key]['dateEnvoi'] = "Hier ".$aDate[$key]['dateEnvoi']->format('à H:i');
                break;
            default:
                $aDate[$key]['dateEnvoi'] =   "Le ".$aDate[$key]['dateEnvoi']->format('d/m/Y ');
            endswitch;
        }
        return $aDate;
    }

    public function encodeId($aResult)
    {   
        if (!is_array($aResult)) return $aResult; 
        
        foreach ($aResult as $key => $value) 
        {   

           $aResult[$key]['id'] = base64_encode( (int) $aResult[$key]['id'] * 9999);

        }

        return $aResult;
    }
    public function decodeId($aMessageId)
    {   
        if (!is_array($aMessageId)) return $aMessageId; 

        foreach ($aMessageId as $key => $value) 
        {   
           $aMessageId[$key] = base64_decode($value) / 9999;
        }

        return $aMessageId;
    }

    public function getIdsNotViewed($aArray)
    {   
        if (!is_array($aArray)) return $aArray; 
         
        $aResult = array();
        foreach ($aArray as $key => $value) 
        {   
           if ((int)$aArray[$key]['vue'] == 0) $aResult[] = $aArray[$key]['id'] ;
        }

        return $aResult;
    }

    public function setTri($sTri)
    {
        if (!is_string($sTri)) return $sTri;
        
        switch ($sTri)
        {
            case "Tous":
                 $sResultTri = "";
                break;
            case "Lus":
                 $sResultTri = "AND m.lu = 1 ";
                break;
            case "Non-lus":
                 $sResultTri = "AND m.lu = 0 ";
                break;              
            default:
                $sResultTri =  "";
        }

        return $sResultTri;
    }

    public function generateUrlsFromArray($aArray = array())
    {
        if ( gettype($aArray) !== "array" ) return $aArray;
        foreach ($aArray as $iKey => $sValue) 
        {
            if ( array_key_exists('username', $sValue) )
            {
                $aArray[$iKey]['sUrl'] = $this->oRouter->generate('m_core_profile_voir', array('sUser' => $sValue['username']));
            }
        }
        return $aArray;
    }

    public function encodeUrlsFromArray($aArray = array())
    {
        if ( gettype($aArray) !== "array" ) return $aArray;
     
        foreach ($aArray as $iKey => $sValue) 
        {   
            $sA = array("à", "á", "â", "ã", "ã", "ä");
            $sE = array("è", "é", "ê", "ë");
            $sI = array("ì", "í", "î", "ï");
            $sO = array("ò", "ó", "ô", "õ", "ö");
            $sU = array("ù", "ú", "û", "ü");
            $sY = array("ý", "ÿ");
           //$aArray[$iKey]['link'] = strtolower($aArray[$iKey]['link']); Cause un prb de charset

            $aArray[$iKey]['link'] = str_replace("ç", "c", $aArray[$iKey]['link']);
            $aArray[$iKey]['link'] = str_replace($sA, "a", $aArray[$iKey]['link']);
            $aArray[$iKey]['link'] = str_replace($sE, "e", $aArray[$iKey]['link']);

            $aArray[$iKey]['link'] = str_replace($sI, "i", $aArray[$iKey]['link']);
            $aArray[$iKey]['link'] = str_replace($sO, "o", $aArray[$iKey]['link']);
            $aArray[$iKey]['link'] = str_replace($sU, "u", $aArray[$iKey]['link']);
            $aArray[$iKey]['link'] = str_replace($sY, "y", $aArray[$iKey]['link']);
   
           $aArray[$iKey]['link'] = urlencode(str_replace(" ", "-", $aArray[$iKey]['link']));
       
        }
        return $aArray;
    }

    public function setEventPlace($aArray = array(),$iPlace)
    {
        
        if ( gettype($aArray) !== "array" || !is_int($iPlace) ) return False;

        $iTotalHomme = 0;
        $iTotalFemme = 0;

       $aPlace = $this->calculePlace($iPlace); 

        foreach ($aArray as $iKey => $sValue) 
        {   
            if (!$aArray[$iKey]['sexe']) $iTotalHomme++ ; 
            else $iTotalFemme++ ; 
        }

        $aResult = array('Placehomme' => $aPlace['Homme'],'Placefemme' => $aPlace['Femme'],
                         'HommeInscrit' => $iTotalHomme,'FemmeInscrit' => $aPlace['Femme'],
                         'ResteHomme' => $aPlace['Homme']-$iTotalHomme,'ResteFemme' => $aPlace['Femme']-$iTotalFemme);


        return $aResult;
    }

    public function calculePlace($iPlace)
    {
        if ($iPlace%2 == 0 ) 
        {
            $aPlace['Homme'] = $iPlace/2;
            $aPlace['Femme'] = $iPlace/2;

        }    
        else
        {   

            $aPlace['Homme'] = ($iPlace-1)/2;
            $aPlace['Femme'] = ($iPlace-1)/2 + $iPlace % 2;
        }    
        return $aPlace;
    }

    public function setEventStatus($aArray = array(),$aNbrStatus = array(),$bSexe)
    {
        if ( gettype($aArray) !== "array" || gettype($aNbrStatus) !== "array" || !is_bool($bSexe) ) return $aArray;

        $sSexe = ($bSexe) ? "Femme" :  "Homme" ;


        foreach ($aArray as $iKey => $sValue) 
        {  

            $aArray[$iKey]['sDate'] = $this->formatDate($aArray[$iKey]['date']);
            $aArray[$iKey]['ouvert'] = 1;
            
            foreach ($aNbrStatus as $Key => $sVal) 
            {        
                    if ($sVal['id'] ==  $sValue['id'] ) 
                    {   
                        $aPlace = $this->calculePlace($sValue['places']);
                        $iReste = $aPlace[$sSexe] - $sVal['inscrit'];
                        $aArray[$iKey]['ouvert'] = ($iReste != 0) ? 1 : 0 ;
                    }
            }


        }
        
        return $aArray;
    }


    public function formatDate($sDate)
    {
        if ( !is_string($sDate) ) return $sDate;

        $jour = array(
            "Dimanche",
            "Lundi",
            "Mardi",
            "Mercredi",
            "Jeudi",
            "Vendredi",
            "Samedi"
            );
        $mois = array(
            "",
            "Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"
            );
        $dateDuJour = $jour[date("w", strtotime("$sDate"))]." ".date("d", strtotime("$sDate"))." ".$mois[date("n", strtotime("$sDate"))]." ".date("Y", strtotime("$sDate"));


        return $dateDuJour;
    }


    public function regroupArrayByUsername($aArray)
    {   
        if ( !is_array($aArray) ) return $aArray;

        $aArrayFinal = array();
        foreach ($aArray as $key => $value) 
        {
                // Supposant qu'il existe déjà
            if ( array_key_exists($value['username'], $aArrayFinal) )
            {
                $aArrayFinal[$value['username']]['images'][] = 
                array(
                    'nom'=> $value['image'],
                    'id'=> $value['id']
                    );
            }
                // Il n'existe pas
            else
            {
               if (isset($value['username']))  $aArrayFinal[$value['username']] = 
                array(
                    'images'=> array(
                        array(
                            'nom' => $value['image'],
                            'id' => $value['id']
                        )
                    )
                );
            }
        }

        return $aArrayFinal;
    }

    public function getErrorsAsArray($sErrors)
    {
        $aArray = preg_split('/\r\n|\r|\n/', $sErrors);
        $aArray = array_map('trim', $aArray);
        $aErrors = array();
        foreach ($aArray as $iKey => $sValue) 
        {
            if ( strpos($sValue, 'ERROR: ') !== false )
            {
                $aErrors[] = str_replace('ERROR: ', '', $sValue);
            }
        }
        
        return $aErrors;
        
    }

    public function buildThumbnail($sImage, $iWidth = 124, $iHeight = 156)
    {
/*        // Catch la position de l'encodage
        $iEncodePosition = strpos($sImage, 'base64,') + 7;
        // Catch l'encodage au complet
        $sEncode = substr($sImage, 0, $iEncodePosition);

        // Elemination de l'encodage
        $sImage = preg_replace('#^data:image/[^;]+;base64,#', '', $sImage);

        $oThumb = new \Imagick();
        $oThumb->readimageblob(base64_decode($sImage));
        //echo '<pre>', print_r(strlen($oThumb->getImageBlob()), true), '</pre>';
        $oThumb->thumbnailImage( $iWidth, $iHeight );
        //echo '<pre>', print_r(strlen($oThumb->getImageBlob()), true), '</pre>';
        return $sEncode . base64_encode($oThumb->getImageBlob());*/
        return $sImage;
    }


}
