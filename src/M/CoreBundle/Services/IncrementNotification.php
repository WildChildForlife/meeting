<?php
namespace M\CoreBundle\Services;
use M\CoreBundle\Entity\Notification;

/**
* 
*/
// Visite Visite = 0
    // Visite Coupdecoeur = 1
    // Visite Message = 2
    // Visite Tchat = 3

class IncrementNotification
{
    
    function __construct($oContainer)
    {
        $this->oRepositoryNotif  = $oContainer->get("doctrine")->getManager()->getRepository("MCoreBundle:Notification");
        $this->oManager     = $oContainer->get("doctrine")->getManager();
        $this->oSession     = $oContainer->get("session");
        $this->oRequest     = $oContainer->get("request");
        $this->sIP          = $this->oRequest->server->get("REMOTE_ADDR");
    }

    private $oRepositoryNotif;
    private $oManager;
    private $oSession;
    private $oRequest;
    private $sIP;
    
    public function incrementNotification($iType,$oUser,$oUserNotifieur, $bVue = false)
    {
        
        if (!is_int($iType) || !is_object($oUser) || !is_object($oUserNotifieur)) return false; 
        //if ($this->oRepositoryNotif->FindNotificationByDate($oUser,$oUserNotifieur,$iType)) return false;
        //Mise a jour des vue des type précédents pour evité les non vue
        if ($iType != 2) $this->oRepositoryNotif->UpdateNotificationViewedPerSender($oUser,$oUserNotifieur,$iType);

        $oEm = $this->oManager;
        $oNotif = new Notification();
        $oNotif->setType($iType);
        $oNotif->setUserNotifieur($oUserNotifieur);
        $oNotif->setUser($oUser);
        $oNotif->setVue($bVue);
        $oNotif->setDateNotification(new \Datetime());
        $oEm ->persist($oNotif);
        $oEm ->flush();
        //Si c un message on retourne l'objet notification
        if ($iType === 2 || $iType === 3) return $oNotif;
        return true;
    }


   
}





 ?>