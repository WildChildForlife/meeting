<?php
namespace M\CoreBundle\Services;
use M\CoreBundle\Entity\User;
use Symfony\Component\Templating\EngineInterface;
/**
* 
*/
class Mail
{
    
    private $mailer;

    protected $templateEngine;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $templateEngine) {
        $this->mailer = $mailer;
        $this->templateEngine = $templateEngine;
    }




    public function mailInscription($sUsername,$sEmail)
    {
      if(!is_string($sEmail)) return false;

       $sSubject ="Demande d'inscription sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:inscription.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sUsername));
       return true; 
    }

    public function mailValidation($sUsername,$sEmail,$sCode)
    {
       $sSubject ="Validation de votre inscription sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:valide.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sUsername,'code'=>$sCode));
       return true; 
    }
   
    public function mailRejete($sEmail)
    {
       $sSubject ="Inscription rejetée sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:rejete.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView);
       return true; 
    }

    public function mailChangementRejete($sUsername,$sEmail)
    {
       $sSubject ="Changement du  profil refusé ";
       $sSenderView='MCoreBundle:Mail:changementrejete.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sUsername));
       return true; 
    }


    public function mailSignalement($sUsername,$sEmail)
    {
       $sSubject ="Avertissement sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:signalement.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sUsername));
       return true; 
    }

    public function mailEventvalide($sUsername,$sEmail)
    {
       $sSubject ="Votre sortie sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:eventvalide.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sUsername));
       return true; 
    }

    public function mailEventrefuse($sUsername,$sEmail,$sMessage)
    {
       $sSubject ="Votre sortie sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:eventrefuse.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sUsername,'sMessage'=>$sMessage));
       return true; 
    }

    public function mailEventpaiement($sEmail,$sMessage)
    {
       $sSubject ="Confirmation de votre réservation Rencontres & Vous ";
       $sSenderView='MCoreBundle:Mail:eventpaiement.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sMessage'=>$sMessage));
       return true; 
    }


    public function mailSuppression($sUsername,$sEmail)
    {
       $sSubject ="Suppression de votre compte  sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:suppression.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('user'=>$sUsername));
       return true; 
    }

    public function mailMdp($sUsername,$sEmail,$sCode)
    {
       $sSubject ="Votre mot de passe sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:motdepasse.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sUsername,'code'=>$sCode));
       return true; 
    }

    public function mailDesinscription($sUsername,$sEmail)
    {
      if(!is_string($sEmail)) return false;

       $sSubject ="Désinscription de votre compte sur Rencontres & Vous";
       $sSenderView='MCoreBundle:Mail:desinscription.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('user'=>$sUsername));
       return true; 
    }

    public function mailVisite($sCurrentUsername,$sUsername,$sEmail)
    {
       if(!is_string($sUsername) || !is_string($sEmail)) return false;
      
       $sSubject ='Vous avez reçu une nouvelle visite sur Rencontres & Vous';
       $sSenderView='MCoreBundle:Mail:visite.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sCurrentUsername,'user'=>$sUsername));
       return true; 

    }

    public function mailAbonnement($sCurrentUsername,$sDate,$sEmail)
    {
       if(!is_string($sCurrentUsername) || !is_string($sEmail)) return false;
      
       $sSubject ='Votre abonnement sur Rencontres & Vous';
       $sSenderView='MCoreBundle:Mail:abonnement.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sCurrentUsername,'sDate'=>$sDate));
       return true; 

    }

    public function mailResiiationAbonnement($sCurrentUsername,$sDate,$sEmail)
    {
       if(!is_string($sCurrentUsername) || !is_string($sEmail)) return false;
      
       $sSubject ='Désabonnement sur Rencontres & Vous';
       $sSenderView='MCoreBundle:Mail:resiliation.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sCurrentUsername,'sDate'=>$sDate));
       return true; 

    }

    public function mailCpc($sCurrentUsername,$sUsername,$sEmail)
    {  

       if(!is_string($sUsername) || !is_string($sEmail)) return false;
       
       $sSubject ='Vous avez reçu un nouveau coup de cœur sur Rencontres & Vous';
       $sSenderView='MCoreBundle:Mail:cpc.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sCurrentUsername,'user'=>$sUsername));
       return true; 
    }

    public function mailMessage($sCurrentUsername,$sUsername,$sEmail)
    {
       if(!is_string($sUsername) || !is_string($sEmail)) return false;
       
       $sSubject ='Vous avez reçu un nouveau message sur Rencontres & Vous';
       $sSenderView='MCoreBundle:Mail:message.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('sUsername'=>$sCurrentUsername,'user'=>$sUsername));
       return true; 
    }

    public function mailEventRappel()
    {
       $sSubject ='Sortie  « Rencontres & Vous »';
       $sSenderView='MCoreBundle:Mail:eventrapel.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('user'=>$sUsername));
       return true; 
    }

    public function mailEventCreation($sEmail)
    {
       $sSubject ='Votre sortie sur Rencontres & Vous';
       $sSenderView='MCoreBundle:Mail:eventcreation.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView);
       return true; 
    }

    public function mailInformation()
    {
       $sSubject ='Informations du site Rencontres & Vous';
       $sSenderView='MCoreBundle:Mail:information.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,array('user'=>$sUsername));
       return true; 
    }

    public function mailContact($aInfo) 
    {

       $sSubject = $aInfo['service'];
       $sSenderView='MCoreBundle:Mail:contact.html.twig';
       $this->send('admin@rencontres-et-vous.ma',$sSubject,$sSenderView,array('info'=>$aInfo));
       return true; 
    }

    public function mailOrderConfirmation($sEmail,$CurrentUsername,$codeCommande,$modePaiement,$offre,$prixUnitaire,$prixRemise,$reduction)
    {
       $sSubject ='Confirmation de votre commande « Rencontres & Vous » ';
       $sSenderView='MCoreBundle:Mail:order_conf.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,
        array('username'=>$CurrentUsername,'codeCommande'=>$codeCommande,'modePaiement'=>$modePaiement,
              'offre'=>$offre,'prixUnitaire'=>$prixUnitaire,'prixRemise'=>$prixRemise,'reduction'=>$reduction)
        );
       return true; 
    }

    public function mailParrainage($sEmail,$sCurrentUsername,$sEncryptedEmail)
    {
       $sSubject ='Parrainage « Rencontres & Vous » ';
       $sSenderView='MCoreBundle:Mail:parrainage.html.twig';
       $this->send($sEmail,$sSubject,$sSenderView,
        array('username'=>$sCurrentUsername,'sEncryptedEmail'=>$sEncryptedEmail));
       return true;
    }






    public function send($to, $subject, $view, $params = array(), $contentType = 'text/html') 
    {
        $mail = \Swift_Message::newInstance()
          ->setSubject($subject)
          ->setFrom(array('admin@rencontres-et-vous.ma' => "Rencontres & Vous"))
          ->setTo($to)
          ->setBody($this->templateEngine->render($view, $params), $contentType);
        $this->mailer->send( $mail );
    }
}




