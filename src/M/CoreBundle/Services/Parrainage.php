<?php
namespace M\CoreBundle\Services;
use MCoreBundle\Entity\User;
/**
* 
*/
class Parrainage
{

	private $oRepositoryUser;

	 function __construct($oContainer)
    {
        $this->oRepositoryUser  = $oContainer->get("doctrine")->getManager()->getRepository("MCoreBundle:User");
     }


	function encryptEmail($data){
        $cipher  = MCRYPT_RIJNDAEL_128;          // Algorithme utilisé pour le cryptage des blocs
        $key     = 'm8BngOmOUQ';    // Clé de cryptage
        $mode    = 'cbc';  
        $keyHash = md5($key);
        $key = substr($keyHash, 0,   mcrypt_get_key_size($cipher, $mode) );
        $iv  = substr($keyHash, 0, mcrypt_get_block_size($cipher, $mode) );
 
        $data = mcrypt_encrypt($cipher, $key, $data, $mode, $iv);
        return base64_encode($data);
    }
 
        function decryptEmail($data){
        $cipher  = MCRYPT_RIJNDAEL_128;          // Algorithme utilisé pour le cryptage des blocs
        $key     = 'm8BngOmOUQ';    // Clé de cryptage
        $mode    = 'cbc'; 
        $keyHash = md5($key);
        $key = substr($keyHash, 0,   mcrypt_get_key_size($cipher, $mode) );
        $iv  = substr($keyHash, 0, mcrypt_get_block_size($cipher, $mode) );
 
        $data = base64_decode($data);
        $data = mcrypt_decrypt($cipher, $key, $data, $mode, $iv);
        return rtrim($data);
    }	

	function isEmail($email) {
    return preg_match('|^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]{2,})+$|i', $email);
		}

		function isAlreadyMembre($email)
		{
			$aUser  = $this->oRepositoryUser->findOneBy(array('email'=>$email));

			return  !empty($aUser) ;
		}

	}