<?php
namespace M\CoreBundle\Services;
use M\CoreBundle\Entity\User;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use Ratchet\Wamp\WampServerInterface;

use React\EventLoop\LoopInterface;

/**
* 
*/
class EventHandler implements WampServerInterface
{
    
    function __construct(LoopInterface $loop)
    {
        $this->loop = $loop;
    }

    

    private $oRepository;
    private $oUser;
    /**
     * @var array List of connected clients
     */
    private $clients;

    /**
     * @var \React\EventLoop\LoopInterface
     */
    private $loop;
    protected $subscribedTopics = array();
    protected $subscription;


   /**
     * A user connects, we store the connection by the unique resource id
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients[$conn->resourceId]['conn'] = $conn;
        //var_dump("New connection");
    }
    /**
     * A user subscribes. The JSON is in $subscription->getId()
     */
    public function onSubscribe(ConnectionInterface $conn, $subscription)
    {
        $this->subscription = $subscription;
        // This is the JSON passed in from your JavaScript
        // Obviously you need to validate it's JSON and expected data etc...
        $data = json_decode($subscription->getId());

        //var_dump("onSubscribe : User subscribes step1");
        // Validate the users id and token together against the db values

        // When a visitor subscribes to a topic link the Topic object in a  lookup array
        $subject = $subscription->getId();
        $ip = $conn->remoteAddress;

        if (!array_key_exists($subject, $this->subscribedTopics)) 
        {
            $this->subscribedTopics[$subject] = $subscription;
        }

        $this->clients[] = $conn; // you add connection to the collection
        //var_dump("onSubscribe : User subscribes step1..5");



        // Now, let's subscribe this user only
        // 5 = the interval, in seconds
        $timer = $this->loop->addPeriodicTimer(10, function() use ($subscription) 
        {
            //var_dump("onSubscribe : User subscribes step2");

            //$data = "whatever data you want to broadcast";

           // var_dump("onSubscribe : User subscribes step3 (Send)");
            //return $subscription->broadcast(json_encode($data));
        });

        //var_dump("onSubscribe : User subscribes step4");
        // Store the timer against that user's connection resource Id
        $this->clients[$conn->resourceId]['timer'] = $timer;
    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onMessage($jData) 
    {
        $aData = json_decode($jData, true);
        
        //var_dump('onMessage : Step1');
        //var_dump($aData);


        if (!array_key_exists($aData['topic'], $this->subscribedTopics)) 
        {
            return;
        }

        $topic = $this->subscribedTopics[$aData['topic']];

        // This sends out everything to multiple users, not what I want!!
        //var_dump('onMessage : Step2 (SENT)');
        // re-send the data to all the clients subscribed to that category
        $this->subscription->broadcast($aData);

        //$this->clients->send($aData);
    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic) 
    {
        //var_dump('UNSUBSCRIBE');
    }
    
    public function onClose(ConnectionInterface $conn)
    {
        // There might be a connection without a timer
        // So make sure there is one before trying to cancel it!
        if (isset($this->clients[$conn->resourceId]['timer']))
        {
            if ($this->clients[$conn->resourceId]['timer'] instanceof TimerInterface)
            {
                $this->loop->cancelTimer($this->clients[$conn->resourceId]['timer']);
            }
        }

        unset($this->clients[$conn->resourceId]);
    }
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) 
    {
        // In this application if clients send data it's because the user hacked around in console
        //$conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) 
    {
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }
    public function onError(ConnectionInterface $conn, \Exception $e) 
    {
    }

    public function OnlineUsers()
    {
        // Je vérifie si $this->oUser est un object avant de n'envoyer cela au Repository
        if (is_object($this->oUser)) 
        {
            $bSexe =  !$this->oUser->getSexe(); 
            $iUserId = $this->oUser->getId();
            $aIdBloque[] = 0;// nous initialison ici cette variable pour qu'elle soit valide au niveau du repository est evité un résultat null
            //on génére le tableau de contact bloqué 
            foreach ($this->oUser->getContactbloque() as $key => $value) 
            {
                $aIdBloque[] = $key;
            }
            return $this->oRepository->FetchOnlineUser($bSexe,$iUserId,$aIdBloque);
        }

      return false;
    }

   
}




 ?>