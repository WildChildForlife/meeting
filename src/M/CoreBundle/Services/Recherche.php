<?php
namespace M\CoreBundle\Services;
use M\CoreBundle\Entity\User;

/**
* 
*/
class Recherche
{
    
    function __construct($oContainer)
        {
            $this->oRepository = $oContainer->get("doctrine")->getManager();
            $this->oFonctionDiver = $oContainer->get("FonctionDiver");
            $this->oPaginator = $oContainer->get("Paginator");
           
        }

    private $oRepository;
    private $oFonctionDiver;
    private $oPaginator;

    /*
        But : Retourner le resultat de la recherche
        Param : 
            * oRequest : Formulaire : Array 
            * bSexe : Sexe de l'utilisateur : Bool
            * oParam : Parametre requete sql : Array 
        Retour :
            * Erreur  = False
            * Success = Array        
    */
    public function recherche($oRequest,$bSexe,$iIdUser,$iPage,$aIdBloque)
    {
      
        if (!is_array($oRequest) || !is_bool((bool)$bSexe)|| !is_int($iPage) || !is_int($iIdUser) || !is_array($aIdBloque)  ) return false; 
        //On crée les parametres de la recherche en fonction des champs remplis
        
        //On supprime les clé inutile _token 
        unset($oRequest['_token']);
        $oParam = array();
        

        $sQuery ="u.actif=1 "; //On prépare la variable de la requéte
        //Cette partie de requete a été ajouté dans une variable pour évité la redondance elle permet de transformé le timestamp en  age 
        $sTimediff = "TimeDIFF(YEAR,DATEFORMAT(DATEADD('1970-01-01 00:00:00',INTERVAL (u.datenaissance) SECOND),'%Y%-%m%-%d'),:today)";
        $_aResult['ResultPerPage'] = 20;//Nombre resultat par page

        foreach ($oRequest as $key => $value) 
        {   
            //On supprime les clés vide
            if(!empty($value) )  $oParam[$key] = $oRequest[$key];
        }


        // Tableau de paramétre temporaire;
        $_oParam = $oParam;
        // on rajoute les paramétre du sexe et de la date du jour
        $oParam['today']  = date('Y-m-d');
        if (!array_key_exists('sexe',$oRequest )) 
            { 
                $sQuery =  $sQuery."AND u.sexe =  :sexe ";  
                unset($_oParam['sexe']);     
                $oParam['sexe']   = $bSexe; 
            }
        else
            {
                $_aResult['ResultPerPage'] = 1000000;//Nombre resultat par page
               // Dans le cas ou sexe est soumis
                if ($oRequest['sexe']!="")
                {   
                   $oParam['sexe']   = $oRequest['sexe'];
                   $sQuery =  $sQuery."AND u.sexe =  :sexe ";  
        
 
                }

                if ($oRequest['typeabonne']!="")
                {   
                   $oParam['typeabonne']   = "%".$oRequest['typeabonne']."%";
                   $sQuery =  $sQuery."AND u.roles LIKE  :typeabonne ";  
                   unset($_oParam['typeabonne']);     

        
 
                }

                if (!empty($oRequest['inscriptiondu']) || !empty($oRequest['inscriptionau']))
                {  
            
                   //On supprime le paramétre age de fin qui ne sera pas utilisé
                   if (empty($oRequest['inscriptionau']))
                    {
                        unset($oParam['inscriptionau']);
                        $sQuery =  $sQuery."  AND u.date_inscription >= :inscriptiondu";

                    }
                    else
                    {   // Sinon on faire une recherche par intervall
                        $sQuery =  $sQuery." AND u.date_inscription >= :inscriptiondu  AND  u.date_inscription <= :inscriptionau ";
                    }

                    //On détruit ces clés car elles ont étées traitées préalablement
                    unset($_oParam['inscriptiondu']);
                    unset($_oParam['inscriptionau']);
                     }


            }
          


        $oParam['aIdBloque'] = $aIdBloque;
        $oParam['admin'] = '%ADMIN%';
        $oParam['moderateur'] = '%MODERATEUR%';

 

       /* $oParam['bloque'] ="%i:".$iIdUser.";%";*/// id de l'utilisateur actuelle pour voir si il est bloqué
        // Verifier si un pseudo et soumis toutes les autres requetes s'annule
        if (!empty($oParam['username']))  
        {  

             if (!array_key_exists('sexe',$oRequest )) 
            { 
                $sQuery =  "u.sexe = :sexe AND ";  
                $oParamUsername['sexe']     = $bSexe;

            }
            // on génére la requéte sql
            $sQuery = $sQuery."u.username = :username" ;
            // on génére la liste des paramétre a envoyé puisque oRequest comporte plus de paramétre qu'il ne faut
            $oParamUsername['username'] = $oRequest['username'];
            $oParamUsername['today']    = $oParam['today'];

            $oParamUsername['admin']    = $oParam['admin'] = '%ADMIN%';
            $oParamUsername['moderateur']    = $oParam['moderateur'] = '%MODERATEUR%';
          /*  $oParamUsername['bloque'] ="%i:".$iIdUser.";%";*/
            $oParamUsername['aIdBloque'] = $oParam['aIdBloque'];// ici les ids des contact le l'utilisateur a bloqué

            //On Lance la recherche 
            $_aResult = $this->oRepository->getRepository("MCoreBundle:User")->SearchUser($oParamUsername, $sQuery,0,0);
            $aResult['count'] = ($_aResult) ? 1 : 0;
            //On Injecte le signe astrologique
            $aResult['Result'] = $this->oFonctionDiver->setSigneToResult($_aResult);
            //On Injecte verifie si le user et en ligne ou pas 
            $aResult['Result'] = $this->oFonctionDiver->setEnligneUserToResult($aResult['Result']);
            $aResult['iTotalPages'] = 0 ;
            
            return  $aResult;

        }

        /////// ICi nous traiterons les champs qui font appelle a des tables étrangére : ville
        if (!empty($oParam['ville']))  
        {   
            // on génére la requéte sql
            $sQuery = $sQuery." AND v.id = :ville" ;
            //On détruit cette clé car elle a été traité préalablement
            unset($_oParam['ville']);
        
        }  

        if (!empty($oParam['nationalite']))  
        {   
            // on génére la requéte sql
            $sQuery = $sQuery." AND n.id = :nationalite" ;
            //On détruit cette clé car elle a été traité préalablement
            unset($_oParam['nationalite']);
        
        }
        if (!empty($oParam['profession']))  
        {   
            // on génére la requéte sql
            $sQuery = $sQuery." AND p.id = :profession" ;
            //On détruit cette clé car elle a été traité préalablement
            unset($_oParam['profession']);
        
        }   
        //////// FIn tables clés étrangére

        // Dans le cas ou l'age est pas soumis
        if (!empty($oParam['ageDebut']) || !empty($oParam['ageFin']))
        {  
            
           //On supprime le paramétre age de fin qui ne sera pas utilisé
           if (empty($oParam['ageFin']))
            {
                unset($oParam['ageFin']);
                $sQuery =  $sQuery."  AND $sTimediff  = :ageDebut";

            }
            else
            {   // Sinon on faire une recherche par intervall
                $sQuery =  $sQuery." AND $sTimediff  >= :ageDebut  AND  $sTimediff <= :ageFin ";
            }

            //On détruit ces clés car elles ont étées traitées préalablement
            unset($_oParam['ageDebut']);
            unset($_oParam['ageFin']);
        }


        // Dans le cas ou taille n'est pas soumis
        if (!empty($oParam['tailleDebut']) || !empty($oParam['tailleFin']))
        {  
        
           //On supprime le paramétre taille de fin qui ne sera pas utilisé
           if (empty($oParam['tailleFin']))
            {
                unset($oParam['tailleFin']);
                $sQuery =  $sQuery."  AND taille  = :tailleDebut";
            }
            else
            {   // Sinon on faire une recherche par intervall
                $sQuery =  $sQuery." AND u.taille BETWEEN  :tailleDebut AND  :tailleFin ";
            }

            //On détruit ces clés car elles ont étées traitées préalablement
            unset($_oParam['tailleDebut']);
            unset($_oParam['tailleFin']);
        }

        // Dans le cas ou Enligne est soumis
        if (!empty($oParam['EnLigne']))
        {   
            if ($oParam['EnLigne'] == 1 )
            {     
                $dDelay = new \DateTime();
                $dDelay->setTimestamp(strtotime('10 minutes ago'));
                
                $oParam['EnLigne'] = $dDelay;
                $sQuery =  $sQuery." AND u.lastActivity > :EnLigne ";  
                //On détruit cette clé car elle a été traité préalablement
                unset($_oParam['EnLigne']);
            }
        }

        // Dans le cas ou la date d'nscription est soumise
        if (!empty($oParam['inscritDepuis']))
        {   
                $oParam['inscritDepuis'] = date('Y-m-d', strtotime("-".$oParam['inscritDepuis']." days"));
                $sQuery =  $sQuery." AND u.date_inscription >=  :inscritDepuis ";  
                //On détruit cette clé car elle a été traité préalablement
                unset($_oParam['inscritDepuis']);
      
        }

        // Dans le cas ou Enligne est soumis
        if (!empty($oParam['Centresinteret']))
        {   
               
          unset($_oParam['Centresinteret']);     
              
        }


        //On verify si les Autre champs on été remplis et on génére la requete where au fur et a mesure;
        foreach ($_oParam as $key => $value) 
        {   
            //On verifie que la valeur de nos paramétres ne sont pas vide et on exclut ceux déja été traité o paravant
            $sQuery = $sQuery." AND u.".$key." =:".$key;
                                 
        }
            

    $_aResult['count'] = $this->oRepository->getRepository("MCoreBundle:User")->CountSearchUser($oParam, $sQuery);//Total resultats
    $oPaginator  = $this->oPaginator->getPagination((int)$_aResult['count'],(int)$_aResult['ResultPerPage'],(int)$iPage);//Service Pagination
 
    $_aResult['iTotalPages'] = $oPaginator['iTotalPages'];
    $_aResult['Result'] = $this->oRepository->getRepository("MCoreBundle:User")->SearchUser($oParam, $sQuery,$_aResult['ResultPerPage'],$oPaginator['FirstResultPerCurrentPage']);//Resultat final

    //On Injecte le signe astrologique
    $_aResult['Result'] = $this->oFonctionDiver->setSigneToResult($_aResult['Result']);
    //On Injecte verifie si le user et en ligne ou pas 
    $_aResult['Result'] = $this->oFonctionDiver->setEnligneUserToResult($_aResult['Result']);
    //Retour du resultats des users recherché
    return $_aResult;
    
    }

  
    public function SetRechercheInfo($aRecherche)
    {
     
       if (!is_array($aRecherche)) return new user(); 
       
       $oUser = new user();
       //Ici on traitre les champs qui on des clés étrangére

            if (!empty($aRecherche['Centresinteret'])) 
            {
                foreach($aRecherche['Centresinteret'] as $CI)
                { 
                
                $oUser->addCentreinteret($this->oRepository->getRepository('MCoreBundle:Centresinteret')->find($CI));
                
                }
                 unset($aRecherche['Centresinteret']);    
            }
                
 

        foreach ($aRecherche as $key => $value) 
                {  

                    //Ici on traitre les champs qui on des clés étrangére
                    if ($key == "nationalite" || $key == "profession" || $key == "ville" )
                    {       
                        $value =  $this->oRepository->getRepository("MCoreBundle:".ucwords($key)."")->find($value);

                    }
                    $func="set".ucwords($key);
                    if ($value != "" && method_exists($oUser,$func))
                    {
                        $oUser->$func($value);
                    }
                }

        return $oUser;
    }


}



