<?php

namespace M\CoreBundle\Services;


/**
* 
*/
class Paginator
{
	
	private $iTotalData;
	private $iCurrentPage;
	private $iTotalPages;


	// Params
	private $iResultPerPage;
	// Return
	private $iTotalResults;


	public function getPagination($iTotalData,$iResultPerPage, $iCurrentPage)
	{	
		if (!is_int($iTotalData) || !is_int($iResultPerPage) || !is_int($iCurrentPage)) return $oPaginator['FirstResultPerCurrentPage']=0;

		$this->_hydrate($iTotalData,$iResultPerPage, $iCurrentPage);
		$this->getTotalPages();
		$this->getFirstResultPerCurrentPage();

		return array('FirstResultPerCurrentPage' => $this->getFirstResultPerCurrentPage(), 'iTotalPages' => $this->iTotalPages);
	}

	public function _hydrate($iTotalData,$iResultPerPage, $iCurrentPage)
	{
		$this->iTotalData 		= $iTotalData;
		$this->iResultPerPage 	= $iResultPerPage;
		$this->iCurrentPage 	= $iCurrentPage;
	}

	public function getFirstResultPerCurrentPage()
	{
		return (int) ( ($this->iCurrentPage - 1) * $this->iResultPerPage);
	}


	public function getTotalPages()
	{	

		$this->iTotalPages = ceil($this->iTotalData / $this->iResultPerPage);

	}

}




 ?>