<?php
namespace M\CoreBundle\Services;
use M\CoreBundle\Entity\User;

/**
* 
*/
class MenuNotification
{
    
    function __construct($oContainer)
    {
        $this->oEm = $oContainer->get("doctrine")->getManager();
        $this->oUser = (is_object($oContainer->get("security.context")->getToken())) ? $oContainer->get("security.context")->getToken()->getUser() : false;
    }
    // Visite Visite = 0
    // Visite Coupdecoeur = 1
    // Visite Message = 2
    // Visite DemandeTChat = 3
    private $oEm;
    private $oUser;

    public function fetchNotification()
    {
         // Je vérifie si $this->oUser est un object avant de n'envoyer cela au Repository
        return (is_object($this->oUser)) ? $this->oEm->getRepository("MCoreBundle:Notification")->FindNotificationNotViewed($this->oUser->getId(),$this->oUser->getContactbloque()) : false;
    }

    
    public function getNotification()
    {
        $aNotification = array(
                "Visites"       => 0,
                "CPC"           => 0,
                "Messages"      => 0,
                "EnLigne"       => 0,
                "DemandeTchat"  => 0
            );  
        $aResult = $this->fetchNotification();
/*echo "<pre>" . print_r($aResult, true) . "</pre>";
           die();*/
        $aInfoAdmin = $this->oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $aNotification['ActiveOnline'] = ($aInfoAdmin) ? $aInfoAdmin[0]->getOnlineuser() : 0;

        $aNotification['EnLigne'] = $this->oEm->getRepository('MCoreBundle:User')->CountAllOnlineUser();

        if (!is_array($aResult)) return $aNotification;
        foreach ($aResult as $key => $value) 
        { 
            if (isset($value['type']) && $value['type'] == 0)  $aNotification['Visites']      = $value['Compteur'] ;
            if (isset($value['type']) && $value['type'] == 1)  $aNotification['CPC']          = $value['Compteur'] ;
            if (isset($value['type']) && $value['type'] == 2)  $aNotification['Messages']     = $value['Compteur'] ;
            if (isset($value['type']) && $value['type'] == 3)  $aNotification['DemandeTchat'] = $value['Compteur'] ;           
        }    

        return  $aNotification;
    }

        public function getAllNotification()
    {
        $aStat['Received'] = array("Visites"=>0,"CPC"=>0,"Messages"=>0,"DemandeTchat"=>0);
        $aStat['Sended'] = $aStat['Received'];
        $aStatReceivedResult = (is_object($this->oUser)) ? $this->oEm->getRepository("MCoreBundle:Notification")->FindAllNotificationReceived($this->oUser->getId()) : false;
        $aStatSendedResult = (is_object($this->oUser)) ? $this->oEm->getRepository("MCoreBundle:Notification")->FindAllNotificationSended($this->oUser->getId()) : false;

        if (!is_array($aStatReceivedResult)) return $aStat;

        foreach ($aStatReceivedResult as $key => $value) 
        { 
              if (isset($value['type']) && $value['type'] == 0)  $aStat['Received']['Visites'] = $value['Compteur'] ;
              if (isset($value['type']) && $value['type'] == 1)  $aStat['Received']['CPC']     = $value['Compteur'] ;
              if (isset($value['type']) && $value['type'] == 2)  $aStat['Received']['Messages']= $value['Compteur'] ;
              if (isset($value['type']) && $value['type'] == 3)  $aStat['Received']['DemandeTchat']= $value['Compteur'] ;

             
        }    

        if (!is_array($aStatSendedResult)) return $aStat;
        
        foreach ($aStatSendedResult as $key => $value) 
        { 
              if (isset($value['type']) && $value['type'] == 0)  $aStat['Sended']['Visites'] = $value['Compteur'] ;
              if (isset($value['type']) && $value['type'] == 1)  $aStat['Sended']['CPC']     = $value['Compteur'] ;
              if (isset($value['type']) && $value['type'] == 2)  $aStat['Sended']['Messages']= $value['Compteur'] ;
              if (isset($value['type']) && $value['type'] == 3)  $aStat['Sended']['DemandeTchat']= $value['Compteur'] ;

             
        }    

        return  $aStat;
    }
    
}




