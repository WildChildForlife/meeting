<?php
namespace M\CoreBundle\Services;
use M\CoreBundle\Entity\User;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use Ratchet\Wamp\WampServerInterface;


/**
* 
*/
class Chat implements WampServerInterface 
{
    
    function __construct($oContainer)
    {
        $this->oRepository = $oContainer->get("doctrine")->getManager()->getRepository("MCoreBundle:User");
        $this->oUser = (is_object($oContainer->get("security.context")->getToken())) ? $oContainer->get("security.context")->getToken()->getUser() : false;
        $this->clients = new \SplObjectStorage;
    }

    private $oRepository;
    private $oUser;
    protected $clients;
    protected $subscribedTopics = array();

    /**
     * A lookup of all the topics clients have subscribed to
     */
    public function onSubscribe(ConnectionInterface $conn, $topic) 
    {
        // When a visitor subscribes to a topic link the Topic object in a  lookup array
        $subject = $topic->getId();
        $ip = $conn->remoteAddress;

        if (!array_key_exists($subject, $this->subscribedTopics)) 
        {
            $this->subscribedTopics[$subject] = $topic;
        }

        $this->clients[] = $conn; // you add connection to the collection

        $conn->send($topic); // you send a message only to this one user

    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onMessage($jData) 
    {
        $aData = json_decode($jData, true);

        if (!array_key_exists($aData['topic'], $this->subscribedTopics)) {
            return;
        }

        $topic = $this->subscribedTopics[$aData['topic']];

        // This sends out everything to multiple users, not what I want!!

        // re-send the data to all the clients subscribed to that category
        $topic->broadcast($aData);

        //$this->clients->send($aData);
    }
    public function onUnSubscribe(ConnectionInterface $conn, $topic) 
    {
    }
    public function onOpen(ConnectionInterface $conn) 
    {
        $this->clients->attach($conn);
    }
    public function onClose(ConnectionInterface $conn) 
    {
    }
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) 
    {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) 
    {
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }
    public function onError(ConnectionInterface $conn, \Exception $e) 
    {
    }

    /*public function OnlineUsers()
    {
        // Je vérifie si $this->oUser est un object avant de n'envoyer cela au Repository
        if (is_object($this->oUser)) 
        {
            $bSexe =  !$this->oUser->getSexe(); 
            $iUserId = $this->oUser->getId();
            $aIdBloque[] = 0;// nous initialison ici cette variable pour qu'elle soit valide au niveau du repository est evité un résultat null
            //on génére le tableau de contact bloqué 
            foreach ($this->oUser->getContactbloque() as $key => $value) 
            {
                $aIdBloque[] = $key;
            }
            return $this->oRepository->FetchOnlineUser($bSexe,$iUserId,$aIdBloque);
        }

      return false;
    }*/

   
}




 ?>