<?php

// src/Acme/HelloBundle/DataFixtures/ORM/LoadUserData.php

namespace M\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use M\CoreBundle\Entity\Agenda;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * {@inheritDoc}
     */

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function getUserforagenda()
    {
        $em = $this->container->get('doctrine')->getEntityManager('default');
        $user = $em->getRepository('MCoreBundle:User')->find(69);
        return $user;
    }
    public function load(ObjectManager $manager)
    {
        $now = new \DateTime;
        $agenda = new Agenda();

        for ($i =0 ; $i<2 ; $i++)
        {
        $agenda->setCommentaires('agenda 1');
        $agenda->setDate($now);
        $agenda->setUser($this->getUserforagenda());
        $agenda->setTitre('titre agenda 1');
        $agenda->setLieu('lieu agenda 1');

        $manager->persist($agenda);
        $manager->flush();
        }
    }
}