<?php

namespace M\CoreBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use M\CoreBundle\Payzone\Connect2PayClient;
use M\CoreBundle\Entity\User;
use M\CoreBundle\Entity\Abonnement;

class PayController extends Controller
{
  
    public function verifypayAction()
    {
      $oUserCurrent = $this->get('security.context')->getToken()->getUser();
      $em = $this->getDoctrine()->getManager();
        $c2pClient = $this->get('payzone_service');
       $response = array("status" => "KO", "message" => "Status Not recorded");
        if ($c2pClient->handleCallbackStatus()) {
          // In a real utilization we must fetch the local transaction
          // having the received status merchantToken and update it with the status
          // As an example, here we just log the received status

          
          $sCustomerToken = $c2pClient->getStatus()->getMerchantToken();
          $oAbonnement = $em->getRepository('MCoreBundle:Abonnement')
          ->findOneBy(array('CustomerToken' => $sCustomerToken ));

          $status = $c2pClient->getStatus();
          
          if($status->getErrorCode() == '000'){

              $oStatutCommande = $em->getRepository('MCoreBundle:StatutCommande')->find(2);
              $response = array("status" => "OK", "message" => "Status recorded");

          }
          else{
            $oStatutCommande = $em->getRepository('MCoreBundle:StatutCommande')->find(4);
            

          }
      
          $oAbonnement->setStatutCommande($oStatutCommande);
          $em->persist($oAbonnement);
          $em->flush();

          if($status->getErrorCode() == '000')
          {
             $oActualDate = new \DateTime();
             $oAbonnement = $em->getRepository('MCoreBundle:Abonnement')
          ->findOneBy(array('CustomerToken' => $sCustomerToken ));
            switch($oAbonnement->getTypeAbonnement())
                {
                    case 0 : $sTypeAbonnement = "Abonnement permium 1 mois sur rencontres & vous";
                             $iPrixUnitaire = 220;
                             $addmonth='+1 month';
                             $recompenseParrainage ='+15 days';
                             break;
                    case 1 :$sTypeAbonnement = "Abonnement permium 3 mois sur rencontres & vous";
                            $iPrixUnitaire = 480;
                            $addmonth='+3 month';
                            $recompenseParrainage ='+1 month';
                            break;
                    case 2 : $sTypeAbonnement = "Abonnement permium 6 mois sur rencontres & vous";
                             $iPrixUnitaire = 720;
                             $addmonth='+6 month';
                             $recompenseParrainage ='+2 month';
                             break;
                    case 3 : $sTypeAbonnement = "Abonnement permium 12 mois sur rencontres & vous";
                             $iPrixUnitaire = 1080;
                             $addmonth='+12 month';
                             $recompenseParrainage ='+3 month';
                             break;
                }
            $oDateaddMonths = $oActualDate->modify($addmonth);
            $oAbonnement->setDateExpiration($oDateaddMonths);
            $iIdUser = $oAbonnement->getUser();
            $oUser= $em->getRepository('MCoreBundle:User')->find($iIdUser);
            $oUser->setRoles(array('ROLE_USER'));
            // Recompense parraiange ; 

            if($oUser->getParrain() instanceof User)
            {
              // on recompense le feiulle
              $oDateaddMonths->modify($recompenseParrainage); 
              $oAbonnement->setDateExpiration($oDateaddMonths);

              // on recompense le parrain
              // $iParrainUser = $oUser->getParrain();
              $oParrainUser = $oUser->getParrain();

              // # 2 cas s'il y a un abonnement on prolonge sinon on crée un abonnement
              $oAbonnementParrain = $em->getRepository('MCoreBundle:Abonnement')->getLastActiveAbonnementByUser($oParrainUser);
             if($oAbonnementParrain)
              {
                $objAbonnementParrain = $oAbonnementParrain[0];
                $oDateaddrecompense = new \DateTime();
                $dateExpiration = $objAbonnementParrain->getDateExpiration();
                $interval = $oDateaddrecompense->diff($dateExpiration);
                $oDateaddrecompense->modify($interval->format('%R%a days'));
                $oDateaddrecompense->modify($recompenseParrainage);
                $oDateaddrecompense->modify('+1 day');
                $objAbonnementParrain->setDateExpiration($oDateaddrecompense);

                $em->persist($objAbonnementParrain);
                $em->flush();
                
              }
              else
              {
                $aujourdhui = new \DateTime();
                $oAbonnementParrain = new Abonnement();
                $oAbonnementParrain->setUser($oParrainUser);
                $oAbonnementParrain->setDateCreation(new \DateTime());
                $oAbonnementParrain->setDateExpiration($aujourdhui->modify($recompenseParrainage));
                $oParrainUser->setRoles(array('ROLE_USER'));
                $em->persist($oAbonnementParrain);
                $em->persist($oParrainUser);
                $em->flush();
              }
            }

            $em->persist($oUser);
            $em->persist($oAbonnement);
            $em->flush();
            $sCodePromo = $oAbonnement->getCodePromotionnel();
            $sReduction = 0 ;
                if(isset($sCodePromo))
                {
                $aReduction= $em->getRepository('MCoreBundle:Admin')->getReductionForCodePromo($sCodePromo);
                $sReduction = (!empty($aReduction)) ? $aReduction['0']['reduction'] : 0;
                }
             
          }
          
        }
        
          header("Content-type: application/json");
          // syslog(LOG_ERR, "Error. Received an incorrect status from " . $_SERVER["REMOTE_ADDR"] . ".");
        return new response(json_encode($response));

        // return new response('notOk');
    }

    public function redirectAction()
    {
      $oUserCurrent = $this->get('security.context')->getToken()->getUser();
       //Instantiate a new connect2pay client with the same configuration already used to initialize the trasaction
        $c2pClient = $this->get('payzone_service');

      /**
       * 
       * Notification with redirectURL parameter
       * 
       * After payment processing, the customer will be presented a result page with a link to return to the merchant site.
       * This link is a form that will generate an HTTP POST request towards the redirectURL address.
       * 
       * In that case, the merchant has to decrypt the data field with his merchant token to be able to get the status of the transaction.
       * 
       * Please refer you to the documentation - Transaction Status Response section - for more iformations
       **/

        if (isset($_POST['data']) and !empty($_POST['data'])) {
          $encryptedStatus = $_POST['data'];
        }
        else{
          die();
        }

        //You must already have registered the merchant token in the session or database
         $oSession = $this->get('session');
        $merchantToken = $oSession->get('merchantToken');
        if($c2pClient->handleRedirectStatus($encryptedStatus, $merchantToken)){

          $status = $c2pClient->getStatus();
          $message = $status->getErrorMessage();

          if($status->getErrorCode() == '000'){
            //if status code == '000' it means that the transaction was successful
            $sCustomerToken = $status->getMerchantToken();

            $em = $this->getDoctrine()->getManager();
            $abonnement = $em->getRepository('MCoreBundle:Abonnement')
            ->findOneBy(array('CustomerToken' =>$sCustomerToken)); 

            if($abonnement->getStatutCommande()->getId() == 2)
            {
              switch($abonnement->getTypeAbonnement())
                {
                    case 0 : $sTypeAbonnement = "Abonnement permium 1 mois sur rencontres & vous";
                             $iPrixUnitaire = 220;
                             break;
                    case 1 :$sTypeAbonnement = "Abonnement permium 3 mois sur rencontres & vous";
                            $iPrixUnitaire = 480;
                             break;
                    case 2 : $sTypeAbonnement = "Abonenment permium 6 mois sur rencontres & vous";
                             $iPrixUnitaire = 720;
                             break;
                    case 3 : $sTypeAbonnement = "Abonnement permium 12 mois sur rencontres & vous";
                             $iPrixUnitaire = 1080;
                             break;
                }
                $codepromo = $abonnement->getCodePromotionnel();
                $reduction = 0;
                if(isset($codepromo))
                {
                $admin = $em->getRepository('MCoreBundle:Admin')->findOneBy(array('codepromo'=>$codepromo));
                $reduction = $admin->getReduction();
                }
                $dDateAbonnement = date_format($abonnement->getDateExpiration(),'d/m/Y');
              $this->get('Mail')->mailOrderConfirmation($oUserCurrent->getEmail(),$oUserCurrent->getUsername(),$abonnement->getnumeroCommande(),"Par Carte bancaire",$sTypeAbonnement,$iPrixUnitaire,$abonnement->getTotalPaye(),$reduction);
              $this->get('Mail')->mailAbonnement($oUserCurrent->getUsername(),$dDateAbonnement,$oUserCurrent->getEmail()); 
              return $this->render('MCoreBundle:Profile:recapafterpaiement.html.twig',
                array('oAbonnement' => $abonnement,'sTypeAbonnement' => $sTypeAbonnement,'iPrixUnitaire'=>$iPrixUnitaire,'reduction' => $reduction));
            }

            
          }
            else{
            //else it means that the transaction was failed
             return new response( "We are sorry, an error has occurred" );
          }
        }
    }
}