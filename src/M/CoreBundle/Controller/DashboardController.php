<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use M\CoreBundle\Entity\User;
use M\CoreBundle\Entity\Agenda;
use M\CoreBundle\Entity\Recherche;
use M\CoreBundle\Entity\Abonnement;

use M\CoreBundle\Form\DefaultType;
use M\CoreBundle\Form\AgendaType;
use M\CoreBundle\Form\FiltreContactType;
use M\CoreBundle\Form\RechercheRapideType;
use M\CoreBundle\Form\RechercheAvanceType;
use M\CoreBundle\Form\RechercheFavType;
use M\CoreBundle\Form\ChangePasswordType;
use M\CoreBundle\Form\UnsubcribeType;
use M\CoreBundle\Form\AbonnementType;
use M\CoreBundle\Form\AbonnementCoordonneesType;
use M\CoreBundle\Form\AbonnementCoordonneesUserType;
use M\CoreBundle\Payzone\Connect2PayClient;


//use Zend\ServiceManager\ServiceLocatorAwareInterface;

class DashboardController extends Controller
{
   
    public function indexAction()
    {
        
        if ($this->get('security.context')->isGranted('ROLE_UNSUBSCRIB')) { return $this->redirect($this->generateUrl('m_core_homepage')); }
        //Recupération des données user et de son sexe
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $bSexe = !$oUserCurrent->getSexe(); // Sexe opposé
        $oEm = $this->getDoctrine()->getManager();
        $aImage = $oEm->getRepository('MCoreBundle:Images')->getProfilByUser($this->get('security.context')->getToken()->getUser()->getId());
        //on génére le tableau de contact bloqué 
        $aIdBloque = ( count($oUserCurrent->getContactbloque()) === 0 ) ? array('0') : array_keys($oUserCurrent->getContactbloque());
        
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->find(1);
        $oUser = new User();

        $oForm = $this->createForm(new RechercheRapideType(), $oUser);

        $oFonctionDiver = $this->get('FonctionDiver');
        //on recupére les suggestions de profil userame age ville
        $aSuggest = $oEm->getRepository("MCoreBundle:User")->SuggestUser($bSexe,$aIdBloque);
        $aSuggest = $this->get('FonctionDiver')->setEnligneUserToResult($aSuggest);
        
        // Resize Images
        foreach ($aSuggest as $iKey => $sValue) 
        {
            $aSuggest[$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 130, 170);
        }

        shuffle($aSuggest);
        //Event Actus
        $aSortieActus=array();
        $_aEvents=array();
        $aActus = $oEm->getRepository("MCoreBundle:Actus")->dashbordActus();
        $aEvents = $oEm->getRepository("MCoreBundle:Events")->dashbordEvents();

        foreach ($aEvents as $iKey => $sValue) 
        {  

            $string = $oFonctionDiver->formatDate($aEvents[$iKey]['date']);
            $aEvents[$iKey]['date'] = substr(substr($string, 0, strpos($string, " ")), 0, 3) . "." . substr($string, strpos($string, ' '));
            

        }

        // On va boucler sur le tableau le plus grand des deux tout en détectant celui qui a été rempli
        $_PlusGrandTableau = array();
        $_PlusPetitTableau = array();

        $_PlusGrandTableau = ( count($aActus) > count($aEvents) ) ? $aActus : $aEvents;
        $_PlusPetitTableau = ( count($aActus) > count($aEvents) ) ? $aEvents : $aActus;

        foreach ($_PlusGrandTableau as $iKey1 => $sValue1) 
        {
            if (count($aSortieActus) < 6 )
            {
                $aSortieActus[] = $sValue1;
                if ( isset($_PlusPetitTableau[$iKey1]) ) 
                    $aSortieActus[] = $_PlusPetitTableau[$iKey1];
            }
        }

        
        // On Encode les urls d'event et actus
        $aSortieActus= $this->get('FonctionDiver')->encodeUrlsFromArray($aSortieActus);

        return $this->render('MCoreBundle:Profile:index.html.twig', array(
            'oForm' => $oForm->createView(),
            'aImage' => $aImage, 
            'aInfoAdmin' => $aInfoAdmin, 
            'aSuggest'=> $aSuggest,
            'aSortieActus' => $aSortieActus
        ));
    }

    public function rechercheAction()
    {
        //Recupération des données user et de son sexe
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $bSexe = !$oUserCurrent->getSexe(); // Sexe opposé
        //Récupération des centres d'interet et de leur catégorie pour généré leur formulaire
        $aCategorieCI = $oEm->getRepository('MCoreBundle:CategorieCentresinteret')->findAll();
        $aCentresInteret = $oEm->getRepository('MCoreBundle:Centresinteret')->FetchAllCentres();

        $aResult = array();
        $aCentreinteretSelected = array();

        $oFormFavorite = $this->createForm(new RechercheFavType($oUserCurrent->getId()));
        $oUser = new User();

        $oRequest = $this->get('request');
 
        if ($oRequest->getMethod() == 'POST') 
        {
             
            if ($oRequest->request->get("rechercher") || $oRequest->request->get("username") )
            {   
                $aRequest = $oRequest->request->get('m_corebundle_usertype');

                /*                if ($oRequest->request->get("rechercher"))
                {
                    $aRequest['username']="";//on vide le usernam car il ne fais pas partie de la recherche
                } */

                $oSessionSearch = $this->get('session');
                $oSessionSearch->set('Search',$aRequest);
                return $this->redirect($this->generateUrl('m_core_profile_resultat'));
               // return $this->render('MCoreBundle:Profile:resultat.html.twig', array('aResult' => $aResult));  

            }
            $iRecherchFav = $oRequest->request->get("m_corebundle_Recherchetype");
            if ($iRecherchFav['recherche'])
            {   

                $Favorite = $oRequest->request->get("m_corebundle_Recherchetype");
                // Ici on récupére l'id de la recherche
                $oRechercheFavorite = $oEm->getRepository('MCoreBundle:Recherche')->find($Favorite["recherche"]);
                //Service de recherche en fonction des données soumises on récupére que la liste des paramétres de recherche
                $oSessionSearch = $this->get('session');
                $oSessionSearch->set('Search',$oRechercheFavorite->getRecherche());
                return $this->redirect($this->generateUrl('m_core_profile_resultat'));
            }

            if ($oRequest->request->get("enregistrer"))
            {

                $oRechercheReq = $oRequest->request->get('m_corebundle_usertype');
                unset($oRechercheReq['_token']);
                $aNbrRecherche = $oEm->getRepository('MCoreBundle:Recherche')->FindCountSearch($oUserCurrent->getId());

                //Ici on vérifie le nombre de recherche enregistré

                if ((int)$aNbrRecherche <= 11)
                {   //SI inferieur a 11 on creer une nouvelle recherche
                    //on verifie si le nom existe déja
                   
                    $aNomRecherche = $oEm->getRepository('MCoreBundle:Recherche')->FindNameExist($oUserCurrent->getId(),$oRequest->request->get("recherche"));
                    if (!empty($aNomRecherche)) 
                    {    
                        $this->get('session')->getFlashBag()->add('Nom','Cette Recherche Existe Déja');

                    }
                    else    
                    {     
                        $oRecherche = new recherche();
                        $oRecherche->setUser($oUserCurrent); 
                        $oRecherche->setRecherche($oRechercheReq);
                        $oRecherche->setNom($oRequest->request->get("recherche")); 
                        $oEm->persist($oRecherche);
                        $oEm->flush();                     
                        
                    }
                }    
                //sinon en écrase la recherche la plus ancienne !
                else
                {   
                    $aOldRecherche = $oEm->getRepository('MCoreBundle:Recherche')->FindOldestSearch($oUserCurrent->getId());
                    $oRecherche = $oEm->getRepository('MCoreBundle:Recherche')->find($aOldRecherche[0]['id']);
                    $oRecherche->setRecherche($oRechercheReq);
                    $oEm->persist($oRecherche);
                    $oEm->flush(); 
                }

                //Service Remplissage des champs de la recherche 

                $oUser = $this->get('Recherche')->SetRechercheInfo($oRechercheReq);
               
            }    
          
           
        }
        
        $oForm = $this->createForm(new RechercheAvanceType($bSexe), $oUser);

        //Appel du service qui reorganise les centres d'interet par cat nom et selected
        $aFormCentresInteret = $this->get('FonctionDiver')->Centresinteret($aCategorieCI,$aCentresInteret,$oUser->getCentresinteret());

        return $this->render('MCoreBundle:Profile:recherche.html.twig', array('oFormFav' => $oFormFavorite->createView(),'oForm' => $oForm->createView(),'aFormCentresInteret' => $aFormCentresInteret));
    
    }


    public function contactsAction($iPage)
    {   
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oFormFiltre = $this->createForm(new FiltreContactType);
        $oFormContact = $this->createForm(new DefaultType);

        $sQuery ="";
        $oRequest = $this->get('request');
 
        if ($oRequest->getMethod() == 'POST') 
        {
            $oParam = ($oRequest->request->get("SelectAtoZ")) ? (array)$oRequest->request->get("SelectAtoZ") : $oRequest->request->get('m_corebundle_Usertype') ;
            //Service de mise en place de la requette sql et des param 
            $aResult = $this->get('FonctionDiver')->filtreContact($oParam);
        
            $oParam = $aResult['param'];
            $sQuery = $aResult['query'];

            if ($oRequest->request->get("m_corebundle_usertype_infocontact"))
            {
                $aInfoContact = $oRequest->request->get("m_corebundle_usertype_infocontact");
                $sUsername = key($aInfoContact);
                $oUserContact=$oEm->getRepository('MCoreBundle:User')->findOneByUsername($sUsername);

                //on verifie que le user existe bien
                if (is_object($oUserContact))
                {
                    //on recupere la liste des contacts de l'utilisateur actuel
                    $aCurrentContact = $oUserCurrent->getContact();
                    $iIdContact = $oUserContact->getId();

                    //On verifie que le user existe bien dans les contacts
                    if (array_key_exists($iIdContact, $aCurrentContact))
                    { 

                       $aCurrentContact[$iIdContact]= $aInfoContact[$sUsername];
                       $oUserCurrent->setContact($aCurrentContact); 
                       $oEm->persist($oUserCurrent);
                       $oEm->flush();
                    }
                }
            }
        }
        
        //initialisation du tableau d'id
        $oParam['aId'] = '';
       
        //On Rajoute tout les contacts sauf ceux bloquer par l'utilisateur
        if (count($oUserCurrent->getContactbloque()) !== 0)
        {   
            foreach ($oUserCurrent->getContact() as $key => $value) 
            {
               if (!array_key_exists($key,$oUserCurrent->getContactbloque())) $oParam['aId'][] = $key;
            }
        }    
        else
        {     
            $oParam['aId'] = array_keys($oUserCurrent->getContact());
        }


        $iTotalResult = $oEm->getRepository('MCoreBundle:User')->GetCountUsersById($oParam,$sQuery,0,0);
        // On utilise le service pour connaitre le premier résultat a affiché
        $iNbrResultPerPage = 20; // Nombre de resultat par affichage

        $oPaginator = $this->get("Paginator")->getPagination($iTotalResult,$iNbrResultPerPage, (int)$iPage);
        //On rajoute le paramétre avec la date du jour
        $oParam['today'] = date('Y-m-d'); 
        $_aResult = $oEm->getRepository('MCoreBundle:User')->GetUsersById($oParam,$sQuery,$iNbrResultPerPage,$oPaginator['FirstResultPerCurrentPage']);
 
        //On ajoute les signes astrologique et on convertie les date en lettre a partir du service
        $aResult = $this->get('FonctionDiver')->setSigneToResult($_aResult);
        //On verifie les users en ligne
        $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($aResult);

        return $this->render('MCoreBundle:Profile:contact.html.twig', array(
                            'oFormContact' => $oFormContact->createView(),
                            'oFormFiltre' => $oFormFiltre->createView(),
                            'iPage'     => $iPage,
                            'aResult'    => $aResult,
                            'iTotalResult'=> $iTotalResult,
                            'iTotalPage'=> $oPaginator['iTotalPages']));
    }


    public function agendaAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();

        $oForm = $this->createForm(new AgendaType);

        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        {

            $aAgendaInfo = $oRequest->request;
            $dDate = new \DateTime($aAgendaInfo->get("date_agenda")." ".$aAgendaInfo->get("heure").":".$aAgendaInfo->get("minute").":00");

            $iAgendaId = $oEm->getRepository('MCoreBundle:Agenda')->rdvByUserID($oUserCurrent->getId(),$dDate);

            $oAgenda = (is_array($iAgendaId)) ? $oEm->getRepository('MCoreBundle:Agenda')->find($iAgendaId['id']) : "";

            //On verifie si l'info l'agenda exisite deja
            if ($aAgendaInfo->get("supprimer") && is_object($oAgenda))
            {
                $oEm->remove($oAgenda);
                $oEm->flush();
            }
            else
            {   

                if (is_object($oAgenda))
                {
                    $oAgenda->setTitre($aAgendaInfo->get("titre"));
                    $oAgenda->setLieu($aAgendaInfo->get("lieu"));
                    $oAgenda->setDate($dDate);
                    $oAgenda->setCommentaires($aAgendaInfo->get("commentaire"));
                }
                else
                {
                    $oAgenda = new Agenda();
                    $oAgenda->setTitre($aAgendaInfo->get("titre"));
                    $oAgenda->setLieu($aAgendaInfo->get("lieu"));
                    $oAgenda->setDate($dDate);
                    $oAgenda->setCommentaires($aAgendaInfo->get("commentaire"));
                    $oAgenda->setUser($oUserCurrent);
                }    

                $oEm->persist($oAgenda);
                $oEm->flush();
            }
        }

        $aAgenda = $oEm->getRepository('MCoreBundle:Agenda')->agendaByUser($oUserCurrent->getId());
        $aEvent = $oEm->getRepository('MCoreBundle:Events')->eventAgenda($oUserCurrent->getId());
        $bVerif = 0;


            foreach ($aEvent as $_key => $_value) 
            {   
                $aEvent[$_key]['type'] =  1;
                $aEvent[$_key]['lieu'] =  "";
                $aEvent[$_key]['commentaires'] =  "";

                foreach ($aAgenda as $key => $value) 
                {
                   $aAgenda[$key]['type'] =  0;


                    if($_value['date'] == $value['date'])
                    {   
                        $aAgenda[$key]['titre'] =  $_value['titre'];
                        $aAgenda[$key]['lieu'] =  "";
                        $aAgenda[$key]['commentaires'] =  "";
                        $aAgenda[$key]['heure'] =  "00";
                        $aAgenda[$key]['minute'] =  "01";
                        $aAgenda[$key]['type'] =  1;
                        $bVerif = 1;
             
                    }
                }

                if($bVerif == 0)
                    {
                        $aAgenda[] = $aEvent[$_key];
                    }

                $bVerif = 0;

            }



        return $this->render('MCoreBundle:Profile:agenda.html.twig', array(
                            'oForm' => $oForm->createView(),
                            'aAgenda' => $aAgenda
                            ));
    }
    
    public function statsAction()
    {  
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $aStat = $this->get("MenuNotification")->getAllNotification();
        $aStat['Events'] = count($oEm->getRepository('MCoreBundle:Eventsuser')->findByUser($oUserCurrent));
      
        
        return $this->render('MCoreBundle:Profile:stats.html.twig', array(
                            'aStat' => $aStat));
    }
    public function compteAction()
    {  
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oFormPassword = $this->createForm(new ChangePasswordType);
        $oFormMail = $this->createForm(new DefaultType);
        $oFormUnsubcribe = $this->createForm(new UnsubcribeType);
        $oRequest = $this->get('request');
        $oSession = $this->get('session');

        if ($oRequest->getMethod() == 'POST') 
        {

            if ($oRequest->request->get("m_corebundle_unsubcribetype")) 
            {
                $oFormUnsubcribe->bind($oRequest);
                if ($oFormUnsubcribe->isValid()) 
                {
                    $aUnsubcribe = $oRequest->request->get("m_corebundle_unsubcribetype");
                    $sPassword = $aUnsubcribe['password'];// Mot de passe saisie
                    $sPseudoMail = $aUnsubcribe['username'];// pseudo ou mail

                    $oEncodeFactory = $this->container->get('security.encoder_factory');

                    //J'encode le passowrd saisie
                    $oEncoder = $oEncodeFactory->getEncoder($oUserCurrent);
                    $sRequetPassword = $oEncoder->encodePassword($sPassword , $oUserCurrent->getSalt());

                    //Je teste si le password et le usrename sont correct
                    if ($sRequetPassword === $oUserCurrent->getPassword() && ($oUserCurrent->getUsername() == $sPseudoMail  || $oUserCurrent->getEmail() == $sPseudoMail))
                    {  
                        $aDesinscription = array();
                        $aDesinscription['raison'] = $aUnsubcribe['raison'];
                        $aDesinscription['commentaire'] = $aUnsubcribe['commentaire'];

                        $oUserCurrent->setDesinscription($aDesinscription);
                        $oUserCurrent->setActif(0);
                        $oUserCurrent->setRoles(array('ROLE_UNSUBSCRIB'));
                        $oEm->persist($oUserCurrent);
                        $oEm->flush();

                        /*   Servie d'envoi de mail   */
                        $this->get('Mail')->maildesinscription($oUserCurrent->getUsername(),$oUserCurrent->getEmail());
                        $oSession->getFlashBag()->add('success', "Votre compte a été désactivé");
                        return $this->redirect($this->generateUrl('m_core_logout'));
                    }
                    else
                    {
                        $oSession->getFlashBag()->add('error', "Combinaison Pseudo / Mot de passe : La combinaison est incorrecte, veuillez réessayer.");
                    }
                }    
            }

            if ($oRequest->request->get("changermdp")) 
            {
                $oFormPassword->bind($oRequest);
                $oEncodeFactory = $this->container->get('security.encoder_factory');
                $oEncoder = $oEncodeFactory->getEncoder($oUserCurrent);
                $sRequetPassword = $oEncoder->encodePassword($oFormPassword->getData()->getOldPassword() ,$oUserCurrent->getSalt());

                if ($oFormPassword->isValid()) 
                {
                    $aPassword = $oRequest->request->get("m_corebundle_usertype");
                    $sPassword = $aPassword['oldpassword'];// Mot de passe saisie
                    $oEncodeFactory = $this->container->get('security.encoder_factory');

                    //J'encode le passowrd saisie
                    $oEncoder = $oEncodeFactory->getEncoder($oUserCurrent);
                    $sRequetPassword = $oEncoder->encodePassword($sPassword , $oUserCurrent->getSalt());

                    //Je teste si le password encodé est le même que celui que l'actuelle
                    if ($sRequetPassword === $oUserCurrent->getPassword())
                    {   
                        //Je flush le nouveau mdp
                        $oUserCurrent->setPassword($oEncoder->encodePassword($aPassword['password']['first'], $oUserCurrent->getSalt()));
                        $oEm->persist($oUserCurrent);
                        $oEm->flush();
                        $oSession->getFlashBag()->add('success', "Votre mot de passe a été changer avec succés.");
                    }
                    else
                    {
                        $oSession->getFlashBag()->add('error', "Mot de passe actuel : Mot de passe incorrect.");
                    }
                }
                else
                {
                    $aErrors = $this->get('FonctionDiver')->getErrorsAsArray($oFormPassword->getErrorsAsString());
                    foreach ($aErrors as $iKey => $sError) 
                    {
                        $oSession->getFlashBag()->add('error', $sError);
                    }
                }  
            }

            if ($oRequest->request->get("enregisteralerte")) 
            {

                 $aAlerte = $oRequest->request->get("m_corebundle_usertype");
                 unset($aAlerte['_token']);
                 $oUserCurrent->setAlertemail($aAlerte);
                 $oEm->persist($oUserCurrent);
                 $oEm->flush();
                    
                 $oSession->getFlashBag()->add('success', "Vos modifications ont été bien enregistrées.");
            }
       
        }
        return $this->render('MCoreBundle:Profile:compte.html.twig', array('oFormUnsubcribe' => $oFormUnsubcribe->createView(),'oFormPassword' => $oFormPassword->createView(),'oFormMail' => $oFormMail->createView()));
    }
    public function abonnementAction()
    {  
        $oAbonnement = new Abonnement();
        $oForm = $this->createForm(new AbonnementType, $oAbonnement);

        $oSession = $this->get('session');
        $oSession->remove('TypeAbonnement');


        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        { 
            $oForm->bind($oRequest);

            if ($oForm->isValid()) 
            {
                $oSession->set('TypeAbonnement', (int) $oAbonnement->getTypeAbonnement());

                return $this->redirect($this->generateUrl('m_core_profile_abonnement_coordonnes'));
            }
        }


        if ( $this->get('security.context')->isGranted('ROLE_USER') )
        {
            $oEm = $this->getDoctrine()->getManager();
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();
            $oAbonnement = $oEm->getRepository('MCoreBundle:Abonnement')->findOneBy(
                                                            array('user' => $oUserCurrent, 'termine' => false),
                                                            array('dateExpiration' => 'DESC')
                                                            );
            // $oAbonnement = ( $oAbonnement ) ? $oAbonnement : new Abonnement();
        }

        return $this->render('MCoreBundle:Profile:abonnement.html.twig', array(
                'oForm'       => $oForm->createView(),
                'oAbonnement' => $oAbonnement
            ));
    }

    public function abonnementCoordonneesAction()
    {  

        $oSession = $this->get('session');
            $iTypeAbonnement = $oSession->get('TypeAbonnement');

        if (!is_int($iTypeAbonnement)) return $this->redirect($this->generateUrl('m_core_profile_abonnement'));


        $oEm = $this->getDoctrine()->getManager();
        $oUserRepository = $oEm->getRepository('MCoreBundle:User');
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
    
        $oSession = $this->get('session');

        $oAbonnement = new Abonnement();

        $oFormAbonnement = $this->createForm(new AbonnementCoordonneesType, $oAbonnement);
        // $oFormUser = $this->createForm(new AbonnementCoordonneesUserType, $oUserCurrent);

        $oSession = $this->get('session');

        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        { 
            $oFormAbonnement->bind($oRequest);
            // $oFormUser->bind($oRequest);

            if ($oFormAbonnement->isValid()) 
            {
                $oAbonnement->setTypeAbonnement($iTypeAbonnement);

                $aHighestNumeroCommande = $oEm->getRepository('MCoreBundle:Abonnement')->getLastAbonnement();
                $iHighestNumeroCommande = (count($aHighestNumeroCommande) > 0) ? 
                                                'REV' . ((int) substr($aHighestNumeroCommande[0]['numeroCommande'], 3) + 1) 
                                                : 
                                                'REV1';
                $iLastId = $aHighestNumeroCommande[0]['id']+1;
                $oAbonnement->setNumeroCommande('REV'.$iLastId);

                // return new Response($oAbonnement->getNumeroCommande());
                $aForm = $oRequest->request->get('m_corebundle_abonnementcoordonneestype');
                
                switch($aForm['offreAbonnement'])
                {
                    case 0 : $oAbonnement->setTotalPaye('220');
                             break;
                    case 1 : $oAbonnement->setTotalPaye('480');
                             break;
                    case 2 : $oAbonnement->setTotalPaye('720');
                             break;
                    case 3 : $oAbonnement->setTotalPaye('1080');
                             break;
                }
                // On appliquer le code Promo s'il est renseigné
                $sCodePromo = $oAbonnement->getCodePromotionnel();
                if(isset($sCodePromo))
                {
                $aReduction= $oEm->getRepository('MCoreBundle:Admin')->getReductionForCodePromo($sCodePromo);
                $sReduction = (!empty($aReduction)) ? $aReduction['0']['reduction'] : 0;
                }
                else
                {
                    $sReduction = 0 ;
                }
                $dTotalapaye = ($sReduction > 0) ? $oAbonnement->setTotalPaye($oAbonnement->getTotalpaye() - (($oAbonnement->getTotalpaye()*$sReduction)/100))
                                                 :
                                                 $oAbonnement->getTotalpaye();
                /**
                * Traitement du paiement Payzone et la redirection à la plateforme du paiement
                */
                $oCurrentUser=$this->get('security.context')->getToken()->getUser();
                $sRouteCallBack=$this->generateUrl('payzone_verifypay',array(),true);
                $sRouteRedirectAfterPaiement=$this->generateUrl('payzone_redirect',array(),true);
                $c2pClient = $this->get('payzone_service');
                $c2pClient->setOrderID($oAbonnement->getNumeroCommande());
                $c2pClient->setCustomerIP(@$_SERVER['REMOTE_ADDR']);
                $c2pClient->setPaymentType((isset($paymentType)) ? $paymentType : Connect2PayClient::_PAYMENT_TYPE_CREDITCARD);
                $c2pClient->setPaymentMode(Connect2PayClient::_PAYMENT_MODE_SINGLE);
                $c2pClient->setShopperID($oCurrentUser->getId());
                $c2pClient->setShippingType(Connect2PayClient::_SHIPPING_TYPE_VIRTUAL);
                $iAmountOrder=$oAbonnement->getTotalpaye()*100;
                $c2pClient->setAmount($iAmountOrder);
                $c2pClient->setOrderDescription("Abonnement chez rencontres-et-vous.ma");
                //Currency must be in MAD iso code of 'Morrocan Dirham'
                $c2pClient->setCurrency('MAD');
                $c2pClient->setShopperFirstName("NA");
                $c2pClient->setShopperLastName("NA");
                $c2pClient->setShopperAddress("NA");
                $c2pClient->setShopperZipcode("NA");
                $c2pClient->setShopperCity("NA");
                $c2pClient->setShopperCountryCode("MA");
                $c2pClient->setShopperPhone("NA");
                $c2pClient->setShopperEmail("NA@email.com");
                $c2pClient->setCtrlCustomData("Give that back to me please !!");
                $c2pClient->setCtrlRedirectURL($sRouteRedirectAfterPaiement);
                // callbackURL URL for the callback - please refer to the documentation for more explanation
                $c2pClient->setCtrlCallbackURL($sRouteCallBack);
                $c2pClient->setSecure3d(isset($secure3d) ? $secure3d : false);
                //if all fields are validated
                  
                 // Ici on affecte l'abonnement à l'utilisateur courant celui qui a fait la commande 
                $oAbonnement->setUser($oCurrentUser);
                
                $oAbonnement->setTermine(false);
                
                $oSession->set('iCommandeID', (int) $oAbonnement->getId());
                // en verifie si le moyen de paiement est par carte, et on met le statut de commande en : en attente
                if($oAbonnement->getMoyenPaiement() == '0')
                {
                     $oStatutCommande = $oEm->getRepository('MCoreBundle:StatutCommande')->find(1);
                    $oAbonnement->setStatutCommande($oStatutCommande);
                if ($c2pClient->validate()) {
                    //then prepare trasaction
                  if ($c2pClient->prepareTransaction()) {
                   $oAbonnement->setCustomerToken($c2pClient->getMerchantToken());
                   $oAbonnement->setCodeReponse($c2pClient->getReturnCode());
                   $oAbonnement->setMessageReponse($c2pClient->getReturnMessage());
                   $oSession->set('merchantToken',$c2pClient->getMerchantToken());
                    $oEm->persist($oAbonnement);
                    $oEm->persist($oUserCurrent);

                    $oEm->flush();
                
                    return  $this->redirect($c2pClient->getCustomerRedirectURL());
                  } else {
                    echo "Result code:" . $c2pClient->getReturnCode() . "<br>";
                    echo "Preparation error occured: " . $c2pClient->getClientErrorMessage() . "<br>";
                  }
                } else {
                  echo "Validation error occured: " . $c2pClient->getClientErrorMessage() . "<br>";
                }
                } 

                return $this->redirect($this->generateUrl('m_core_profile_abonnement_recap'));
            }
        }

        return $this->render('MCoreBundle:Profile:abonnementCoordonnees.html.twig', array(
                'oFormAbonnement'   => $oFormAbonnement->createView(),
                // 'oFormUser'         => $oFormUser->createView()
            ));
    }

    public function abonnementRecapAction()
    {  

        $oSession = $this->get('session');
            $iCommandeID = $oSession->get('iCommandeID');

        if (!is_int($iCommandeID)) return $this->redirect($this->generateUrl('m_core_profile_abonnement_coordonnes'));

        $oEm = $this->getDoctrine()->getManager();
        $oAbonnement = $oEm->getRepository('MCoreBundle:Abonnement')->find((int)$iCommandeID);

        if (!is_object($oAbonnement)) return $this->redirect($this->generateUrl('m_core_profile_abonnement_coordonnes'));

        $oSession = $this->get('session');
        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        { 
            echo '<pre>', print_r('Module de paiement ok, en attente du module MTC.', true), '</pre>';
            die();
        }

        return $this->render('MCoreBundle:Profile:abonnementRecap.html.twig', array(
                'oAbonnement'   => $oAbonnement
            ));
    }

    public function bloquesAction($iPage)
    {   
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
       
        $oParam['aId'] = '';
        if ($oUserCurrent->getContactbloque())
        {   
            foreach ($oUserCurrent->getContactbloque() as $key => $value) 
            {
                $oParam['aId'][] = $key;
            }


        }    

        
        $iTotalResult = $oEm->getRepository('MCoreBundle:User')->GetCountUsersById($oParam,"",0,0);
        // On utilise le service pour connaitre le premier résultat a affiché
        $iNbrResultPerPage = 3; // Nombre de resultat par affichage

        $oPaginator = $this->get("Paginator")->getPagination($iTotalResult,$iNbrResultPerPage, (int)$iPage);
        //On rajoute le paramétre avec la date du jour
        $oParam['today'] = date('Y-m-d'); 
        $_aResult = $oEm->getRepository('MCoreBundle:User')->GetUsersById($oParam,"",$iNbrResultPerPage,$oPaginator['FirstResultPerCurrentPage']);
 
        //On ajoute les signes astrologique et on convertie les date en lettre a partir du service
        $aResult = $this->get('FonctionDiver')->setSigneToResult($_aResult);
        //On verifie les users en ligne
        $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($aResult);

        return $this->render('MCoreBundle:Profile:bloques.html.twig', array(

                            'iPage'     => $iPage,
                            'aResult'    => $aResult,
                            'iTotalResult'=> $iTotalResult,
                            'iTotalPage'=> $oPaginator['iTotalPages']));
    }

    public function parrainageAction()
    {
        
        $sparrainage = $this->get('parrainage');
        $oSession = $this->get('session');
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $sEmailencrypted = urlencode($sparrainage->encryptEmail($oUserCurrent->getEmail()));

        $request = $this->get('request');

        if($request->getMethod() == 'POST')
        {
            $sEmails = $request->request->get('emails');
            $aEmails = split(',',$sEmails);
            $iCountarrayemails = count($aEmails);
            for($i=0 ; $i < $iCountarrayemails ; $i++)
            {
               /* if($sparrainage->isEmail($aEmails[$i]) && $sparrainage->isAlreadyMembre($aEmails[$i]) || !$aEmails[$i]='')
                {
                    echo '--';
                }
                else
                {*/
             $this->get('Mail')->mailParrainage($aEmails[$i],$oUserCurrent->getUsername(),$sEmailencrypted); 
                // return new  Response(var_dump($aEmails));
                /*    unset($aEmails[$i]);
                }*/
            } 
            
           $oSession->getFlashBag()->add('success', 'Les mails sont envoyés .');
          return $this->redirect($this->generateUrl('m_core_profile_parrainage'));
        }
      return $this->render('MCoreBundle:Profile:parrainage.html.twig',
        array('sEmailencrypted'=>$sEmailencrypted));
    }

    public function calculatepromoAction()
    {
        $request = $this->get('request');
        if($request->isXmlHttpRequest())
        {
            $codepromo = $request->request->get('codepromo');
            $prix = $request->request->get('prix');
            $oEm = $this->getDoctrine()->getManager();
            $aReduction= $oEm->getRepository('MCoreBundle:Admin')->getReductionForCodePromo($codepromo);
            $sReduction = (!empty($aReduction)) ? $aReduction['0']['reduction'] : 0;
            $dTotalapaye = ($sReduction > 0) ? $prix - (($prix*$sReduction)/100)
                                                 : $prix;
            $dTotalapaye = number_format ( floatval($dTotalapaye) , 2 );
            $aResult = array('sReduction'=>$sReduction,'dTotalapaye'=>$dTotalapaye) ; 
            return new Response( json_encode($aResult));
        }
        
    }
}