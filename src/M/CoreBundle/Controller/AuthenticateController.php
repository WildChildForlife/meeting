<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Security\Core\SecurityContext;

use M\CoreBundle\Entity\User;

class AuthenticateController extends Controller
{
    public function loginAction()
    {
        // Si le visiteur est déjà identifié, on le redirige vers l'accueil
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) 
        {
            return $this->redirect($this->generateUrl('m_core_logout'));
        }

        $oUser = new User();

        $oRequest = $this->get('request');

        $oSession = $oRequest->getSession();

        // On vérifie s'il y a des erreurs d'une précédent soumission du formulaire
        if ($oRequest->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) 
        {
            $sError = $oRequest->attributes->get(SecurityContext::AUTHENTICATION_ERROR);

        } 
        else
        {
            $sError = $oSession->get(SecurityContext::AUTHENTICATION_ERROR);
            $oSession->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        echo '<pre>', print_r($sError, true), '</pre>';
        die();

        // le message d'erreur $sError je le prend pas en charge sur TWIG template , par ce qu'il peut contenir une requete sql comme erreur !
        return $this->render('MCoreBundle:Login:Login.html.twig', array(
            // Valeur du précédent nom d'utilisateur rentré par l'internaute
            'derniere_username' => $oSession->get(SecurityContext::LAST_USERNAME),
            'sError' => $sError,
            'form' => $oForm->createView(),
        ));
    }
}
