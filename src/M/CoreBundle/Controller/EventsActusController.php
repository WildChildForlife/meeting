<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use M\CoreBundle\Entity\Events;
use M\CoreBundle\Entity\Actus;
use M\CoreBundle\Entity\Publication;
use M\CoreBundle\Entity\Commentaire;
use M\CoreBundle\Entity\Eventsuser;
use M\CoreBundle\Form\EventsType;
use M\CoreBundle\Form\FiltreEventType;
use M\CoreBundle\Form\PublicationType;
use Symfony\Component\HttpFoundation\Response;

class EventsActusController extends Controller
{
   
    public function indexAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $aActus  = $oEm->getRepository('MCoreBundle:Actus')->allActus();
        $oForm = $this->createForm(new FiltreEventType);
        $oRequest = $this->get('request');
        $aParms = array();

        if($oRequest->getMethod() == 'POST')
        {        
            $aParms = $oRequest->get('Events');
        }

        $aEvents = $oEm->getRepository('MCoreBundle:Events')->allEvents($aParms);

        //Nombre de personne inscrite
        $aInscrit = $oEm->getRepository('MCoreBundle:Eventsuser')->countEventsBysexe($oUserCurrent->getSexe());
        
        if(!is_array($aInscrit)) $aInscrit = array();

        //ajoute le status Ouvert ou ferme

        $aEvents = $this->get('FonctionDiver')->setEventStatus($aEvents,$aInscrit,$oUserCurrent->getSexe());

        $aEvents= $this->get('FonctionDiver')->encodeUrlsFromArray($aEvents);
        $aActus  = $this->get('FonctionDiver')->encodeUrlsFromArray($aActus);

        return $this->render('MCoreBundle:EventsActus:eventsactus.html.twig',
                                         array(
                                                'aEvents'     => $aEvents,
                                                'aActus'     => $aActus, 
                                                'oFormFiltre'     => $oForm->createView()
                                                ));
    }
    public function actualiteAction($sTitre)
    {   
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $sTitre = str_replace("-", " ", $sTitre);
        $sTitre = urldecode($sTitre);

        $sRootProfile = $this->redirect($this->generateUrl('m_core_profile'));

        $oActu = $oEm->getRepository('MCoreBundle:Actus')->findOneByTitre($sTitre);

        if (!( $oActu instanceof Actus)) return $sRootProfile;
        if (!( $oActu instanceof Actus)) return $sRootProfile;

        return $this->render('MCoreBundle:EventsActus:actualite.html.twig',
                                         array(
                                                'oActu'     => $oActu
                                                ));
    }

    public function evenementAction($sTitre)
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $sRootProfile = $this->redirect($this->generateUrl('m_core_profile')); 
        $sTitre = str_replace("-", " ", $sTitre);
        $sTitre = urldecode($sTitre);
        $oSession = $this->get('session');

        $oEvents = $oEm->getRepository('MCoreBundle:Events')->findOneByTitre($sTitre);

        if (!($oEvents instanceof Events) || $this->get('security.context')->isGranted('ROLE_FREE') ) return $sRootProfile;

        $dDate = $oEvents->getDateEvent();
        $oEvents->setDateEvent($this->get('FonctionDiver')->formatDate($dDate->format('Y-m-d'))." à ".$dDate->format('h')."h".$dDate->format('i'));

        $oRequest = $this->get('request');
 
        if($oRequest->getMethod() == 'POST')
        {

            if($oRequest->get('inscrire'))
            {  
                
                $aVerification = $oEm->getRepository('MCoreBundle:Eventsuser')->VerifyEventsuser($oEvents->getId(),$oUserCurrent->getId());

                if (!$aVerification)
                { 
                    $oEventUser = new Eventsuser();
                    $oEventUser ->setUser($oUserCurrent);
                    $oEventUser ->setEvent($oEvents);                
                    $oEm->persist($oEventUser);
                    $oEm->flush($oEventUser);
                    $oSession->getFlashBag()->add('success', 'Vous êtes désormais inscrit à la sortie.');
                }
            }

            if($oRequest->get('desinscrire'))
            {  
                $oEm->getRepository('MCoreBundle:Eventsuser')->DeleteUserEvent($oEvents->getId(),$oUserCurrent->getId());
                $oSession->getFlashBag()->add('success', 'Vous êtes désormais désinscrit de la sortie.');
            }

          

        }

       

        $aInscrit = $oEm->getRepository('MCoreBundle:Eventsuser')->EventByID($oEvents->getId());
        $aInscrit = $this->get('FonctionDiver')->setEnligneUserToResult($aInscrit);
        $iTotalInscrit = count($aInscrit);
        $aPlaces = $this->get('FonctionDiver')->setEventPlace($aInscrit,$oEvents->getPlaces());
        $sImage = $oEm->getRepository('MCoreBundle:Images')->getProfilDefaultByUser($oEvents->getUser()->getId());
        //On Verifie si l'utilisateur organisateur est en ligne
        $dDelay = new \DateTime();
        $dDelay->setTimestamp(strtotime('10 minutes ago'));
        $oEvents->getUser()->setEnLigne(($oEvents->getUser()->getLastactivity() > $dDelay ) ? 1 : 0 );

                //On Rajoute tout les contacts sauf ceux bloquer par l'utilisateur
        if (count($oUserCurrent->getContactbloque()) !== 0)
        {   

            foreach ($aInscrit as $key => $value) 
            {
               if (array_key_exists($aInscrit[$key]['id'],$oUserCurrent->getContactbloque())) unset($aInscrit[$key]);
            }

        }

        $publications = $oEm->getRepository('MCoreBundle:Publication')->findByEvent($oEvents);

        $oPublication = new Publication();
        $oForm = $this->createForm(new PublicationType, $oPublication);

        // on verifie si lutilisateur courant est inscrit
        $oEventuserCurrent = $oEm->getRepository('MCoreBundle:Eventsuser')
                                 ->findOneBy(array('user'=>$oUserCurrent,'event'=>$oEvents));
        $bIsMembre = ($oEventuserCurrent instanceof Eventsuser) ? true : false ;

        return $this->render('MCoreBundle:EventsActus:evenement.html.twig',
                                         array(
                                                'oEvents'       => $oEvents,
                                                'aInscrit'      => $aInscrit,
                                                'iTotalInscrit' => $iTotalInscrit,
                                                'aPlaces' => $aPlaces,
                                                'sImage' => $sImage,
                                                'bIsMembre'=> $bIsMembre,
                                                'oFormPublication'=>$oForm->createView(),
                                                'publications' => $publications,

                                                ));
    }

    public function evenementCreationAction()
    {
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oEm = $this->getDoctrine()->getManager();
        $oSession = $this->get('session');
        $oRequest = $this->get('request');
        $oEvent = new Events();
        $oForm = $this->createForm(new EventsType, $oEvent);
        if($oRequest->getMethod() == 'POST')
        {
            $oForm->bind($oRequest);
            if ( $oForm->isValid() )        
            {
                $oEvent->setImage($oRequest->get('ADD_upload0'));
                $oEvent->setUser($oUserCurrent);
                $aDate = split('/', $oEvent->getDateEvent());
                $iHeure= $oEvent->getHeure();
                $sDateconvet = $oEvent->getDateEvent() . ' ' . $iHeure .':00:00';
                $dDateconverted = \DateTime::createFromFormat('d/m/Y H:i:s',$sDateconvet);
                $oEvent->setDateEvent($dDateconverted);
                $oEm->persist($oEvent);
                $oEm->flush($oEvent);
                $oSession->getFlashBag()->add('success', 'Votre sortie a été créée avec succés, un modérateur procédera à la vérification de votre sortie avant sa publication.');
                return $this->redirect($this->generateUrl('m_core_profile_eventsactus'));
            }
            else
            {
                $aErrors = $this->get('FonctionDiver')->getErrorsAsArray($oForm->getErrorsAsString());
                foreach ($aErrors as $iKey => $sError) 
                {
                    $oSession->getFlashBag()->add('error', $sError);
                }
            }
        }
        return $this->render('MCoreBundle:EventsActus:evenement_creation.html.twig', array( 'oFormEvent' => $oForm->createView()));
                                         
                                                
    }

    public function addcommentAction()
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        if($request->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();
            $txtcomment  = $request->request->get('txtcomment');
            $publicationtarget = $request->request->get('publication_target');
            $publication = $em->getRepository('MCoreBundle:Publication')->find($publicationtarget);

            $commentaire = new Commentaire();

            $commentaire->setText($txtcomment);
            $commentaire->setUser($oUserCurrent);
            $commentaire->setPublication($publication);
            
            $em->persist($commentaire);

            $em->flush();
            $images = $commentaire->getUser()->getImages();
            foreach ($images as $image)
            {
                if( $image->getProfileDefault()==1)
                {
                    $imageuser = $image->getImage();
                }
            }
            $nowtimestamp = time();
            $age = ($nowtimestamp - $commentaire->getUser()->getDatenaissance() )/ (365 * 24 * 60 * 60);
            $ville = $commentaire->getUser()->getVille()->getNom();
            $sexe = ($commentaire->getUser()->getSexe() == 1) ? 'femme' : 'homme' ;
            $datecommentaire =strtotime($commentaire->getDateadd()->format('Y-m-d H:i:s'));
            $aCommentaire = array('txtcommentaire'=>$commentaire->getText(),
                                  'datecommentaire'=>$this->activityFilter($datecommentaire),
                                  'username' => $commentaire->getUser()->getUsername(),
                                  'imageuser' => $imageuser,
                                  'sexe' => $sexe,
                                  'age'=>floor($age),
                                  'ville' => $ville,
                                  );
            return new Response(json_encode($aCommentaire));



        }

          if($request->get('deletecomment'))
            {
                $oSession = $this->get('session');
                $commentaire_target = $request->request->get('comment_target');
                $commentaireTarget = $em->getRepository('MCoreBundle:Commentaire')->findOneById(intval($commentaire_target));
                if($commentaireTarget instanceof Commentaire)
                {
                   
                  $em->remove($commentaireTarget);
                $em->flush();
                 $oSession->getFlashBag()->add('success', 'Votre commentaire est bien supprimé');
                 return $this->redirect($this->generateUrl('m_core_profile_evenement',array('sTitre'=>$commentaireTarget->getPublication()->getEvent()->getTitre())));
                }
                
            }
        
    }

    public function addpublicationAction()
    {
         $oUserCurrent = $this->get('security.context')->getToken()->getUser();
         $em= $this->getDoctrine()->getManager();
         $oSession = $this->get('session');
        
        $request= $this->get('request');
        if($request->isXmlHttpRequest())
        {
        $txtpub = $request->request->get('txtpublication');
        $event_target = $request->request->get('event_target');
        if(is_int(intval($event_target))){ $event = $em->getRepository('MCoreBundle:Events')->find(intval($event_target)); }
        
        $publication = new Publication();
        $publication->setText($txtpub);
        $publication->setUser($oUserCurrent);
        $publication->setEvent($event);
        $em->persist($publication);
        $em->flush();

        $nowtimestamp = time();
        $age = ($nowtimestamp - $publication->getUser()->getDatenaissance() )/ (365 * 24 * 60 * 60);
        $images = $publication->getUser()->getImages();
        $datapub =$publication->getDateadd()->format('Y-m-d H:i:s');
        $datepublication = strtotime($datapub);
            foreach ($images as $image)
            {
                if( $image->getProfileDefault()==1)
                {
                    $imageuser = $image->getImage();
                }
            }
            $ville = $publication->getUser()->getVille()->getNom();
        $aPublication = array('txtpublication'=>$publication->getText(),
                              'datepublication'=>$this->activityFilter($datepublication),
                              'username'=>$oUserCurrent->getUsername(),
                              'age'=>floor($age),
                              'imageuser' => $imageuser,
                              'ville'=>$ville,
                                );
        return new Response(json_encode($aPublication));
        }

         if($request->get('deletepost'))
            {
                $post_target = $request->request->get('post_target');
                $postTarget = $em->getRepository('MCoreBundle:Publication')->find($post_target);
                $em->remove($postTarget);
                $em->flush();
                $oSession->getFlashBag()->add('success', 'Votre publication est bien supprimé');
                return $this->redirect($this->generateUrl('m_core_profile_evenement',array('sTitre'=>$postTarget->getEvent()->getTitre())));
            }
    }

    public function getcommentaireAction()
    {
       $request = $this->get('request');
       $em = $this->getDoctrine()->getManager();
       $oUserCurrent = $this->get('security.context')->getToken()->getUser()->getId();
       if($request->isXmlHttpRequest())
       {
        $publication = $request->request->get('publication');
        $commentaires = $em->getRepository('MCoreBundle:Commentaire')->findByPublication($publication);
        $html='';
        foreach($commentaires as $commentaire)
        {
            $images = $commentaire->getUser()->getImages();
            foreach ($images as $image)
            {
                if( $image->getProfileDefault()==1)
                {
                    $imageuser = $image->getImage();
                }
            }
            $nowtimestamp = time();
            $sexe = ($commentaire->getUser()->getSexe() == 0 ) ? 'homme' : 'femme'; 
            $age = ($nowtimestamp - $commentaire->getUser()->getDatenaissance() )/ (365 * 24 * 60 * 60);
            $ville = $commentaire->getUser()->getVille()->getNom();
            $datecommentaire =strtotime($commentaire->getDateadd()->format('Y-m-d H:i:s'));
            $path =  $this->generateUrl('m_core_profile_voir', array('sUser' => $commentaire->getUser()->getUsername() ));
           $html.='<li>
         <span><img src="' .$imageuser.'">     </span> <span class="text-commentaire"> 
     <a href="' .$path .'">
        <strong class="'.$sexe.'"> ' . $commentaire->getUser()->getUsername(). ' </strong>
     </a>
      <span class="detail-user-commentaire"> - ' .floor($age).' ans - '.$ville.' </span><br>' . $commentaire->getText().' <br>
         ';
          if($oUserCurrent == $commentaire->getUser()->getId() OR $oUserCurrent == $commentaire->getPublication()->getEvent()->getUser()->getId())
          {
               $html.=   '    <form method="POST">

        <input type="hidden" name="comment_target" value="'.$commentaire->getId().'">
      <input type="submit" name="deletecomment" id="delete-comment" value="Supprimer ce commentaire">
       </form> ';
          }
       

               $html.= '  <span class="date-commentaire"> '.$this->activityFilter($datecommentaire).' </span></span>
     </li>';
 
        }
        $result = array('publication' => $publication, 'commentaires' => $html);
     return new Response (json_encode($result));
       }
    }



        private function activityFilter($timestamp)
        {
            $activity = '';
        $seconds = time() - $timestamp; 
        $minutes = (int)($seconds / 60);
        $hours = (int)($minutes / 60);
        $days = (int)($hours / 24);
        if ($days >= 1) {
          $activity = $days . ' jour' . ($days != 1 ? 's' : '');
        } else if ($hours >= 1) {
          $activity = $hours . ' heure' . ($hours != 1 ? 's' : '');
        } else if ($minutes >= 1) {
          $activity = $minutes . ' minute' . ($minutes != 1 ? 's' : '');
        } else {
          $activity = $seconds . ' second' . ($seconds != 1 ? 's' : '');
          return 'il y a quelques seconds';
        }
        return 'il y a '.$activity;
        }
}