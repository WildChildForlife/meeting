<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use M\CoreBundle\Entity\User;
use M\CoreBundle\Entity\Images;
use M\CoreBundle\Form\FastRegistrationPreType;
use M\CoreBundle\Form\FastRegistrationStep1Type;
use M\CoreBundle\Form\FastRegistrationStep2Type;
use M\CoreBundle\Form\FastRegistrationStep3Type;

class RegistrationController extends Controller
{
    public function registerAction()
    {
        $oSession = $this->get('session');
        //$oSession->clear();
       
        $oUser = new User();
        $oForm = $this->createForm(new FastRegistrationPreType, $oUser);

        $oRequest = $this->get('request');

        $oEm = $this->getDoctrine()->getManager();
        // Check for incomplete user
        if ( $oSession->has('Steps') && $oSession->has('UserID') && $oSession->get('UserID') > 0 )
        {
            $oUser = $oEm->getRepository('MCoreBundle:User')->find((int)$oSession->get('UserID'));
            if ( $oUser && $oUser->getEtatInscription() !== 4 )
            {
                $oEm->remove($oUser);
                $oEm->flush();
            }
        }

		if ($oRequest->getMethod() == 'POST') 
		{ 
			$oForm->bind($oRequest);
			if ($oForm->isValid()) 
			{
                $oUser->setEtatInscription(1);
                $oEm->persist($oUser);
                $oEm->flush();

                $oSession->set('UserID', (int) $oUser->getId());

                $sSexe = ($oUser->getSexe()) ? 'Femme' : 'Homme';
                $oSession->set('UserRegistration_Sexe', $sSexe);

                $oSession->set('Steps', array('1'));

                return $this->redirect($this->generateUrl('m_core_registration_step1'));
			}
            else
            {
                $sEmail = $oUser->getEmail();
                $oCurrentUser = $oEm->getRepository('MCoreBundle:User')->findOneByEmail($sEmail);
                $iEtat = $oCurrentUser->getEtatInscription();

                if($iEtat== 1)
                {
                $oSession->set('UserID', (int) $oCurrentUser->getId());
                $sSexe = ($oCurrentUser->getSexe()) ? 'Femme' : 'Homme';
                $oSession->set('UserRegistration_Sexe', $sSexe);
                $oSession->set('Steps', array('1'));
                 return $this->redirect($this->generateUrl('m_core_registration_step1'));
                }

                if($iEtat == 2)
                {
                 $oSession->set('UserID', (int) $oCurrentUser->getId());
                $sSexe = ($oCurrentUser->getSexe()) ? 'Femme' : 'Homme';
                $oSession->set('UserRegistration_Sexe', $sSexe);
                $oSession->set('Steps', array('2'));
                 return $this->redirect($this->generateUrl('m_core_registration_step2'));
                }
                $aErrors = $this->get('FonctionDiver')->getErrorsAsArray($oForm->getErrorsAsString());
                foreach ($aErrors as $iKey => $sError) 
                {
                    $oSession->getFlashBag()->add('error', $sError);
                }
            }
		}
        
        return $this->redirect($this->generateUrl('m_core_homepage'));
    }

    public function registerStep1Action()
    {   
        $oSession = $this->get('session');
            $iUserId = $oSession->get('UserID');
            $aSteps = $oSession->get('Steps');

        if (!is_int($iUserId) || !is_array($aSteps ) || $iUserId <= 0 ) return $this->redirect($this->generateUrl('m_core_homepage'));

        $oEm = $this->getDoctrine()->getManager();

        $oUser = $oEm->getRepository('MCoreBundle:User')->find($iUserId);

        if ( !is_object($oUser) || !count(array_intersect(array('1'), $aSteps)) ) return $this->redirect($this->generateUrl('m_core_homepage'));

        $oForm = $this->createForm(new FastRegistrationStep1Type(), $oUser);

        $oRequest = $this->get('request');
        if ($oRequest->getMethod() == 'POST') 
        {
            $oForm->bind($oRequest);
            if ($oForm->isValid()) 
            {
                $oEncodeFactory = $this->container->get('security.encoder_factory');

                $oEncoder = $oEncodeFactory->getEncoder($oUser);
                $oUser->setPassword($oEncoder->encodePassword($oUser->getPassword(), $oUser->getSalt()));

                $oUser->setEtatInscription(2);
                $oEm->persist($oUser);
                $oEm->flush();

                if ( !in_array('2', $aSteps) ) array_push($aSteps, '2');

                $oSession->set('Steps', $aSteps);
                return $this->redirect($this->generateUrl('m_core_registration_step2'));
            }
            else
            {
                $aErrors = $this->get('FonctionDiver')->getErrorsAsArray($oForm->getErrorsAsString());
                foreach ($aErrors as $iKey => $sError) 
                {
                    $oSession->getFlashBag()->add('error', $sError);
                }
            }
        } 

        return $this->render('MCoreBundle:Registration:Step1.html.twig', array(
            'oForm' => $oForm->createView()
        ));
    }

    public function registerStep2Action()
    {
        $oSession = $this->get('session');
            $iUserId = $oSession->get('UserID');
            $aSteps = $oSession->get('Steps');

        if (!is_int($iUserId) || !is_array($aSteps) || $iUserId <= 0) return $this->redirect($this->generateUrl('m_core_homepage'));

        $oEm = $this->getDoctrine()->getManager();
        
        $oUser = $oEm->getRepository('MCoreBundle:User')->find($iUserId);

        if ( !is_object($oUser) || !count(array_intersect(array('2', '3'), $aSteps)) ) return $this->redirect($this->generateUrl('m_core_registration_step2'));
                  
        $oForm = $this->createForm(new FastRegistrationStep2Type(strtolower($oSession->get('UserRegistration_Sexe'))), $oUser);

        $oRequest = $this->get('request');
        if ($oRequest->getMethod() == 'POST') 
        {
           $oForm->bind($oRequest);
            if ($oForm->isValid()) 
            {
                $oUser->setEtatInscription(3);
                $oEm->persist($oUser);
                $oEm->flush();

                if ( !in_array('3', $aSteps) ) array_push($aSteps, '3');

                $oSession->set('Steps', $aSteps);

                return $this->redirect($this->generateUrl('m_core_registration_step3'));
            }
            else
            {
                $aErrors = $this->get('FonctionDiver')->getErrorsAsArray($oForm->getErrorsAsString());
                foreach ($aErrors as $iKey => $sError) 
                {
                    $oSession->getFlashBag()->add('error', $sError);
                }
            }
        }

        return $this->render('MCoreBundle:Registration:Step2.html.twig', array(
            'oForm' => $oForm->createView()
        ));
    }

    public function registerStep3Action()
    {
        $oSession = $this->get('session');
            $iUserId = $oSession->get('UserID');
            $aSteps = $oSession->get('Steps');

        if ( !is_int($iUserId) || !is_array($aSteps) || $iUserId <= 0 ) return $this->redirect($this->generateUrl('m_core_homepage'));

        $oEm = $this->getDoctrine()->getManager();
        
        $oUser = $oEm->getRepository('MCoreBundle:User')->find($iUserId);

        if ( !is_object($oUser) || !count(array_intersect(array('3'), $aSteps)) ) return $this->redirect($this->generateUrl('m_core_registration_step2'));

        $oImages = new Images();

        $oForm = $this->createForm(new FastRegistrationStep3Type, $oImages);

        $oRequest = $this->get('request');
        if ($oRequest->getMethod() == 'POST') 
        {
            $oForm->bind($oRequest);

            if ($oForm->isValid()) 
            {
                $oEm = $this->getDoctrine()->getManager();
                // Le sel et les rôles sont vides pour l'instant
                $oUser->setRoles(array('ROLE_NON_MODERE'));

                $oImages->setProfile(1);
                $oImages->setProfileDefault(1);
                
                $oImages->setUser($oUser);
            
                $oUser->setEtatInscription(4);
                $cookies = $oRequest->cookies;
                 if ($cookies->has('sponsor'))
                 {
                    $sEmailcrypted = $cookies->get('sponsor');
                    $sparrainage = $this->get('parrainage');
                    $sEmail = $sparrainage->decryptEmail(urldecode($sEmailcrypted));
                    $oUserParrain = $oEm->getRepository('MCoreBundle:User')->findOneByEmail($sEmail);
                    if($oUserParrain instanceof User)
                    {
                    // recompense membre d'honneur 
                    $abonnement = $oEm->getRepository('MCoreBundle:Abonnement')->findOneByUser($oUserParrain);
                    $oDateaddrecompense = new \DateTime();
                    $dateExpiration = $abonnement->getDateExpiration();
                    $interval = $oDateaddrecompense->diff($dateExpiration);
                    $oDateaddrecompense->modify($interval->format('%R%a days'));
                    $oDateaddrecompense->modify('+15 days');
                    $oDateaddrecompense->modify('+1 day');
                    $abonnement->setDateExpiration($oDateaddrecompense);
                    $oEm->persist($abonnement);
                    //-------------------------------------------
                    $oUser->setParrain($oUserParrain); 
                    $response = new Response();
                    $response->headers->clearCookie('sponsor');
                    $response->send();
                    }
                  }
                $oEm->persist($oUser);
                $oEm->persist($oImages);
              
                // On déclenche l'enregistrement
                $oEm->flush();

                $oSession->remove('Steps');
               
                /*   Servie d'envoi de mail   */
                $this->get('Mail')->mailInscription($oUser->getUsername(),$oUser->getEmail());
                $oSession->getFlashBag()->add('successinscription', 'affichage popup');
                // $oSession->getFlashBag()->add('success', 'Enregistrement avec succés, l\'activation de votre compte est en cours de traitement, vous serez notifié dans les 48h qui suivent');
                return $this->redirect($this->generateUrl('m_core_homepage'));

            }
            else
            {
                $aErrors = $this->get('FonctionDiver')->getErrorsAsArray($oForm->getErrorsAsString());
                foreach ($aErrors as $iKey => $sError) 
                {
                    $oSession->getFlashBag()->add('error', $sError);
                }
            }
        }
        return $this->render('MCoreBundle:Registration:Step3.html.twig', array(
            'oForm' => $oForm->createView()
        ));

    }

}