<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use M\CoreBundle\Entity\User;
use M\CoreBundle\Form\FastRegistrationPreType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class HomeController extends Controller
{
    public function indexAction($sActivation = null,$sponsor = null)
    {
        // return $this->render('MCoreBundle::Intro.html.twig');
        /* POUR MASQUER LE SITE JUSQU'AU LANCEMENT */
        /*if ( !isset($_SESSION['cle']) || $_SESSION['cle'] === false )
        {
            echo '
                <html>
                    <head>
                    </head>
                    <body>
                        <form action="#" method="POST" >
                            <input name="cle" type="text" placeholder="Mot secret" />
                            <input name="envoyer" type="submit" value="Acceder au site" />
                        </form>
                    </body>
                </html>
                ';
            if ( !isset($_POST['envoyer']) || $_POST['cle'] !== "68pMAHZ1ns" )
            {
                die("Vous n'avez pas le droit d'acceder au site.");
            }
            else $_SESSION['cle'] = true;
        }*/
        /***************************************************************/


        $oEm = $this->getDoctrine()->getManager();
        $oSession = $this->get('session');
        if ( strlen($sActivation) > 10 && $sActivation != null )
        {
            $oRequest = $this->container->get('request');
            $sRouteName = $oRequest->get('_route');
            // Traitement de l'activation du compte
            if ( $sRouteName == 'm_core_homepage_activation' )
            {
                $oUser = $oEm->getRepository('MCoreBundle:User')->findOneBy(array('code_activation' => $sActivation));
                if ( $oUser )
                {
                    $oUser->setCodeActivation('1');
                    $oUser->setActif(1);
                    $oEm->persist($oUser);
                    $oEm->flush();

                    $oSession->getFlashBag()->add('success', 'Votre compte est activé, vous pourrez à présent vous y connecter en toute sécurité.');
                }
                else
                {
                    $oSession->getFlashBag()->add('error', "Code d'activation : Votre code est invalide.");
                }
            }
            // Traitement de la récupération du mot de passe
            else if ( $sRouteName == 'm_core_homepage_recuperation' )
            {
                $oUser = $oEm->getRepository('MCoreBundle:User')->findOneBy(array('code_recuperation' => $sActivation));
                if ( $oUser )
                {
                    $oSession->getFlashBag()->add('recuperation', $sActivation);
                }
                else
                {
                    $oSession->getFlashBag()->add('error', "Recuperation : Votre lien de récupération du mot de passe est invalide.");
                }
            }

            return $this->redirect($this->generateUrl('m_core_homepage'));

        }
        
        if(strlen($sponsor) > 10 && $sponsor!= null)
        {
          $cookie_info = array(
                'name'  => 'sponsor',
                'value' => $sponsor,
                'time'  => time() + 3600 * 24 * 4
            );
 
            // Cree le cookie
            $cookie = new Cookie($cookie_info['name'], $cookie_info['value'], $cookie_info['time']);
 
            // Envoie le cookie
            $response = new Response();
            $response->headers->setCookie($cookie);
            $response->send();
           // return $this->redirect($this->generateUrl('m_core_homepage'));
        }
        // Si le visiteur est déjà identifié, on le redirige vers l'accueil
        if ($this->get('security.context')->isGranted('ROLE_USER') || $this->get('security.context')->isGranted('ROLE_FREE') ) 

        {
            return $this->redirect($this->generateUrl('m_core_profile'));
        }
        
        $oUser = new User();
        $oForm = $this->createForm(new FastRegistrationPreType, $oUser);


        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;



		$oRequest = $this->get('request');
		if ($oRequest->getMethod() == 'POST') 
		{
			$oForm->bind($oRequest);

			if ($oForm->isValid())
			{
				$oEm = $this->get('doctrine')->getEntityManager();
				$oEncodeFactory = $this->container->get('security.encoder_factory');

				$encoder = $oEncodeFactory->getEncoder( $oUser );
				$oUser->setPassword($encoder->encodePassword($oUser->getPassword() , $oUser->getSalt()));

				// Le sel et les rôles sont vides pour l'instant
				$oUser->setRoles(array('ROLE_USER'));

				// On le persiste
				$oEm->persist($oUser);
				// On déclenche l'enregistrement
				$oEm->flush();

                return $this->render('MCoreBundle:Home:index.html.twig');
			}
		}

        $aTemoignage = $oEm->getRepository('MCoreBundle:Temoignages')->findAll();
        shuffle($aTemoignage);

        /*$aIdMessage = $this->get('GenerateUsers')->generate();//Service de genration d'utilisateur*/
        return $this->render('MCoreBundle:Home:index.html.twig', array(
            'oForm'             => $oForm->createView(),
            'NbrConnecte'       => $iMembre,
        	'bAffichage'        => $bAffichage,
            'aTemoignage'       => $aTemoignage
        ));
    }

    public function decouvrirAction()
    {   
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

    	return $this->render('MCoreBundle:Home:decouvrir.html.twig', array(
        	'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'bAffichage' => $bAffichage,
            'NbrConnecte' => $iMembre
        ));
    }
    public function temoignagesAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

        $aTemoignage = $oEm->getRepository('MCoreBundle:Temoignages')->findAll();

    	return $this->render('MCoreBundle:Home:temoignages.html.twig', array(
        	'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'temoignage' => $aTemoignage,
            'bAffichage' => $bAffichage
        ));
    }
    public function evenementsAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

    	return $this->render('MCoreBundle:Home:evenements.html.twig', array(
        	'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage

        ));
    }
    public function servicesAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

    	return $this->render('MCoreBundle:Home:services.html.twig', array(
        	'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage

        ));
    }
    public function presseAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;
        

        $aPresse = $oEm->getRepository('MCoreBundle:Presse')->findAll();

    	return $this->render('MCoreBundle:Home:presse.html.twig', array(
        	'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'presse' => $aPresse,
            'bAffichage' => $bAffichage

        ));
    }
}
