<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use M\CoreBundle\Entity\UserIdeal;
use M\CoreBundle\Entity\Questions;


use M\CoreBundle\Form\DefaultType;
use M\CoreBundle\Form\ProfilIdealType;
use M\CoreBundle\Form\AProposType;

class ProfileController extends Controller
{
    public function detailsAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oSession = $this->get("session");
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();

        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        {
            if ($oRequest->request->get('send_pictures')) 
            {
                $this->get('TraitementPhotos')->TraitementPhotos($oRequest->request);
            }
            else if ($oRequest->request->get('profil_default_submit') && $oRequest->request->get('profil_default')) 
            {
                $this->get('TraitementPhotos')->ProfilDefaultSelect($oRequest->request);
            }
            $oSession->getFlashBag()->add('success', "Vos modifications ont bien été enregistrées");
        }

        $aImageProfil = $oEm->getRepository('MCoreBundle:Images')->getProfilByUser($oUserCurrent->getId());
        $oSession->set('ImagesProfil', $aImageProfil); // Pour la suppression d'image

        return $this->render('MCoreBundle:Profile:details.html.twig', array(
                'aImageProfil'      => $aImageProfil
                )
        );
    }
    public function aproposAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oForm = $this->createForm(new AProposType($oUserCurrent->getSexe()), $oUserCurrent);
        $oSession = $this->get("session");

        $oRequest = $this->get('request');
        if ($oRequest->getMethod() == 'POST') 
        { 
            $oForm->bind($oRequest);
            if ($oForm->isValid()) 
            {  
                
                if ($oUserCurrent->getDescription() != $oUserCurrent->getAncienneDescription()) $oUserCurrent->setDescriptionModerer(0);
                $oEm->persist($oUserCurrent);
                $oEm->flush();
                $oSession->getFlashBag()->add('success', "Vos modifications ont bien été enregistrées");
            }
        }

        return $this->render('MCoreBundle:Profile:apropos.html.twig', array('oForm' => $oForm->createView()));
    }
    public function centresinteretAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $aCentresInteret = $oEm->getRepository('MCoreBundle:Centresinteret')->FetchAllCentres();
        $aCategorieCI = $oEm->getRepository('MCoreBundle:CategorieCentresinteret')->findAll();
        $oForm = $this->createForm(new DefaultType,$oUserCurrent);
        $oRequest = $this->get('request'); 
        $oSession = $this->get("session");

        if ($oRequest->getMethod() == 'POST') 
        { 
            $oForm->bind($oRequest);

            $aNewCentresInteret = $aCentresInteretautre = (array)$oRequest->request->get('m_corebundle_usertype');
            $aNewCentresInteret = $aNewCentresInteret['Centresinteret'];
            $aCentresInteretautre = $aCentresInteretautre['Centresinteretautre']; 

            //On insére les réponses de l'utilisateur
            $oUserCurrent->setCentresinteretautreBuild($aCentresInteretautre);

            // On boucle sur les centreinteret pour les lier à users
            foreach($aNewCentresInteret as $CI)
            { 
                $oUserCurrent->removeCentreinteret($oEm->getRepository('MCoreBundle:Centresinteret')->find($CI));
                $oUserCurrent->addCentreinteret($oEm->getRepository('MCoreBundle:Centresinteret')->find($CI));
            }         
                
                $oEm->persist($oUserCurrent);
                $oEm->flush();
                $oSession->getFlashBag()->add('success', "Vos modifications ont bien été enregistrées");
        }
        //Appel du service qui reorganise les centres d'interet par cat nom et selected
        $aFormCentresInteret = $this->get('FonctionDiver')->Centresinteret($aCategorieCI,$aCentresInteret,$oUserCurrent->getCentresinteret(),(array)$oUserCurrent->getCentresinteretautre());
        return $this->render('MCoreBundle:Profile:centres_interet.html.twig', array('oForm' => $oForm->createView(),'aFormCentresInteret' => $aFormCentresInteret));
     
    }
    public function questionnaireAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oForm = $this->createForm(new DefaultType,$oUserCurrent);
        $aQuestions = $oEm->getRepository('MCoreBundle:Questions')->findAll();
        $oSession = $this->get("session");
        
        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        { 
          $oForm->bind($oRequest);
 
                $aReponseform = $oRequest->request->get('m_corebundle_usertype_question'); 
                //On insére les réponses de l'utilisateur
        

                $oUserCurrent->setQuestionsBuild($aReponseform);
                             
                $oEm->persist($oUserCurrent);
                $oEm->flush();  
                $oSession->getFlashBag()->add('success', "Vos modifications ont bien été enregistrées");
        }

        $aFormQuestions = $this->get('FonctionDiver')->Questions($oUserCurrent->getSexe(),$aQuestions,(array)$oUserCurrent->getQuestions());

        return $this->render('MCoreBundle:Profile:questionnaire.html.twig', array('oForm' => $oForm->createView(),'aFormQuestions' => $aFormQuestions));
     
    }
    public function profilidealAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $_oProfilIdeal=$oEm->getRepository('MCoreBundle:Userideal')->findOneById($oUserCurrent->getUserideal());
        $oProfilIdeal = (is_object($_oProfilIdeal)) ? $_oProfilIdeal : new UserIdeal ;
        $oForm = $this->createForm(new ProfilidealType($oUserCurrent->getSexe()),$oProfilIdeal);      
        $oSession = $this->get("session");    
        
        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        { 
            $oForm->bind($oRequest);
            $oUserCurrent->setUserideal($oProfilIdeal);
            $oEm->persist($oProfilIdeal);
            $oEm->persist($oUserCurrent);
            $oEm->flush();
            $oSession->getFlashBag()->add('success', "Vos modifications ont bien été enregistrées");
            
        }
      
      
        
        return $this->render('MCoreBundle:Profile:profil_ideal.html.twig', array('oForm' => $oForm->createView()));

    }
    public function albumAction()
    {   
        if ($this->get('security.context')->isGranted('ROLE_FREE')) { return $this->redirect($this->generateUrl('m_core_profile_abonnement')); }
        // Load admin content here
    
        $oEm = $this->getDoctrine()->getManager();
        $oSession = $this->get("session");
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $iCountImageProfil = $oEm->getRepository('MCoreBundle:Images')->getCountProfilByUser($oUserCurrent->getId());
        $oSession = $this->get("session");    

        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        {
            $this->get('TraitementPhotos')->TraitementPhotos($oRequest->request, 'Album');
            $oSession->getFlashBag()->add('success', "Vos modifications ont bien été enregistrées");
        }

        $aImagesAlbum = $oEm->getRepository('MCoreBundle:Images')->getAlbumByUser($oUserCurrent->getId());
        $oSession->set('ImagesAlbum', $aImagesAlbum);

        return $this->render('MCoreBundle:Profile:album.html.twig', array(
                'aImagesAlbum'      => $aImagesAlbum
                )
        );
        
    }
}