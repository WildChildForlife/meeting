<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use M\CoreBundle\Entity\User;
use M\CoreBundle\Entity\Images;
use M\CoreBundle\Form\RechercheAvanceType;
use M\CoreBundle\Form\RechercheFavType;
use M\CoreBundle\Entity\Messages;
use M\CoreBundle\Entity\Notification;
use M\CoreBundle\Entity\Moderation;
use M\CoreBundle\Entity\Events;
use M\CoreBundle\Entity\Temoignages;
use M\CoreBundle\Entity\Presse;
use M\CoreBundle\Entity\Actus;
use M\CoreBundle\Entity\Abonnement;
use M\CoreBundle\Form\EventsType;
use M\CoreBundle\Form\ActusType;

use Symfony\Component\HttpFoundation\Response; 
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class AdminController extends Controller
{
   
    public function indexAction()
    {

        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oEm = $this->getDoctrine()->getManager();
        $oRequest = $this->get('request');


        if ($oRequest->getMethod() == 'POST') 
        {   

            //Si l'utilisateur est validé en envoye mail d'activation
            //en met description en description ancienne
            //photo ancienne



                if ($oRequest->request->get("m_corebundle_uservalide"))
                {   
                   $aRequest = $oRequest->request->get("m_corebundle_uservalide");
                   $oUser = $oEm->getRepository("MCoreBundle:User")->findOneById($aRequest['id']);

                    if (array_key_exists('valide',$aRequest) && is_object($oUser))
                    { 
                        
                        $oUserimage = $oEm->getRepository("MCoreBundle:Images")->findOneByUser($aRequest['id']);

                        $sCode = str_shuffle(base64_encode($oUser->getUsername().$oUser->getEmail()));
                        //description et photo moderé
                        $oUser->setDescriptionModerer(1);
                        $oUser->setAncienneDescription($oUser->getDescription());
                        $sType = (array_key_exists('type',$aRequest)) ? array('ROLE_USER') : array('ROLE_FREE') ;
                        $oUser->setRoles($sType);
                        // abonnement membre d'honneur
                        if(array_key_exists('type',$aRequest))
                        {
                            $aujourdhui = new \DateTime();
                            $abonnement = new Abonnement();
                            $abonnement->setUser($oUser);
                            $abonnement->setDateCreation(new \DateTime());
                            $abonnement->setDateExpiration($aujourdhui->modify('+12 month'));
                            $oEm->persist($abonnement);
                        }
                        $oUserimage->setModerer(1);
                        $oUserimage->setImageOld($oUserimage->getImage());
                        $oUser->setCodeActivation($sCode);
                        


                        $oUser->setDateActivation(new \DateTime());

                        //Envoi du mail pour activer son compte
                        $oEm->persist($oUserimage);
                        $oEm->persist($oUser);
                        $this->get('Mail')->mailValidation($oUser->getUsername(),$oUser->getEmail(),$sCode);
                    }

                    if (array_key_exists('rejeter',$aRequest) && is_object($oUser))
                    {   
                         //Envoi du mail refus
                            $this->get('Mail')->mailRejete($oUser->getEmail());
                            //Supression compte
                            $oEm->remove($oUser);
                    }      
                        //moderateur qui a traiter action Type 1 = Traitement nouveau Membre

                        $oModeration = new Moderation();
                        $oModeration->setType("Nouveaux Membres");
                        $oModeration->setModerateur($oUserCurrent);
                        $oModeration->setDatemoderation(new \Datetime());
                        $oEm->persist($oModeration);

                        $oEm->flush(); 
                } 


                if ($oRequest->request->get("m_corebundle_signalement"))
                {
                   $aRequest = $oRequest->request->get("m_corebundle_signalement");
                   $oUser = $oEm->getRepository("MCoreBundle:User")->findOneById($aRequest['id']);

                    if (array_key_exists('vue',$aRequest) && is_object($oUser))
                    { 
                            $oUser->setSignalementModerer(1);
                    }

                    if (array_key_exists('avertir',$aRequest) && is_object($oUser))
                    { 

                             $oUser->setSignalementModerer(1);
                             $oSignalement = $oEm->getRepository("MCoreBundle:Signalement")->find($aRequest['lastsignalementid']);
                             if($oSignalement->getAverti() == 0) $this->get('Mail')->mailSignalement($oUser->getUsername(),$oUser->getEmail());

                             if(is_object($oSignalement)) $oSignalement->setAverti(1);
                          
                    }

                    if (array_key_exists('bloquer',$aRequest) && is_object($oUser))
                    {   
                            $oUser->setSignalementModerer(1);
                            $oUser->setActif(0);
                            $oUser->setRoles(array('ROLE_DELETED'));
                            

                            //Envoi du mail pour activer son compte
                            $this->get('Mail')->mailSuppression($oUser->getUsername(),$oUser->getEmail());
                    }

                    if(is_object($oUser))
                    { 
                        $oEm->persist($oUser);
                        

                        //moderateur qui a traiter action Type 2 = Traitement signalement Membre

                        $oModeration = new Moderation();
                        $oModeration->setType('Signalements');
                        $oModeration->setModerateur($oUserCurrent);
                        $oModeration->setDatemoderation(new \Datetime());
                        $oEm->persist($oModeration);

                        $oEm->flush(); 
                    }
                }  

                if ($oRequest->request->get("m_corebundle_descriptionabonne"))
                {
                    $aRequest = $oRequest->request->get("m_corebundle_descriptionabonne");
                    $oUser = $oEm->getRepository("MCoreBundle:User")->findOneById($aRequest['id']);

                    if (array_key_exists('valide',$aRequest) && is_object($oUser))
                    { 

                            $oUser->setDescriptionModerer(1);
                            $oUser->setAncienneDescription($oUser->getDescription());
                    }

                   if (array_key_exists('rejeter',$aRequest) && is_object($oUser))
                    { 
                            $oUser->setDescriptionModerer(1);
                            $oUser->setDescription($oUser->getAncienneDescription());

                            //Envoi du mail rejet
                            $this->get('Mail')->mailChangementRejete($oUser->getUsername(),$oUser->getEmail());
                    }

                    if(is_object($oUser))
                    { 
                        $oEm->persist($oUser);

                        //moderateur qui a traiter action Type 3 = Traitement description abonne

                        $oModeration = new Moderation();
                        $oModeration->setType("Descriptions Abonnes");
                        $oModeration->setModerateur($oUserCurrent);
                        $oModeration->setDatemoderation(new \Datetime());
                        $oEm->persist($oModeration);

                        $oEm->flush(); 
                    }
                     
                }  

                if ($oRequest->request->get("m_corebundle_descriptiongratuit"))
                {
                   $aRequest = $oRequest->request->get("m_corebundle_descriptiongratuit");
                   $oUser = $oEm->getRepository("MCoreBundle:User")->findOneById($aRequest['id']);

                    if (array_key_exists('valide',$aRequest) && is_object($oUser))
                    { 

                            $oUser->setDescriptionModerer(1);
                            $oUser->setAncienneDescription($oUser->getDescription());
                    }

                    if (array_key_exists('rejeter',$aRequest) && is_object($oUser))
                    { 

                            $oUser->setDescriptionModerer(1);
                            $oUser->setDescription($oUser->getAncienneDescription());

                            //Envoi du mail rejet
                            $this->get('Mail')->mailChangementRejete($oUser->getUsername(),$oUser->getEmail());
                    }

                    if(is_object($oUser))
                    { 
                        $oEm->persist($oUser);
                        

                        //moderateur qui a traiter action Type 4 = Traitement description membre gratuits

                        $oModeration = new Moderation();
                        $oModeration->setType("Descriptions Membres gratuits");
                        $oModeration->setModerateur($oUserCurrent);
                        $oModeration->setDatemoderation(new \Datetime());
                        $oEm->persist($oModeration);

                        $oEm->flush(); 
                    }
                     
                }  

                if ($oRequest->request->get("m_corebundle_photogratuit"))
                {
                   $aRequest = $oRequest->request->get("m_corebundle_photogratuit");

                   $oImage = array();

                   if ($aRequest['refus'][0] != '') 
                    { 

                        $aRefus = explode(";",substr($aRequest['refus'][0],1));
                        foreach ($aRefus as $key => $value)
                        {   

                            $oImage[$value] = $oEm->getRepository("MCoreBundle:Images")->findOneById($value);
                            if(is_object($oImage[$value]))
                            { 

                                $Mail = $oImage[$value]->getUser()->getEmail();
                                $sUsername = $oImage[$value]->getUser()->getUsername();
                                
                                $oEm->remove($oImage[$value]);
                            }

                        }
                        
                        if(is_object($oImage[$value]))
                        { 
                            $this->get('Mail')->mailChangementRejete($sUsername,$Mail);
                        }

                    }

                   if ($aRequest['accepte'][0] != '') 
                    { 

                        $aAccepte = explode(";",substr($aRequest['accepte'][0],1));
                        foreach ($aAccepte as $key => $value)
                        {
                            $oImage[$value] = $oEm->getRepository("MCoreBundle:Images")->findOneById($value);
                            if(is_object($oImage[$value]))
                            { 
                                $oImage[$value]->setModerer(1);
                                $oEm->persist($oImage[$value]);
                            }
                        }

                    }       

                    if(is_object($oImage[$value]))
                    {                     
                
                        //moderateur qui a traiter action Type 1 = Traitement nouveau Membre

                        $oModeration = new Moderation();
                        $oModeration->setType("Photos Membres gratuits");
                        $oModeration->setModerateur($oUserCurrent);
                        $oModeration->setDatemoderation(new \Datetime());
                        $oEm->persist($oModeration);


                        $oEm->flush(); 
                    }
                     
                }  

                if ($oRequest->request->get("m_corebundle_photoabonne"))
                {
                   $aRequest = $oRequest->request->get("m_corebundle_photoabonne");

                   $oImage = array();

                   if ($aRequest['refus'][0] != '') 
                    { 

                        $aRefus = explode(";",substr($aRequest['refus'][0],1));
                        foreach ($aRefus as $key => $value)
                        {   

                            $oImage[$value] = $oEm->getRepository("MCoreBundle:Images")->findOneById($value);
                            if(is_object($oImage[$value]))
                            { 
                                $Mail = $oImage[$value]->getUser()->getEmail();
                                $sUsername = $oImage[$value]->getUser()->getUsername();
                                $bSexe = $oImage[$value]->getUser()->getSexe();
                                $sphotoHomme="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAUAAA/+IMWElDQ19QUk9GSUxFAAEBAAAMSExpbm8CEAAAbW50clJHQiBYWVogB84AAgAJAAYAMQAAYWNzcE1TRlQAAAAASUVDIHNSR0IAAAAAAAAAAAAAAAAAAPbWAAEAAAAA0y1IUCAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARY3BydAAAAVAAAAAzZGVzYwAAAYQAAABsd3RwdAAAAfAAAAAUYmtwdAAAAgQAAAAUclhZWgAAAhgAAAAUZ1hZWgAAAiwAAAAUYlhZWgAAAkAAAAAUZG1uZAAAAlQAAABwZG1kZAAAAsQAAACIdnVlZAAAA0wAAACGdmlldwAAA9QAAAAkbHVtaQAAA/gAAAAUbWVhcwAABAwAAAAkdGVjaAAABDAAAAAMclRSQwAABDwAAAgMZ1RSQwAABDwAAAgMYlRSQwAABDwAAAgMdGV4dAAAAABDb3B5cmlnaHQgKGMpIDE5OTggSGV3bGV0dC1QYWNrYXJkIENvbXBhbnkAAGRlc2MAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAADzUQABAAAAARbMWFlaIAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9kZXNjAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2aWV3AAAAAAATpP4AFF8uABDPFAAD7cwABBMLAANcngAAAAFYWVogAAAAAABMCVYAUAAAAFcf521lYXMAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAKPAAAAAnNpZyAAAAAAQ1JUIGN1cnYAAAAAAAAEAAAAAAUACgAPABQAGQAeACMAKAAtADIANwA7AEAARQBKAE8AVABZAF4AYwBoAG0AcgB3AHwAgQCGAIsAkACVAJoAnwCkAKkArgCyALcAvADBAMYAywDQANUA2wDgAOUA6wDwAPYA+wEBAQcBDQETARkBHwElASsBMgE4AT4BRQFMAVIBWQFgAWcBbgF1AXwBgwGLAZIBmgGhAakBsQG5AcEByQHRAdkB4QHpAfIB+gIDAgwCFAIdAiYCLwI4AkECSwJUAl0CZwJxAnoChAKOApgCogKsArYCwQLLAtUC4ALrAvUDAAMLAxYDIQMtAzgDQwNPA1oDZgNyA34DigOWA6IDrgO6A8cD0wPgA+wD+QQGBBMEIAQtBDsESARVBGMEcQR+BIwEmgSoBLYExATTBOEE8AT+BQ0FHAUrBToFSQVYBWcFdwWGBZYFpgW1BcUF1QXlBfYGBgYWBicGNwZIBlkGagZ7BowGnQavBsAG0QbjBvUHBwcZBysHPQdPB2EHdAeGB5kHrAe/B9IH5Qf4CAsIHwgyCEYIWghuCIIIlgiqCL4I0gjnCPsJEAklCToJTwlkCXkJjwmkCboJzwnlCfsKEQonCj0KVApqCoEKmAquCsUK3ArzCwsLIgs5C1ELaQuAC5gLsAvIC+EL+QwSDCoMQwxcDHUMjgynDMAM2QzzDQ0NJg1ADVoNdA2ODakNww3eDfgOEw4uDkkOZA5/DpsOtg7SDu4PCQ8lD0EPXg96D5YPsw/PD+wQCRAmEEMQYRB+EJsQuRDXEPURExExEU8RbRGMEaoRyRHoEgcSJhJFEmQShBKjEsMS4xMDEyMTQxNjE4MTpBPFE+UUBhQnFEkUahSLFK0UzhTwFRIVNBVWFXgVmxW9FeAWAxYmFkkWbBaPFrIW1hb6Fx0XQRdlF4kXrhfSF/cYGxhAGGUYihivGNUY+hkgGUUZaxmRGbcZ3RoEGioaURp3Gp4axRrsGxQbOxtjG4obshvaHAIcKhxSHHscoxzMHPUdHh1HHXAdmR3DHeweFh5AHmoelB6+HukfEx8+H2kflB+/H+ogFSBBIGwgmCDEIPAhHCFIIXUhoSHOIfsiJyJVIoIiryLdIwojOCNmI5QjwiPwJB8kTSR8JKsk2iUJJTglaCWXJccl9yYnJlcmhya3JugnGCdJJ3onqyfcKA0oPyhxKKIo1CkGKTgpaymdKdAqAio1KmgqmyrPKwIrNitpK50r0SwFLDksbiyiLNctDC1BLXYtqy3hLhYuTC6CLrcu7i8kL1ovkS/HL/4wNTBsMKQw2zESMUoxgjG6MfIyKjJjMpsy1DMNM0YzfzO4M/E0KzRlNJ402DUTNU01hzXCNf02NzZyNq426TckN2A3nDfXOBQ4UDiMOMg5BTlCOX85vDn5OjY6dDqyOu87LTtrO6o76DwnPGU8pDzjPSI9YT2hPeA+ID5gPqA+4D8hP2E/oj/iQCNAZECmQOdBKUFqQaxB7kIwQnJCtUL3QzpDfUPARANER0SKRM5FEkVVRZpF3kYiRmdGq0bwRzVHe0fASAVIS0iRSNdJHUljSalJ8Eo3Sn1KxEsMS1NLmkviTCpMcky6TQJNSk2TTdxOJU5uTrdPAE9JT5NP3VAnUHFQu1EGUVBRm1HmUjFSfFLHUxNTX1OqU/ZUQlSPVNtVKFV1VcJWD1ZcVqlW91dEV5JX4FgvWH1Yy1kaWWlZuFoHWlZaplr1W0VblVvlXDVchlzWXSddeF3JXhpebF69Xw9fYV+zYAVgV2CqYPxhT2GiYfViSWKcYvBjQ2OXY+tkQGSUZOllPWWSZedmPWaSZuhnPWeTZ+loP2iWaOxpQ2maafFqSGqfavdrT2una/9sV2yvbQhtYG25bhJua27Ebx5veG/RcCtwhnDgcTpxlXHwcktypnMBc11zuHQUdHB0zHUodYV14XY+dpt2+HdWd7N4EXhueMx5KnmJeed6RnqlewR7Y3vCfCF8gXzhfUF9oX4BfmJ+wn8jf4R/5YBHgKiBCoFrgc2CMIKSgvSDV4O6hB2EgITjhUeFq4YOhnKG14c7h5+IBIhpiM6JM4mZif6KZIrKizCLlov8jGOMyo0xjZiN/45mjs6PNo+ekAaQbpDWkT+RqJIRknqS45NNk7aUIJSKlPSVX5XJljSWn5cKl3WX4JhMmLiZJJmQmfyaaJrVm0Kbr5wcnImc951kndKeQJ6unx2fi5/6oGmg2KFHobaiJqKWowajdqPmpFakx6U4pammGqaLpv2nbqfgqFKoxKk3qamqHKqPqwKrdavprFys0K1ErbiuLa6hrxavi7AAsHWw6rFgsdayS7LCszizrrQltJy1E7WKtgG2ebbwt2i34LhZuNG5SrnCuju6tbsuu6e8IbybvRW9j74KvoS+/796v/XAcMDswWfB48JfwtvDWMPUxFHEzsVLxcjGRsbDx0HHv8g9yLzJOsm5yjjKt8s2y7bMNcy1zTXNtc42zrbPN8+40DnQutE80b7SP9LB00TTxtRJ1MvVTtXR1lXW2Ndc1+DYZNjo2WzZ8dp22vvbgNwF3IrdEN2W3hzeot8p36/gNuC94UThzOJT4tvjY+Pr5HPk/OWE5g3mlucf56noMui86Ubp0Opb6uXrcOv77IbtEe2c7ijutO9A78zwWPDl8XLx//KM8xnzp/Q09ML1UPXe9m32+/eK+Bn4qPk4+cf6V/rn+3f8B/yY/Sn9uv5L/tz/bf///+4ADkFkb2JlAGTAAAAAAf/bAIQAAgICAgICAgICAgMCAgIDBAMCAgMEBQQEBAQEBQYFBQUFBQUGBgcHCAcHBgkJCgoJCQwMDAwMDAwMDAwMDAwMDAEDAwMFBAUJBgYJDQsJCw0PDg4ODg8PDAwMDAwPDwwMDAwMDA8MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAmgB8AwERAAIRAQMRAf/EAK4AAQACAwEBAQEAAAAAAAAAAAAHCAUGCQQDAQIBAQABBQEBAAAAAAAAAAAAAAAGAgMEBQcBCBAAAQMDAgQDBgMGBQUBAAAAAQIDBAAFBhEHITESE0EiCFFhcYEyFEIjFZFScjMkFqGxwWIlgpI0JhgJEQABAwIDAwkFBgYCAwAAAAABAAIDEQQhMQVBUWHwcYGRoRIiEwax0TJCUsFicoIzFOHxkrIjU0MVsyQW/9oADAMBAAIRAxEAPwDt/W0WlSiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlEXmkzIkNAXLktx0qOiS4oJ6j7BrzPuFeEgZr1rS7JeU3FxTanmLXNejpGplONpitAe3rlKZGnvHyq35wJoMVe/buAqaAcVqz+4eLxl9qVkuMxHdNezIv8Abm3ADyJT3Tz+NZIt7h2Iiefyn3LGdPbNwMzB+Ye9Zy3X9q6pK7Yy1eGkp6lPWqbCnJCeHHRp8r04j8PjVmQuj+Nrm84or0bGy/pva7mNV727pCW8mMt1UWWr6YkpC47p+CHQkn5V42RrsijonNzCyNVq2lESiJREoiURKIlESiL+VKShKlrUEIQCpa1HQADiSSaIozzTcmxYlCiS7pPdgt3VzsWCDEjqmXe8PcAGbXAT5nCSQO4sBA1Hhxq7a2s144iEYD4nHBrRvJ2cqBUXd1BZNDpjifhaMXOO4AYnlUqKb7f8yjsovGY5Ra/TtYJiAuPD/Lv+czmSQCVLUlxmIFj8LTThbOoVppW3s9Pgc7uwRuuXjNx8EIPYXdJaDsWj1HWJIGd65lbaRnJoo+dw5sQ3oa4jaVAV83A2EYfD52+yLeO6Mk/+xbgXyRJKiNOKYylPsge4NI9nLhUpt9F1EjGZkI+mJg9uB7SoHd+r9Ka7wW8k5+qZ5/t8Q7GrCq9QeOQgGrV6e9tYMUcSx+kNHVXidUJbHEaeFXj6YecXXUxP4v5rHb66HyWcAH4f5LxDebZO6PI/uf00Y7D0PC5Yu+bPJb5+ZBiNMr6uPMOpNUu0G+jH+K8eeDx3x2k+xZUXquwmP+axYOLD3D2Ae0Ka8OzfFr8hEHaffKfZpritWNrd22xdbW+rkhhmctRfZBPAduQ4r/ZWgvtOljxu7Zrm/wCyHwuHEtyPS0DipZpurwzYWV05jv8AXP4mngHZjoeTwUx2fcmdZLzCxHcKwu7Z5ZPc7Not9xk/d4/eV+CbTedNG3VeDD+ihqlI4mtHLpp7hltX+bGMxk9n4m50+8KjmUhj1MeYIbtnkynI5xv/AAuyr900OWam6LMbldxHQuPJjq6ZUN4dLrSvYpPHn4EcD4Gte14cKhZ74yw0K9lVKhKIlESiJREoi/OXE8AOZoihfcDOpUByyWbHbYnJsvyx9UfBMQUopalKb/m3W5KT5m4EbmfFw8Bw4jJsrMXRc+R3dhZi52/c1u9ztnWVYvbw2gayNvfnkwY3dvc7c1u3qCrbmOfwdprpd02a7t7g76XRCmMy3YltpcatupVrbrQwrqQy2zrp0p8o/F1K8qJnpukO1BjXSN8u2GLIhm77zzma9e6gxPNte9Tt0qR8du4S3ZwfMcQz7kYyFOobanBtULpdbne58m6Xi4SLpcpiyuVOlOKddcUfFSlEk1NoYWQtDGABoyAwC5XcXElw8ySuLnHMk1JWMcOiauqyFiXiONeFZMaxD1eFZcaxL/jVJWwjU+7aeoqZYLcrb7dS2jcvaW5AR51huP58mC3yDsJ1Z6h2+aUdQ0I/LU2rzVHtS9Ptld59sfKnGIIwDuDhx39dVK9K9Sugb+3ux5tucC04lvFp4buohXdxvJ28Ibxda8sXmGz2Vqbi7W7tyVF2Ran3D0Isl+WQlSmFLBQ08sBTSvI5ppqINeWZui9zWeXcsxfGMnj62cdpaM82rodlfNtWsa5/mWsmEchzYfofw2Bxy+Fys1ClmUhwONGNLjLLM6Io6qadTzSSOY46gjmCDWkY8OFQt3JGWGhXsqpUJREoiURKItNzTIrTjtlulzvkkxLFZoD10yOSj60wo44tI5arfXo2ga6nU6ca9ZC+4kbDHi55oF66aO3jdPJg1gqeXLFVAzTM71t/jczKLqkQN7t64aHpTaT+Zi2LAkQrXF4AtK6R5ilIJc61nzIQam2ladHeShjcbaA0G6WX5nHePsoMiVzb1NrkunQF1aXdyKnfDD8rW7if7u8cwCqbEkkknUnman646vwkDnRFd/08emzFs5xSLnmcOSLhDujz6LRYo7qo7RajuqZW4+43o4SVoUAEqToBqSdeEB9S+qJ7Sc29vQEAVcRU4iuAyyO2q6z6K9DW1/ai7uiXBxPdaDQUaS0lxGOYOAI6a4Whc9MuxrsR2GcCjobdSEl1MqZ3U9IIBS53yoHj7ePjrUSHqjUu93vNPU2nVRdB/wDidH7pb5A63V661VEPVF6d7LtNAsWUYhImO2C5ylW+5RJriXlR5SkqdZLa0oSShaELGitSCnmerhOfTHqKTUXOimA74FQRhUZHpGHXwXOvV3pOLSWsmtye4490gmtDmKHcQDnuzxVJ3jzqXqHMCwEo86rCqKsN6cN4Lfil2nbY7g9Nz2h3K/43I7dKVqzBff0bbnIJP5eh0DhTodAF/U2mo76g0p07BcwYTx4tIzIHy8eHVtKk3pnWW20htbjG3lwcDk0nJ3Dj15gLo9t7c7xjF1vu2uVzHbhf9uW44h318+e+YnLUUW64rJ+t2GvVh9Q9mpJJrnOpMY4Mu4hRkmDh9Eg+Icx+JvArqWmvkb37KY1fFi1x+eM/CecfC7iBvU+1grMSiJREoiURV7zlEfMcz2/wKW4BZbtcJWb5yTr0iw4qpKITLo1A7Uic4lZ+HyOw00mGKa5HxYRs/E/MjiGg9awNTDZZYLV3w4yyfgjyB4F5HUud+5GeTNw85yPLZS1dF0lqNvZVw7MRvyR2tNSB0tpTrpzOp8a6vpdi2ytmQjYMeJ2nrXANd1B+pXslw75jhwaMGjoHbitL+4NbBabuLJMp7byHUud5tbSltpA07qNOl5vn5VBJP+nMa2XPqKchuKzYYO64EYilecfMOBpX7MwuyHp/ssvHtn8JtM6KqJJYjSHFsrGhIflPPJXpqfrSsK+dcU9RTtnv5XtNQSOwAdlKL6R9JWjrXS4YnChAPa4mvTWqmOtKpGqm+tNDitj560RnX0tXi3qddbbK0sJK1J7jhH0JKiEan8SgnxqVejSBqAx+V3Tyz6FDPXjSdMOFfE3o4ndu5zRcfg3pCeUpfb+4QVLXproy2rhw/wB7mgHw9ldYLvFyz/gFx2OOkZ2V9g97qDoWq3Fv7foQterxT1PNafyyeSSfbpzHhy561eYa8ysSs7mBz28OXYtWkq51eCxyuqG224P9ybR7ObxT3g7dtor0MA3LdWrjJxy7lmGXH/xL7YdjOgk/Wlw68TXM9RsBHd3FmB4Zm+YzhI2rqDno4cxC63peomWytr0nxQO8uTjG6janmqw84KvPa+43GVDeWVv2x1yE8s81dhRSlZ/jTor51DYnd5oU0mb3XkLJVcVpKIlEXlmvGNDlyBzjsuOD/oST/pXhNAvWipAVSs7uRs//ANM34qKXsNwXFcEtr3HyC7NLkStP9ylT2jqDwP8AhItJh7wso9jpHyH8mA/sKjWuzlo1CXa2JkQ/PUn+8Lmqh/Txrqi4a6NZyOHkFyO/G7jakhYU30lwAfjZIPnHtA1BHs5i05wOIPLjuV5kDgS1zajhnzt3+z2jIIc60gd1CCdHGX2gQkqb5PNgAEKTw7idNSPNpqKtE05dh+wrKZHXaN4I4fMOI+YZ0xpULs1sduHZtxMAs0u3O6XKxxY1sySCUdBjzWY7fcCRyU2rXqQpPAj3ggcY1zTpLO5cHfC4lzTvaSe3eF9BenNUjv7Nhb8TQGuG5wAr0biMKKYK063yph64M3tdi2obw5cn/ns5nx0W6GCQPt4DzUmQ+6QNOhBCBx8SCOR0lvo61e+884DwsBrzuBAHPmoj6ymYbPyCaOkIpzNIJJ4DBcjJEx881pjNM6KHWngktjpSVjjr2weX4nFFOh0NdNDieXLPsC5uLWNvMN/Df+HteSMVgX+4SpxTRddZSVpYcI8gPHuyVkgdRPJJ+HLgq4HHfy3D38hSYIxj3ASN4HW47+H8jrchh9HQ4+goL+q0BWgUQePV08wDrwOmh8KvNfXIrHdAwYlox4BXV9Iq15Hh3qO2wdPXGyPCJFzhMqGoRLhJW0hwDQ8et9BP8IqKepT5U9rcbWyAdB/kVKPT0bZba7twMHRk9IB+0hdM9ur2vI8YxrIHV9x/J8ZsN8lK1B/OmQUJd104a9TJB9+tc+uYfIuZYhk17h1EqeW83n20Mpzcxp6wD9q3+raqSiJRFib8krsd5SkaqVBkBI95aVVL/hPMq4/iHOqgbwqSMP8AWg0AX5Kcgw2Sp8E6/buxbMppBR4BtII18flUr0b9fTzs7knXV/tUR14f+tqQzPfiPRSOnUubEdaXVdJeQ1w1Cl66E+zUA/410kmi5KIQ45gLYYynGmgNSUJPU6Ffnxtf3utklTZ+HH31Zc4E8ge3NX2Qlo4f1N624tPbxWSQ+FjqWFKDhBUtKgoqWNdCHEeVax4EaLHsXyNs4cuVB2cyyGsrnt9vOMCeODuDlfj0QXm4u3POrQzEVJsX2sWS/d2xoy1MaWptDB5aLW2tRKTy6OGn0pgPrRjC2N1fFUinA7eYH29J6d6GbIzzBTwkDHiPYaHLhhhgOh1QFdCVNPVF6cr/ALuT7NmGLXVLl6sFuXbk41LWhth1tTqnS6w6U6JdUVdKg4ehSQOKdD1Sr09rsdi10Ug8LjXvDPKlDw5sQd6jOvaG+9e2Vh8TRShyzrUcdmOBC5q5Psvuri3U5ftvL7DajjrTIbhuPxmgNQFrlspea6+egClEcwFKJXU9t9VtJsGStPTQ9RoadXVgoNcaZdQ4vjcKcMBxLhUV3ZkbKklyh58Opc7KUlt5pRPaDZcdQr8RRH1PSoeKnT1nnwragDPl1+7BalzjWgz5qnobsPFx73MtelxdFuureS1zPS86HHVq8dQ2CUkn979tXmuwosWRuJJNOc1J6vt61cb0KAt7jbgTVuduLDwK6KlE69IHfinqPwAP7ai3rDG3iG0yN9hUn9Hu/wA8p2CJ3tC6IbFtrb2y2yQ4koV/YGOK6Tz0WiStJ+aVAioNqhrf3H4z7SpnpQI062r/AKx7ApkrDWWlESiL4vtJfYeYX9LyFNq+ChoaEVXoNCqsZPY3r9kO72FpR3Ze7e1EG7Whs8nLzjK3oTjKOHFXUtgkDjpx04Gt1p1x5dvbzH/hmLXfhkoa9jlpNUtjLcXUA/5oA5v4o6ina1ckocllwhtYdUtRAb7WhPw6Tz/bXWnAhcYjocDXoUzbebbZ3uZc/wBPxDHJdzcZIMm4OQ0Q4sbXxelB1sJ9umpUfAGtTf6lb2TO9K8DhWpPMKH3Le6fpdzfP7sTCTvLQ0DncCPer67f+iOG02Jm6GSLnylK89lsLim4629OCXpb7f3CwfFKenl9RFQbUPWTneG2ZQb3Z9DR4faug6X6KbH47p9Xbm5dLj4jzYdKuZarNhO2GMOR7ZEt+IYtZ2y9JXqllltIABcddWdVHgB1KJJ4CohJLPeS1cS955YKaxQxWsdGgNaFXu8+s/Z61TnYcVm/39polP6hb4bIYUR+791IjrI9/TW5i9LXb21PdbwJNewFYL9agaaCp5h7yFK22++e3O6ajGxm8lu7oQXHLBPR9vMCE81JQSpLgHiW1K08dK199pFxZ4yN8O8Yj+HSsq2vorjBpx3HNS9WsWYtBzTa3bzcOI7EzPD7ZfUu85L7CUyUnTTVEhHS6g/wqFZtpqNxaGsTy3pw6slhXenW903uysDujHrzXDX1F7aW3andTJMRgGZIsUNTEmzOLZbbIYmMoeDankpAWUElGvT4fGuwaFfuvrVsrqd41Bx2g0y7Vx7X7FljdOjFS0UIwAzFaV27slMnpmiSMY2V9R25CGl/c3C0NYfjCkDVa51y1ZCEe092SxWr19wmvrW32B3fd+FuPsDlsdArDp93cbS3y2/idh7XNXT3E7O3YLdGsTJSpjGbfbbBGWkaJKLZDaaJAHADrKh8q506UzSPlPzOJ6zVdF8kQRxxD5GgdQp9i2qipSiJREoir/uyZWJrsO5tujuSZm0t2XfpUVlIU5Ixu7JES/MoTpxLWqZPu6QefEbDSgJHyWjjQTNoOEjcWdvh/MsHViY447xoqYHVPGN2D+oUd+VcxfU7t4jbnc+4SrOEvYTnSf7jwm4s8WHIk3R1bTauI/KWogDXXoKFfirpfpy//d2oD/1GeF421GFen21XJ/VGm/srwln6UnjYdlHY06D2U3rsD6ZbIxYdhtsI7MZuMZ9kj3R/oToXF3AfdFxR5kqDg4n/ACrlXqKYy6hMSa0cR/Th9i7F6XgEOmQAClWh39Xi+1TtWlW+VIt7U3Hd/evEdi2ZrsHFLXGTe8wWwrRaz0qc0PAjyt9CUajQKc1IOgqV6T3bGyfdkVeT3W8ufPgFpL2tzcNgr4RieXLNT3eWbZtPa8Ds2C2O3Wa3XnKbZY5rCGeKo8nu9xZUCFKc8v1rKjz11rUQl166R0ziSGOcMdop2cFmy0tgxsYABcB0FQz6ldpLbBsi938DjN4zm2EvNXKTKt6EsiUwhaQpa0JASXGtQvq8UhSVdXl02ehak5z/ANtMe9G/DHYfcfasTUrQBvnR4ObjhtVktt8tGd4HimXdtLLt8tzUiWyj6USAOh9CfclxKgPdWjvrb9tO+L6T2bOxbK2m82Nr94W7ViK+uPv/AOgOJylbw4XcrdEW+/llhZgsMNIJW/MjS3W+lOn1KKXmkgc+Xtrqfoi5H7ORrj8DieYED3Fcl9eWzv30bmjF7QOcgn3hWAxjBouJN7RbIo6ZMbbNpG5e7zrJ1Dl2JItMBRHAqdlL6glXNDSDpoQa0N5fmRs96c5P8UX4fnd1Yc7ipDZ6eI3W9gMov80p+98jf6sabmhW3tzDseG0iQoLlL6npixyU86ouOkfFajUcY3ugBSKR3ecSvdVSoSiJREoixd0iGQ0l1thuS/H6v6V3QtyGnE9D0dwHUFLqCUnXx0PhVDwTiMxkrkbgDQ4g4FUwyrbOyZFYh6esom/p9qmOPXX0yZ7M1JjujXv45McUSoOMKV2wkk9xvpI87aE1K7PVHsd/wBhEKnBs7B/5AOO3c6uwlRO+0lj2/8AWzGjcXW7z2xE8Nn1NptAC6DY3Zmccx2w49GOsew26Lbo548URWUtJ5+5NQm4mM0jpDm4k9Zqp5bQiGJsYyaAOoUWaqyryo7uTPZ2o9TVg3AyAKbwzP7ObNcroNemM4hCWV6lPEBHQyskHXpKiOWlSyxYb3TXQs+Nju8Bv2+9aO5cLe7EjvhcKcuxbduJs/t7HY25k2ODcJEa8ZhaI8iQi93aUhyFIDqlKQpcxwJBABDiNCPBQ1qxY6pcEyh5FQxx+FoxFOHYV7d2EIDC0HF4+Zxw6+1YXfpe3+z23WSY5jbcr+69x4qLXDtD1ynXF1TCllLj3blvv9tKUqUkFIBUsgcdDpc0f9xf3DXyU7kZrWgbjuwAr7kvhFaxOayvefhSpPtJVhNnsVmYTtjhWMXAdFwtltR+oNa69t98qfdb18ehbhT8q0up3AuLl8jcicOYYLZWcRiha05gKSqwFkqtu+C8Hst0wvcy/WpzIcqxByZbNscXZ6VO3i+XUsJYaaQAVKLRZ1CuSOpSuYFSDRxPIyS3Y7usfQyO+hja1J569OAUd1s28T47mRvekZVsbdr3vpQDmp0YlePbTDLhZI0+Rkcxq75hf7kb7uNfGv5Um8qSENQ454/01vaAbbGunUNRodRVF9dNupAIxSKMd1g4bzxccT/Be2No60iPmGs0h70h4/SPutGA/iphrGV9KIlESiJREoij3OsHtOX2afaLrDcmWme4iTKjxllmZGmM/wAi4294cWpTJ4gjgseVXvuW9xLayiWLPaNjhtBG0Hliqbi3iu4jDMPCciM2nY4HYRsP2LVcZ3Zvm2v2WO7yzP1PGHnhExbe1lsiFIBV0tRr6hI/oZY4JKlDtrPHUEKJypdPivwZbMUdm6L5hvMf1N4ZhYsOpS6cRFfGrMmzfKdwk+l3H4TvVo48iPLYZlRX25MaQhLkeQ0oLbWhQ1SpKkkggjiCKjzmlpoRQqStcHCoNQVrGbYPjW4WPysayu3JuNslaLSNSl1l1OvQ8y4OKFp14Ee8HVJIORaXclrIJIzQjlQq1PAyZvdeKhVYY9M25uJB23bb763SzY46pfatMtDv5CVnj0dp3tlXtUlDZqQnXrafxT24Lt429le0rVjTJo8I5SBu5fwW9bb+maw4jkAzbMMgm7j5slwPMXe5g9pl0cnUtrcdUtxPgpazpwKUpUNaw77XpJ4/KiaI49w5DDmCv22mNjd33kudvKss4821p1q0KvpQNSo/BI1J+VaIAlbIkBQzuLvDYsOlxsajRJOXZ9dkn9B21sxS7cpSiPKuUoaoiRxrqt1wgdOp82hTW3sNKkuAZCQyMZyO+Ec31O3AbetafUdXjtnCMAvlPwxtxcef6W7ydm/JRziOHX+fkT+d53PjXfcJ5hUOM/ABNqxWC4NHLdZwrXrkL5PSef4RwGhyLu8Y+P8Ab2wLYQakn4pHfU7hubkM81i2lm+OT9zdEOnIo0D4Ym/S3j9TszlkpsjR2YjDUaO2GmWUhLaB4Af5n2msICmCyiSTUr716vEoiURKIlESiJRFh59mjzESglLSfvmyzcIzzSH4stsjQtyo7mqHUkcOPH2GqS2hDmmjhkRmrgk8Ja4AtOYOShZrba8YM65J2syy5bZtqUp13FyyvIMTeUT1KKIDqhJhFZPmLCwkDlyrYu1MTCl5EJPvjwyf1DB1PvDpWtbpfkGtlKYvuHxx9DTi2v3SOZbRb9zt64GjN42rsueoQr8274NkEcHp9v6ddhHdSR4juq/ZxqybPTpcY5yzhI0/3MqOwLIF7qUOElu1/GN4/tf3T2lZdO+8pI6Zex26cN7QEoFjjyBxHMLjzHU8/aQfdXn/AEo2XEJ/MR7WhVDXTttpx+QH2OK80neDcCc2pePen/K+kfU9k1ytGPsIOmvmWJcpwj+FCvHhwr0aXbR/qXUf5Q5/2NHavDq11J+laSfnLIx7XHsWoyU7xZqHGb9nltwa0yD/AFFg23YXOubrZ5oev85KGmFg8CploH2Grjbiwg/RidK7fJgz+huY4Od0K0+31C4/XlbC3dHi8/ndkeLW9K23C9tcewyFKhWK1N2Ni5K7l5fQ6uVdLmskkruNzd1ed1JJ6EkJGpA4Gse7up7xwdM6oGTRg1vMBgFftLa3smlsDaE5uOLncSTieWCkhppthtDLLaWmmwEttoASlIHIADgKtZK4TVfSi8SiJREoiURKIlESiJREoi8Um3QJv/lwWJR9rraVn9pBrwtBzVQcRkV8BZrangiP20/uoWtI+QCgKp8pu5V+c/eibLaELDn6bHW6OTq20rX/ANygTXoY0bFSZXHaVkwAAABoBwAFVKhftESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiL/2Q==";
                                $sphotofemme="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAUAAA/+IMWElDQ19QUk9GSUxFAAEBAAAMSExpbm8CEAAAbW50clJHQiBYWVogB84AAgAJAAYAMQAAYWNzcE1TRlQAAAAASUVDIHNSR0IAAAAAAAAAAAAAAAAAAPbWAAEAAAAA0y1IUCAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARY3BydAAAAVAAAAAzZGVzYwAAAYQAAABsd3RwdAAAAfAAAAAUYmtwdAAAAgQAAAAUclhZWgAAAhgAAAAUZ1hZWgAAAiwAAAAUYlhZWgAAAkAAAAAUZG1uZAAAAlQAAABwZG1kZAAAAsQAAACIdnVlZAAAA0wAAACGdmlldwAAA9QAAAAkbHVtaQAAA/gAAAAUbWVhcwAABAwAAAAkdGVjaAAABDAAAAAMclRSQwAABDwAAAgMZ1RSQwAABDwAAAgMYlRSQwAABDwAAAgMdGV4dAAAAABDb3B5cmlnaHQgKGMpIDE5OTggSGV3bGV0dC1QYWNrYXJkIENvbXBhbnkAAGRlc2MAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAADzUQABAAAAARbMWFlaIAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9kZXNjAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2aWV3AAAAAAATpP4AFF8uABDPFAAD7cwABBMLAANcngAAAAFYWVogAAAAAABMCVYAUAAAAFcf521lYXMAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAKPAAAAAnNpZyAAAAAAQ1JUIGN1cnYAAAAAAAAEAAAAAAUACgAPABQAGQAeACMAKAAtADIANwA7AEAARQBKAE8AVABZAF4AYwBoAG0AcgB3AHwAgQCGAIsAkACVAJoAnwCkAKkArgCyALcAvADBAMYAywDQANUA2wDgAOUA6wDwAPYA+wEBAQcBDQETARkBHwElASsBMgE4AT4BRQFMAVIBWQFgAWcBbgF1AXwBgwGLAZIBmgGhAakBsQG5AcEByQHRAdkB4QHpAfIB+gIDAgwCFAIdAiYCLwI4AkECSwJUAl0CZwJxAnoChAKOApgCogKsArYCwQLLAtUC4ALrAvUDAAMLAxYDIQMtAzgDQwNPA1oDZgNyA34DigOWA6IDrgO6A8cD0wPgA+wD+QQGBBMEIAQtBDsESARVBGMEcQR+BIwEmgSoBLYExATTBOEE8AT+BQ0FHAUrBToFSQVYBWcFdwWGBZYFpgW1BcUF1QXlBfYGBgYWBicGNwZIBlkGagZ7BowGnQavBsAG0QbjBvUHBwcZBysHPQdPB2EHdAeGB5kHrAe/B9IH5Qf4CAsIHwgyCEYIWghuCIIIlgiqCL4I0gjnCPsJEAklCToJTwlkCXkJjwmkCboJzwnlCfsKEQonCj0KVApqCoEKmAquCsUK3ArzCwsLIgs5C1ELaQuAC5gLsAvIC+EL+QwSDCoMQwxcDHUMjgynDMAM2QzzDQ0NJg1ADVoNdA2ODakNww3eDfgOEw4uDkkOZA5/DpsOtg7SDu4PCQ8lD0EPXg96D5YPsw/PD+wQCRAmEEMQYRB+EJsQuRDXEPURExExEU8RbRGMEaoRyRHoEgcSJhJFEmQShBKjEsMS4xMDEyMTQxNjE4MTpBPFE+UUBhQnFEkUahSLFK0UzhTwFRIVNBVWFXgVmxW9FeAWAxYmFkkWbBaPFrIW1hb6Fx0XQRdlF4kXrhfSF/cYGxhAGGUYihivGNUY+hkgGUUZaxmRGbcZ3RoEGioaURp3Gp4axRrsGxQbOxtjG4obshvaHAIcKhxSHHscoxzMHPUdHh1HHXAdmR3DHeweFh5AHmoelB6+HukfEx8+H2kflB+/H+ogFSBBIGwgmCDEIPAhHCFIIXUhoSHOIfsiJyJVIoIiryLdIwojOCNmI5QjwiPwJB8kTSR8JKsk2iUJJTglaCWXJccl9yYnJlcmhya3JugnGCdJJ3onqyfcKA0oPyhxKKIo1CkGKTgpaymdKdAqAio1KmgqmyrPKwIrNitpK50r0SwFLDksbiyiLNctDC1BLXYtqy3hLhYuTC6CLrcu7i8kL1ovkS/HL/4wNTBsMKQw2zESMUoxgjG6MfIyKjJjMpsy1DMNM0YzfzO4M/E0KzRlNJ402DUTNU01hzXCNf02NzZyNq426TckN2A3nDfXOBQ4UDiMOMg5BTlCOX85vDn5OjY6dDqyOu87LTtrO6o76DwnPGU8pDzjPSI9YT2hPeA+ID5gPqA+4D8hP2E/oj/iQCNAZECmQOdBKUFqQaxB7kIwQnJCtUL3QzpDfUPARANER0SKRM5FEkVVRZpF3kYiRmdGq0bwRzVHe0fASAVIS0iRSNdJHUljSalJ8Eo3Sn1KxEsMS1NLmkviTCpMcky6TQJNSk2TTdxOJU5uTrdPAE9JT5NP3VAnUHFQu1EGUVBRm1HmUjFSfFLHUxNTX1OqU/ZUQlSPVNtVKFV1VcJWD1ZcVqlW91dEV5JX4FgvWH1Yy1kaWWlZuFoHWlZaplr1W0VblVvlXDVchlzWXSddeF3JXhpebF69Xw9fYV+zYAVgV2CqYPxhT2GiYfViSWKcYvBjQ2OXY+tkQGSUZOllPWWSZedmPWaSZuhnPWeTZ+loP2iWaOxpQ2maafFqSGqfavdrT2una/9sV2yvbQhtYG25bhJua27Ebx5veG/RcCtwhnDgcTpxlXHwcktypnMBc11zuHQUdHB0zHUodYV14XY+dpt2+HdWd7N4EXhueMx5KnmJeed6RnqlewR7Y3vCfCF8gXzhfUF9oX4BfmJ+wn8jf4R/5YBHgKiBCoFrgc2CMIKSgvSDV4O6hB2EgITjhUeFq4YOhnKG14c7h5+IBIhpiM6JM4mZif6KZIrKizCLlov8jGOMyo0xjZiN/45mjs6PNo+ekAaQbpDWkT+RqJIRknqS45NNk7aUIJSKlPSVX5XJljSWn5cKl3WX4JhMmLiZJJmQmfyaaJrVm0Kbr5wcnImc951kndKeQJ6unx2fi5/6oGmg2KFHobaiJqKWowajdqPmpFakx6U4pammGqaLpv2nbqfgqFKoxKk3qamqHKqPqwKrdavprFys0K1ErbiuLa6hrxavi7AAsHWw6rFgsdayS7LCszizrrQltJy1E7WKtgG2ebbwt2i34LhZuNG5SrnCuju6tbsuu6e8IbybvRW9j74KvoS+/796v/XAcMDswWfB48JfwtvDWMPUxFHEzsVLxcjGRsbDx0HHv8g9yLzJOsm5yjjKt8s2y7bMNcy1zTXNtc42zrbPN8+40DnQutE80b7SP9LB00TTxtRJ1MvVTtXR1lXW2Ndc1+DYZNjo2WzZ8dp22vvbgNwF3IrdEN2W3hzeot8p36/gNuC94UThzOJT4tvjY+Pr5HPk/OWE5g3mlucf56noMui86Ubp0Opb6uXrcOv77IbtEe2c7ijutO9A78zwWPDl8XLx//KM8xnzp/Q09ML1UPXe9m32+/eK+Bn4qPk4+cf6V/rn+3f8B/yY/Sn9uv5L/tz/bf///+4ADkFkb2JlAGTAAAAAAf/bAIQAAgICAgICAgICAgMCAgIDBAMCAgMEBQQEBAQEBQYFBQUFBQUGBgcHCAcHBgkJCgoJCQwMDAwMDAwMDAwMDAwMDAEDAwMFBAUJBgYJDQsJCw0PDg4ODg8PDAwMDAwPDwwMDAwMDA8MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAmgB8AwERAAIRAQMRAf/EALMAAQACAgMBAQAAAAAAAAAAAAAGCAcJAgQFAwEBAQABBQEBAAAAAAAAAAAAAAADAQIEBQcGCBAAAQMEAQIDBQQHBwUBAAAAAQIDBAARBQYHIRIxEwhBUWEiFHGBMiORoUIzJBUW0VJicoKyF/DBczRFJxEAAgEDAAYFBwkGBwEAAAAAAAECEQMEITFBURIFYXGhIgbwgZGxMhMUwdFCUmKSIzMH8XKCorJT4UNzNEQ1FiX/2gAMAwEAAhEDEQA/AN4FXG5FAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgOKlJQkrWoISkXUomwA+JoDzU5eC6opiKdyKge0iEy5JsfcSylQH3mhFK9BbTsB6evq3gcisWvctto8fg44k/qqhZ8VA4KnONAGTi8lHT7VGI64B9paCwPvoVWTB7TlGyEGYVJiy2n1o/G2lQ70/BSfEfeKqSxmpamd2hcKAUAoBQCgFAKAUAoCHbLuWG1vFZDNZPKwsNhMVcZLY8i55cNhfX8pNvnfdNujbYJJ6dDVYxcnRKrLsezeyrqs48HOb2Ly1b3qW1mIkbLyHvTS8hp2rxdX1VJBa5O5LStlt5JP7zG682W1WIsptchae4fHpU3u4Q9p1e5fObpcpwsWSjl3ZXrv9qxpp0TuaY9DUVJreeNkMPrj6VDeeZOQeR5BBQ/jsNJGt4Y/BMXHiMq32uL/71fxtezFLtZt7Eclf7bEsWFsc172596fF6okRlYb0+Nq/M4flTihXel+Vn8m66VdfnUpx9airr4kk1T31zf2I2UI85f8Ay0uq3BLsSPgwrgqKvux2D3njmV1Cclq+zZFKkk9O4tuSw37Bf8s39t6p76e2j60hPF5vJd6dm8t1y1H1qNe0neJc22cUf8fcu4fl5hkXb0fkKMiBmwlI6pi5eE2w6XD4dymVJH961U4rcvaVOr5jRZuHir/e4krD/uWHxW/PCTdF1TT6CY6tym1KzTWn7Bi8lo29rSoo4/2dSA9LCbBS8PlU2jz0AmwFws9fACrZ2WlxJ1W9fLuNTmclvY9p5Ficb+P9eGuP+pB96D69H2jM0SYxNbUtkkKbUUPsrBQ40seKHEGxSR7jURq4zUlVHaoXCgFAKAUAoBQGLORt/wAbqOHXPlokzW3ZaMZicDj+uQzuUeIS1jYIHXxP5rgHyJvbrer7dtzdF+wyeX8vu8yve6ttRilWc37MILXJvyq9Bi5WFcw2Tx+68tog7RyXFR36lx8we/XdQZX1QllofK9JtYreVdRP4ClISoz8WjhhoW/az0+OletPGwK28bVO5qu331/RhuitG+rbR42b2HM7FKMzMT3Jjtz5aFGzbYPsbQLJSPsFEktRucTCs4seG1FJdr63tPDWbJNGZSI5NI61YzKtkOne2rGZtsh8xSkKC0KKFoIUhaTYgjqCCPdVpnQVVRmTcNy/j85ikaFzXi/640h9SUsZR25ymLcT0RJjyEkOEo94V3geCiPkN0JODrHWefy/DkrVz4rl0vdXls+hP7Mo6tP3d62rOmG2XN6Hk9Zw+0bOjbNO23sjcVc2PKBMlXbdrDbEoADzVWKWpB+ZSh8w7+8CSUVcTlHQ1rXyo8RncthmxuXsa37rIt6b1jZTbO19na4/R2aKVsjDlpltqV5amHmVlqVFc6LadT+JCrfpB9o6joaxzzcJqaqjt0LxQCgFAKA8XNz24cVSVy0Y9C23XZeRcNkRIkdBdkyVE9AG2wSL+0iiVSyXFJqEFWUnRLe2Vy1vJ/WD/nTLQ1MSMqw7iuBdYk//ACsCflcyjjR8JE/94pRuewoR3EE1lSVF7tefr3eY9k8FQS5XafdhSWRJfTubLaf1YauvilSpFZEh+U+7JkurfkPrLjzzhKlLUo3JJPiTVT0MIRhFRiqJakfKheehg8HkNpy8XCYtAVIkklbiuiG209VuLPuSP7PGrJSoY+Zl28O07tzUu17jPZ9OuAcaQH9gyCn+nmuIQ0lB69bJKVEXH+I1B7xnjv8A2l9PRbjTz+XYdjO+nTR8jjXI+KXMxGSCP4fI+cp9JWBYea0s2IPtCe37apxMixfGeZbuVuUlHaqU9DXy1NfO24TJazmclgcux9PkMY8pmQgdUm3VKknpdKgQpJ9oNVOuYGVbyrUbtt1jJVXlvWpmN5yvGrkba2ZW4d5Fw0I5LivkZKcjxdvv8Hko76rJgSXSA3KbX4tgKCSpQt2kBwdU9apuL4lrR5/xJya5dUc7E0ZNrSqfTitcXv6N+mO0uBx9mc5r2UznHW5zXJ+28eIYS5m3vxZ3WJCi3jcso2Hc9HV+RIPXqLkm4q67FOk46n2M5Zzmxaahn4ypZvaJRX+XdXtw6n7UPsvVoM91CasUAoBQCgK/ctIf2x7WeM4jrjTvLOd/kmQebPYtvW8KgTs6pC7jtLxCY594NvgZrGis9y7dhuOQNWbl7PlpWPCsf9Wfdt+h1l/CQLe9qZzWzTfoexrD4q2OwkZoBLTcaN8ifLSnoEqIKh9tqR0I9byfAePjR4vbl3pPbxS06erURcTRbxFX1NlwHLz+7y1pd7bqsVeHYvxTf4H3/wBlUbLeHWqFluAsOkR89sa2wDKdRBiEixQGx5jyR7bFSkj/AE1DcZ4Xxfk1lbsV1Lifn0R7K+ksVUZ4sUBSz1Xam225r+6xkBD8juw85drJ7+1TrDiiP7qA7c/BPuqqOleAs9tXMaWpd9epr08PpZQPIpShvzPMP5ij5KCLEoFx3nr0ufAfb98iOrWnV0IPPX+KrkbK0i8er73/ADvjDjLmWW4X8xwzmRqHJS1G6pmr5YtRHlOi4U55aXWHQST87a1e02kgqtw3rR1o5VzTlCs5+Ty5L8PKh720vq3oVlRbq0nHqlFF1sYHWWHYD6yt/FPLhuuK8VBs/lrPxU2Uq++sU5tYnxQR6VVJRQCgOK1BCVLV4IBJ+wUBW5c5TXJW2bCSf/zLiRgxgeoayu0S5EtawbWupMVCb38L1NqtLpfqPQcvs8XLbFr+/k1fTC0kqemcitTE/oOtW1OkytntMSnAUHyw4h4EAEix9tgr2K/XVamPKC9B2RKSE9oHckns6/KTfr5bg9hv1Sr/AKFKkbh5fKvlXk9g/GmKXh9G1yK8yGJLkRMmWn9ouSPzSV/4rKAP6Kib0nHueZCv5tySdVWi6lo0dBOqoaoUBXf1UIWnhTZpraQt3GSMc+2g9ArvmsxyO647R2um5916ugqs9T4NuuHM7a3qS/lb+Q0+zs5kZJQVO+YXPmS22hCVvkdLpum7bKB0B9v+3JUEdxttx20+T55Py6YPkMnmFh2U44lDSldiSlLYQbdLNi3zAW6kX+NTRhE2dmVKRT8uktR6Q5r23r5q4mmdr0TfdImKZQfH6qNdhooAB6gSyroL/KPdVt1cDjJbGeP8dSeP8Jm7bV5eiWl/0dpsJ4gz7my6Lo+feWXZOf1HCTZ7x/bmIYVFkn3/AImBWNejwza6TlHNsZYvMcmytUbkqdVXTsoZVqMwhQCgOnP/APRm/wDgc/2mhbLUysOcU4md6qFNE/UDX9FWg36/SfRu/hJ6dvcHeg9vd7amn+XDznruV0dvlddXFe+9Vdvs9hU+NOCilJcCAf21XsP0Amozpc7ZIYklaUntWVd3iW7OoI9negdQPif0UqYlyKflT0Mm2jtIze36vjHWBMjTcpEYlsj50qYLyfNSSQSE9gPyuD2dDVGzWc0m7GLdmnRqMmuumjt2o2fAAAACwHQAVYcNP2gFAV69U+Pn5Hg3c2oDXnmN9FLmM/L1jxpjLzqiFlKbICe89xtZJ8fA32/aPTeELkIcztOWivEl1uLS9OrzmmKY4oEoISFPW6OdxCyOqT5YT5jvwukNj3WrLR3a3Dbu8teqPpcukiOUZW453vPKZUBZxyYpKF/ABhHctIHgALj7KlgbKw0lRKvV8+plnPQipZ9QMdLau5AwOT81Q8Cmzdj1sfG1W5Xsec8n+pC/+Rp+vH5S/np17f8Aijjfs/dHCTDF8bfTnKSSx23/AGfLt2/CsbJ/MZy/xP8A9xkb6xr18Ma9usz5UJphQCgOKkhaVJULpUCFD4GgK1SsY7J5H2rWEjrynxD9LEUTZLuY1eU/EKAPAqSmYhXwsam12up+s9By7IUOXWLz/wCPlaeiFxJ+uDRQmNPQlRS+Fi1wQDYgj33FR0OyzttrQTrAY/K7BkoOEw+Pkz8hPcDUSIIqAVHx6uFabAAXKiQAOpqhrMu/bx7crtySUVpbq/VTsLycR8EbJp+z4/adlyWPcVBZe+ngRFOvLS482WvmceTYWStV+0+PvFWtnMPEHiqxm48rFiMtLVW6LQnXUupayzGYzOJ17GTMznclGxGJx7Zdm5GW4lplpA9qlqIA9w99Em3RHisXFu5VyNqzFynJ0SSq35ipGZ9dHCWLnORIbOx7AyhRT/McfBZSwq3tAlyIzlv9FZKxJvcdJxf0j51ehxSdqD3Sk6/yRmu0zfxlzhxty207/R2fS/kYyPMl4KWgx5rSb27i0v8AEnqLqQVJ+N6huWpQ1nk+feE+Y8ka+Kt0i9Ul3oPzrU+h0fQdrmzIyMVxHyLOjLZbebwMxCS+CUEOtltSQE3PcoKIT0PzEdD4VbBVaMLkFpXeYWIutONauh1/b0GiGU7IsseStTa7lxCGpF1X8e5KEx0H7xWckvLyZ9G24R39q+XiZFZ6SWll1iUllHzK7YDbNgPe4FE2HxqWPlpNjZ16Gq/vN9haz0cqTrLfO3LagtmHomkS2oTilWUZUq7zSErAHzExbC3tUKtyFxOMd7PG+PV8Q8PB23byb6lof9XYbIOJNfc1jSNL195HlyNe1TCY6e34FEz6cyZQP+t8Vg3ZcU2+k5DzXKWVzDIvLVK5KnVV07KGUasMMUAoBQFfuXxJ1hOD5KgMLfmcR5xOzvMNJK3XtfyCPodgYbSD4obUJFz0HaKmsaW4P6Xr2G48P8N29dwZukcmHCtyuxfFbf3lw/xFK+fdWa03kKZNxRS9qe6Np2DU57PVh2NN/MUltQAFkLUQAP2Cg+2rV0nWfCee87BjGf5truTT1px0aetdtdxmb0VsRJ+97PPdCXJWMwoTFBFyn6h9AUse42Rb7CatmtB5v9SZyt4duC1Snp8yfzmyuozjJQPmeNkOd/UTr/BCpz0LR9MhIzm5tx1WcecW2h29/C/Y8y0gn8JcUrr0FZlr8O257Wdk8LTt+HPD1znHCnfvSdu3XUlVr1xlKW/hSLHZxGG4hjcZa1our4fE4fbNsi63k4qY5H8I9CmyFuBSFpK3SqOm63Cu4Krgk3qGNblW3qVTwdi5e51LJv5d2cp27TuJ1+kpQjTVojST0R4aaKFePVDxJC0aJE5+4tYb1LbdMmx38y1j20sx5TDzqWfNUygBPeFuBLlhZaFK772qbHucXclpTPc/p/4knzGb5NzBu7ZvRajxOsotKtOJ6aUVY/VklwltMU5r3L/GmHlZzEs5HX96wsSXPw74JR2yWkPFskEEFtfgoG4IBBvY1jNOEuo5rmWr3JuYXLduTU7M5RUl9ltV862bnRmor1d+n7HcKZrCZfUzKXpe0JdbZZlOecuFNZPcpjzLAlCkKCkd11dFXJtes/Huca06zt/gXxNPnNqdu/T3sKatHFF7ab666aNRSpxwrNh1v4CspI6LGNDafx5xynWOM+KuEsggNZvlXJI5B5ZZUk3h6ziFNyQw/Y3R5y2mGRfp3FwVhOdZSubtC6zifN+cK/m5XM0/w8eLs2X9a7OseJb+FOc+rhL3YsOrjrmSEFuTk3nJj7Z8UF43Sg/5EdqfurBOaWYcMUj0qqSigFAKA8nLwhKYDiYzcxbAWFwXgFNSWHUFt+M4FAgpdQSnr0vY+yhZNS0Si6STqn0op5lOP4WbxKvT3mp3keUJOb9Mm5S72diJBVIwMpw9Q7E/dqTckthK7flhNZMnxL3i/i+c9/gc7dqa5vZVU6QyoLZLZdS3S19Eqxr3myP+hvDbBhOTeX8TsePdxeVwEGJAyUB+wWy79QspT0JCgQgqCk3SRYg2Iql9JRVDK/UjKtZOHjXbUuKM22mtqovn1GzWsU5Aa/N+nw+HvVorddt7o+icwa8MJkM1dSW4yksx46wXEdqkdq4jKlKB+VK+6/SsyC47VFrTOzcnsz574U+ExtORi3ONR2y0yktG2qnNJbXGm0mfJnBnGkOXw+vCYzKPxM7vMGJkHk5/NSkrhO46e6VIW5Nc8u6kIIcbKVW6BVlEGlu9J8Vd25Gg5L4n5hOOWrsopwsSa/Dtx7ynBaUoKut911XRoId6lmtA4s48y3GOixZkrduWpGOhpwa8nkMtIDEaT5iHQ3MkSS33qJaQE9pWpX7XZ0usOU5cUtSN54Bjnc25hDPymlYxlOXFwQtxrKNKVhGNaLvOteFLZxabl8a60/pvHuk6pLX5kzX8JBgzVgggvssIS7226W7wbfCsWcuKTZzLnufHP5hfyI+zcuSkupt07CtPrxx0Kb6fcnKkhP1GIzWMlY1SrXDy3THV2/Hy3l/dU2I/xD1X6Z3ZQ5zGK1ShJPqpxetI19+mzh/EPNTOcuVm1Y7inQ1JlR23kXVmZ6FgMxo7Zt5o8ztSQPxrKUDxVbMvXG3wR1s6j4w59ci1y3B72Td0On+XHa29mj0Kstxsc451/P5Ofnt53mIIm78gmNKzWLV838kwUe68Xg0mwss386TYC6jZQvY1hXZLRGOpdr3nG+c5NmThhYzrYs1739y4/budWyO6K6TPFQmrFAKAUAoBQGON90PC7diJWNy0eQ5AdfROZlQFFrI4vIM9WcnjXU9W32z1UB+MdOvVKr4XHB1RPg517l9731mmqkovTGcXrjJbU/8AFaSJaPySjS9ji4LmZjHwdnzjDWN1rmqM0ljFbRFYUox2pL3QRpiO5Xcw4bdxV5ZsoJqSdvjVYat27/A2edyxZ+P77lzcrUW5SsN1nZb9pxX0ofaWmntLQWvBBAINweoIrGPGEI5C461LlDXJGr7jjE5HHPKDjDiT2PxnkghLzDo6oWm/2EXCgUkg3wm4OqNtyXnmXyfIWRiz4ZLQ9qkvqyW1ftVHRlTI/pN5N1htzF8f+o3P4DWrkxMQ4h8eQD7ElmWhF/eUoR9lZHxEXriqnR5/qNy3MfvM3llud3bLRp+9BvzNyMn8Tel7U+N885uubzU7kPfXSpX9TZcdGnFiy3WWlLdUHFDoVrcWoDokjreO5kOaotCNB4j/AFAyua2PhbUI2Mf6kNq2Jui7q3KMVvropZyoDwJX71HNcXzdHh47lXLORsArLRZbOAhXXkMw/GKi3Biso/NcU4tQv2C9vakfMJrCnxd39h6jwnLPt5TnhRrPha4n7NtPXOT1Ki3+h6jGeu6pmduzGB2Dctdj63jNVS2rinhpISqHr7aU2aymZQiyHJxT+6Z8Gv8AOVKF85qCcYuret7+ro9ZnZ3MoY0J42HN3Llz86/tnvhbetW971z/AHSw0OIiGz5SVrdWtRckSHDdx1xXVS1n2kmoDQQgoqiO3QuFAKAUAoBQCgIxnNXxuag5HHSoMPIY3Lp7cxgsgymRAmDxu6yr8KwbELRZQNj1tVU3F1WsWpXLFxXbMnCa1NOjMQwtV37jQIa4w3UYzBsdGuON68/J4RtP92Bmmv4uIhI6JQ8FJv1tbpU3vYz9tad6+bUbi5zbEzv+xsNT/u2aRk+mcH3JPe+6ybRubtxxTY/rjgza4nsTltSMbace4B070qhrRJSD4gKYuPbVPcxfsyXn0GO+QYt/Ti5lqS3XK2ZfzJw9EzuD1NcTIHbNl7JjJI/eQpWqbAHUX6juCMesdQb+NU+Gn0elfOW/+Q5g/ZVuS3q7ap/WcT6jNWmnt1TSeQN2Ur90vFavkGWlHwF3sg3DbSLgi6lDwNPh2tbS84/8pkQ/Pu2LX712Dfog5t+ZEcye886bOTDx2CwXCkB75Tks7IRsOwFBtZyNiMeSwlQv4PPKHvTVeG1HW3Lq0L0kscbk2HpuXJ5M/qwTt2+pzl32v3YrrOWpcWwcPmHNrlSMjsG6ym/LlcibM4iXmPLN+5qAwE/TY9o9xADSe6xsapO85KmpbkR5vN8jLt+4io2bC1W4KifTJ65vpk31Iy5Ehx4TXkxm+xJUVrUSVKWtXitajcqUfaSb1Ea6MVFUR2qFwoBQCgFAKAUAoBQH549D1B8RQHmqw+NLhebiiM8r8T8ZSo6zf2lbJQf10I5WoS1o+ghLHROUyYA8B9dIP61LJ/SaoWfDQ3HBeMZeBEmTOlpPQofmSFoI/wAhc7f1UKrHgth2Y0OJDSURIrUZJ8UtISi/22AvVSVRS1HZoVFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAf/2Q==";

                                if ($oImage[$value]->getProfileDefault()==1)
                                {   
                                    $oImage[$value]->setImage(($bSexe) ? $sphotofemme : $sphotohomme );
                                    $oImage[$value]->setModerer(1);

                                }
                                else
                                {   

                                    $oEm->remove($oImage[$value]);
                                    $oEm->flush(); 
                                    

                                }
                         }


                        }

                        if(is_object($oImage[$value]))
                        { 
                            $this->get('Mail')->mailChangementRejete($sUsername,$Mail);
                        }

                    }

                   if ($aRequest['accepte'][0] != '') 
                    { 

                        $aAccepte = explode(";",substr($aRequest['accepte'][0],1));
                        foreach ($aAccepte as $key => $value)
                        {
                            $oImage[$value] = $oEm->getRepository("MCoreBundle:Images")->findOneById($value);
                            if(is_object($oImage[$value]))
                            { 
                                $oImage[$value]->setModerer(1);
                                $oEm->persist($oImage[$value]);
                            }
                        }

                    }       
                    

                    if(is_object($oImage[$value]))
                    {                     
                
                        //moderateur qui a traiter action Type 1 = Traitement nouveau Membre

                        $oModeration = new Moderation();
                        $oModeration->setType("Photos Abonnes");
                        $oModeration->setModerateur($oUserCurrent);
                        $oModeration->setDatemoderation(new \Datetime());
                        $oEm->persist($oModeration);

                        $oEm->flush(); 
                    }
                   
                }  

                if ($oRequest->request->get("m_corebundle_photoalbum"))
                {
                   $aRequest = $oRequest->request->get("m_corebundle_photoalbum");

                   $oImage = array();

                   if ($aRequest['refus'][0] != '') 
                    { 

                        $aRefus = explode(";",substr($aRequest['refus'][0],1));
                        foreach ($aRefus as $key => $value)
                        {   

                            $oImage[$value] = $oEm->getRepository("MCoreBundle:Images")->findOneById($value);
                            if(is_object($oImage[$value]))
                            { 
                                $Mail = $oImage[$value]->getUser()->getEmail();
                                $sUsername = $oImage[$value]->getUser()->getUsername();

                                $oEm->remove($oImage[$value]);
                            }

                        }
                        
                        if(is_object($oImage[$value]))
                        {
                            $this->get('Mail')->mailChangementRejete($sUsername,$Mail);
                        }

                    }

                   if ($aRequest['accepte'][0] != '') 
                    { 

                        $aAccepte = explode(";",substr($aRequest['accepte'][0],1));
                        foreach ($aAccepte as $key => $value)
                        {
                            $oImage[$value] = $oEm->getRepository("MCoreBundle:Images")->findOneById($value);
                            if(is_object($oImage[$value]))
                            {
                                $oImage[$value]->setModerer(1);
                                $oEm->persist($oImage[$value]);
                            }
                        }

                    }       
                    

                    if(is_object($oImage[$value]))
                    {                     
                
                        //moderateur qui a traiter action Type 1 = Traitement nouveau Membre

                        $oModeration = new Moderation();
                        $oModeration->setType("Photos Albums");
                        $oModeration->setModerateur($oUserCurrent);
                        $oModeration->setDatemoderation(new \Datetime());
                        $oEm->persist($oModeration);


                        $oEm->flush(); 
                    }
                     
                }  


        }

        $aNew = $oEm->getRepository("MCoreBundle:User")->NewUser();
        $aNewdescriptionGratuit = $oEm->getRepository("MCoreBundle:User")->NewUserDescriptiongratuit();
        $aNewdescriptionAbonnes = $oEm->getRepository("MCoreBundle:User")->NewUserDescriptionabonne();

        $oService = $this->get('FonctionDiver');

        $aNewimageGratuit = $oService->regroupArrayByUsername($oEm->getRepository("MCoreBundle:Images")->ImageProfilByUserFree());
        $aNewimageAbonne  = $oService->regroupArrayByUsername($oEm->getRepository("MCoreBundle:Images")->ImageProfilByUserAbonne());
        $aNewimageAlbum   = $oService->regroupArrayByUsername($oEm->getRepository("MCoreBundle:Images")->ImageAlbumByUserAbonne());

        $aUserSignaler = $oEm->getRepository("MCoreBundle:User")->NewSignalement();
        $aListSignalement = $oEm->getRepository("MCoreBundle:Signalement")->signalement($aUserSignaler);
        $oFonctionDiver = $this->get('FonctionDiver');

        // Resize Images
        foreach ($aNew as $iKey => $sValue) 
        {
            $aNew[$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 130, 170);
        }
        foreach ($aNewdescriptionGratuit as $iKey => $sValue) 
        {
            $aNewdescriptionGratuit[$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 130, 170);
        }
        foreach ($aNewdescriptionAbonnes as $iKey => $sValue) 
        {
            $aNewdescriptionAbonnes[$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 130, 170);
        }
        foreach ($aNewimageGratuit as $iKey => $sValue) 
        {
            $aNewimageGratuit[$iKey]['images'] = $oFonctionDiver->buildThumbnail($sValue['images'], 130, 170);
        }

        foreach ($aNewimageAbonne as $iKey => $sValue) 
        {   
            foreach ($aNewimageAbonne[$iKey]['images'] as $_iKey => $_sValue) 
            {   
                $aNewimageAbonne[$iKey]['images'][$_iKey]['nom'] = $oFonctionDiver->buildThumbnail($_sValue['nom'], 130, 170);
            }
        }
        foreach ($aNewimageAlbum as $iKey => $sValue) 
        {   
            foreach ($aNewimageAlbum[$iKey]['images'] as $_iKey => $_sValue) 
            {   
                $aNewimageAlbum[$iKey]['images'][$_iKey]['nom'] = $oFonctionDiver->buildThumbnail($_sValue['nom'], 130, 170);
            }
        }
        foreach ($aUserSignaler as $iKey => $sValue) 
        {
            $aUserSignaler[$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 130, 170);
        }
        return $this->render('MCoreBundle:Admin:index.html.twig', array(
            'aNew' => $aNew,
            'aSignalement' => $aUserSignaler, 
            'aListSignalement' => $aListSignalement, 
            'aNewdescriptionGratuit' => $aNewdescriptionGratuit, 
            'aNewdescriptionAbonnes' => $aNewdescriptionAbonnes, 
            'aNewimageGratuit'=>  $aNewimageGratuit,
            'aNewimageAbonne'=>  $aNewimageAbonne,
            'aNewimageAlbum'=>  $aNewimageAlbum
        ));

    }

    public function messageAction()
    {
        //Recupération des données user et de son sexe
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $bSexe = !$oUserCurrent->getSexe(); // Sexe opposé
        //Récupération des centres d'interet et de leur catégorie pour généré leur formulaire
        $aCategorieCI = $oEm->getRepository('MCoreBundle:CategorieCentresinteret')->findAll();
        $aCentresInteret = $oEm->getRepository('MCoreBundle:Centresinteret')->FetchAllCentres();

        $aResult = array();
        $aCentreinteretSelected = array();

        $oFormFavorite = $this->createForm(new RechercheFavType($oUserCurrent->getId()));
        $oUser = new User();

        $oRequest = $this->get('request');
 
        if ($oRequest->getMethod() == 'POST') 
        {
             
            if ($oRequest->request->get("rechercher") || $oRequest->request->get("username") )
            {   
                $aRequest = $oRequest->request->get('m_corebundle_usertype');
                $sMessage = $aRequest['description'];
                unset($aRequest['description']); 


                if ($oRequest->request->get("rechercher"))
                {
                    $aRequest['username']=""; //on vide le usernam car il ne fais pas partie de la recherche
                } 


                 //Recupération des données user et de son sexe
                  $oUserCurrent = $this->get('security.context')->getToken()->getUser();
                  $bSexe = !$oUserCurrent->getSexe(); // Sexe opposé
                  $oEm = $this->getDoctrine()->getManager();
                  $aIdBloque = ( count($oUserCurrent->getContactbloque()) === 0 ) ? array('0') : array_keys($oUserCurrent->getContactbloque());
                  $aPagination = $this->get('Recherche')->recherche($aRequest,0,$oUserCurrent->getId(),1,$aIdBloque);
                  
                  
                  $aUsers = array();
                  $aNotifications = array();




                  foreach ($aPagination['Result'] as $key => $value) 
                  {
                    

                       $aUsers[$key] = $oEm->getRepository("MCoreBundle:User")->findOneById($value['id']);


                      if(is_object($aUsers[$key]))
                      {
                          $oNotif[$key] = new Notification();
                          $oNotif[$key]->setType(2);
                          $oNotif[$key]->setDateNotification(new \Datetime());
                          $oNotif[$key]->setUserNotifieur($oUserCurrent);
                          $oNotif[$key]->setUser($aUsers[$key]);

                          $oMessage[$key]  = new Messages(); 
                          $oMessage[$key]->setMessage($sMessage);
                          $oMessage[$key]->setFrom($oUserCurrent);
                          $oMessage[$key]->setTo($aUsers[$key]);
                          $oMessage[$key]->setNotification($oNotif[$key]);


                          $oEm ->persist($oNotif[$key]);
                          $oEm->persist($oMessage[$key]);
                      }
                     
                  }

                $oEm->flush();                               

               // return $this->render('MCoreBundle:Profile:resultat.html.twig', array('aResult' => $aResult));  

            }
            $iRecherchFav = $oRequest->request->get("m_corebundle_Recherchetype");
            if ($iRecherchFav['recherche'])
            {   

                $Favorite = $oRequest->request->get("m_corebundle_Recherchetype");
                // Ici on récupére l'id de la recherche
                $oRechercheFavorite = $oEm->getRepository('MCoreBundle:Recherche')->find($Favorite["recherche"]);
                //Service de recherche en fonction des données soumises on récupére que la liste des paramétres de recherche
                $oSessionSearch = $this->get('session');
                $oSessionSearch->set('Search',$oRechercheFavorite->getRecherche());
                return $this->redirect($this->generateUrl('m_core_profile_resultat'));
            }

            if ($oRequest->request->get("enregistrer"))
            {

                $oRechercheReq = $oRequest->request->get('m_corebundle_usertype');
                unset($oRechercheReq['_token']);
                $aNbrRecherche = $oEm->getRepository('MCoreBundle:Recherche')->FindCountSearch($oUserCurrent->getId());

                //Ici on vérifie le nombre de recherche enregistré

                if ((int)$aNbrRecherche <= 1000)
                {   //SI inferieur a 11 on creer une nouvelle recherche
                    //on verifie si le nom existe déja
                   
                    $aNomRecherche = $oEm->getRepository('MCoreBundle:Recherche')->FindNameExist($oUserCurrent->getId(),$oRequest->request->get("recherche"));
                    if (!empty($aNomRecherche)) 
                    {    
                        $this->get('session')->getFlashBag()->add('Nom','Cette Recherche Existe Déja');

                    }
                    else    
                    {     
                        $oRecherche = new recherche();
                        $oRecherche->setUser($oUserCurrent); 
                        $oRecherche->setRecherche($oRechercheReq);
                        $oRecherche->setNom($oRequest->request->get("recherche")); 
                        $oEm->persist($oRecherche);
                        $oEm->flush();                     
                        
                    }
                }    
                //sinon en écrase la recherche la plus ancienne !
                else
                {   
                    $aOldRecherche = $oEm->getRepository('MCoreBundle:Recherche')->FindOldestSearch($oUserCurrent->getId());
                    $oRecherche = $oEm->getRepository('MCoreBundle:Recherche')->find($aOldRecherche[0]['id']);
                    $oRecherche->setRecherche($oRechercheReq);
                    $oEm->persist($oRecherche);
                    $oEm->flush(); 
                }

                //Service Remplissage des champs de la recherche 

                $oUser = $this->get('Recherche')->SetRechercheInfo($oRechercheReq);
               
            }    
          
           
        }

        
        $oForm = $this->createForm(new RechercheAvanceType($bSexe), $oUser);

        //Appel du service qui reorganise les centres d'interet par cat nom et selected
        $aFormCentresInteret = $this->get('FonctionDiver')->Centresinteret($aCategorieCI,$aCentresInteret,$oUser->getCentresinteret());

        return $this->render('MCoreBundle:Admin:messages.html.twig', array('oFormFav' => $oFormFavorite->createView(),'oForm' => $oForm->createView(),'aFormCentresInteret' => $aFormCentresInteret));
    
    }

    public function eventAction()
    {
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        
        $oEm = $this->getDoctrine()->getManager();

        ///////
        $oRequest = $this->get('request');
        if($oRequest->getMethod() == 'POST')
        {
            $aRequest =$oRequest->request->get("m_corebundle_event");

            
            $oEvent = $oEm->getRepository('MCoreBundle:Events')->find($aRequest['id']);
            if (array_key_exists('affiche',$aRequest)) $oEvent->setAccueil(true);
            else $oEvent->setAccueil(false);
            

            $oEm->persist($oEvent);
            $oEm->flush(); 
        }

        // EVENT DEJA MODER
        $_aEventModerer = $oEm->getRepository('MCoreBundle:Events')->allEvents();
        $aInscrit = $oEm->getRepository('MCoreBundle:Eventsuser')->countEventsBysexe($oUserCurrent->getSexe());
        if(!is_array($aInscrit)) $aInscrit = array();
        $aEvents = $this->get('FonctionDiver')->setEventStatus($_aEventModerer,$aInscrit,$oUserCurrent->getSexe());
        $aEventModerer= $this->get('FonctionDiver')->encodeUrlsFromArray($aEvents);
        
        //EN COURS DE MODERATION
        $aEventNonModerer = $this->get('FonctionDiver')->encodeUrlsFromArray($oEm->getRepository('MCoreBundle:Events')->allEventsBystatus());

        // ANCIEN EVENT DEJA MODER
        $_aEventModerer = $oEm->getRepository('MCoreBundle:Events')->oldEventsBystatus();
        $aInscrit = $oEm->getRepository('MCoreBundle:Eventsuser')->countEventsBysexe($oUserCurrent->getSexe());
        if(!is_array($aInscrit)) $aInscrit = array();
        $aEvents = $this->get('FonctionDiver')->setEventStatus($_aEventModerer,$aInscrit,$oUserCurrent->getSexe());
        $aOldEventModerer= $this->get('FonctionDiver')->encodeUrlsFromArray($aEvents);

        return $this->render('MCoreBundle:Admin:events.html.twig', array(
            'aNouveau' => $aEventNonModerer,
            'aEvents' => $aEventModerer, 
            'aAncien' => $aOldEventModerer 

        ));
    
    }

    public function eventvoirAction($iId)
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $sRootProfile = $this->redirect($this->generateUrl('m_core_admin_event')); 
        $oRequest = $this->get('request');

        $oEvents = $oEm->getRepository('MCoreBundle:Events')->findOneById($iId);

        

        if (!($oEvents instanceof Events)) return $sRootProfile;
        $sDateevent = $oEvents->getDateEvent()->format('Y-m-d H:i:s');
         $dDate = $oEvents->getDateEvent();
        $oEvents->setDateEvent($sDateevent);
        $oForm = $this->createForm(new EventsType,$oEvents);

        if($oRequest->getMethod() == 'POST')
        {

            $oModeration = new Moderation();
            $oModeration->setModerateur($oUserCurrent);
            $oModeration->setDatemoderation(new \Datetime());

            if ($oRequest->request->get("m_corebundle_uservalide"))
            {
                    $aRequest = $oRequest->request->get("m_corebundle_uservalide");
                   

                    if (array_key_exists('valide',$aRequest))
                    { 
                        $oEvents->setDateEvent($dDate);
                        $oEvents->setStatus(1);
                        $this->get('Mail')->mailEventvalide($oEvents->getUser()->getUsername(), $oEvents->getUser()->getEmail());
                               
                    }

                   if (array_key_exists('rejeter',$aRequest))
                    { 
                        $oEvents->setDateEvent($dDate);
                        $oEm->flush(); 
                        $this->redirect($this->generateUrl('m_core_admin_event'));
                        $this->get('Mail')->mailEventrefuse($oEvents->getUser()->getUsername(), $oEvents->getUser()->getEmail(),$aRequest['message']);

                    }
                    $oModeration->setType("Sorties");

            }
            
            
            if ($oRequest->request->get("m_corebundle_usertype"))
            {
                $oForm->bind($oRequest);
                if ( $oForm->isValid() )        
                {

                    $oEvents->setDateEvent($dDate);
                    $oEvents->setImage($oRequest->get('ADD_upload0'));
                    $oEm->persist($oEvents);

                    $oModeration->setType("Creations Sortie");


                }
            }
            

            $oEm->persist($oModeration);
            $oEm->flush(); 

        }


       
        
        $dDateEvent = $this->get('FonctionDiver')->formatDate($dDate->format('Y-m-d'))." à ".$dDate->format('h')."h".$dDate->format('i');

        $aInscrit = $oEm->getRepository('MCoreBundle:Eventsuser')->EventByID($iId);
        $aInscrit = $this->get('FonctionDiver')->setEnligneUserToResult($aInscrit);

        $iTotalInscrit = count($aInscrit);
        if (!is_array($aInscrit)) $aInscrit = array();
        $aPlaces = $this->get('FonctionDiver')->setEventPlace($aInscrit,(int)$oEvents->getPlaces());

        //On Rajoute tout les contacts sauf ceux bloquer par l'utilisateur
        if (count($oUserCurrent->getContactbloque()) !== 0)
        {   

            foreach ($aInscrit as $key => $value) 
            {
               if (array_key_exists($aInscrit[$key]['id'],$oUserCurrent->getContactbloque())) unset($aInscrit[$key]);
            }

        }
        
        return $this->render('MCoreBundle:Admin:eventvoir.html.twig',
                                         array(
                                                'oEvents'       => $oEvents,
                                                'aInscrit'      => $aInscrit,
                                                'dDateEvent'      => $dDateEvent,
                                                'iTotalInscrit' => $iTotalInscrit,
                                                'oFormEvent' => $oForm->createView(),
                                                'aPlaces' => $aPlaces
                                                ));
    }


    public function actusAction($iId)
    {   
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oEm = $this->getDoctrine()->getManager();
        $oActu = new Actus() ;
        
        $oRequest = $this->get('request');

        $oForm = $this->createForm(new ActusType,$oActu);

        if($oRequest->getMethod() == 'POST')
        {
            $oModeration = new Moderation();
            $oModeration->setModerateur($oUserCurrent);
            $oModeration->setDatemoderation(new \Datetime());

            if ($oRequest->request->get("m_corebundle_actu"))
            {   
                $aRequest = $oRequest->request->get("m_corebundle_actu");
                if (array_key_exists('id',$aRequest)) $oActu = $oEm->getRepository('MCoreBundle:Actus')->find($aRequest['id']);
                   

                    $oActu->setImage($oRequest->get('ADD_upload0'));
                    $oActu->setTitre($aRequest['titre']);
                    $oActu->setSource($aRequest['date']);
                    $oActu->setDate($aRequest['source']);
                    $oActu->setPublication(new \Datetime($aRequest['publication']));
                    $oActu->setDescription($aRequest['description']);

                    if (array_key_exists('accueil',$aRequest)) $oActu->setAccueil($aRequest['accueil'][0]);
                    else $oActu->setAccueil(0);
                    if (array_key_exists('status',$aRequest))  $oActu->setStatus($aRequest['status'][0]);
                    else $oActu->setStatus(0);

                    $oModeration->setType("Creations Actus");
                    $oEm->persist($oModeration);

                    $oEm->persist($oActu);
                    $oEm->flush(); 

            }

            if ($oRequest->request->get("Actu"))
            {
                $aRequest = $oRequest->request->get("Actu");
                $oCurrentActu = $oEm->getRepository('MCoreBundle:Actus')->find($aRequest['id']);
                if (array_key_exists('supprimer',$aRequest) && is_object($oCurrentActu))
                { 

                    $oModeration->setType("Suppression Actus");
                    $oEm->persist($oModeration);
                        
                        $oEm->remove($oCurrentActu);
                        $oEm->flush(); 

                               
                }

                if (array_key_exists('modifier',$aRequest) && is_object($oCurrentActu))
                { 

                    $oModeration->setType("Modification Actus");
                    $oEm->persist($oModeration);
 
                    $oForm = $this->createForm(new ActusType,$oCurrentActu);
                    $oActu = $oCurrentActu;

                }    


            }
        }
        //Chargement des actus

        $aActus = $oEm->getRepository('MCoreBundle:Actus')->adminActus();

        return $this->render('MCoreBundle:Admin:actus.html.twig',
                                         array(
                                                'oActu'       => $oActu,
                                                'aActus'       => $aActus,
                                                'oFormActu' => $oForm->createView(),
                                                ));
        
    }


    public function pageAction()
    {
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oEm = $this->getDoctrine()->getManager();


        $oInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findOneById(1);

        $oRequest = $this->get('request');

        if ($oRequest->getMethod() == 'POST') 
        {

            $oModeration = new Moderation();
            $oModeration->setModerateur($oUserCurrent);
            $oModeration->setDatemoderation(new \Datetime());
          
  

            if ($oRequest->request->get("enligne")) 
            {     
                 $aRequest = $oRequest->request->get('enligne');
                 $oInfoAdmin->setOnlineuser($aRequest['EnLigne']);
                 $oInfoAdmin->setMembreinscrit($aRequest['membreinscrit']);
                 $oInfoAdmin->setNbrconnecte($aRequest['nbrconnecte']);
                 $oEm->persist($oInfoAdmin);
                 $oModeration->setType("Modification Membres en ligne");



            }

            if ($oRequest->request->get("promo")) 
            {     
                 $aRequest = $oRequest->request->get('promo');
                 $oInfoAdmin->setCodepromo($aRequest['codepromo']);
                 $oInfoAdmin->setReduction($aRequest['reduction']);
                 $oInfoAdmin->setPromodebut(new \Datetime($aRequest['promodebut']));
                 $oInfoAdmin->setPromofin(new \Datetime($aRequest['promofin']));
                 $oEm->persist($oInfoAdmin);
                 $oModeration->setType("Modification Promo");



            }

            if ($oRequest->request->get("baffiche")) 
            {     
                $aRequest = $oRequest->request->get('affiche');

                $oInfoAdmin->setimage($aRequest['ADD_upload0']);
                $oInfoAdmin->setLienaffiche($aRequest['lienaffiche']);
                $oEm->persist($oInfoAdmin);
                $oModeration->setType("Modification a L'affiche");


 

            }

            if ($oRequest->request->get("modifiertemoignage")) 
            {    
                $aRequestId = array_keys($oRequest->request->get("modifiertemoignage"));
                $aRequest = $oRequest->request->get("Temoignages");
                $aRequest = $aRequest[$aRequestId[0]];

                $oTemoignage = $oEm->getRepository('MCoreBundle:Temoignages')->find($aRequest['id']);
                 
                if(is_object($oTemoignage))
                {      
                    $oTemoignage->setimage($aRequest['ADD_upload']);
                    $oTemoignage->setTexte($aRequest['texte']);
                    $oTemoignage->setTitre($aRequest['titre']);
                    $oTemoignage->setUsername($aRequest['username']);
                    $oTemoignage->setSexe($aRequest['sexe']);
                    $oEm->persist($oTemoignage);
                    $oModeration->setType("Modification Temoignages");



                }
  
            }

            if ($oRequest->request->get("NewTemoignage")) 
            {    
                $aRequest = $oRequest->request->get("NewTemoignages");

                    $oTemoignage = new Temoignages();             
                    $oTemoignage->setimage($aRequest['ADD_upload1']);
                    $oTemoignage->setTexte($aRequest['texte']);
                    $oTemoignage->setTitre($aRequest['titre']);
                    $oTemoignage->setUsername($aRequest['username']);
                    $oTemoignage->setSexe($aRequest['sexe']);
                    $oEm->persist($oTemoignage);
                    $oModeration->setType("Creation Temoignages");



            }
           

            if ($oRequest->request->get("modifierpresse")) 
            {    
                $aRequestId = array_keys($oRequest->request->get("modifierpresse"));
                $aRequest = $oRequest->request->get("presse");
                $aRequest = $aRequest[$aRequestId[0]];

                $oPresse = $oEm->getRepository('MCoreBundle:Presse')->find($aRequest['id']);



                if(is_object($oPresse))
                {      
                    $oPresse->setimage($aRequest['ADD_upload']);
                    $oPresse->setTexte($aRequest['texte']);
                    $oPresse->setSource($aRequest['source']);
                    $oEm->persist($oPresse);
                    $oModeration->setType("Modification Presses");



                }
  
            }

            if ($oRequest->request->get("Newpresse")) 
            {    
                $aRequest = $oRequest->request->get("Newpresses");

                    $oPresse = new Presse();             
                    $oPresse->setimage($aRequest['ADD_upload2']);
                    $oPresse->setTexte($aRequest['texte']);
                    $oPresse->setSource($aRequest['source']);

                    $oEm->persist($oPresse);
                    $oModeration->setType("Creations Presse");


            }


            $oEm->persist($oModeration);
            $oEm->flush();    
        }

        $aPresse = $oEm->getRepository('MCoreBundle:Presse')->findAll();
        $aTemoignage = $oEm->getRepository('MCoreBundle:Temoignages')->findAll();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        return $this->render('MCoreBundle:Admin:page.html.twig', array(
            'temoignage' => $aTemoignage,
            'presses' => $aPresse,
            'oInfoAdmin' => $oInfoAdmin
        ));
    }


    public function ModerateurAction()
    {

        //Recupération des données user et de son sexe
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oRequest = $this->get('request');
        
         if ($oRequest->getMethod() == 'POST') 
        {   
                if ($oRequest->request->get("nouveau"))
                {   
                       $aRequest=$oRequest->request->get("nouveau");
                       $oEncodeFactory = $this->container->get('security.encoder_factory');
                       $oModerateur = new User();

                       $oEncoder = $oEncodeFactory->getEncoder($oModerateur);

  
                       $oModerateur->setRoles(array('ROLE_MODERATEUR'));
                       $oModerateur->setDescriptionModerer(1);

                       $oModerateur->setActif(1);
                       $oModerateur->setSexe($aRequest['sexe']);
                       $oModerateur->setEmail($aRequest['mail']);
                       $oModerateur->setUsername($aRequest['username']);
                       $oModerateur->setVille($oEm->getRepository("MCoreBundle:Ville")->findOneById(1));
                       $oModerateur->setNationalite($oEm->getRepository("MCoreBundle:Nationalite")->findOneById(1));
                       $oModerateur->setProfession($oEm->getRepository("MCoreBundle:Profession")->findOneById(1));
                       $oModerateur->setPassword($oEncoder->encodePassword($aRequest['mdp'], $oModerateur->getSalt()));
                       $oModerateur->setCodeActivation($aRequest['mdp']);

                       $oEm->persist($oModerateur);
                       $oEm->flush($oModerateur);        

                       $oImages = new Images();
                       $oImages->setProfile(1);
                       $oImages->setUser($oModerateur);
                       $oImages->setProfileDefault(1);
                       $oImages->setModerer(1);
                       $oImages->setImage($oRequest->get('ADD_upload4'));

                       $oEm->persist($oImages);
                }

                if ($oRequest->request->get("supprimer"))
                {   
                    $aRequest=$oRequest->request->get("supprimer");
                    $oUser = $oEm->getRepository("MCoreBundle:User")->findOneById($aRequest['id']);

                    if (is_object($oUser))
                    {   
                            $oUser->setActif(0);
                            $oUser->setRoles(array('ROLE_DELETED'));
                    }
                       

                                 
                }
                $oEm->flush();    
                
        }
        $aModerateurs = $oEm->getRepository('MCoreBundle:User')->findModerateur();
        $aTraitement = $oEm->getRepository('MCoreBundle:Moderation')->findtodayModeration();

        return $this->render('MCoreBundle:Admin:moderateurs.html.twig', array(
            'aModerateurs' => $aModerateurs,
            'aTraitement' => $aTraitement
        ));
    }

    public function moderateurvoirAction($iId)
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $sRootProfile = $this->redirect($this->generateUrl('m_core_admin_moderateur')); 
        $oRequest = $this->get('request');

        $oUser = $oEm->getRepository('MCoreBundle:User')->find($iId);

        if (!($oUser instanceof User)) return $sRootProfile;

        $aTraitement = $oEm->getRepository('MCoreBundle:Moderation')->findModerationByUser($oUser->getId(),date('Y-m-d'),date('Y-m-d'));

        if($oRequest->getMethod() == 'POST')
        {

           $aRequest = $oRequest->request->get('m_corebundle_usertype');
           $aTraitement = $oEm->getRepository('MCoreBundle:Moderation')->findModerationByUser($oUser->getId(),$aRequest['inscriptiondu'],$aRequest['inscriptionau']);

        }
            

        return $this->render('MCoreBundle:Admin:moderationvoir.html.twig',
                                         array(
                                                'oUser'       => $oUser,
                                                'aTraitement' => $aTraitement,
                                                ));
    }

    public function StatsAction()
    {

        //Recupération des données user et de son sexe
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $bSexe = !$oUserCurrent->getSexe(); // Sexe opposé
        //Récupération des centres d'interet et de leur catégorie pour généré leur formulaire
        $aCategorieCI = $oEm->getRepository('MCoreBundle:CategorieCentresinteret')->findAll();
        $aCentresInteret = $oEm->getRepository('MCoreBundle:Centresinteret')->FetchAllCentres();

        $aResult = array();
        $aCentreinteretSelected = array();

        $oFormFavorite = $this->createForm(new RechercheFavType($oUserCurrent->getId()));
        $oUser = new User();

        $oRequest = $this->get('request');
 

        if ($oRequest->getMethod() == 'POST') 
        {

            if ($oRequest->request->get("rechercher") || $oRequest->request->get("username") )
            {   
                $aRequest = $oRequest->request->get('m_corebundle_usertype');


                $oSessionSearch = $this->get('session');
                $oSessionSearch->set('Stats',$aRequest);
                return $this->redirect($this->generateUrl('m_core_admin_resultat'));
                  

            }

           
        }

        
        $oForm = $this->createForm(new RechercheAvanceType($bSexe), $oUser);

        //Appel du service qui reorganise les centres d'interet par cat nom et selected
        $aFormCentresInteret = $this->get('FonctionDiver')->Centresinteret($aCategorieCI,$aCentresInteret,$oUser->getCentresinteret());

        return $this->render('MCoreBundle:Admin:stats.html.twig', array('oForm' => $oForm->createView(),'aFormCentresInteret' => $aFormCentresInteret));
        
    }

    public function resultatAction($iPage)
    {   

        $oSessionSearch = $this->get('session');
        if (!is_array($oSessionSearch->get('Stats'))) return $this->redirect($this->generateUrl('m_core_admin_stats'));
        //Recupération des données user et de son sexe
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $bSexe = !$oUserCurrent->getSexe(); // Sexe opposé
        $oEm = $this->getDoctrine()->getManager();
        $aIdBloque = ( count($oUserCurrent->getContactbloque()) === 0 ) ? array('0') : array_keys($oUserCurrent->getContactbloque());
        //Résultat finale avec paramétre de pagination
        $aPagination = $this->get('Recherche')->recherche($oSessionSearch->get('Stats'),$bSexe,$oUserCurrent->getId(),(int)$iPage,$aIdBloque);

        $oFonctionDiver = $this->get('FonctionDiver');

        // Resize Images
        foreach ($aPagination['Result'] as $iKey => $sValue) 
        {
            $aPagination['Result'][$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 76, 97);
        }

        return $this->render('MCoreBundle:Admin:resultat.html.twig', array(
                            'iPage'     => $iPage,
                            'aResult'    => $aPagination['Result'],
                            'iTotalPage'=> $aPagination['iTotalPages'],
                            'iTotalResult'=> $aPagination['count']
                             )); 

    }
}