<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NotificationsController extends Controller
{

    public function coeurAction($iPage)
    {   
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $aIdBloque = ( count($oUserCurrent->getContactbloque()) === 0 ) ? array('0') : array_keys($oUserCurrent->getContactbloque());

        // On calcule le nombre total de resultat
        $iTotalResult = $oEm->getRepository('MCoreBundle:Notification')->CountUserNotificationByType($oUserCurrent->getId(),1,$aIdBloque);
        // On utilise le service pour connaitre le premier résultat a affiché
       
        $iNbrResultPerPage = 20; // Nombre de resultat par affichage
        $oPaginator = $this->get("Paginator")->getPagination((int)$iTotalResult,$iNbrResultPerPage, (int)$iPage);
        //Résultat finale avec paramétre de pagination
        $_aResult = $oEm->getRepository('MCoreBundle:Notification')->FindUserNotificationByType($oUserCurrent->getId(),1,$aIdBloque,$iNbrResultPerPage,$oPaginator['FirstResultPerCurrentPage']);
        //On ajoute les signes astrologique et on convertie les date en lettre a partir du service
        $aResult = $this->get('FonctionDiver')->setSigneToResult($_aResult);
        //On ajoute les date en lettre
        $aResult = $this->get('FonctionDiver')->setDateTimeToString($aResult);
        //On verifie les users en ligne 
        $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($aResult);
        
      

        //On récupére les id a  mettre a jour
        $aArrayID = $this->get('FonctionDiver')->getIdsNotViewed($aResult);

        //On change le statut de la notification en vue !
        $oEm->getRepository('MCoreBundle:Notification')->UpdateNotificationViewed($aArrayID ,1);

        return $this->render('MCoreBundle:Profile:coupdecoeur.html.twig', array(
                            'iPage'     => $iPage,
                            'aResult'   => $aResult,
                            'iTotalPage'=> $oPaginator['iTotalPages']));
    }

    public function visitesAction($iPage)
    {   
        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $aIdBloque = ( count($oUserCurrent->getContactbloque()) === 0 ) ? array('0') : array_keys($oUserCurrent->getContactbloque());

        // On calcule le nombre total de resultat
        $iTotalResult = $oEm->getRepository('MCoreBundle:Notification')->CountUserNotificationByType($oUserCurrent->getId(),0,$aIdBloque);
        /*echo "<pre>" . print_r($oEm->getRepository('MCoreBundle:Notification')->FindNotificationNotViewed($oUserCurrent->getId()), true) . "</pre>";
                   die();*/
        // On utilise le service pour connaitre le premier résultat a affiché
        $iNbrResultPerPage = 20; // Nombre de resultat par affichage
        $oPaginator = $this->get("Paginator")->getPagination((int)$iTotalResult,$iNbrResultPerPage, (int)$iPage);

        //Résultat finale avec paramétre de pagination
        $_aResult = $oEm->getRepository('MCoreBundle:Notification')->FindUserNotificationByType($oUserCurrent->getId(),0,$aIdBloque,$iNbrResultPerPage,$oPaginator['FirstResultPerCurrentPage']);

        //On ajoute les signes astrologique et on convertie les date en lettre a partir du service
        $aResult = $this->get('FonctionDiver')->setSigneToResult($_aResult);
        $aResult = $this->get('FonctionDiver')->setDateTimeToString($aResult);
        //On verifie les users en ligne 
        $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($aResult);

        //On récupére les id a  mettre a jour
        $aArrayID = $this->get('FonctionDiver')->getIdsNotViewed($aResult);

        //On change le statut de la notification en vue !
        $oEm->getRepository('MCoreBundle:Notification')->UpdateNotificationViewed($aArrayID ,0);

        return $this->render('MCoreBundle:Profile:visite.html.twig', array(
                            'iPage'     => $iPage,
                            'aResult'    => $aResult,
                            'iTotalPage'=> $oPaginator['iTotalPages']));

    }
}