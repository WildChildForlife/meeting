<?php

namespace M\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use M\CoreBundle\Entity\Signalement;
use M\CoreBundle\Entity\User;

class AjaxController extends Controller
{
   
    public function setCoeurAction()
    {
        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            if ($this->get('security.context')->isGranted('ROLE_FREE')) { return new Response('permissionDenied'); }
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();
            // a inserer dans la condition en dessous

            $oEm = $this->getDoctrine()->getManager();
            $oRepository = $oEm->getRepository('MCoreBundle:User');
            $sUser = $oRequest->request->get('username');
            if(!is_string($sUser) || strlen($sUser) > 40 ) return false;
            //$sUser = mysql_real_escape_string($sUser);//On reformate le string pour evité les injection
            // Le username de la personne qui le reçois
            $oUser = $oRepository->findOneByUsername($sUser);
            if(!is_object($oUser) || !is_object($oUserCurrent) ) return false;

            if(!$this->get('security.context')->isGranted('ROLE_ADMIN'))
            $bResult = $this->get('IncrementNotification')->incrementNotification(1,$oUser,$oUserCurrent);

            $dDelay = new \DateTime();
            $dDelay->setTimestamp(strtotime('10 minutes ago'));
            $oUser->setEnLigne(($oUser->getLastactivity() > $dDelay ) ? 1 : 0 );
       
             /*   Servie d'envoi de mail   */
            if (array_key_exists('cpc', $oUser->getAlertemail()) && $oUser->getEnLigne() == 0 ) 
                $this->get('Mail')->mailCpc($oUser->getUsername(),$oUserCurrent->getUsername(),$oUser->getEmail());
        
            return ( $bResult ) ? new Response('success') : new Response('failure');
        }
    }

    public function setNewContactAction()
    {

        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser(); 

            $oEm = $this->getDoctrine()->getManager();
            $oRepository = $oEm->getRepository('MCoreBundle:User');

            $sUsername = $oRequest->request->get('username');
            $sTypeOperation = $oRequest->request->get('typeOP');

            if(!is_string($sUsername) || strlen($sUsername) > 40 || !is_string($sTypeOperation)) return false;
            //$oUserAjouter = mysql_real_escape_string($sUsername);//On reformate le string pour evité les injection
            // Le username de la personne qui le reçois
            $oUserAjouter = $oRepository->findOneByUsername($sUsername);

            //On vérifie que les Users existe bien
            if(!is_object($oUserAjouter) || !is_object($oUserCurrent)) return false;//Doit retourné autre chose!!!

            //On verifie si le contact exisit déja dans la liste sinon on le rajoute
            $iDUser = $oUserAjouter->getId();
            
            $aCurrentContact = ($oUserCurrent->getContact()) ? $oUserCurrent->getContact() : array();
            $bResult = false;
            if(is_array($aCurrentContact))
            {
                //On verifie que L'id n'existe pas dans le tableau
                if (!array_key_exists($iDUser, $aCurrentContact) && $sTypeOperation === 'add')
                { 
                    $aCurrentContact[$iDUser] = '';
                    $oUserCurrent->setContact($aCurrentContact); 
                    $bResult = true;
                   
                }
                //Si elle existe
                else if (array_key_exists($iDUser, $aCurrentContact) && $sTypeOperation === 'delete')
                {
                    unset($aCurrentContact[$iDUser]);
                    $oUserCurrent->setContact($aCurrentContact); 
                    $bResult = true;
                }   

            }  
            
            $oEm->persist($oUserCurrent);
            $oEm->flush();

            return ( $bResult ) ? new Response('success') : new Response('failure');
            
        }
    }

    public function allowAlbumAction()
    {

        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser(); 

            $oEm = $this->getDoctrine()->getManager();
            $oRepository = $oEm->getRepository('MCoreBundle:User');

            $sUsername = $oRequest->request->get('username');
            $sTypeOperation = $oRequest->request->get('typeOP');

            if(!is_string($sUsername) || strlen($sUsername) > 40 || !is_string($sTypeOperation)) return false;
            //$oUserAjouter = mysql_real_escape_string($sUsername);//On reformate le string pour evité les injection
            // Le username de la personne qui le reçois
            $oUserAjouter = $oRepository->findOneByUsername($sUsername);

            //On vérifie que les Users existe bien
            if(!is_object($oUserAjouter) || !is_object($oUserCurrent)) return false;//Doit retourné autre chose!!!

            //On verifie si le contact exisit déja dans la liste sinon on le rajoute
            $iDUser = $oUserAjouter->getId();
            
            $aCurrentContact = ($oUserCurrent->getAllowToAlbum()) ? $oUserCurrent->getAllowToAlbum() : array();
            $bResult = false;
            if(is_array($aCurrentContact))
            {
                //On verifie que L'id n'existe pas dans le tableau
                if (!array_key_exists($iDUser, $aCurrentContact) && $sTypeOperation === 'add')
                { 
                    $aCurrentContact[$iDUser] = '';
                    $oUserCurrent->setAllowToAlbum($aCurrentContact); 
                    $bResult = true;
                }
                //Si elle existe
                else if (array_key_exists($iDUser, $aCurrentContact) && $sTypeOperation === 'delete')
                {
                    unset($aCurrentContact[$iDUser]);
                    $oUserCurrent->setAllowToAlbum($aCurrentContact); 
                    $bResult = true;
                }  
                $bResult = true; 

            }  
            
            $oEm->persist($oUserCurrent);
            $oEm->flush();

            return ( $bResult ) ? new Response('success') : new Response('failure');
            
        }
    }

    

    public function bloquerContactAction()
    {

        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser(); 

            $oEm = $this->getDoctrine()->getManager();
            $oRepository = $oEm->getRepository('MCoreBundle:User');

            $sUsername = $oRequest->request->get('username');
            $sTypeOperation = $oRequest->request->get('typeOP');

            if(!is_string($sUsername) || strlen($sUsername) > 40 || !is_string($sTypeOperation)) return false;
            //$oUserAjouter = mysql_real_escape_string($sUsername);//On reformate le string pour eviter les injection
            // Le username de la personne qui le reçois
            $oUserAjouter = $oRepository->findOneByUsername($sUsername);

            //On vérifie que les Users existe bien
            if(!is_object($oUserAjouter) || !is_object($oUserCurrent)) return false;//Doit retourné autre chose!!!

            //On verifie si le contact exisit déja dans la liste sinon on le rajoute
            $iDUser = $oUserAjouter->getId();
            
            $aCurrentBloquer = ($oUserCurrent->getContactBloque()) ? $oUserCurrent->getContactBloque() : array();
            $bResult = false;
            if(is_array($aCurrentBloquer))
            {
                //On verifiedsds que L'id n'existe pas dans le tableau
                if (!array_key_exists($iDUser,$aCurrentBloquer) && $sTypeOperation === 'bloquer')
                { 
                    $aCurrentBloquer[$iDUser] = '';
                    $oUserCurrent->setContactBloque($aCurrentBloquer); 
                    $bResult = true;
                   
                }
                //Si elle existe
                else if (array_key_exists($iDUser, $aCurrentBloquer) && $sTypeOperation === 'debloquer')
                {
                    //ON rajoute un autre signalement 
                    //Ou en dis que vous l'avez déja signaler
                    unset($aCurrentBloquer[$iDUser]);
                    $oUserCurrent->setContactBloque($aCurrentBloquer); 
                    $bResult = true;
                }   

            }  
            
            $oEm->persist($oUserCurrent);
            $oEm->flush();

            return ( $bResult ) ? new Response('success') : new Response('failure');
            
        }
    }
    public function signalContactAction()
    {
        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();
            // a inserer dans la condition en dessous
            if ($this->get('security.context')->isGranted('ROLE_FREE')) { return new Response('permissionDenied'); }
            $oEm = $this->getDoctrine()->getManager();
            $oRepository = $oEm->getRepository('MCoreBundle:User');
            $sUserSignaler = $oRequest->request->get('username');
            $sMessageSignalement = $oRequest->request->get('message');
            //$sTypeOperation = $oRequest->request->get('typeOP');

            if(!is_string($sUserSignaler) || strlen($sUserSignaler) > 40 || !is_string($sMessageSignalement) || empty($sMessageSignalement) ) new Response('failure');
            //$sUser = mysql_real_escape_string($sUser);//On reformate le string pour evité les injection

            
            // Le username de la personne qui le reçois
            $oUserSignaler = $oRepository->findOneByUsername($sUserSignaler);
            if(!is_object($oUserSignaler) || !is_object($oUserCurrent)) new Response('failure');

            $oSignalenment = new Signalement();
            $oSignalenment ->setUserSignaleur($oUserCurrent);
            $oSignalenment ->setUserSignaler($oUserSignaler);                
            $oSignalenment ->setMessage($sMessageSignalement); 
            $oSignalenment ->setAverti(0); 
            $oEm->persist($oSignalenment);

            $oUserSignaler->setSignalementModerer(0); 
            $oEm->persist($oUserSignaler);

            $oEm->flush();

 

            $bResult = true;
            return ( $bResult ) ? new Response('success') : new Response('failure');
        }
    }
    public function getChatContactsAction()
    {
        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();
            $oEm = $this->getDoctrine()->getManager();
            $oRepository = $oEm->getRepository('MCoreBundle:User');

            $aParams = array();
            $bContacts = ( $oRequest->request->get('bContacts') ) ? true : false;

            if ( $oRequest->request->get('iAgeDebut')) $aParams['iAgeDebut'] = $oRequest->request->get('iAgeDebut');
            if ( $oRequest->request->get('iAgeFin'))   $aParams['iAgeFin']   = $oRequest->request->get('iAgeFin');
            if ( $oRequest->request->get('iVille'))    $aParams['iVille']    = $oRequest->request->get('iVille');

            $sUsername = ( $oRequest->request->get('sUsername') ) ? $oRequest->request->get('sUsername') : null;

            if ( !isset($aParams['iAgeDebut']) &&  isset($aParams['iAgeFin'])) $aParams['iAgeDebut'] = $aParams['iAgeFin'];
            if ( !isset($aParams['iAgeFin']) &&  isset($aParams['iAgeDebut'])) $aParams['iAgeFin'] = $aParams['iAgeDebut'];

            $aContacts = $oRepository->FetchOnlineUser(
                    $oUserCurrent->getId(),
                    $sUsername,
                    $bContacts,
                    $aParams
                );

            $aContacts = $this->get('FonctionDiver')->generateUrlsFromArray($aContacts);

            foreach ($aContacts as $iKey => $sValue) 
            {
                $aContacts[$iKey]['salt']       = base64_encode('rencontres' . (string) $sValue['salt']);
                $aContacts[$iKey]['idEncoded']  = base64_encode( (int) $sValue['id'] * 9999);
            }

            $oFonctionDiver = $this->get('FonctionDiver');

            // Resize Images
            foreach ($aContacts as $iKey => $sValue) 
            {
                $aContacts[$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 40, 40);
            }

            return new Response(json_encode($aContacts));
        }
    }
    public function demandeDeChatAction()
    {
        $oRequest = $this->get('request');
        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();
            $oEm = $this->getDoctrine()->getManager();

            // Rajouter une notification
            if ( $oRequest->request->get('username') )
            {
                $sUsername = $oRequest->request->get('username');
                $oUser = $oEm->getRepository('MCoreBundle:User')->findOneByUsername($sUsername);
                $oMessage = $oEm->getRepository('MCoreBundle:Messages')->findOneBy(
                                                            array(
                                                                'from' => $oUser,
                                                                'to'   => $oUserCurrent
                                                                ),
                                                            array('dateEnvoi' => 'DESC')
                                                            );
                $oMessage->setLu(false);
                $oMessage->getNotification()->setVue(false);
                $oEm->persist($oMessage);
                $oEm->flush();
            }

            else
            {

            }
            $oRepository = $oEm->getRepository('MCoreBundle:Notification');

            $aIdBloque = (count($oUserCurrent->getContactbloque()) > 0) ? array_keys($oUserCurrent->getContactbloque()) : array(0);

            return new Response($oRepository->countByType($oUserCurrent->getId(), 3, $aIdBloque));
        }
    }
    public function demandeDeChatMessagesAction()   
    {
        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();
            $oEm = $this->getDoctrine()->getManager();
            $oRepository = $oEm->getRepository('MCoreBundle:Messages');

            $oFonctionDiver = $this->get('FonctionDiver');

            $aResult = $oRepository->GetMessagesChatReceivedByUserId($oUserCurrent->getId());
            //On verifie les users en ligne
            $aResult = $oFonctionDiver->setEnligneUserToResult($aResult);
            // on formate les dates
            $aResult = $oFonctionDiver->setDateTimeToStringFromMessage($aResult);

            // Resize Images
            foreach ($aResult as $iKey => $sValue) 
            {
                $aResult[$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 40, 40);
            }

            if( $this->get('security.context')->isGranted('ROLE_FREE') )
            {
                foreach ($aResult as $iKey => $sValue) 
                {
                    $aResult[$iKey]['Message'] = 'Souhaite tchater avec vous.';
                }
            }

            return new Response(json_encode($aResult));
        }
    }

    public function updateDemandeChatMessageAction()
    {
        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();

            $oEm = $this->getDoctrine()->getManager();

            $oUserSender = $oEm->getRepository('MCoreBundle:User')->findOneByUsername($oRequest->request->get('sUsername'));
                                        
            if ( !is_object($oUserSender) ) return new Response(json_encode(array('error')));
            $oRepositoryNotification = $oEm->getRepository('MCoreBundle:Notification');
            $oRepositoryMessages = $oEm->getRepository('MCoreBundle:Messages');

            //On change le statut de la notification de message en vue !
            /*$oRepositoryNotification->UpdateNotificationViewedPerSender(
                    $oUserCurrent->getId(),
                    $oUserSender->getId(),
                    3
                );*/
            $aMessages = $oRepositoryMessages->findBy(array('from' => $oUserSender->getId(), 'to' => $oUserCurrent->getId(), 'chat' => true));

            foreach ($aMessages as $iKey => $sValue) 
            {
                $sValue->setLu(true);
                $sValue->getNotification()->setVue(true);
                $oEm->persist($sValue);
            }

            $oEm->flush();

            //return new Response(json_encode(array($oUserCurrent->getId(),$oUserSender->getId() )));
            $aIdBloque = (count($oUserCurrent->getContactbloque()) > 0) ? array_keys($oUserCurrent->getContactbloque()) : array(0);

            return new Response($oRepositoryNotification->countByType($oUserCurrent->getId(), 3, $aIdBloque));

            //return new Response(json_encode(array('success')));
        }
    }

    public function checkUsernameExistsAction()
    {
        $oRequest = $this->get('request');

        if( $oRequest->isXmlHttpRequest() )
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();

            $oEm = $this->getDoctrine()->getManager();
            $oUserToCheck = $oEm->getRepository('MCoreBundle:User')->findOneByUsername($oRequest->request->get('sUsername'));
                                        
            if ( is_object($oUserToCheck) ) return new Response('error');
            return new Response('success');
        }
    }

    public function addinchatboxAction()
    {
        $oRequest = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        if($oRequest->isXmlHttpRequest())
        {
            $sender = $oRequest->request->get('sender');
            $receiver = $oRequest->request->get('receiver');

            $oUser = $em->getRepository('MCoreBundle:User')->findOneByUsername($receiver);
            // return new Response($receiver);
            if($oUser instanceof User)
            {
                $aUsers  = ($oUser->getInchatBox()) ? $oUser->getInchatBox() : array();
                if (in_array($sender,$aUsers) === false)
                {
                array_push($aUsers, $sender);
                $oUser->setInchatBox($aUsers);
                $em->persist($oUser);
                $em->flush();
                }

                return new Response('success');
            }
            return new Response('failure  '.$oUser->getUsername());
        }
    }

    public function getlastmessagechatAction()
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        if($request->isXmlHttpRequest())
        {
            $data = array();
          $idlastmessage = $request->request->get('lastmessage'); 
          $username = $request->request->get('username');
          $oUser = $em->getRepository('MCoreBundle:User')->findOneByUsername($username);
          $messages = $em->getRepository('MCoreBundle:Messages')
                         ->getLastesMessages($oUserCurrent,$oUser,$idlastmessage);
        $data['lastexist'] = (!empty($messages)) ? 'yes' : 'none' ;
        $data['salt']       = base64_encode('rencontres' . (string) $oUser->getSalt());
        $data['idEncoded']  = base64_encode( (int) $oUser->getId() * 9999);            
        return new Response (json_encode($data));
        }
        
    }

    public function deletefromchatboxAction()
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        if($request->isXmlHttpRequest())
        {
            $username = $request->request->get('username');
            $actuel = $request->request->get('actuel');

            $oUser = $em->getRepository('MCoreBundle:User')->findOneByUsername($username);

            $aUsers = $oUser->getInchatBox() ;
                if(in_array($actuel,$aUsers))
            {
                for ($i= 0 ; $i<count($aUsers) ; $i++ )
                {
                    if($aUsers[$i] === $actuel) {unset($aUsers[$i]);}
                }
                
            } 
            $aUsers1 = array_values($aUsers);
            $oUser->setInchatBox($aUsers1);
            $em->persist($oUser);
            $em->flush();

            return new Response ('success');
        }
    }

    

}