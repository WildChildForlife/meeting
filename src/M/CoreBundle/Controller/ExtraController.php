<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use M\CoreBundle\Entity\User;

class ExtraController extends Controller
{
    public function voirAction($sUser)
    {
        $oEm = $this->getDoctrine()->getManager();

        $sRootProfile = $this->redirect($this->generateUrl('m_core_profile'));
        if(empty($sUser) || !is_string($sUser)) return $sRootProfile;
        //Recupération des données user et de son sexe
        $oCurrentUser = $this->get('security.context')->getToken()->getUser();

        //On cherche si le user existe
        $oUser = $oEm->getRepository("MCoreBundle:User")->findOneByUsername($sUser);

        // Si le user n'existe pas a on retourne au dashbord 
        if (!($oUser instanceof User) || $oCurrentUser->getSexe() === $oUser->getSexe() || $oUser->getActif() == 0) return $sRootProfile;
        
        $sRole = $oUser->getRoles();
        
        if ($sRole[0] == "ROLE_ADMIN" || $sRole[0] == "ROLE_MODERATEUR") return $sRootProfile;
    
        
        //on recupere les categories de centre d'interets
        $aCategorieCI = $oEm->getRepository('MCoreBundle:CategorieCentresinteret')->findAll();
        //on recupere les question est reponse grace au service
        $aQuestions = $oEm->getRepository('MCoreBundle:Questions')->findAll();
        $aQuestionsreponse = $this->get('FonctionDiver')->Questions($oUser->getSexe(),$aQuestions,(array)$oUser->getQuestions());
        $aProfilIdeal =  $oEm->getRepository("MCoreBundle:UserIdeal")->findOneById($oUser->getUserideal());

        //on recupere l'image du profil que l'on veut voir
        $aImage = $oEm->getRepository('MCoreBundle:Images')->getProfilByUser($oUser->getId());

        //On Verifie si l'utilisateur est en ligne
        $dDelay = new \DateTime();
        $dDelay->setTimestamp(strtotime('10 minutes ago'));
        $oUser->setEnLigne(($oUser->getLastactivity() > $dDelay ) ? 1 : 0 );

        //On convertie les date en lettre
        $aDateToString = array();
        //On regroupe les date dans un tableau pour evité plusieur allé retour vers le service;
        $aDateToString['lastActivity'] = $oUser->getLastActivity();
        $aDateToString['inscritDepuis'] = $oUser->getDateInscription();
        $aDateToString=$this->get('FonctionDiver')->setDateTimeToStringFromUser($aDateToString);

        $aAlbum = $oEm->getRepository('MCoreBundle:Images')->getAlbumByUser($oUser->getId());
        
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN')) 
        {

            //Appel du service d'Incrementation des notification ici 0 pour les visites 
            $this->get('IncrementNotification')->incrementNotification(0,$oUser,$oCurrentUser);
            if (array_key_exists('visite', $oUser->getAlertemail()) && $oUser->getEnLigne() == 0 ) $this->get('Mail')->mailVisite($oUser->getUsername(),$oCurrentUser->getUsername(),$oUser->getEmail());
            
        }



        return $this->render('MCoreBundle:Profile:voir.html.twig', array(
                                    'oUser' => $oUser , 
                                    'aImage' => $aImage,
                                    'aDate' => $aDateToString,
                                    'aCategorieCI' =>  $aCategorieCI,
                                    'aAlbum' =>  $aAlbum,                                    
                                    'aQuestionsreponse' =>  $aQuestionsreponse,
                                    'aProfilIdeal' =>  $aProfilIdeal)
        );
    }
    public function resultatAction($iPage)
    {   

        $oSessionSearch = $this->get('session');

        if (!is_array($oSessionSearch->get('Search'))) return $this->redirect($this->generateUrl('m_core_profile_recherche'));
        //Recupération des données user et de son sexe
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $bSexe = !$oUserCurrent->getSexe(); // Sexe opposé
        $oEm = $this->getDoctrine()->getManager();
        $aIdBloque = ( count($oUserCurrent->getContactbloque()) === 0 ) ? array('0') : array_keys($oUserCurrent->getContactbloque());
        //Résultat finale avec paramétre de pagination
        $aPagination = $this->get('Recherche')->recherche($oSessionSearch->get('Search'),$bSexe,$oUserCurrent->getId(),(int)$iPage,$aIdBloque);

        $oFonctionDiver = $this->get('FonctionDiver');

        // Resize Images
        foreach ($aPagination['Result'] as $iKey => $sValue) 
        {
            $aPagination['Result'][$iKey]['image'] = $oFonctionDiver->buildThumbnail($sValue['image'], 76, 97);
        }

        return $this->render('MCoreBundle:Profile:resultat.html.twig', array(
                            'iPage'     => $iPage,
                            'aResult'    => $aPagination['Result'],
                            'iTotalPage'=> $aPagination['iTotalPages'],
                            'iTotalResult'=> $aPagination['count']
                             )); 

    }

    public function sendContactFormAction()
    {
        $oRequest = $this->get('request');
        $oSession = $this->get('session');
        if ( $oRequest->getMethod() === 'POST' )
        {
            $aRequest = $oRequest->request;

            $this->get('Mail')->mailContact($aRequest->all());

            $oSession->getFlashBag()->add('success', 'Votre formulaire de contact a été envoyé avec succés, merci de votre collaboration'); 
        }
        return $this->redirect($oRequest->headers->get('referer'));
    }
    
    public function forgotPasswordAction()
    {
        $oRequest = $this->get('request');
        $oSession = $this->get('session');
        $oEm = $this->getDoctrine()->getManager();


        if ( $oRequest->getMethod() === 'POST' )
        {
            $oRepository = $this->getDoctrine()->getManager()->getRepository('MCoreBundle:User');
            $oUser = $oRepository->loadUserByUsername($oRequest->request->get('usernameOrEmail'), false);
            if ( $oUser )
            {
                $sCode = str_shuffle(base64_encode($oUser->getUsername().$oUser->getEmail()));
                $oUser->setCodeRecuperation($sCode);
                $oEm->persist($oUser);
                $oEm->flush(); 


                $this->get('Mail')->mailMdp($oUser->getUsername(),$oUser->getEmail(),$sCode);

                $oSession->getFlashBag()->add('success', 'Un email de récupération de mot de passe vous a été envoyé sur votre adresse email.');
            }
            else $oSession->getFlashBag()->add('error', 'Pseudo/Email : Le pseudo/Email que vous avez saisi est introuvable.');
        }
        return $this->redirect($this->generateUrl('m_core_homepage'));
    }
    public function newPasswordAction()
    {
        $oRequest = $this->get('request');
        $oSession = $this->get('session');

        $oEm = $this->getDoctrine()->getManager();

        if ( $oRequest->getMethod() === 'POST' )
        {
            $oRepository = $this->getDoctrine()->getManager()->getRepository('MCoreBundle:User');
            $oUser = $oRepository->findOneBy(array('code_recuperation' => $oRequest->request->get('recuperation')));
            if ( $oUser )
            {
                $sPassword = $oRequest->request->get('nouveau_passe');
                $oEncodeFactory = $this->container->get('security.encoder_factory');

                $oEncoder = $oEncodeFactory->getEncoder($oUser);

                if ($sPassword === $oRequest->request->get('confirmation_passe'))
                {
                    if ( preg_match('/^(?=.*?[A-Za-z])(?=.*?[0-9]).{8,}$/', $sPassword) )
                    {
                        $oUser->setPassword($oEncoder->encodePassword($sPassword, $oUser->getSalt()));
                    } 
                    else
                    {
                        $oSession->getFlashBag()->add('error', 'Mot de passe : Veuillez respecter le format requis.');
                        $oSession->getFlashBag()->add('recuperation', $oRequest->request->get('recuperation')); 
                        return $this->redirect($this->generateUrl('m_core_homepage'));
                    }

                }
                else
                {
                    $oSession->getFlashBag()->add('error', 'Confirmation : Votre confirmation de mot de passe est erronée.');
                    $oSession->getFlashBag()->add('recuperation', $oRequest->request->get('recuperation')); 
                    return $this->redirect($this->generateUrl('m_core_homepage'));
                }

                
                $oUser->setCodeRecuperation(1);
                $oEm->persist($oUser);
                $oEm->flush(); 

                $oSession->getFlashBag()->add('success', 'Votre mot de passe a été changé avec succés.');
            }
            else $oSession->getFlashBag()->add('error', 'Changement du mot de passe : Une erreur est survenue.');
        }
        return $this->redirect($this->generateUrl('m_core_homepage'));
    }

    
}