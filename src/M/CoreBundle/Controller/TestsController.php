<?php

namespace M\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class TestsController extends Controller
{
   
    public function test1Action()
    {
        // This is our new stuff
        $oContext = new \ZMQContext();
        $oSocket = $oContext->getSocket(\ZMQ::SOCKET_PUSH, 'PushMe');
        $oSocket->connect("tcp://rencontres-mariages.ma:5555");

        $aData = array(
            'topic'         => 'message',
            'sUsername'     => 'aaaaaa',
            'sMessage'      => 'bbbbbbb'
        );

        $oSocket->send(json_encode($aData));   
        
        return $this->render('MCoreBundle:Tests:test1.html.twig');
    }

    public function test2Action()
    {
        return $this->render('MCoreBundle:Tests:test2.html.twig');
    }

    
}