<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use M\CoreBundle\Entity\Messages;
use M\CoreBundle\Entity\Notification;

use M\CoreBundle\Form\DefaultType;
use Symfony\Component\HttpFoundation\Response;

use React\ZMQ\Context;

class MessagesController extends Controller
{
    public function nouveauAction()
    {
        $oRequest = $this->get('request');
        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();
            $aUsers = ($oUserCurrent->getInchatBox()) ? $oUserCurrent->getInchatBox() : array() ;
            // a inserer dans la condition en dessous
             if ($this->get('security.context')->isGranted('ROLE_FREE')) { return new Response('permissionDenied'); }
            $oEm = $this->getDoctrine()->getManager();
            $oRepository = $oEm->getRepository('MCoreBundle:User');
            $sUser = $oRequest->request->get('username');
            $sMessage = $oRequest->request->get('message');
            $sType = $oRequest->request->get('type');

            if(!is_string($sUser) || strlen($sUser) > 40 || !is_string($sMessage) || empty($sMessage) ) new Response('failure');
            
            // Le username de la personne qui le reçois
            $oUser = $oRepository->findOneByUsername($sUser);
            if(!is_object($oUser)) new Response('failure');

            if($oUser === $oUserCurrent ) new Response('failure');

            
            //on crée la notification est on retourne l'objet
            if( $sType === "chat" ){
                if(in_array($oUser->getUsername(),$aUsers))
                {
            $oNotification =$this->get('IncrementNotification')->incrementNotification(3 , $oUser, $oUserCurrent, true);
                }
                else{
                     $oNotification =$this->get('IncrementNotification')->incrementNotification(3 , $oUser, $oUserCurrent, false);
                }
               
            } 
             else
             {
               $oNotification = $this->get('IncrementNotification')->incrementNotification(2 , $oUser, $oUserCurrent);
             }
                                
            
            //On Verifie si l'utilisateur est en ligne
            $dDelay = new \DateTime();
            $dDelay->setTimestamp(strtotime('10 minutes ago'));
            $oUser->setEnLigne(($oUser->getLastactivity() > $dDelay ) ? 1 : 0 );
    
              /*   Service d'envoi de mail   */
            if ( array_key_exists('message', $oUser->getAlertemail()) && $oUser->getEnLigne() == 0  ) 
            {     
                $this->get('Mail')->mailMessage($oUser->getUsername(),$oUserCurrent->getUsername(),$oUser->getEmail());
       
                 }
            if(!is_object($oNotification)) new Response('failure');
            //On crée l'objet message est on lui inject les données
            
            $oMessage = new Messages();
            $oMessage->setFrom($oUserCurrent);
            $oMessage->setTo($oUser);
            $oMessage->setMessage($sMessage);
            $oMessage->setNotification($oNotification);
            if ( $sType === "chat" )
            {
                $oMessage->setChat(true);
                if (in_array($oUser->getUsername(),$aUsers)) { $oMessage->setLu(true); }
                else { $oMessage->setLu(false); }
            }
            $oEm->persist($oMessage);
            $oEm->flush($oMessage);
            $isLu = ($oMessage->getLu()) ? 1 : 0 ;
            return ( $oNotification ) ? new Response('success:' . $oMessage->getId() .':'. $isLu ) : new Response('failure');
        }
    }

    public function lookForMessageAction()
    {
        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();

            if ( (base64_decode($oRequest->request->get('iReceiverID')) / 9999) !== (int)$oUserCurrent->getId() ) return new Response('failed');

            $oEm = $this->getDoctrine()->getManager();

            $iCheckUsers = $oEm->getRepository('MCoreBundle:User')->getUsersByTokens(
                                                        array(
                                                            'sReceiverTocken' => substr((base64_decode($oRequest->request->get('sReceiverTocken'))), 10), 
                                                            'sSenderTocken' => substr((base64_decode($oRequest->request->get('sSenderTocken'))), 10)
                                                        ));
            
            
            if ( $iCheckUsers != 2 ) return new Response('failed');


            //return new Response($iCheckUsers);

            // a inserer dans la condition en dessous
            $aParams = array(
                            'iSenderID'       => base64_decode($oRequest->request->get('iSenderID')) / 9999,
                            'iReceiverID'     => base64_decode($oRequest->request->get('iReceiverID')) / 9999,
                            'sDateEnvoi'      => $oRequest->request->get('sDateEnvoi')['date']
                            );

            
            //return new Response(json_encode($aParams));

            $oMessages = $oEm->getRepository('MCoreBundle:Messages')->getMessageByParams($aParams);
            
            //return new Response(json_encode(var_dump($oMessages)));

            if ( count($oMessages) <= 0 ) return new Response('failed');

            //On encode les id des messages !
            //$aResult = $this->get('FonctionDiver')->encodeId($oMessages);

            return new Response(json_encode($oMessages));
            
        }
    }

    public function prevMessagesAction()
    {
        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();

            if ( (base64_decode($oRequest->request->get('iReceiverID')) / 9999) !== (int)$oUserCurrent->getId() ) return new Response('failed');

            $oEm = $this->getDoctrine()->getManager();

            $oUser = $oEm->getRepository('MCoreBundle:User')->find((base64_decode($oRequest->request->get('iSenderID')) / 9999));

            if (!is_object($oUser)) return new Response('failed'); 

            $iTotalResult = $oEm->getRepository('MCoreBundle:Messages')->countMessagesUserId($oUserCurrent->getId(),$oUser->getId());

            // On utilise le service pour connaitre le premier résultat a affiché
            $iNbrResultPerPage = 10; // Nombre de resultat par affichage
            $aPaginator = $this->get("Paginator")->getPagination(
                                    (int)$iTotalResult,
                                    $iNbrResultPerPage, 
                                    (int)$oRequest->request->get('iPage')
                                );
            $_aResult = $oEm->getRepository('MCoreBundle:Messages')->GetMessagesUserId(
                                $oUserCurrent->getId(),
                                $oUser->getId(), 
                                $iNbrResultPerPage,
                                $aPaginator['FirstResultPerCurrentPage']
                            );

            //On verifie les users en ligne
            $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($_aResult);
            // on formate les dates
            $aResult = $this->get('FonctionDiver')->setDateTimeToStringFromMessage($aResult);

            if ( count($aResult) === 0) return new Response('failed');

           /* //On recupére les données pour mettre a jour les notifs a savoir id et vue
            foreach ( $aResult as $key => $value )
            {
                if ((int)$aResult[$key]['lu'] == 0) $aArrayID[] = $aResult[$key]['id'] ;
            }

        return new Response('success');
            //On récupére les id a  mettre a jour
            $aArrayID = $this->get('FonctionDiver')->getIdsNotViewed($aArrayID);

            //On change le statut de la notification de message en vue !
            $oEm->getRepository('MCoreBundle:Messages')->UpdateMessageViewed($aArrayID ,2);*/

            // On encode les id des messages !
            $aResult = $this->get('FonctionDiver')->encodeId($aResult);

            return new Response(json_encode(array_reverse($aResult)));
            
        }
    }

    public function getMessagesAction()
    {
        $oRequest = $this->get('request');

        if($oRequest->isXmlHttpRequest())
        {
            $oUserCurrent = $this->get('security.context')->getToken()->getUser();

            $oEm = $this->getDoctrine()->getManager();

            $oUser = $oEm->getRepository('MCoreBundle:User')->find((base64_decode($oRequest->request->get('iSenderID')) / 9999));

            if (!is_object($oUser)) return new Response('failed1'); 

            $iTotalResult = $oEm->getRepository('MCoreBundle:Messages')->countMessagesUserId($oUserCurrent->getId(),$oUser->getId());

            // On utilise le service pour connaitre le premier résultat a affiché
            $iNbrResultPerPage = 10; // Nombre de resultat par affichage
            $aPaginator = $this->get("Paginator")->getPagination(
                                    (int)$iTotalResult,
                                    $iNbrResultPerPage, 
                                    (int)$oRequest->request->get('iPage')
                                );
            $_aResult = $oEm->getRepository('MCoreBundle:Messages')->GetMessagesUserId(
                                $oUserCurrent->getId(),
                                $oUser->getId(), 
                                $iNbrResultPerPage,
                                $aPaginator['FirstResultPerCurrentPage']
                            );

            //On verifie les users en ligne
            $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($_aResult);
            // on formate les dates
            $aResult = $this->get('FonctionDiver')->setDateTimeToStringFromMessage($aResult);
            if ( count($aResult) === 0 && $oRequest->request->get('nullable') !== 'yes') return new Response('failed2');
            
            $aArrayID = array();

            if ( $oRequest->request->get('nullable') !== 'yes' )
            {
                //On recupére les données pour mettre a jour les notifs a savoir id et vue
                foreach ( $aResult as $key => $value )
                {
                    if ((int)$aResult[$key]['lu'] == 0) $aArrayID[] = $aResult[$key]['id'] ;
                }
                //On récupére les id a  mettre a jour
                $aArrayID = $this->get('FonctionDiver')->getIdsNotViewed($aArrayID);

                //On change le statut de la notification de message en vue !
                $oEm->getRepository('MCoreBundle:Messages')->UpdateMessageViewed($aArrayID ,2);

                // On encode les id des messages !
                $aResult = $this->get('FonctionDiver')->encodeId($aResult);
            }

            if ( $oRequest->request->get('destroyNotif') == 1 )
            {
                //On change le statut de la notification de message en vue !
                $oEm->getRepository('MCoreBundle:Notification')->UpdateNotificationViewedPerSender(
                        $oUserCurrent->getId(),
                        $oUser->getId(),
                        3
                    );
            }

            if( $this->get('security.context')->isGranted('ROLE_FREE') )
            {
                if ( count($aResult) > 0 )
                {
                    foreach ($aResult as $iKey => $sValue) 
                    {
                        $aResult[0]['Message'] = 'Pour tchater avec ' . $oUser->getUsername() . ' et communiquer en illimité sur Rencontres & Vous, <a href="' . $this->generateUrl('m_core_profile_abonnement') . '" >Devenez  membre Premium !</a>';
                    }
                    $aFreeResult = array();
                    $aFreeResult[0] = $aResult[0];
                    $aResult = $aFreeResult;
                }
            }

            $oFonctionDiver = $this->get('FonctionDiver');
            $aResult['imageSender'] = $oEm->getRepository('MCoreBundle:Images')->getProfilDefaultByUser($oUser->getId());
            $aResult['imageSender'] = $oFonctionDiver->buildThumbnail($aResult['imageSender'], 42, 42);

            return new Response(json_encode(array_reverse($aResult)));
            
        }
    }
    

    public function recusAction($iPage,$sTri)
    {   

        $sRootProfile = $this->redirect($this->generateUrl('m_core_profile'));
        $oRequest = $this->get('request');
        $sTri = ($oRequest->query->get('sTri')) ? $oRequest->query->get('sTri') : "Tous" ;

        if(!is_string($sTri) || strlen($sTri) > 7 ) return $sRootProfile;
        $sTri = $this->get('FonctionDiver')->setTri($sTri);
        
        $aArrayID = array();

        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oForm = $this->createForm(new DefaultType);

        $oRequest = $this->get('request');
 
        if ($oRequest->getMethod() == 'POST') 
        {

          if ($oRequest->request->get('filtrer_messages')) $this->get('router')->generate('m_core_profile_messages_recus', array('sTri' => 'Lus','iPage' => 1));
           
           $aSupprimer=$oRequest->request->get('m_corebundle_usertype');  
           $aIdMessage = (isset($aSupprimer["delete_message"]["Received"])) ? $this->get('FonctionDiver')->decodeId($aSupprimer["delete_message"]["Received"]) : array(); ;


           $oEm->getRepository('MCoreBundle:Messages')->SetMessageReceivedInCorbeille($aIdMessage);
        }


        $iTotalResult = $oEm->getRepository('MCoreBundle:Messages')->countMessagesReceivedByUserId($oUserCurrent->getId(),$sTri);
        // On utilise le service pour connaitre le premier résultat a affiché
        $iNbrResultPerPage = 10; // Nombre de resultat par affichage

        $aPaginator = $this->get("Paginator")->getPagination((int)$iTotalResult,$iNbrResultPerPage, (int)$iPage);

        $_aResult = $oEm->getRepository('MCoreBundle:Messages')->GetMessagesReceivedByUserId($oUserCurrent->getId(),$sTri,$iNbrResultPerPage,$aPaginator['FirstResultPerCurrentPage']);

        //On verifie les users en ligne
        $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($_aResult);

       
        // on formate les dates
        $aResult = $this->get('FonctionDiver')->setDateTimeToStringFromMessage($aResult);

        //On recupére les donné pour mettre a jour les notifs a savoir id et vue
        foreach ( $aResult as $key => $value )
        {
            if ( $aResult[$key]['chat'] ) continue;

            $aArrayID[$key]['id'] =  $aResult[$key]['notification'];
            $aArrayID[$key]['vue'] = $aResult[$key]['vue'];
        }

        //On récupére les id a  mettre a jour
        $aArrayID = $this->get('FonctionDiver')->getIdsNotViewed($aArrayID);


        //On change le statut de la notification de message en vue !
        $oEm->getRepository('MCoreBundle:Notification')->UpdateNotificationViewed($aArrayID ,2);

        //On encode les id des messages !
        $aResult = $this->get('FonctionDiver')->encodeId($aResult);

        return $this->render('MCoreBundle:Messages:recus.html.twig',
                                       array( 'aResult'   => $aResult,
                                                'iPage'     => $iPage,
                                                'oForm'     => $oForm->createView(),
                                                'iTotalResult' => $iTotalResult,
                                                'iFirstResult' => $aPaginator['FirstResultPerCurrentPage'],
                                                'iTotalPage'    => $aPaginator['iTotalPages'] ));
    }

    public function envoyesAction($iPage)
    {   


        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oForm = $this->createForm(new DefaultType);

        $oRequest = $this->get('request');
 
        if ($oRequest->getMethod() == 'POST') 
        {
           /*$id base64_decode( (int) $aResult[$key]['id'] / 9999)*/
           $aSupprimer=$oRequest->request->get('m_corebundle_usertype');
           $aIdMessage = (isset($aSupprimer["delete_message"]["Sended"])) ? $this->get('FonctionDiver')->decodeId($aSupprimer["delete_message"]["Sended"]) : array() ;
           $oEm->getRepository('MCoreBundle:Messages')->SetMessageSendedInCorbeille($aIdMessage);
        }


        $iTotalResult = $oEm->getRepository('MCoreBundle:Messages')->countMessagesSendedByUserId($oUserCurrent->getId());

        // On utilise le service pour connaitre le premier résultat a affiché
        $iNbrResultPerPage = 10; // Nombre de resultat par affichage

        $aPaginator = $this->get("Paginator")->getPagination((int)$iTotalResult,$iNbrResultPerPage, (int)$iPage);

        $_aResult = $oEm->getRepository('MCoreBundle:Messages')->GetMessagesSendedByUserId(
                                $oUserCurrent->getId(),
                                $iNbrResultPerPage,
                                $aPaginator['FirstResultPerCurrentPage']
                            );

        //On verifie les users en ligne
        $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($_aResult);
        // on formate les dates
        $aResult = $this->get('FonctionDiver')->setDateTimeToStringFromMessage($aResult);
        //On encode les id des messages !
        $aResult = $this->get('FonctionDiver')->encodeId($aResult);

        return $this->render('MCoreBundle:Messages:envoyes.html.twig',
                                         array( 'aResult'   => $aResult,
                                                'iPage'     => $iPage,
                                                'oForm'     => $oForm->createView(),
                                                'iTotalResult' => $iTotalResult,
                                                'iFirstResult' => $aPaginator['FirstResultPerCurrentPage'],
                                                'iTotalPage'    => $aPaginator['iTotalPages'] ));
    }

    public function corbeilleAction($iPage)
    {


        $oEm = $this->getDoctrine()->getManager();
        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oForm = $this->createForm(new DefaultType);

        $oRequest = $this->get('request');
 
        if ($oRequest->getMethod() == 'POST') 
        {
           
           /*$id base64_decode( (int) $aResult[$key]['id'] / 9999)*/
           $aSupprimer=$oRequest->request->get('m_corebundle_usertype'); 
           // On recupere les message envoyer et reçu pour pouvoir changer leur statut delete from delte to! 
           $aIdMessageSended = (isset($aSupprimer["delete_message"]["Sended"])) ? $this->get('FonctionDiver')->decodeId($aSupprimer["delete_message"]["Sended"]) : array();
           $aIdMessageReceived = (isset($aSupprimer["delete_message"]["Received"])) ? $this->get('FonctionDiver')->decodeId($aSupprimer["delete_message"]["Received"]) : array();
           

          
           $oEm->getRepository('MCoreBundle:Messages')->DeleteMessageReceived($aIdMessageReceived);
           $oEm->getRepository('MCoreBundle:Messages')->DeleteMessageSended($aIdMessageSended);
           // Suppression definitive des messages avec del_to et del_from a 1 
           $oEm->getRepository('MCoreBundle:Messages')->DeleteDefinitelyMessages(array_merge((array)$aIdMessageReceived, (array)$aIdMessageSended));
         

        }

        $iTotalResult = $oEm->getRepository('MCoreBundle:Messages')->countMessagesDeletedByUserId($oUserCurrent->getId());
        // On utilise le service pour connaitre le premier résultat a affiché
        $iNbrResultPerPage = 10; // Nombre de resultat par affichage

        $aPaginator = $this->get("Paginator")->getPagination((int)$iTotalResult,$iNbrResultPerPage, (int)$iPage);
        //On rajoute le paramétre avec la date du jour
        $oParam['today'] = date('Y-m-d'); 
        $_aResult = $oEm->getRepository('MCoreBundle:Messages')->GetMessagesDeletedByUserId($oUserCurrent->getId(),$iNbrResultPerPage,$aPaginator['FirstResultPerCurrentPage']);
        //On verifie les users en ligne
        $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($_aResult);
        // on formate les dates
        $aResult = $this->get('FonctionDiver')->setDateTimeToStringFromMessage($aResult);
        //On encode les id des messages !
        $aResult = $this->get('FonctionDiver')->encodeId($aResult);
        return $this->render('MCoreBundle:Messages:corbeille.html.twig',
                                            array('aResult'   => $aResult,
                                                'iPage'     => $iPage,
                                                'oForm'     => $oForm->createView(),
                                                'iTotalResult' => $iTotalResult,
                                                'iFirstResult' => $aPaginator['FirstResultPerCurrentPage'],
                                                'iTotalPage'    => $aPaginator['iTotalPages']));
    }

    public function messagesAction($suser , $ipage)
    {

        $oUserCurrent = $this->get('security.context')->getToken()->getUser();
        $oEm = $this->getDoctrine()->getManager();
        $sRootProfile = $this->redirect($this->generateUrl('m_core_profile'));

        if(empty($suser) || !is_string($suser) || strlen($suser) > 50) return $sRootProfile;

        $aArrayID = array();

        //On cherche si le user existe
        $oUser = $oEm->getRepository("MCoreBundle:User")->findOneBy(array('username' => $suser));

        // Si le user n'existe pas a on retourne au dashbord 
        if (!is_object($oUser)) return $sRootProfile;
        $sRole = $oUser->getRoles();
       

        if ($sRole[0] != "ROLE_ADMIN")
        {
           if ($oUser->getSexe() == $oUserCurrent->getSexe() or $this->get('security.context')->isGranted('ROLE_FREE')) return $sRootProfile;
        }
            

        $oForm = $this->createForm(new DefaultType);

        $oRequest = $this->get('request');
 
        if ($oRequest->getMethod() == 'POST') 
        {
           
           $aSupprimer = $oRequest->request->get('m_corebundle_usertype'); 
           // On recupere les message envoyer et reçu pour pouvoir changer leur statut delete from delte to! 
           $aIdMessageSended = (isset($aSupprimer["delete_message"]["Sended"])) ? $this->get('FonctionDiver')->decodeId($aSupprimer["delete_message"]["Sended"]) : array();
           $aIdMessageReceived = (isset($aSupprimer["delete_message"]["Received"])) ? $this->get('FonctionDiver')->decodeId($aSupprimer["delete_message"]["Received"]) : array();

           //On change le status des message (del_to)
           $oEm->getRepository('MCoreBundle:Messages')->SetMessageReceivedInCorbeille($aIdMessageReceived);
           $oEm->getRepository('MCoreBundle:Messages')->SetMessageSendedInCorbeille($aIdMessageSended);

        }

        $iTotalResult = $oEm->getRepository('MCoreBundle:Messages')->countMessagesUserId($oUserCurrent->getId(),$oUser->getId());

        // On utilise le service pour connaitre le premier résultat a affiché
        $iNbrResultPerPage = 10; // Nombre de resultat par affichage
        $aPaginator = $this->get("Paginator")->getPagination((int)$iTotalResult,$iNbrResultPerPage, (int)$ipage);

        //On rajoute le paramétre avec la date du jour
        $oParam['today'] = date('Y-m-d'); 
        $_aResult = $oEm->getRepository('MCoreBundle:Messages')->GetMessagesUserId(
                            $oUserCurrent->getId(),
                            $oUser->getId(),
                            $iNbrResultPerPage,
                            $aPaginator['FirstResultPerCurrentPage']
                        );

        //On verifie les users en ligne
        $aResult = $this->get('FonctionDiver')->setEnligneUserToResult($_aResult);
        // on formate les dates
        $aResult = $this->get('FonctionDiver')->setDateTimeToStringFromMessage($aResult);

        if ( count($aResult) === 0) return $sRootProfile;

        //On recupére les données pour mettre a jour les notifs a savoir id et vue
        foreach ( $aResult as $key => $value )
        {
            if ((int)$aResult[$key]['lu'] == 0) $aArrayID[] = $aResult[$key]['id'] ;
        }

        //On change le statut de la notification de message en vue !
        $oEm->getRepository('MCoreBundle:Messages')->UpdateMessageViewed($aArrayID,$oUserCurrent->getId());

        //On change le statut de la notification de message en vue !
        $oEm->getRepository('MCoreBundle:Notification')->UpdateNotificationViewedPerSender(
            $oUserCurrent->getId(),
            $oUser->getId(),
            2
        );

        //Tableau des username avec qui nous avons echanger des messages
        $aUsernameMessage = $oEm->getRepository('MCoreBundle:Messages')->GeAllMessageUsername($oUserCurrent->getId());
        $iPosition = 0;
        $aPagination = array();

        foreach ($aUsernameMessage as $key => $value )
        {
            if ($value['username'] === $suser) $iPosition = $key;  
        }

        $aPagination['prev'] = ($iPosition-1 != -1) ? $aUsernameMessage[$iPosition-1]['username'] : "";
        $aPagination['next'] = ($iPosition+1 <= $key ) ? $aUsernameMessage[$iPosition+1]['username'] : "";

        //On encode les id des messages !
        $aResult = $this->get('FonctionDiver')->encodeId($aResult);

        $sImageSender = $oEm->getRepository('MCoreBundle:Images')->getProfilDefaultByUser($oUserCurrent->getId());
        $sImageReceiver = $oEm->getRepository('MCoreBundle:Images')->getProfilDefaultByUser($oUser->getId());

        return $this->render('MCoreBundle:Messages:messages.html.twig', array(
                                                'aResult'   => array_reverse($aResult),
                                                'sImageProfilUserActual' => $sImageSender,
                                                'sImageProfilUserSender' => $sImageReceiver,
                                                'iPage'     => $ipage,
                                                'oForm'     => $oForm->createView(),
                                                'iTotalResult' => $iTotalResult,
                                                'iFirstResult' => $aPaginator['FirstResultPerCurrentPage'],
                                                'iTotalPage'    => $aPaginator['iTotalPages'],
                                                'oUserReceivedID'    => $oUser->getId(),
                                                'aPagination'    => $aPagination,

                                                'iSenderID' => base64_encode(((int)$oUser->getId() * 9999)),
                                                'iReceiverID' => base64_encode(((int)$oUserCurrent->getId() * 9999)),
                                                'sSenderTocken' => base64_encode('rencontres' . (string) $oUserCurrent->getSalt()),
                                                'sReceiverTocken' => base64_encode('rencontres' . (string) $oUser->getSalt())
                                                )
                                           );
    }
}
