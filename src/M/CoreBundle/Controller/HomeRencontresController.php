<?php

namespace M\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use M\CoreBundle\Entity\User;
use M\CoreBundle\Form\FastRegistrationPreType;

class HomeRencontresController extends Controller
{
    public function rencontresAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

    	return $this->render('MCoreBundle:HomeRencontres:rencontres.html.twig', array(
        	'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage
        ));
    }
    public function rencontresCasablancaAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

        return $this->render('MCoreBundle:HomeRencontres:rencontresCasablanca.html.twig', array(
            'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage
        ));
    }
    public function rencontresMarrakeshAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

        return $this->render('MCoreBundle:HomeRencontres:rencontresMarrakesh.html.twig', array(
            'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage
        ));
    }
    public function rencontresRabatAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

        return $this->render('MCoreBundle:HomeRencontres:rencontresRabat.html.twig', array(
            'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage
        ));
    }
    public function rencontresAgadirAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

        return $this->render('MCoreBundle:HomeRencontres:rencontresAgadir.html.twig', array(
            'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage
        ));
    }
    public function rencontresTangerAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;
        
        return $this->render('MCoreBundle:HomeRencontres:rencontresTanger.html.twig', array(
            'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage
        ));
    }
    public function rencontresFesAction()
    {
        $oEm = $this->get('doctrine')->getEntityManager();
        $aInfoAdmin = $oEm->getRepository('MCoreBundle:Admin')->findById(1);
        $iMembre = ($aInfoAdmin) ? $aInfoAdmin[0]->getNbrconnecte() : 0;
        $bAffichage = ($aInfoAdmin) ? $aInfoAdmin[0]->getMembreinscrit() : 0;

        return $this->render('MCoreBundle:HomeRencontres:rencontresFes.html.twig', array(
            'oForm' => $this->createForm(new FastRegistrationPreType, new User())->createView(),
            'NbrConnecte' => $iMembre,
            'bAffichage' => $bAffichage
        ));
    }
}
