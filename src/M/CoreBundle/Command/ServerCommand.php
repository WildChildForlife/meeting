<?php 
namespace M\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use React\EventLoop\Factory as Factory;
use React\ZMQ\Context as Context;
use React\Socket\Server as Server;
use Ratchet\Server\IoServer as IoServer;
use Ratchet\Http\HttpServer as HttpServer;
use Ratchet\WebSocket\WsServer as WsServer;
use Ratchet\Wamp\WampServer as WampServer;

use M\CoreBundle\Services\EventHandler as EventHandler;

class ServerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('chat:server')
            ->setDescription('Start the Chat server');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*global $kernel;

        $oChat = $kernel->getContainer()->get('chat');*/
        $oLoop = Factory::create();
        
        $oEventHandler = new EventHandler($oLoop);
        // Listen for the web server to make a ZeroMQ push after an ajax request
        $oContext = new Context($oLoop);
        $oPull = $oContext->getSocket(\ZMQ::SOCKET_PULL);
        // LET IT 127.0.0.1
        $oPull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
        $oPull->on('message', array($oEventHandler, 'onMessage'));

        //$oServer = new SessionProvider($server, $handler);

        // Set up our WebSocket server for clients wanting real-time updates
        $oWebSock = new Server($oLoop);
        $oWebSock->listen(7979, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
        $oWebServer = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $oEventHandler
                    )
                )
            ),
            $oWebSock
        );


        $oLoop->run();



/*global $kernel;

        $oChat = $kernel->getContainer()->get('chat');

        $oLoop = Factory::create();

        // Listen for the web server to make a ZeroMQ push after an ajax request
        $oContext = new Context($oLoop);
        $oPull = $oContext->getSocket(\ZMQ::SOCKET_PULL);
        // LET IT 127.0.0.1
        $oPull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
        $oPull->on('message', array($oChat, 'onMessage'));

        //$oServer = new SessionProvider($server, $handler);

        // Set up our WebSocket server for clients wanting real-time updates
        $oWebSock = new Server($oLoop);
        $oWebSock->listen(7979, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
        $oWebServer = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $oChat
                    )
                )
            ),
            $oWebSock
        );


        $oLoop->run();*/














/*
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                        $kernel->getContainer()->get('chat')
                    )
                ), 8080
            );
        
        $server->run();*/
    }
}
?>