$(document).ready(function(){
	/* Scroll du Chat */
	$(".chat_content").mCustomScrollbar({
		theme:"dark-thick",
		scrollInertia: 100,
		autoHideScrollbar: true,
		advanced: {
			updateOnContentResize: true, 
			autoScrollOnFocus: true
		}
	});
	

	$(".description_data").mCustomScrollbar({
		theme:"dark-thick",
		scrollInertia: 100,
		autoHideScrollbar: true,
		advanced: {
			updateOnContentResize: true, 
			autoScrollOnFocus: true
		}
	});

	$(".messages.conversation_detail .list_messages").mCustomScrollbar({
		theme:"dark-thick",
		scrollInertia: 100,
		autoHideScrollbar: true,
		advanced: {
			updateOnContentResize: true, 
			autoScrollOnFocus: true
		}
	});

	$(".events .evenements").mCustomScrollbar({
		theme:"dark-thick",
		scrollInertia: 100,
		autoHideScrollbar: true,
		advanced: {
			updateOnContentResize: true, 
			autoScrollOnFocus: true
		}
	});

	$(".actus .actualites").mCustomScrollbar({
		theme:"dark-thick",
		scrollInertia: 100,
		autoHideScrollbar: true,
		advanced: {
			updateOnContentResize: true, 
			autoScrollOnFocus: true
		}
	});

	$(".actualite article .right_article .content_p").mCustomScrollbar({
		theme:"dark-thick",
		scrollInertia: 100,
		autoHideScrollbar: true,
		advanced: {
			updateOnContentResize: true, 
			autoScrollOnFocus: true
		}
	});

	$(".evenement article .left_article .inscrits .content_inscrits").mCustomScrollbar({
		theme:"dark-thick",
		scrollInertia: 100,
		autoHideScrollbar: true,
		advanced: {
			updateOnContentResize: true, 
			autoScrollOnFocus: true
		}
	});

	$(".evenement article .right_article .content_p").mCustomScrollbar({
		theme:"dark-thick",
		scrollInertia: 100,
		autoHideScrollbar: true,
		advanced: {
			updateOnContentResize: true, 
			autoScrollOnFocus: true
		}
	});

	/* Carousel du Dashboard */
	$('.sky-carousel').carousel({
		itemWidth: 110,
		itemHeight: 150,
		distance: 10,
		selectedItemDistance: 10,
		selectedItemZoomFactor: 1,
		unselectedItemZoomFactor: 0.5,
		unselectedItemAlpha: 0.9,
		motionStartDistance: 200,
		topMargin: 140,
		gradientStartPoint: 0.35,
		gradientOverlayColor: "#ffffff",
		gradientOverlaySize: 190,
		selectByClick: false,
		loop: true,
		preload:true,
		showPreloader:true,
		topMargin:25,
		slideSpeed:0.30
	});

	$(document).on('click', '.refresh_suggestions', function(e){
		e.preventDefault();
		refreshSuggestUsers();
	});

	/*
		TOOLTIP CALLS
	*/


	$(".fancyBox:not(.inconnu)").slimbox({
		loop 		: true,
		counterText	: ""
	}, null, function(el) {
		return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
	});

	$(document).on('click', ".fancyBox.inconnu", function(e){
		e.preventDefault();
	});

	/* TABS */
	$('#tab-container').easytabs({
		animate: false,
		animationSpeed: "slow",
		cache: true,
		transitionIn: 'slideDown',
		updateHash: true
	});


	/* En cliquant sur la croix pour supprimer une image chargée */
	$(document).on("click", ".refresh", function(){ 
		location.reload();
	});

	/* Définition d'une fonction jQuery permettant l'insensibilité à la casse
	   Cette fonction est utilisé dans la recherche du Chat 
	*/
	jQuery.expr[':'].Contains = function(a,i,m){return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;};
	/* Traitement recherche du chat */
	$("#Recherche_chat").change(function(e)
	{
		clearTimeout($.data(this, 'timer'));
		var sFilter = $(this).val();
		sFilter = $.trim(sFilter);
		var oList = $(".chat_content ul");
	    if (sFilter) 
	    {
			$(oList).find(".username:not(:Contains(" + sFilter + "))").parent().parent().stop().slideUp();
			$(oList).find(".username:Contains(" + sFilter + ")").parent().parent().stop().slideDown();
			// Dans le cas où aucun utilisateur n'a été trouvé dans la liste chargée au préalable
			if ( $(oList).find(".username:Contains(" + sFilter + ")").length === 0 )
			{						    
				if ( sFilter.length < 4 ) return false;
			    if (e.keyCode == 13) searchOnChatByUsernameExt(true);
			    else $(this).data('timer', setTimeout(searchOnChatByUsernameExt, 500));
			}
	    } 
	    else 
	    {
	    	$(oList).find("li").stop().slideDown();
	    }
	}).keyup(function(){
		$(this).change();
	});


	$(document).on('click', '.close_popup', function(){
		console.log('fermeture');
		deletePopup();
	});


	function checkQualiteChecked () {
		if ($('.block_center .qualite:checked').length >= 6)
		{
			$('.block_center .qualite:not(:checked)').attr('disabled', 'disabled');
			$('.block_center .qualite:not(:checked)').next("label").css({ color:"gray" });
		}
		else 
		{
			$('.block_center .qualite:not(:checked)').removeAttr('disabled');
			if ($('.etapes_inscription .formulaire').length === 0) $('.block_center .qualite').next("label").css({ color:"black" });
			else $('.block_center .qualite:not(:checked)').next("label").css({ color:"white" });
		}
	}

	/* Désactivation des qualités après selection de 6 qualités */
	$(document).on('click', '.block_center .qualite', function(){ checkQualiteChecked() });
	checkQualiteChecked();

	$(document).on('click', '.corps .declenche_emo', function(){
		$('.corps .liste_emo').stop().slideToggle();
	});
	$(document).on('click', '.chatbox .emo', function(e){
		$(this).parents('.chatbox').find('.liste_emo').stop().fadeToggle();
	});

	// Affichage des émoticones
	$(document).on('click', '.liste_emo li', function(){
		
		if ( $('.ecrire_message').length === 0 ) return false;

		if ( $('.ecrire_message').val().substr($('.ecrire_message').val().length - 1) === " " || $('.ecrire_message').val().length === 0 )
		{
			$('.ecrire_message').val($('.ecrire_message').val() + $(this).find('img').attr('alt'));
		}
		else 
		{
			$('.ecrire_message').val($('.ecrire_message').val() + ' ' + $(this).find('img').attr('alt'));
		}

		$(this).parents('.liste_emo').slideUp();
	});

	// Selection automatique de tout les élements
	$(document).on('click', '.checkall', function(){
		if ( $(this).is(':checked') ) 
		{
			$('.liste').find('input[type="checkbox"]').prop('checked', true);
		}
		else 
		{
			$('.liste').find('input[type="checkbox"]').prop('checked', false);
		}
	});

	// Retour à la page precedante
	$(document).on('click', '.prevPage', function(e){
		e.preventDefault();
		parent.history.back();
		return false;
	})

	// Affichage de la box de filtrage des contacts du chat
	$(document).on('click', '.filtre_chat a', function(e){
		e.preventDefault();
		$('.filtre_box').stop().slideToggle();
	});

	// Stop de la selection d'un age fin inférieur de l'age début
	$('.ageFin').change(function(e)
	{
		if ( parseInt($(this).parent('div').find('.ageDebut option:selected').text()) > parseInt($(this).find(':selected').text()) )
		{
			alert("L'age fin ne peut être inférieur que l'age de début, merci de corriger");
			$(this).find(':selected').removeAttr('selected');
			$(this).find('option:eq(0)').attr('selected', 'selected');
			return false;
		}

		
	});

	// En cliquant sur le bouton de filtrage des contacts du chat
	$(document).on('click', '.filtrer_button', function(e){
		e.preventDefault();
	});




	/* CHAT BOX */

	// Ouvrir une conversation de message
	$(document).on('click', '.chatbox.reduced .entete', function(e){
		$(this).parents('.chatbox.reduced').removeClass('reduced new').addClass('open');
	});
	// Réduire une conversation de message
	$(document).on('click', '.chatbox .chatbox_reduceit', function(e){
		$(this).parents('.chatbox.open').removeClass('open').addClass('reduced');
	});
	// Fermeture d'une conversation de message
	$(document).on('click', '.chatbox .chatbox_closeit', function(e){
		$(this).parents('.chatbox').remove();
		updateRegroupementChat();
	});
	// Affichage et disparition du sous menu d'une conversation de messages
	$(document).on('click', '.chatbox .entete .chat_box_menu li.chatbox_params', function(e){
		$(this).find('.chat_box_submenu').stop().slideToggle();
		$(this).toggleClass('active');
	});
	// Affichage disparition des elements regroupés
	$(document).on('click', '.chatbox_container .regroupement', function(e){
		$(this).find('ul').slideToggle();
	});
	// Affichage de la box de conversation cachée
	$(document).on('click', '.regroupement ul li.group', function(e){
		var $sUsername = $(this).text();
		$('.chatbox_list .chatbox:hidden .username:contains("' + $sUsername + '")').parents('.chatbox').exchangePositionWith('.chatbox_list .chatbox:eq(0)');
		updateRegroupementChat();
		updateRegroupementChat();
		$('.chatbox_container .regroupement ul').slideDown();
	});

	// State ON and OFF pour l'affichage de la date dans une conversation chat
	$(document).on('mouseenter', '.chatbox_message', function(e){
		$(this).find('.time').stop().animate({ opacity : 1 });
	});
	$(document).on('mouseleave', '.chatbox_message', function(e){
		$(this).find('.time').stop().animate({ opacity : 0 });
	});
	



	
	


});