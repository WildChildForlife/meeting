$(document).ready(function(){
	/* Définition des variables d'image */
	var image_obj 	= new Image();
	var upload 		= '';
	var image_x 	= '';
	var image_y 	= '';
	var image_w 	= '';
	var image_h 	= '';

	/* Coordonnées de l'image après crop */
	function showCoords(c)
	{
		image_x = c.x;
		image_y = c.y;
		image_h = c.h;
		image_w = c.w;
		console.log("W : " + image_w + " H : " + image_h);
		// c.x, c.y, c.x2, c.y2, c.w, c.h
	};
	function getDimensionsByDoc(imageObj) 
	{
		var srcWidth = imageObj.width;
		var srcHeight = imageObj.height;
		var maxWidth = (window.innerWidth * 75) / 100;
		var maxHeight = (window.innerHeight * 90) / 100;
	    var ratio = [maxWidth / srcWidth, maxHeight / srcHeight ];
	    ratio = Math.min(ratio[0], ratio[1]);
	    imageObj.width = srcWidth * ratio;
	    imageObj.height = srcHeight * ratio;

	    return imageObj;
 	}

	function handleFileSelect(upload_id) 
	{
		upload = upload_id;
		var file = document.forms['uploadData'][upload_id].files[0];
		// Only process image files.
		if (!file.type.match('image.*')) 
		{
			return false;
		}

      	var reader = new FileReader();

      	/*reader.onprogress = (function(file)
      	{
      		$("#" + upload_id).parent(".empty").find(".loader").css({ opacity:1 });
      	})(file);

      	reader.onload = (function(file)
      	{
      		$("#" + upload_id).parent(".empty").find(".loader").css({ opacity:1 });
      	})(file);*/

		// Closure to capture the file information.
		reader.onloadend = (function(file) 
		{
			return function(e) 
			{
				var imageObj 	= new Image();
				imageObj.src 	= e.target.result;
				if ( imageObj.width <= 165 || imageObj.height <= 212 )
				{
					alert("Image trop petite, veuillez selectionner une image plus grande.");
					return false;	
				} 
				imageObj 		= getDimensionsByDoc(imageObj);
				image_obj 		= imageObj;

				
				popUpCreate("<img class='crop' src='" + imageObj.src + "' >", 'crop');
				$(".crop").Jcrop({
					aspectRatio	: 0.78,
					minSize		: [164, 210],
					setSelect	: [200, 200, 100, 100],
					bgColor		: 'black',
					onSelect: showCoords,
            		onChange: showCoords,
            		boxWidth: imageObj.width, 
            		boxHeight: imageObj.height
				});

			};
		})(file);

		// Read in the image file as a data URL.
		reader.readAsDataURL(file);
	}

	/*$(document).on("submit", "#uploadData", function(e){
		if ( $(".changesMade").val() == 0 ) e.preventDefault();
	})*/
	
	/* Après upload de la photo = Activation du handler de traitement de la photo capturées */
	$(document).on("change", ".upload", function(){ 
		handleFileSelect($(this).attr('id'));
	});
	
	/* En cliquant sur la photo ou sur le plus = Activation de l'upload */
	$(document).on("click", "li.empty .photo .add,li.empty .photo, .add_pic", function(e){ 
		e.preventDefault()
		if ($(this).hasClass('add_pic')) $(this).prev("li.empty").find(".upload").trigger("click");
		else $(this).parents("li.empty").find(".upload").trigger("click"); 

		/* Activation des changements */
		$(".changesMade").val("1");
	});

	
	/* En cliquant sur la croix pour supprimer une image chargée */
	$(document).on("click", ".full .delete", function(e){ 
		if ($(this).parents("li").hasClass('default')) return false;

			e.preventDefault()
			/* Activation des changements */
			$(".changesMade").val("1");
			/* Vidage du bouton radio associé */
			$(this).parents("li").children(".profil_default").prop('checked', false);;
			/* Désactivation du bouton radio associé */
			$(this).parents("li").children(".profil_default").attr('disabled', 'disabled');
			$(".ADD_" + $(this).parents("li").children(".upload").attr('id').slice(-1)).val('');
			$(this).parents("li.full").next(".DOC").val('');

			$(this).parents("li").removeClass("full").addClass('empty').children("span").html('\
				<a class="photo" href="#"> \
					<span class="loader"></span> \
					<span class="add"> \
						<span class="plus">+</span> \
						<p>Ajouter une photo</p> \
					</span> \
				</a>');

		$(this).parents("li").children(".profil_default").attr("disabled", true);
	});


	/* Validation du crop */
	$(document).on("click", ".valider_crop", function(){ 
		var canvas = document.createElement('canvas');
		var context = canvas.getContext('2d');
		var imageObj = image_obj;
		canvas.width = image_w;
		canvas.height = image_h;
		
		// draw cropped image
		var sourceX = image_x;
		var sourceY = image_y;
		var sourceWidth = image_w;
		var sourceHeight = image_h;
		var destWidth = image_w;
		var destHeight = image_h;
		var destX = 0;
		var destY = 0;
		context.drawImage(imageObj, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);

		// Suppression du popup
		deletePopup();
		
		$("#" + upload).parents(".empty").removeClass("empty").addClass("full");
		$("#" + upload).parents(".full").children("span").html("\
			<a class='photo' href='#''> \
				<img src='" + canvas.toDataURL("image/jpeg") + "'' /> \
			</a> \
			<a class='delete' href='#''> x </a>");
		//alert(upload);

		if ($("#" + upload).parents(".full").next(".DOC").length != 0) 
				$("#" + upload).parents(".full").next(".DOC").val(canvas.toDataURL("image/jpeg"));
		else if ($(".ADD_" + upload).length != 0) $(".ADD_" + upload).val(canvas.toDataURL("image/jpeg"));
	});

	/* En cliquant sur annuler le crop */
	$(document).on("click", ".annuler_crop", function()
	{ 
		// Suppression du popup
		deletePopup();
	});
	

});