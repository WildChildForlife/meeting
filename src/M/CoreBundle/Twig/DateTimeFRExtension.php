<?php 
namespace M\CoreBundle\Twig;

class DateTimeFRExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'dateTimeFR' => new \Twig_Filter_Method($this, 'dateTimeFR')
        );
    }

    public function dateTimeFR(\Datetime $datetime, $lang = 'fr_FR', $pattern = 'd MMMM Y')
    {
        $formatter = new \IntlDateFormatter($lang, \IntlDateFormatter::LONG, \IntlDateFormatter::LONG);
        $formatter->setPattern($pattern);
        setlocale(LC_ALL,'French');
        //return strftime($format, $d);
        return $formatter->format($datetime);
    }

    public function getName()
    {
        return 'datetimefr_extension';
    }
}

?>