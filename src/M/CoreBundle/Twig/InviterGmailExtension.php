<?php


namespace M\CoreBundle\Twig;

class InviterGmailExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('getContact', array($this, 'getContact')),
        );
    }

    public function getContact($authcode)
    {
        $clientid   =   '68146181334-e3u71gc9lcbo9jmihns9udtip2ptp7hk.apps.googleusercontent.com';
        $clientsecret   =   '8OISNHVU1l8bvuRnwgPhTBR4';
        $redirecturi    =   'http://nouhe.rencontres-et-vous.ma/dashboard/parrainage'; //Add your redirect URl 
        $maxresults =    20;
            $fields=array(
            'code'=>  urlencode($authcode),
            'client_id'=>  urlencode($clientid),
            'client_secret'=>  urlencode($clientsecret),
            'redirect_uri'=>  urlencode($redirecturi),
            'grant_type'=>  urlencode('authorization_code') );

            //url-ify the data for the POST

            $fields_string = '';

            foreach($fields as $key=>$value){ $fields_string .= $key.'='.$value.'&'; }

            $fields_string  =   rtrim($fields_string,'&');

            //open connection
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL,'https://accounts.google.com/o/oauth2/token'); //set the url, number of POST vars, POST data

            curl_setopt($ch,CURLOPT_POST,5);

            curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Set so curl_exec returns the result instead of outputting it.

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //to trust any ssl certificates

            $result = curl_exec($ch); //execute post

            curl_close($ch); //close connection

            //print_r($result);

            //extracting access_token from response string

            $response   =  json_decode($result);

            $accesstoken = $response->access_token;

            if( $accesstoken!='')

            $_SESSION['token']= $accesstoken;

            //passing accesstoken to obtain contact details

            $xmlresponse=  file_get_contents('https://www.google.com/m8/feeds/contacts/default/full?max-results='.$maxresults.'&oauth_token='. $accesstoken);

            //reading xml using SimpleXML

            $xml=  new \SimpleXMLElement($xmlresponse);

            $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');

            $result = $xml->xpath('//gd:email');
            $contacts ='';
            foreach ($result as $title) {
                
               $contacts .= $title->attributes()->address . ",";
            }
            
            return $contacts;
    }

    public function getName()
    {
        return 'invitergmail_extension';
    }


}