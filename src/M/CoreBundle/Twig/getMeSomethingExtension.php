<?php 
namespace M\CoreBundle\Twig;

class getMeSomethingExtension extends \Twig_Extension
{
    private $oRepositoryUser;
    private $oRepositoryNotification;
    private $oFonctionDiver;
    private $oUser;

    function __construct($oContainer)
    {
        $this->oRepositoryUser = $oContainer->get("doctrine")->getManager()->getRepository("MCoreBundle:User");
        $this->oRepositoryImage = $oContainer->get("doctrine")->getManager()->getRepository("MCoreBundle:Images");
        $this->oFonctionDiver = $oContainer->get("FonctionDiver");
    }
    public function getFilters()
    {
        return array(
            'getImageByUsernameID' => new \Twig_Filter_Method($this, 'getImageByUsernameID')
        );
    }

    public function getImageByUsernameID($iUserID, $iWidth = 50, $iHeight = 50)
    {
        $oUserCurrent = $this->oRepositoryUser->find($iUserID);

        return $this->oFonctionDiver->buildThumbnail($this->oRepositoryImage->findOneByUser($oUserCurrent)->getImage(), $iWidth, $iHeight);
    }

    public function getName()
    {
        return 'getmesomething_extension';
    }
}

 ?>