<?php 
namespace M\CoreBundle\Twig;

class Base64Extension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'base64Encode' => new \Twig_Filter_Method($this, 'base64Encode'),
            'base64Decode' => new \Twig_Filter_Method($this, 'base64Decode'),
        );
    }

    public function base64Encode($iData)
    {
        return base64_encode($iData);
    }
    public function base64Decode($iData)
    {
        return base64_decode($iData);
    }

    public function getName()
    {
        return 'base64_extension';
    }
}

 ?>