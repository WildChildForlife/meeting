<?php 
namespace M\CoreBundle\Twig;

class TimeToStringExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'timeToString' => new \Twig_Filter_Method($this, 'timeToString')
        );
    }

    public function timeToString(\DateTime $sTime)
    {
        $sTime = time() - $sTime->getTimestamp();
        $d = floor($sTime/86400);
        $_d = ($d < 10 ? '0' : '').$d;

        $h = floor(($sTime-$d*86400)/3600);
        $_h = ($h < 10 ? '0' : '').$h;

        $m = floor(($sTime-($d*86400+$h*3600))/60);
        $_m = ($m < 10 ? '0' : '').$m;

        $s = $sTime-($d*86400+$h*3600+$m*60);
        $_s = ($s < 10 ? '0' : '').$s;

        $time_str = $_d.'j '.$_h.'h '.$_m.' minutes';

        return $time_str;
    }

    public function getName()
    {
        return 'timetostring_extension';
    }
}

?>