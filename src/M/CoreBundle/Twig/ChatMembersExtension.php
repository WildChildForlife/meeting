<?php 
namespace M\CoreBundle\Twig;

class ChatMembersExtension extends \Twig_Extension
{
    private $oChatService;
    private $oRepositoryUser;
    private $oRepositoryNotification;
    private $oFonctionDiver;
    private $oUser;

    function __construct($oContainer)
    {
        $this->oRepositoryUser = $oContainer->get("doctrine")->getManager()->getRepository("MCoreBundle:User");
        $this->oRepositoryNotification = $oContainer->get("doctrine")->getManager()->getRepository("MCoreBundle:Notification");
        $this->oFonctionDiver = $oContainer->get("FonctionDiver");
    }
    public function getFilters()
    {
        return array(
            'onlineUsers' => new \Twig_Filter_Method($this, 'onlineUsers'),
            'fetchCountDemandeChat' => new \Twig_Filter_Method($this, 'fetchCountDemandeChat'),
        );
    }


    /*
        -- Localisation : Au niveau du Twig (layout.html.twig)
        -- Utilisation : Compte le nombre de notifications non vues de type 3 (Demande Chat), Distinct sur le nom d'utilisateur

        -- Input : (int) ID Current User
        -- Output :(int) Count final
    */
    public function fetchCountDemandeChat($iUserID)
    {
        $oUserCurrent = $this->oRepositoryUser->find($iUserID);
        $aIdBloque = (count($oUserCurrent->getContactbloque()) > 0) ? array_keys($oUserCurrent->getContactbloque()) : array(0);

        return $this->oRepositoryNotification->countByType( $iUserID, 3 , $aIdBloque );
    }

    /*
        -- Localisation : Au niveau du Twig (layout.html.twig)
        -- Utilisation : Retourne les données des utilisateurs en ligne

        -- Input : (int) ID Current User
        -- Output :(array) Données des utilisateurs en ligne + données encodés
    */
    public function onlineUsers($oUserID)
    {
        $aData = $this->oRepositoryUser->FetchOnlineUser($oUserID);
        foreach ($aData as $iKey => $sValue) 
        {
            $aData[$iKey]['salt']       = base64_encode('rencontres' . (string) $sValue['salt']);
            $aData[$iKey]['idEncoded']  = base64_encode( (int) $sValue['id'] * 9999);
        }

        // Resize Images
        foreach ($aData as $iKey => $sValue) 
        {
            $aData[$iKey]['image'] = $this->oFonctionDiver->buildThumbnail($sValue['image'], 30, 30);
        }
        return $aData;
    }

    public function getName()
    {
        return 'chatmembers_extension';
    }
}

 ?>