<?php

namespace M\CoreBundle\Twig;

class ActivityExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('activity', array($this, 'activityFilter')),
        );
    }

    public function activityFilter($timestamp)
    {
        $activity = '';
    $seconds = time() - $timestamp; 
    $minutes = (int)($seconds / 60);
    $hours = (int)($minutes / 60);
    $days = (int)($hours / 24);
    if ($days >= 1) {
      $activity = $days . ' jour' . ($days != 1 ? 's' : '');
    } else if ($hours >= 1) {
      $activity = $hours . ' heure' . ($hours != 1 ? 's' : '');
    } else if ($minutes >= 1) {
      $activity = $minutes . ' minute' . ($minutes != 1 ? 's' : '');
    } else {
      $activity = $seconds . ' second' . ($seconds != 1 ? 's' : '');
      return 'il y a quelques seconds';
    }
    return 'il y a '.$activity;
    }

    public function getName()
    {
        return 'activity_extension';
    }
}