<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * recherche
 *
 * @ORM\Table(name="recherche")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\RechercheRepository")
 */
class Recherche
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
    /**
     *
     * @ORM\Column(name="date_recherche", type="datetime", nullable=true)
     */
    private $daterecherche;
    
    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User")
    */
    private $user;

    /**
    * @ORM\Column(name="recherche", type="array", nullable=true)
    */
    private $recherche;
   
   
 public function __construct()
  {

    $this->setDaterecherche( new \DateTime() );
   
  }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Recherche
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set daterecherche
     *
     * @param \DateTime $daterecherche
     * @return Recherche
     */
    public function setDaterecherche($daterecherche)
    {
        $this->daterecherche = $daterecherche;
    
        return $this;
    }

    /**
     * Get daterecherche
     *
     * @return \DateTime 
     */
    public function getDaterecherche()
    {
        return $this->daterecherche;
    }

    /**
     * Set recherche
     *
     * @param array $recherche
     * @return Recherche
     */
    public function setRecherche($recherche)
    {
        $this->recherche = $recherche;
    
        return $this;
    }

    /**
     * Get recherche
     *
     * @return array 
     */
    public function getRecherche()
    {
        return $this->recherche;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return Recherche
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
