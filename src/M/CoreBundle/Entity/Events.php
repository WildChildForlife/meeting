<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Events
 *
 * @ORM\Table(name="events")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\EventsRepository")
 * @UniqueEntity(fields={"titre"}, message="Titre de l'évenement : Ce titre est déjà utilisé.")
 */
class Events
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255)
     */
    protected $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    protected $adresse;

    /**
     * @var integer
     *
     * @ORM\Column(name="places", type="integer")
     */
    protected $places;

    /* gratuite = 0 / payante = 1 */
    /**
    * @var boolean
    *
    * @ORM\Column(name="inscription", type="boolean", nullable=true)
    */
    protected $inscription = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    protected $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=255)
     */
    protected $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255)
     */
    protected $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable = true)
     */
    protected $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="telcontact", type="string", length=255, nullable = true)
     */
    protected $telcontact;

    /**
     * @var integer
     *
     * @ORM\Column(name="prix", type="integer", nullable = true)
     */
    protected $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    protected $dateEvent;

    /**
     * @var integer
     *
     * @ORM\Column(name="heure", type="integer")
     */
    protected $heure;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Ville" , cascade={"persist"})
    */
    protected $ville;

    /**
    * @var string
    *
    * @ORM\Column(name="image", type="text", nullable=false)
    */
    protected $image;

    /* non-modéré = 0 / modéré = 1 */
    /**
    * @var boolean
    *
    * @ORM\Column(name="status", type="boolean", nullable=true)
    */
    protected $status= 0;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    protected $user;

            /* block dashbord events actu non-affiché = 0 / affiché = 1 */
    /**
    * @var boolean
    *
    * @ORM\Column(name="accueil", type="boolean", nullable=true)
    */
    protected $accueil= 0;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Events
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Events
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateEvent
     *
     * @param \DateTime $dateEvent
     * @return Events
     */
    public function setDateEvent($dateEvent)
    {
        $this->dateEvent = $dateEvent;
    
        return $this;
    }

    /**
     * Get dateEvent
     *
     * @return \DateTime 
     */
    public function getDateEvent()
    {
        return $this->dateEvent;
    }

    /**
     * Set heure
     *
     * @param integer $heure
     * @return Events
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;
    
        return $this;
    }

    /**
     * Get heure
     *
     * @return integer 
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Events
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return Events
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;
    
        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Events
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set places
     *
     * @param integer $places
     * @return Events
     */
    public function setPlaces($places)
    {
        $this->places = $places;
    
        return $this;
    }

    /**
     * Get places
     *
     * @return integer 
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Set inscription
     *
     * @param boolean $inscription
     * @return Events
     */
    public function setInscription($inscription)
    {
        $this->inscription = $inscription;
    
        return $this;
    }

    /**
     * Get inscription
     *
     * @return boolean 
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Events
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Events
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    
        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Events
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    
        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Events
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set telcontact
     *
     * @param string $telcontact
     * @return Events
     */
    public function setTelcontact($telcontact)
    {
        $this->telcontact = $telcontact;
    
        return $this;
    }

    /**
     * Get telcontact
     *
     * @return string 
     */
    public function getTelcontact()
    {
        return $this->telcontact;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     * @return Events
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    
        return $this;
    }

    /**
     * Get prix
     *
     * @return integer 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Events
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Events
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set ville
     *
     * @param \M\CoreBundle\Entity\Ville $ville
     * @return Events
     */
    public function setVille(\M\CoreBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return \M\CoreBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return Events
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set accueil
     *
     * @param boolean $accueil
     * @return Events
     */
    public function setAccueil($accueil)
    {
        $this->accueil = $accueil;
    
        return $this;
    }

    /**
     * Get accueil
     *
     * @return boolean 
     */
    public function getAccueil()
    {
        return $this->accueil;
    }
}
