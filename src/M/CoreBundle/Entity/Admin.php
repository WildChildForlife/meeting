<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="admin")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\AdminRepository")
 */
class Admin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nbrconnecte", type="string", length=255)
     */
    protected $nbrconnecte;

    /* affiché = 0 / non-affiché = 1 */
    /**
    * @var boolean
    *
    * @ORM\Column(name="onlineuser", type="boolean", nullable=true)
    */
    protected $onlineuser= 0;

        /* affiché = 0 / non-affiché = 1 */
    /**
    * @var boolean
    *
    * @ORM\Column(name="membreinscrit", type="boolean", nullable=true)
    */
    protected $membreinscrit= 0;

    /**
    * @var string
    *
    * @ORM\Column(name="image", type="text", nullable=true)
    */

    protected $image;

    /**
    * @var string
    *
    * @ORM\Column(name="lienaffiche", type="text", nullable=true)
    */

    protected $lienaffiche;

    /**
     * @var string
     *
     * @ORM\Column(name="codepromo", type="string", length=255 , nullable=true)
     */
    protected $codepromo;

    /**
     * @var string
     *
     * @ORM\Column(name="promodebut", type="datetime", nullable=true)
     */
    protected $promodebut;

    /**
     * @var string
     *
     * @ORM\Column(name="promofin", type="datetime", nullable=true)
     */
    protected $promofin;

    /**
     * @var integer
     *
     * @ORM\Column(name="reduction", type="integer" , nullable=true)
     */
    protected $reduction;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbrconnecte
     *
     * @param string $nbrconnecte
     * @return Admin
     */
    public function setNbrconnecte($nbrconnecte)
    {
        $this->nbrconnecte = $nbrconnecte;
    
        return $this;
    }

    /**
     * Get nbrconnecte
     *
     * @return string 
     */
    public function getNbrconnecte()
    {
        return $this->nbrconnecte;
    }

    /**
     * Set onlineuser
     *
     * @param boolean $onlineuser
     * @return Admin
     */
    public function setOnlineuser($onlineuser)
    {
        $this->onlineuser = $onlineuser;
    
        return $this;
    }

    /**
     * Get onlineuser
     *
     * @return boolean 
     */
    public function getOnlineuser()
    {
        return $this->onlineuser;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Admin
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set membreinscrit
     *
     * @param boolean $membreinscrit
     * @return Admin
     */
    public function setMembreinscrit($membreinscrit)
    {
        $this->membreinscrit = $membreinscrit;
    
        return $this;
    }

    /**
     * Get membreinscrit
     *
     * @return boolean 
     */
    public function getMembreinscrit()
    {
        return $this->membreinscrit;
    }

    /**
     * Set lienaffiche
     *
     * @param string $lienaffiche
     * @return Admin
     */
    public function setLienaffiche($lienaffiche)
    {
        $this->lienaffiche = $lienaffiche;
    
        return $this;
    }

    /**
     * Get lienaffiche
     *
     * @return string 
     */
    public function getLienaffiche()
    {
        return $this->lienaffiche;
    }

    /**
     * Set codepromo
     *
     * @param string $codepromo
     * @return Admin
     */
    public function setCodepromo($codepromo)
    {
        $this->codepromo = $codepromo;
    
        return $this;
    }

    /**
     * Get codepromo
     *
     * @return string 
     */
    public function getCodepromo()
    {
        return $this->codepromo;
    }

    /**
     * Set promodebut
     *
     * @param \DateTime $promodebut
     * @return Admin
     */
    public function setPromodebut($promodebut)
    {
        $this->promodebut = $promodebut;
    
        return $this;
    }

    /**
     * Get promodebut
     *
     * @return \DateTime 
     */
    public function getPromodebut()
    {
        return $this->promodebut;
    }

    /**
     * Set promofin
     *
     * @param \DateTime $promofin
     * @return Admin
     */
    public function setPromofin($promofin)
    {
        $this->promofin = $promofin;
    
        return $this;
    }

    /**
     * Get promofin
     *
     * @return \DateTime 
     */
    public function getPromofin()
    {
        return $this->promofin;
    }

    /**
     * Set reduction
     *
     * @param integer $reduction
     * @return Admin
     */
    public function setReduction($reduction)
    {
        $this->reduction = $reduction;
    
        return $this;
    }

    /**
     * Get reduction
     *
     * @return integer 
     */
    public function getReduction()
    {
        return $this->reduction;
    }
}
