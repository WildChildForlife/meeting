<?php

namespace M\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CentresinteretRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProfessionRepository extends EntityRepository
{

	public function fetchAllProfession()
	{
		
		return $this->getEntityManager()
				->createQuery("SELECT p.id,p.nom
							   FROM MCoreBundle:Profession p 
							  ")
				->getResult();	
	}

}
