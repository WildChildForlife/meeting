<?php

namespace M\CoreBundle\Entity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="Email : Cet email est déjà utilisé.")
 * @UniqueEntity(fields={"username"}, message="Pseudo : Ce pseudo est déjà utilisé.")
 */
class User implements UserInterface, \Serializable
{
    /**
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @ORM\Column(name="sexe", type="boolean")
    */
    protected $sexe;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Ville" , cascade={"persist"})
    */
    protected $ville;
    /**
    * @ORM\OneToOne(targetEntity="M\CoreBundle\Entity\UserIdeal" , cascade={"persist"})
    */
    protected $userideal;
    /**
    * @ORM\Column(name="username", type="string", length=255, unique=true, nullable=true)
    * @Assert\Regex(
    *     pattern ="/[a-zA-Z0-9]{5,12}/",
    *     message = "Pseudo : Votre pseudo est incorrect"
    * )
    */
    protected $username;

    /**
    * @ORM\Column(name="nom", type="string", length=255 , nullable=true)
    * @Assert\Regex(
    *     pattern="/[A-Za-z]{2,20}/",
    *     message="Nom : Votre nom ne peut pas contenir de nombre"
    * )
    */
    protected $nom;

    /**
    * @ORM\Column(name="prenom", type="string", length=255 , nullable=true)
    * @Assert\Regex(
    *     pattern="/[A-Za-z]{2,20}/",
    *     message="Prenom : Votre prenom ne peut pas contenir de nombre"
    * )
    */
    protected $prenom;

    /**
    * @ORM\Column(name="email", type="string", length=255)
    * @Assert\Email(message="Email : L'email est incorrect")
    */
    protected $email;

    /**
    * @ORM\Column(name="password", type="string", length=255, nullable=true)
    * @Assert\Regex(
    *     pattern="/^(?=.*?[A-Za-z])(?=.*?[0-9]).{8,}$/",
    *     message="Mot de passe : Veuillez respecter le format requis"
    * )
    */
    protected $password;

    /**
    * @ORM\Column(name="salt", type="string", length=255 , nullable=true)
    */
    protected $salt;

    /**
    * @ORM\Column(name="roles", type="array", nullable=true)
    */
    protected $roles;

    /**
    * @ORM\Column(name="actif", type="boolean" , nullable=true)
    */
    protected $actif = 0;

    /**
    * @ORM\Column(name="adresse", type="string" , length=255 , nullable=true)
    */
    protected $adresse;

    /**
    * @ORM\Column(name="adresse_suite", type="string" , length=255 , nullable=true)
    */
    protected $adresse_suite;

    /**
    * @ORM\Column(name="code_postal", type="string" , length=255 , nullable=true)
    */
    protected $code_postal;

    /**
    * @ORM\Column(name="telephone", type="string" , length=255 , nullable=true)
    */
    protected $telephone;

    /**
    * @ORM\Column(name="ip", type="string" , length=20 , nullable=true)
    */
    protected $ip;

    /**
    * @ORM\Column(name="temps_connexion", type="string" , length=255, nullable=true )
    */
    protected $temps_connexion = 0;

    /**
    * @ORM\Column(name="date_inscription", type="datetime" , nullable=true)
    */
    protected $date_inscription;

    /**
    * @ORM\Column(name="date_activation", type="datetime" , nullable=true)
    */
    protected $date_activation;

    /**
    * @ORM\Column(name="marital", type="string" , length=255, nullable=true)
    */
    protected $marital;

    /**
    * @ORM\Column(name="vestimentaire", type="string" , length=255, nullable=true)
    */
    protected $vestimentaire;

    /**
    * @ORM\Column(name="sport", type="string", length=255, nullable=true)
    */
    protected $sport;

    /**
    * @ORM\Column(name="fumeur", type="integer", nullable=true)
    */
    protected $fumeur;

    /**
    * @ORM\Column(name="nbEnfant", type="integer" , nullable=true)
    */
    protected $nbEnfant;

    /**
    * @ORM\Column(name="datenaissance", type="string" , length=255, nullable=true )
    */
    protected $datenaissance;

    /**
    * @ORM\Column(name="description", type="text", nullable=true)
    * @Assert\Regex(
    *     pattern="/.{80,1000}/",
    *     message="Mot de passe : Votre mot de passe est incorrect"
    * )
    */
    protected $description;

    /**
    * @ORM\Column(name="ancienne_description", type="text", nullable=true)
    */
    protected $ancienne_description;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Nationalite", cascade={"persist"})
    */
    protected $nationalite;

    /**
    * @ORM\Column(name="taille", type="string" , length=255 , nullable=true)
    */
    protected $taille;

    /**
    * @ORM\Column(name="silhouette", type="string" , length=255 , nullable=true)
    */
    protected $silhouette;

    /**
    * @ORM\Column(name="yeux", type="string" , length=255 , nullable=true)
    */
    protected $yeux;

    /**
    * @ORM\Column(name="cheveux", type="string" , length=255 , nullable=true)
    */
    protected $cheveux;

    /**
    * @ORM\Column(name="etudes", type="string" , length=255 , nullable=true)
    */
    protected $etudes;

    /**
    * @ORM\Column(name="secteur", type="string" , length=255 , nullable=true)
    */
    protected $secteur;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Profession", cascade={"persist"})
    */
    protected $profession;

    /**
    * @ORM\Column(name="parcours", type="text", nullable=true)
    */
    protected $parcours;

    /**
    * @ORM\ManyToMany(targetEntity="M\CoreBundle\Entity\Qualites", cascade={"persist"})
    */
    protected $qualites;

    /**
    * @ORM\ManyToMany(targetEntity="M\CoreBundle\Entity\Centresinteret", cascade={"persist"})
    */
    protected $centresinteret;

    /**
    * @ORM\Column(name="centresinteretautre", type="array", nullable=true)
    */
    protected $centresinteretautre;

    /**
    * @ORM\Column(name="contact", type="array")
    */
    protected $contact = array();

    /**
    * @ORM\Column(name="allowToAlbum", type="array")
    */
    protected $allowToAlbum = array();

    /**
    * @ORM\Column(name="contactbloque", type="array")
    */
    protected $contactbloque = array();

    /**
    * @ORM\Column(name="alertemail", type="array")
    */
    protected $alertemail = array();

    /**
    * @ORM\Column(name="desinscription", type="array")
    */
    protected $desinscription = array();

    /**
    * @ORM\Column(name="questions", type="array")
    */
    protected $questions;

    /**
    * @var boolean
    *
    * @ORM\Column(name="description_moderer", type="boolean")
    */
    protected $description_moderer = 0;

    /**
    * @var boolean
    *
    * @ORM\Column(name="signalement_moderer", type="boolean")
    */
    protected $signalement_moderer = 1;

    /**
    * @ORM\Column(name="code_activation", type="string" , length=255 , nullable=true)
    */
    protected $code_activation;

    /**
    * @ORM\Column(name="code_recuperation", type="string" , length=255 , nullable=true)
    */
    protected $code_recuperation;


    /* 
       Step 0 = 0 
       Step 1 = 1
       Step 2 = 2
       Step 3 = 3
       Inscription finalisée = 4
    */
    /**
    * @ORM\Column(name="etat_inscription", type="integer" , nullable=true)
    */
    protected $etat_inscription = 0;


    /* PRIVE = 0 / PUBLIC = 1 */
    /**
    * @var boolean
    *
    * @ORM\Column(name="status_album", type="boolean", nullable=true)
    */
    protected $status_album = 0;

    /**
    * @ORM\Column(name="lastActivity", type="datetime" , nullable=true)
    */
    protected $lastActivity;
    protected $inscritDepuis;
    protected $tailleDebut;
    protected $tailleFin;
    protected $EnLigne;
    protected $ageDebut;
    protected $ageFin;
    protected $oldpassword;

      /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="M\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="parrain_id", referencedColumnName="id", nullable = true)
     */
    private $parrain = null;

    /**
   * @ORM\OneToMany(targetEntity="M\CoreBundle\Entity\Images", mappedBy="user")
   */
  private $images;

  /**
    * @ORM\Column(name="inchatbox", type="array", nullable=true)
    */
    protected $inchatbox;


    public function __construct()
    {
        $this->roles = array();
        $this->inchatbox = array();
        $this->salt  = md5(uniqid(null, true)); 
        $this->setIp( @$_SERVER['REMOTE_ADDR'] );
        $this->setDateInscription( new \DateTime() );
        $this->setLastActivity( new \DateTime() );
        $this->qualites = new \Doctrine\Common\Collections\ArrayCollection();
        $this->centresinteret = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setAlertemail(array("message" => "1","mail" => "1",  "event" => "1"));

    }
    
    public function setOldpassword($oldpassword)  
    {
        $this->oldpassword = $oldpassword;
        return $this;
    }
 
    public function getOldpassword()  
    {
        return $this->oldpassword;
    }
/**
     * Set inscritDepuis
     *
     * @param integer $inscritDepuis
     * 
     */
    public function setInscritDepuis($inscritDepuis)
    {
        $this->inscritDepuis = $inscritDepuis;
    
        return $this;
    }

    /**
     * Get inscritDepuis
     *
     * @return integer 
     */
    public function getInscritDepuis()
    {
        return $this->inscritDepuis;
    }
    /**
     * Set tailleDebut
     *
     * @param integer $tailleDebut
     * @return UserIdeal
     */
    public function setTailleDebut($tailleDebut)
    {
        $this->tailleDebut = $tailleDebut;
    
        return $this;
    }

    /**
     * Get tailleDebut
     *
     * @return integer 
     */
    public function getTailleDebut()
    {
        return $this->tailleDebut;
    }

    /**
     * Set tailleFin
     *
     * @param integer $tailleFin
     * @return UserIdeal
     */
    public function setTailleFin($tailleFin)
    {
        $this->tailleFin = $tailleFin;
    
        return $this;
    }

    /**
     * Get tailleFin
     *
     * @return integer 
     */
    public function getTailleFin()
    {
        return $this->tailleFin;
    }
   
    /**
     * Set questions
     *
     * @param array $questions
     * @return User
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
    
        return $this;
    }

    public function setQuestionsBuild($_questions)
    {
        $questions = array();

        foreach ($_questions as $key => $value) {
           if(!empty($value))$questions[$key] = $value;
        }

        if(!empty($questions))$this->setQuestions($questions);

        return $this;
    }

    /**
     * Get questions
     *
     * @return array 
     */
    public function getQuestions()
    {
        return $this->questions;
    }

  /**
     * Set centresinteretautre
     *
     * @param array $centresinteretautre
     * @return User
     */
    public function setCentresinteretautre($centresinteretautre)
    {
        $this->centresinteretautre = $centresinteretautre;
    
        return $this;
    }

    public function setCentresinteretautreBuild($_centresinteretautre)
    {
        $centresinteretautre = array();
 
        foreach ($_centresinteretautre as $key => $value) {
            if(!empty($value))$centresinteretautre[$key] = $value;
        }
  
        if(!empty($centresinteretautre))$this->setCentresinteretautre($centresinteretautre);

        return $this;
    }

    /**
     * Get centresinteretautre
     *
     * @return array 
     */
    public function getCentresinteretautre()
    {
        return $this->centresinteretautre;
    }


  /**
    * Add Qualite
    *
    * @param \M\CoreBundle\Entity\Qualites $qualites
    */
  public function addQualite(\M\CoreBundle\Entity\Qualites $qualite) // Qualite sans « s » !
  {
    // Ici, on utilise l'ArrayCollection vraiment comme un tableau, avec la syntaxe []
    $this->qualites[] = $qualite;
  }
 
  /**
    * Remove Qualite
    *
    * @param \M\CoreBundle\Entity\Qualites $qualite
    */
  public function removeQualite(\M\CoreBundle\Entity\Qualites $qualite) // Qualite sans « s » !
  {
    // Ici on utilise une méthode de l'ArrayCollection, pour supprimer la Qualite en argument
    $this->qualites->removeElement($qualite);
  }
 
  /**
    * Get Qualite
    *
    * @return Doctrine\Common\Collections\Collection
    */
  public function getQualites() // Notez le « s », on récupère une liste de Qualite ici !
  {
    return $this->qualites;
  }

  /**
    * Add Centresinteret
    *
    * @param \M\CoreBundle\Entity\Centresinteret $Centresinteret
    */
  public function addCentreinteret(\M\CoreBundle\Entity\Centresinteret $centreinteret) // Centreinteret sans « s » !
  {
    // Ici, on utilise l'ArrayCollection vraiment comme un tableau, avec la syntaxe []
    $this->centresinteret[] = $centreinteret;
  
  }
 
  /**
    * Remove Centresinteret
    *
    * @param \M\CoreBundle\Entity\Centresinteret $Centresinteret
    */
  public function removeCentreinteret(\M\CoreBundle\Entity\Centresinteret $centreinteret) // Qualite sans « s » !
  {
    // Ici on utilise une méthode de l'ArrayCollection, pour supprimer la Qualite en argument
    $this->centresinteret->removeElement($centreinteret);
  }
 
  /**
    * Get Centresinteret
    *
    * @return Doctrine\Common\Collections\Collection
    */
  public function getCentresinteret() // Notez le « s », on récupère une liste de centresinteret ici !
  {
    return $this->centresinteret;
  }

  public function __sleep()
  {
      return array('id');
  }
 
  public function getId()  {
    return $this->id;
  }
 
  public function setUsername($username)  {
    $this->username = ucfirst($username);
    return $this;
  }
 
  public function getUsername()  {
    return $this->username;
  }
 
  public function setPassword($password)  {
    $this->password = $password;
    return $this;
  }
 
  public function getPassword()  {
    return $this->password;
  }
 
  public function setSalt($salt)  {
    $this->salt = $salt;
    return $this;
  }
 
  public function getSalt()  {
    return $this->salt;
  }
 
  public function setRoles(array $roles)  {
    $this->roles = $roles;
    return $this;
  }
 
  public function getRoles() 
  {
    return $this->roles;
  }
 
  public function eraseCredentials() 
  {

  }
  public function isEqualTo(UserInterface $user) 
  {
    return $this->username === $user->getUsername();
  }

  /**
   * Set nom
   *
   * @param string $nom
   * @return User
   */
  public function setNom($nom)
  {
      $this->nom = ucfirst(strtolower($nom));
  
      return $this;
  }

  /**
   * Get nom
   *
   * @return string 
   */
  public function getNom()
  {
      return ucfirst(strtolower($this->nom));
  }

  /**
   * Set prenom
   *
   * @param string $prenom
   * @return User
   */
  public function setPrenom($prenom)
  {
      $this->prenom = ucfirst(strtolower($prenom));
  
      return $this;
  }

  /**
   * Get prenom
   *
   * @return string 
   */
  public function getPrenom()
  {
      return ucfirst(strtolower($this->prenom));
  }

  /**
   * Set email
   *
   * @param string $email
   * @return User
   */
  public function setEmail($email)
  {
      $this->email = $email;
  
      return $this;
  }

  /**
   * Get email
   *
   * @return string 
   */
  public function getEmail()
  {
      return $this->email;
  }


    /**
     * Set actif
     *
     * @param boolean $actif
     * @return User
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    
        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = ucfirst($adresse);
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return ucfirst($this->adresse);
    }

    /**
     * Set adresse_suite
     *
     * @param string $adresseSuite
     * @return User
     */
    public function setAdresseSuite($adresseSuite)
    {
        $this->adresse_suite = ucfirst($adresseSuite);
    
        return $this;
    }

    /**
     * Get adresse_suite
     *
     * @return string 
     */
    public function getAdresseSuite()
    {
        return $this->adresse_suite;
    }

    /**
     * Set ville
     *
     * @param \M\CoreBundle\Entity\Ville $ville
     * @return User
     */
    public function setVille(\M\CoreBundle\Entity\Ville $ville)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return \M\CoreBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return User
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }


    public function serialize()
    {
        return serialize(array(
        $this->id,
        $this->password,
        $this->username
        ));
    }

    public function unserialize($serialized)
    {
        list(
        $this->id,
        $this->password,
        $this->username) = unserialize($serialized);
    }

    /**
     * Set date_inscription
     *
     * @param \DateTime $dateInscription
     * @return User
     */
    public function setDateInscription($dateInscription)
    {
        $this->date_inscription = $dateInscription;
    
        return $this;
    }

    /**
     * Get date_inscription
     *
     * @return \DateTime 
     */
    public function getDateInscription()
    {
        return $this->date_inscription;
    }

    /**
     * Set date_activation
     *
     * @param \DateTime $dateActivation
     * @return User
     */
    public function setDateActivation($dateActivation)
    {
        $this->date_activation = $dateActivation;
    
        return $this;
    }

    /**
     * Get date_activation
     *
     * @return \DateTime 
     */
    public function getDateActivation()
    {
        return $this->date_activation;
    }
    

    /**
     * Set nbEnfant
     *
     * @param integer $nbEnfant
     * @return User
     */
    public function setNbEnfant($nbEnfant)
    {
        $this->nbEnfant = $nbEnfant;
    
        return $this;
    }

    /**
     * Get nbEnfant
     *
     * @return integer 
     */
    public function getNbEnfant()
    {
        return $this->nbEnfant;
    }

    /**
     * Set taille
     *
     * @param string $taille
     * @return User
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    
        return $this;
    }

    /**
     * Get taille
     *
     * @return string 
     */
    public function getTaille()
    {
        return $this->taille;
    }



    /**
     * Set EnLigne
     *
     * @param integer $EnLigne
     * @return User
     */
    public function setEnLigne($EnLigne)
    {
        $this->EnLigne = $EnLigne;
    
        return $this;
    }

    /**
     * Get EnLigne
     *
     * @return integer 
     */
    public function getEnLigne()
    {
        return $this->EnLigne;
    }

    /**
     * Set Agedebut
     *
     * @param integer $agedebut
     * @return User
     */
    public function setAgedebut($ageDebut)
    {
        $this->ageDebut = $ageDebut;
    
        return $this;
    }

    /**
     * Get AgeDebut
     *
     * @return integer 
     */
    public function getAgedebut()
    {
        return $this->ageDebut;
    }

    /**
     * Set Agefin
     *
     * @param integer $agefin
     * @return User
     */
    public function setAgefin($ageFin)
    {
        $this->ageFin = $ageFin;
    
        return $this;
    }

    /**
     * Get AgeFin
     *
     * @return integer 
     */
    public function getAgefin()
    {
        return $this->ageFin;
    }

    /**
     * Set silhouette
     *
     * @param string $silhouette
     * @return User
     */
    public function setSilhouette($silhouette)
    {
        $this->silhouette = $silhouette;
    
        return $this;
    }

    /**
     * Get silhouette
     *
     * @return string 
     */
    public function getSilhouette()
    {
        return $this->silhouette;
    }

    /**
     * Set etudes
     *
     * @param string $etudes
     * @return User
     */
    public function setEtudes($etudes)
    {
        $this->etudes = $etudes;
    
        return $this;
    }

    /**
     * Get etudes
     *
     * @return string 
     */
    public function getEtudes()
    {
        return $this->etudes;
    }

    /**
     * Set secteur
     *
     * @param string $secteur
     * @return User
     */
    public function setSecteur($secteur)
    {
        $this->secteur = $secteur;
    
        return $this;
    }

    /**
     * Get secteur
     *
     * @return string 
     */
    public function getSecteur()
    {
        return $this->secteur;
    }


    /**
     * Set parcours
     *
     * @param string $parcours
     * @return User
     */
    public function setParcours($parcours)
    {
        $this->parcours = $parcours;
    
        return $this;
    }

    /**
     * Get parcours
     *
     * @return string 
     */
    public function getParcours()
    {
        return $this->parcours;
    }
    
     /**
     * Set marital
     *
     * @param string $marital
     * @return User
     */
    public function setMarital($marital)
    {
        $this->marital = $marital;
    
        return $this;
    }

    /**
     * Get marital
     *
     * @return string
     */
    public function getMarital()
    {
        return $this->marital;
    }
    

    /**
     * Set nationalite
     *
     * @param \M\CoreBundle\Entity\Nationalite $nationalite
     * @return User
     */
    public function setNationalite(\M\CoreBundle\Entity\Nationalite $nationalite = null)
    {
        $this->nationalite = $nationalite;
    
        return $this;
    }

    /**
     * Get nationalite
     *
     * @return \M\CoreBundle\Entity\Nationalite 
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set yeux
     *
     * @param string
     * @return User
     */
    public function setYeux($yeux)
    {
        $this->yeux = $yeux;
    
        return $this;
    }

    /**
     * Get yeux
     *
     * @return string
     */
    public function getYeux()
    {
        return $this->yeux;
    }

    /**
     * Set cheveux
     *
     * @param string
     * @return User
     */
    public function setCheveux($cheveux)
    {
        $this->cheveux = $cheveux;
    
        return $this;
    }

    /**
     * Get cheveux
     *
     * @return string
     */
    public function getCheveux()
    {
        return $this->cheveux;
    }

  
    /**
     * Set sexe
     *
     * @param boolean $sexe
     * @return User
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return boolean 
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set majeur
     *
     * @param boolean $majeur
     * @return User
     */
    public function setMajeur($majeur)
    {
        $this->majeur = $majeur;

        return $this;
    }

    /**
     * Get majeur
     *
     * @return boolean 
     */
    public function getMajeur()
    {
        return $this->majeur;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return User
     */
    public function setAge($age)
    {   
      
        $this->age = $age;
        
        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        $t = time();
        $age = ($this->getDatenaissance() < 0) ? ( $t + ($this->getDatenaissance() * -1) ) : $t - $this->getDatenaissance();
        return floor($age/31536000);
    }

    /**
     * Set signe
     *
     * @param string $signe
     * @return User
     */
    public function setSigne($signe)
    {   

        $this->signe = $signe;

        return $signe;
    }

    /**
     * Get signe
     *
     * @return string 
     */
    public function getSigne()
    {
          $dDate = date('m/d', $this->getDatenaissance());

         // Date format dd/mm
          if ( $dDate >  "01/21" && $dDate <  "02/18") return "Verseau";
          if ( $dDate >  "02/19" && $dDate <  "03/20") return "Poissons";
          if ( $dDate >  "03/21" && $dDate <  "04/20") return "Bélier";
          if ( $dDate >  "04/21" && $dDate <  "05/20") return "Taureau";
          if ( $dDate >  "05/21" && $dDate <  "06/21") return "Gémeaux";
          if ( $dDate >  "06/22" && $dDate <  "07/22") return "Cancer";
          if ( $dDate >  "07/23" && $dDate <  "08/22") return "Lion";
          if ( $dDate >  "08/23" && $dDate <  "09/22") return "Vierge";
          if ( $dDate >  "09/23" && $dDate <  "10/22") return "Balance";
          if ( $dDate >  "10/23" && $dDate <  "11/22") return "Scorpion";
          if ( $dDate >  "11/23" && $dDate <  "12/21") return "Sagittaire";
          if ( $dDate >  "12/22" && $dDate >  "01/20") return "Capricorne";


    }

    /**
     * Set datenaissance
     *
     * @param \DateTime $datenaissance
     * @return User
     */
    public function setDatenaissance($datenaissance)
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    /**
     * Get datenaissance
     *
     * @return \DateTime 
     */
    public function getDatenaissance()
    {
        return $this->datenaissance;
    }

    
    /**
     * Set fumeur
     *
     * @param integer $fumeur
     * @return User
     */
    public function setFumeur($fumeur)
    {
        $this->fumeur = $fumeur;

        return $this;
    }

    /**
     * Get fumeur
     *
     * @return integer 
     */
    public function getFumeur()
    {
        return $this->fumeur;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set contact
     *
     * @param array $contact
     * @return User
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return array 
     */
    public function getContact()
    {
        return $this->contact;
    }

  
    /**
     * Set lastActivity
     *
     * @param \DateTime $lastActivity
     * @return User
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
    
        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return \DateTime 
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    public function isActiveNow()
    {
        $this->setLastActivity(new \DateTime());
    }

    /**
     * Set profession
     *
     * @param \M\CoreBundle\Entity\Profession $profession
     * @return User
     */
    public function setProfession(\M\CoreBundle\Entity\Profession $profession = null)
    {
        $this->profession = $profession;
    
        return $this;
    }

    /**
     * Get profession
     *
     * @return \M\CoreBundle\Entity\Profession 
     */
    public function getProfession()
    {
        return $this->profession;
    }

    

    /**
     * Add centresinteret
     *
     * @param \M\CoreBundle\Entity\Centresinteret $centresinteret
     * @return User
     */
    public function addCentresinteret(\M\CoreBundle\Entity\Centresinteret $centresinteret)
    {
        $this->centresinteret[] = $centresinteret;
    
        return $this;
    }

    /**
     * Remove centresinteret
     *
     * @param \M\CoreBundle\Entity\Centresinteret $centresinteret
     */
    public function removeCentresinteret(\M\CoreBundle\Entity\Centresinteret $centresinteret)
    {
        $this->centresinteret->removeElement($centresinteret);
    }

    /**
     * Set userideal
     *
     * @param \M\CoreBundle\Entity\UserIdeal $userideal
     * @return User
     */
    public function setUserideal(\M\CoreBundle\Entity\UserIdeal $userideal = null)
    {
        $this->userideal = $userideal;
    
        return $this;
    }

    /**
     * Get userideal
     *
     * @return \M\CoreBundle\Entity\UserIdeal 
     */
    public function getUserideal()
    {
        return $this->userideal;
    }

    /**
     * Set status_album
     *
     * @param boolean $statusAlbum
     * @return User
     */
    public function setStatusAlbum($statusAlbum)
    {
        $this->status_album = $statusAlbum;
    
        return $this;
    }

    /**
     * Get status_album
     *
     * @return boolean 
     */
    public function getStatusAlbum()
    {
        return $this->status_album;
    }

    /**
     * Set etat_inscription
     *
     * @param integer $etatInscription
     * @return User
     */
    public function setEtatInscription($etatInscription)
    {
        $this->etat_inscription = $etatInscription;
    
        return $this;
    }

    /**
     * Get etat_inscription
     *
     * @return integer 
     */
    public function getEtatInscription()
    {
        return $this->etat_inscription;
    }

    /**
     * Set vestimentaire
     *
     * @param string $vestimentaire
     * @return User
     */
    public function setVestimentaire($vestimentaire)
    {
        $this->vestimentaire = $vestimentaire;
    
        return $this;
    }

    /**
     * Get vestimentaire
     *
     * @return string 
     */
    public function getVestimentaire()
    {
        return $this->vestimentaire;
    }

    /**
     * Set sport
     *
     * @param string $sport
     * @return User
     */
    public function setSport($sport)
    {
        $this->sport = $sport;
    
        return $this;
    }

    /**
     * Get sport
     *
     * @return string 
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set temps_connexion
     *
     * @param string $tempsConnexion
     * @return User
     */
    public function setTempsConnexion($tempsConnexion)
    {
        $this->temps_connexion = $tempsConnexion;
    
        return $this;
    }

    /**
     * Get temps_connexion
     *
     * @return string 
     */
    public function getTempsConnexion()
    {
        return $this->temps_connexion;
    }

    /**
     * Set contactbloque
     *
     * @param array $contactbloque
     * @return User
     */
    public function setContactbloque($contactbloque)
    {
        $this->contactbloque = $contactbloque;
    
        return $this;
    }

    /**
     * Get contactbloque
     *
     * @return array 
     */
    public function getContactbloque()
    {
        return $this->contactbloque;
    }

    /**
     * Set allowToAlbum
     *
     * @param array $allowToAlbum
     * @return User
     */
    public function setAllowToAlbum($allowToAlbum)
    {
        $this->allowToAlbum = $allowToAlbum;
    
        return $this;
    }

    /**
     * Get allowToAlbum
     *
     * @return array 
     */
    public function getAllowToAlbum()
    {
        return $this->allowToAlbum;
    }

    /**
     * Set alertemail
     *
     * @param array $alertemail
     * @return User
     */
    public function setAlertemail($alertemail)
    {
        $this->alertemail = $alertemail;
    
        return $this;
    }

    /**
     * Get alertemail
     *
     * @return array 
     */
    public function getAlertemail()
    {
        return $this->alertemail;
    }

    /**
     * Set desinscription
     *
     * @param array $desinscription
     * @return User
     */
    public function setDesinscription($desinscription)
    {
        $this->desinscription = $desinscription;
    
        return $this;
    }

    /**
     * Get desinscription
     *
     * @return array 
     */
    public function getDesinscription()
    {
        return $this->desinscription;
    }

    /**
     * Add events
     *
     * @param \M\CoreBundle\Entity\Events $events
     * @return User
     */
    public function addEvent(\M\CoreBundle\Entity\Events $events)
    {
        $this->events[] = $events;
    
        return $this;
    }

    /**
     * Remove events
     *
     * @param \M\CoreBundle\Entity\Events $events
     */
    public function removeEvent(\M\CoreBundle\Entity\Events $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Set description_moderer
     *
     * @param boolean $descriptionModerer
     * @return User
     */
    public function setDescriptionModerer($descriptionModerer)
    {
        $this->description_moderer = $descriptionModerer;
    
        return $this;
    }

    /**
     * Get description_moderer
     *
     * @return boolean 
     */
    public function getDescriptionModerer()
    {
        return $this->description_moderer;
    }

    /**
     * Set ancienne_description
     *
     * @param string $ancienneDescription
     * @return User
     */
    public function setAncienneDescription($ancienneDescription)
    {
        $this->ancienne_description = $ancienneDescription;
    
      return $this;
    }

    /**
     * Get ancienne_description
     *
     * @return string 
     */
    public function getAncienneDescription()
    {
        return $this->ancienne_description;
    }

    /**
     * Set signalement_moderer
     *
     * @param boolean $signalementModerer
     * @return User
     */
    public function setSignalementModerer($signalementModerer)
    {
        $this->signalement_moderer = $signalementModerer;
   
       return $this;
    }

    /**
     * Get signalement_moderer
     *
     * @return boolean 
     */
    public function getSignalementModerer()
    {
        return $this->signalement_moderer;
    }

    /**
     * Set code_activation
     *
     * @param string $codeActivation
     * @return User
     */
    public function setCodeActivation($codeActivation)
    {
        $this->code_activation = $codeActivation;
    
        return $this;
    }

    /**
     * Get code_activation
     *
     * @return string 
     */
    public function getCodeActivation()
    {
        return $this->code_activation;

    }

    /**
     * Set code_postal
     *
     * @param string $codePostal
     * @return User
     */
    public function setCodePostal($codePostal)
    {
        $this->code_postal = $codePostal;
    
        return $this;
    }

    /**
     * Get code_postal
     *
     * @return string 
     */
    public function getCodePostal()
    {
        return $this->code_postal;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set code_recuperation
     *
     * @param string $codeRecuperation
     * @return User
     */
    public function setCodeRecuperation($codeRecuperation)
    {
        $this->code_recuperation = $codeRecuperation;
    
        return $this;
    }

    /**
     * Get code_recuperation
     *
     * @return string 
     */
    public function getCodeRecuperation()
    {
        return $this->code_recuperation;
    }

    /**
     * Set parrain
     *
     * @param \M\CoreBundle\Entity\User $parrain
     * @return User
     */
    public function setParrain(\M\CoreBundle\Entity\User $parrain = null)
    {
        $this->parrain = $parrain;

        return $this;
    }

    /**
     * Get parrain
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getParrain()
    {
        return $this->parrain;
    }

    /**
     * Add images
     *
     * @param \M\CoreBundle\Entity\Images $images
     * @return User
     */
    public function addImage(\M\CoreBundle\Entity\Images $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \M\CoreBundle\Entity\Images $images
     */
    public function removeImage(\M\CoreBundle\Entity\Images $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set inchatbox
     *
     * @param array $inchatbox
     * @return User
     */
    public function setInchatbox($inchatbox)
    {
        $this->inchatbox = $inchatbox;

        return $this;
    }

    /**
     * Get inchatbox
     *
     * @return array 
     */
    public function getInchatbox()
    {
        return $this->inchatbox;
    }
}
