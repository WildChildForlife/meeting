<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * moderation
 *
 * @ORM\Table("moderation")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\ModerationRepository")
 */
class moderation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModeration", type="datetime")
     */
    protected $dateModeration;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    protected $type;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    protected $Moderateur;

 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateModeration
     *
     * @param \DateTime $dateModeration
     * @return moderation
     */
    public function setDateModeration($dateModeration)
    {
        $this->dateModeration = $dateModeration;
    
        return $this;
    }

    /**
     * Get dateModeration
     *
     * @return \DateTime 
     */
    public function getDateModeration()
    {
        return $this->dateModeration;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return moderation
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Moderateur
     *
     * @param \M\CoreBundle\Entity\User $moderateur
     * @return moderation
     */
    public function setModerateur(\M\CoreBundle\Entity\User $moderateur = null)
    {
        $this->Moderateur = $moderateur;
    
        return $this;
    }

    /**
     * Get Moderateur
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getModerateur()
    {
        return $this->Moderateur;
    }
}
