<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateadd", type="datetime")
     */
    private $dateadd;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User")
    */
    protected $user;

     /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Publication", inversedBy ="commentaires")
    * @ORM\JoinColumn(name="publication_id", referencedColumnName="id", onDelete="CASCADE")
    */
    protected $publication;

     public function __construct()
    {
        $this->dateadd = new \DateTime();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Commentaire
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set dateadd
     *
     * @param \DateTime $dateadd
     * @return Commentaire
     */
    public function setDateadd($dateadd)
    {
        $this->dateadd = $dateadd;

        return $this;
    }

    /**
     * Get dateadd
     *
     * @return \DateTime 
     */
    public function getDateadd()
    {
        return $this->dateadd;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return Commentaire
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set publication
     *
     * @param \M\CoreBundle\Entity\Publication $publication
     * @return Commentaire
     */
    public function setPublication(\M\CoreBundle\Entity\Publication $publication = null)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return \M\CoreBundle\Entity\Publication 
     */
    public function getPublication()
    {
        return $this->publication;
    }
}
