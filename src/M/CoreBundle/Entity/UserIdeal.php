<?php

namespace M\CoreBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="userideal")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\UserIdealRepository")
  */
class UserIdeal 
{
    /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;
    /**
  * @ORM\OneToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
  */
  protected $user;

  /**
  * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Ville" , cascade={"persist"})
  */
  protected $ville;
  
  /**
  * @ORM\Column(name="marital", type="string" , length=255, nullable=true)
  */
  protected $marital;

   /**
  * @ORM\Column(name="vestimentaire", type="string" , length=255, nullable=true)
  */
  protected $vestimentaire;

  /**
  * @ORM\Column(name="sport", type="string",nullable=true)
  */
  protected $sport;

  /**
  * @ORM\Column(name="fumeur", length=255 , nullable=true)
  */
  protected $fumeur;

  /**
  * @ORM\Column(name="nbEnfant", type="integer" , nullable=true)
  */
  protected $nbEnfant;

  /**
  * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Nationalite")
  */
  protected $nationalite;

  /**
  * @ORM\Column(name="silhouette", type="string" , length=255 , nullable=true)
  */
  protected $silhouette;
  
  /**
  * @ORM\Column(name="yeux", type="string" , length=255 , nullable=true)
  */
  protected $yeux;

  /**
  * @ORM\Column(name="cheveux", type="string" , length=255 , nullable=true)
  */
  protected $cheveux;

  /**
  * @ORM\Column(name="etudes", type="string" , length=255 , nullable=true)
  */
  protected $etudes;
   /**
  * @ORM\Column(name="ageDebut", type="integer" , nullable=true)
  */
  protected $ageDebut;
   /**
  * @ORM\Column(name="ageFin", type="integer" , nullable=true)
  */
  protected $ageFin;
   /**
  * @ORM\Column(name="tailleDebut", type="integer" , nullable=true)
  */
  protected $tailleDebut;
   /**
  * @ORM\Column(name="tailleFin", type="integer" , nullable=true)
  */
  protected $tailleFin;

  
  public function __construct()
  {
   
  }
    

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marital
     *
     * @param string $marital
     * @return UserIdeal
     */
    public function setMarital($marital)
    {
        $this->marital = $marital;
    
        return $this;
    }

    /**
     * Get marital
     *
     * @return string 
     */
    public function getMarital()
    {
        return $this->marital;
    }

    /**
     * Set fumeur
     *
     * @param boolean $fumeur
     * @return UserIdeal
     */
    public function setFumeur($fumeur)
    {
        $this->fumeur = $fumeur;
    
        return $this;
    }

    /**
     * Get fumeur
     *
     * @return boolean 
     */
    public function getFumeur()
    {
        return $this->fumeur;
    }

    /**
     * Set nbEnfant
     *
     * @param integer $nbEnfant
     * @return UserIdeal
     */
    public function setNbEnfant($nbEnfant)
    {
        $this->nbEnfant = $nbEnfant;
    
        return $this;
    }

    /**
     * Get nbEnfant
     *
     * @return integer 
     */
    public function getNbEnfant()
    {
        return $this->nbEnfant;
    }

    /**
     * Set silhouette
     *
     * @param string $silhouette
     * @return UserIdeal
     */
    public function setSilhouette($silhouette)
    {
        $this->silhouette = $silhouette;
    
        return $this;
    }

    /**
     * Get silhouette
     *
     * @return string 
     */
    public function getSilhouette()
    {
        return $this->silhouette;
    }

    /**
     * Set yeux
     *
     * @param string $yeux
     * @return UserIdeal
     */
    public function setYeux($yeux)
    {
        $this->yeux = $yeux;
    
        return $this;
    }

    /**
     * Get yeux
     *
     * @return string 
     */
    public function getYeux()
    {
        return $this->yeux;
    }

    /**
     * Set cheveux
     *
     * @param string $cheveux
     * @return UserIdeal
     */
    public function setCheveux($cheveux)
    {
        $this->cheveux = $cheveux;
    
        return $this;
    }

    /**
     * Get cheveux
     *
     * @return string 
     */
    public function getCheveux()
    {
        return $this->cheveux;
    }

    /**
     * Set etudes
     *
     * @param string $etudes
     * @return UserIdeal
     */
    public function setEtudes($etudes)
    {
        $this->etudes = $etudes;
    
        return $this;
    }

    /**
     * Get etudes
     *
     * @return string 
     */
    public function getEtudes()
    {
        return $this->etudes;
    }

    /**
     * Set ageDebut
     *
     * @param integer $ageDebut
     * @return UserIdeal
     */
    public function setAgeDebut($ageDebut)
    {
        $this->ageDebut = $ageDebut;
    
        return $this;
    }

    /**
     * Get ageDebut
     *
     * @return integer 
     */
    public function getAgeDebut()
    {
        return $this->ageDebut;
    }

    /**
     * Set ageFin
     *
     * @param integer $ageFin
     * @return UserIdeal
     */
    public function setAgeFin($ageFin)
    {
        $this->ageFin = $ageFin;
    
        return $this;
    }

    /**
     * Get ageFin
     *
     * @return integer 
     */
    public function getAgeFin()
    {
        return $this->ageFin;
    }

    /**
     * Set tailleDebut
     *
     * @param integer $tailleDebut
     * @return UserIdeal
     */
    public function setTailleDebut($tailleDebut)
    {
        $this->tailleDebut = $tailleDebut;
    
        return $this;
    }

    /**
     * Get tailleDebut
     *
     * @return integer 
     */
    public function getTailleDebut()
    {
        return $this->tailleDebut;
    }

    /**
     * Set tailleFin
     *
     * @param integer $tailleFin
     * @return UserIdeal
     */
    public function setTailleFin($tailleFin)
    {
        $this->tailleFin = $tailleFin;
    
        return $this;
    }

    /**
     * Get tailleFin
     *
     * @return integer 
     */
    public function getTailleFin()
    {
        return $this->tailleFin;
    }

    /**
     * Set ville
     *
     * @param \M\CoreBundle\Entity\Ville $ville
     * @return UserIdeal
     */
    public function setVille(\M\CoreBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return \M\CoreBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return UserIdeal
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set nationalite
     *
     * @param \M\CoreBundle\Entity\Nationalite $nationalite
     * @return UserIdeal
     */
    public function setNationalite(\M\CoreBundle\Entity\Nationalite $nationalite = null)
    {
        $this->nationalite = $nationalite;
    
        return $this;
    }

    /**
     * Get nationalite
     *
     * @return \M\CoreBundle\Entity\Nationalite 
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set vestimentaire
     *
     * @param string $vestimentaire
     * @return UserIdeal
     */
    public function setVestimentaire($vestimentaire)
    {
        $this->vestimentaire = $vestimentaire;

        return $this;
    }

    /**
     * Get vestimentaire
     *
     * @return string 
     */
    public function getVestimentaire()
    {
        return $this->vestimentaire;
    }

    /**
     * Set sport
     *
     * @param boolean $sport
     * @return UserIdeal
     */
    public function setSport($sport)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return boolean 
     */
    public function getSport()
    {
        return $this->sport;
    }
}
