<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Abonnement
 *
 * @ORM\Table(name="abonnement")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\AbonnementRepository")
 */
class Abonnement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_expiration", type="datetime")
     */
    private $dateExpiration;

    /**
     * @var string
     *
     * @ORM\Column(name="moyen_paiement", type="string", length=255, nullable=true)
     */
    private $moyenPaiement;

    /**
     * @var string
     *
     * @ORM\Column(name="code_promotionnel", type="string", length=255, nullable = true)
     */
    private $codePromotionnel;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_commande", type="string", length=255, nullable=true)
     */
    private $numeroCommande;

    /**
     * @var string
     *
     * @ORM\Column(name="type_abonnement", type="integer", nullable=true)
     */
    private $typeAbonnement;

    /**
     * @var string
     *
     * @ORM\Column(name="total_paye", type="string", length=255, nullable=true)
     */
    private $totalPaye;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="M\CoreBundle\Entity\Abonnement")
     * @ORM\JoinColumn(name="parrain_id", referencedColumnName="id", nullable = true)
     */
    private $parrain = null;

    /**
    * @ORM\Column(name="termine", type="boolean")
    */
    protected $termine = false;

    /**
    * @var string
    * @ORM\Column(name="customer_token", type="string", length=255, nullable=true)
    */

    private $CustomerToken;

    /**
    * @var string
    * @ORM\Column(name="code_reponse", type="string", length=255, nullable=true)
    */

    private $CodeReponse;

    /**
    * @var string
    * @ORM\Column(name="message_reponse", type="string", length=255, nullable=true)
    */

    private $MessageReponse;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\StatutCommande")
    */
    protected $statutCommande;


    public function __construct()
    {
        $oActualDate = new \DateTime();
        $this->setDateCreation($oActualDate);
        $this->setDateExpiration($oActualDate);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Abonnement
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateExpiration
     *
     * @param \DateTime $dateExpiration
     * @return Abonnement
     */
    public function setDateExpiration($dateExpiration)
    {
        $this->dateExpiration = $dateExpiration;

        return $this;
    }

    /**
     * Get dateExpiration
     *
     * @return \DateTime 
     */
    public function getDateExpiration()
    {
        return $this->dateExpiration;
    }

    /**
     * Set moyenPaiement
     *
     * @param string $moyenPaiement
     * @return Abonnement
     */
    public function setMoyenPaiement($moyenPaiement)
    {
        $this->moyenPaiement = $moyenPaiement;

        return $this;
    }

    /**
     * Get moyenPaiement
     *
     * @return string 
     */
    public function getMoyenPaiement()
    {
        return $this->moyenPaiement;
    }

    /**
     * Set codePromotionnel
     *
     * @param string $codePromotionnel
     * @return Abonnement
     */
    public function setCodePromotionnel($codePromotionnel)
    {
        $this->codePromotionnel = $codePromotionnel;

        return $this;
    }

    /**
     * Get codePromotionnel
     *
     * @return string 
     */
    public function getCodePromotionnel()
    {
        return $this->codePromotionnel;
    }

    /**
     * Set numeroCommande
     *
     * @param string $numeroCommande
     * @return Abonnement
     */
    public function setNumeroCommande($numeroCommande)
    {
        $this->numeroCommande = $numeroCommande;

        return $this;
    }

    /**
     * Get numeroCommande
     *
     * @return string 
     */
    public function getNumeroCommande()
    {
        return $this->numeroCommande;
    }

    /**
     * Set totalPaye
     *
     * @param string $totalPaye
     * @return Abonnement
     */
    public function setTotalPaye($totalPaye)
    {
        $this->totalPaye = $totalPaye;

        return $this;
    }

    /**
     * Get totalPaye
     *
     * @return string 
     */
    public function getTotalPaye()
    {
        return $this->totalPaye;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return Abonnement
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set parrain
     *
     * @param \M\CoreBundle\Entity\User $parrain
     * @return Abonnement
     */
    public function setParrain(\M\CoreBundle\Entity\User $parrain = null)
    {
        $this->parrain = $parrain;
    
        return $this;
    }

    /**
     * Get parrain
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getParrain()
    {
        return $this->parrain;
    }

    /**
     * Set typeAbonnement
     *
     * @param integer $typeAbonnement
     * @return Abonnement
     */
    public function setTypeAbonnement($typeAbonnement)
    {
        $this->typeAbonnement = $typeAbonnement;

        return $this;
    }

    /**
     * Get typeAbonnement
     *
     * @return integer 
     */
    public function getTypeAbonnement()
    {
        return $this->typeAbonnement;
    }

    /**
     * Set termine
     *
     * @param boolean $termine
     * @return Abonnement
     */
    public function setTermine($termine)
    {
        $this->termine = $termine;

        return $this;
    }

    /**
     * Get termine
     *
     * @return boolean 
     */
    public function getTermine()
    {
        return $this->termine;
    }

    /**
     * Set CustomerToken
     *
     * @param string $customerToken
     * @return Abonnement
     */
    public function setCustomerToken($customerToken)
    {
        $this->CustomerToken = $customerToken;

        return $this;
    }

    /**
     * Get CustomerToken
     *
     * @return string 
     */
    public function getCustomerToken()
    {
        return $this->CustomerToken;
    }

    /**
     * Set CodeReponse
     *
     * @param string $codeReponse
     * @return Abonnement
     */
    public function setCodeReponse($codeReponse)
    {
        $this->CodeReponse = $codeReponse;

        return $this;
    }

    /**
     * Get CodeReponse
     *
     * @return string 
     */
    public function getCodeReponse()
    {
        return $this->CodeReponse;
    }

    /**
     * Set MessageReponse
     *
     * @param string $messageReponse
     * @return Abonnement
     */
    public function setMessageReponse($messageReponse)
    {
        $this->MessageReponse = $messageReponse;

        return $this;
    }

    /**
     * Get MessageReponse
     *
     * @return string 
     */
    public function getMessageReponse()
    {
        return $this->MessageReponse;
    }

    /**
     * Set statutCommande
     *
     * @param \M\CoreBundle\Entity\StatutCommande $statutCommande
     * @return Abonnement
     */
    public function setStatutCommande(\M\CoreBundle\Entity\StatutCommande $statutCommande = null)
    {
        $this->statutCommande = $statutCommande;

        return $this;
    }

    /**
     * Get statutCommande
     *
     * @return \M\CoreBundle\Entity\StatutCommande 
     */
    public function getStatutCommande()
    {
        return $this->statutCommande;
    }
}
