<?php 

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Ville
*
* @ORM\Table("f_ville")
* @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\VilleRepository")
*/
class Ville
{
	/**
	* @var integer
	*
	* @ORM\Column(name="id", type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	
	/**
	* @var string
	*
	* @ORM\Column(name="nom", type="string", length=255 , nullable=true)
	*/
	protected $nom;


	/**
	* Get id
	*
	* @return integer
	*/
	public function getId()
	{
	return $this->id;
	}

	/**
	* Set nom
	*
	* @param string $nom
	* @return Ville
	*/
	public function setNom($nom)
	{
	$this->nom = $nom;

	return $this;
	}

	/**
	* Get nom
	*
	* @return string
	*/
	public function getNom()
	{
	return $this->nom;
	}

	
}
