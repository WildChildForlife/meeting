<?php 
namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Pays
*
* @ORM\Table("f_nationalite")
* @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\NationaliteRepository")
*/
class Nationalite
{
	/**
	* @var integer
	*
	* @ORM\Column(name="id", type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	* @var string
	*
	* @ORM\Column(name="nom", type="string", length=100 , nullable=true)
	*/
	protected $nom;


	/**
	* Get id
	*
	* @return integer
	*/
	public function getId()
	{
	return $this->id;
	}


    /**
     * Set nom
     *
     * @param string $nom
     * @return Pays
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
}
