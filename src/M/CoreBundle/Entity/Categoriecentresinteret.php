<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Centresinteret
 *
 * @ORM\Table(name="f_categoriecentresinteret")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\CategoriecentresinteretRepository")
 */
class Categoriecentresinteret
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    protected $nom;

     /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    protected $question;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Centresinteret
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Categoriecentresinteret
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
