<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messages
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\MessagesRepository")
 * @ORM\Table(name="messages")
 */
class Messages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    protected $message;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_envoi", type="datetime")
     */
    protected $dateEnvoi;


    /**
     * @var boolean
     *
     * @ORM\Column(name="del_from", type="boolean")
     */
    protected $del_from = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="del_to", type="boolean")
     */
    protected $del_to = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="corbeille_from", type="boolean")
     */
    protected $corbeille_from = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="corbeille_to", type="boolean")
     */
    protected $corbeille_to = 0;


    /**
     * @var boolean
     *
     * @ORM\Column(name="lu", type="boolean")
     */
    protected $lu = 0;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    protected $from;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    protected $to;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Notification" , cascade={"persist"})
    */
    protected $notification;

    /**
     * @var boolean
     *
     * @ORM\Column(name="chat", type="boolean")
     */
    protected $chat = 0;

    public function __construct()
    {
        $this->setDateEnvoi(new \DateTime());
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set dateEnvoi
     *
     * @param \DateTime $dateEnvoi
     * @return Messages
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;
    
        return $this;
    }

    /**
     * Get dateEnvoi
     *
     * @return \DateTime 
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * Set lu
     *
     * @param boolean $lu
     * @return Messages
     */
    public function setLu($lu)
    {
        $this->lu = $lu;
    
        return $this;
    }

    /**
     * Get lu
     *
     * @return boolean 
     */
    public function getLu()
    {
        return $this->lu;
    }

    /**
     * Set from
     *
     * @param \M\CoreBundle\Entity\User $from
     * @return Messages
     */
    public function setFrom(\M\CoreBundle\Entity\User $from = null)
    {
        $this->from = $from;
    
        return $this;
    }

    /**
     * Get from
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param \M\CoreBundle\Entity\User $to
     * @return Messages
     */
    public function setTo(\M\CoreBundle\Entity\User $to = null)
    {
        $this->to = $to;
    
        return $this;
    }

    /**
     * Get to
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Messages
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set del_from
     *
     * @param boolean $delFrom
     * @return Messages
     */
    public function setDelFrom($delFrom)
    {
        $this->del_from = $delFrom;
    
        return $this;
    }

    /**
     * Get del_from
     *
     * @return boolean 
     */
    public function getDelFrom()
    {
        return $this->del_from;
    }

    /**
     * Set del_to
     *
     * @param boolean $delTo
     * @return Messages
     */
    public function setDelTo($delTo)
    {
        $this->del_to = $delTo;
    
        return $this;
    }

    /**
     * Get del_to
     *
     * @return boolean 
     */
    public function getDelTo()
    {
        return $this->del_to;
    }

    /**
     * Set corbeille_from
     *
     * @param boolean $corbeilleFrom
     * @return Messages
     */
    public function setCorbeilleFrom($corbeilleFrom)
    {
        $this->corbeille_from = $corbeilleFrom;
    
        return $this;
    }

    /**
     * Get corbeille_from
     *
     * @return boolean 
     */
    public function getCorbeilleFrom()
    {
        return $this->corbeille_from;
    }

    /**
     * Set corbeille_to
     *
     * @param boolean $corbeilleTo
     * @return Messages
     */
    public function setCorbeilleTo($corbeilleTo)
    {
        $this->corbeille_to = $corbeilleTo;
    
        return $this;
    }

    /**
     * Get corbeille_to
     *
     * @return boolean 
     */
    public function getCorbeilleTo()
    {
        return $this->corbeille_to;
    }

    /**
     * Set notification
     *
     * @param \M\CoreBundle\Entity\Notification $notification
     * @return Messages
     */
    public function setNotification(\M\CoreBundle\Entity\Notification $notification = null)
    {
        $this->notification = $notification;
    
        return $this;
    }

    /**
     * Get notification
     *
     * @return \M\CoreBundle\Entity\Notification 
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set chat
     *
     * @param boolean $chat
     * @return Messages
     */
    public function setChat($chat)
    {
        $this->chat = $chat;

        return $this;
    }

    /**
     * Get chat
     *
     * @return boolean 
     */
    public function getChat()
    {
        return $this->chat;
    }
}
