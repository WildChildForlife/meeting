<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Qualites
 *
 * @ORM\Table("f_qualites")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\QualitesRepository")
 */
class Qualites
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="homme", type="string", length=255)
     */
    protected $homme;
    /**
     * @var string
     *
     * @ORM\Column(name="femme", type="string", length=255)
     */
    protected $femme;

   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Qualites
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set homme
     *
     * @param string $homme
     * @return Qualites
     */
    public function setHomme($homme)
    {
        $this->homme = $homme;
    
        return $this;
    }

    /**
     * Get homme
     *
     * @return string 
     */
    public function getHomme()
    {
        return $this->homme;
    }

    /**
     * Set femme
     *
     * @param string $femme
     * @return Qualites
     */
    public function setFemme($femme)
    {
        $this->femme = $femme;
    
        return $this;
    }

    /**
     * Get femme
     *
     * @return string 
     */
    public function getFemme()
    {
        return $this->femme;
    }

}
