<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Signalement
 *
 * @ORM\Table(name="signalement")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\SignalementRepository")
 */
class Signalement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datesignal", type="datetime")
     */
    private $dateSignal;

   /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    private $userSignaleur;
        /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    protected $message;

    /**
    * @ORM\Column(name="averti", type="boolean", nullable=true)
    */
    protected $averti;

   /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    private $userSignaler;

    public function __construct()
    {
     
        $this->setDateSignal( new \DateTime() );

    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateSignal
     *
     * @param \DateTime $dateSignal
     * @return Signalement
     */
    public function setDateSignal($dateSignal)
    {
        $this->dateSignal = $dateSignal;
    
        return $this;
    }

    /**
     * Get dateSignal
     *
     * @return \DateTime 
     */
    public function getDateSignal()
    {
        return $this->dateSignal;
    }

    /**
     * Set userSignaleur
     *
     * @param \M\CoreBundle\Entity\User $userSignaleur
     * @return Signalement
     */
    public function setUserSignaleur(\M\CoreBundle\Entity\User $userSignaleur = null)
    {
        $this->userSignaleur = $userSignaleur;
    
        return $this;
    }

    /**
     * Get userSignaleur
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUserSignaleur()
    {
        return $this->userSignaleur;
    }

    /**
     * Set userSignaler
     *
     * @param \M\CoreBundle\Entity\User $userSignaler
     * @return Signalement
     */
    public function setUserSignaler(\M\CoreBundle\Entity\User $userSignaler = null)
    {
        $this->userSignaler = $userSignaler;
    
        return $this;
    }

    /**
     * Get userSignaler
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUserSignaler()
    {
        return $this->userSignaler;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Signalement
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;

    }

    /**
     * Set averti
     *
     * @param boolean $averti
     * @return Signalement
     */
    public function setAverti($averti)
    {
        $this->averti = $averti;
    
        return $this;
    }

    /**
     * Get averti
     *
     * @return boolean 
     */
    public function getAverti()
    {
        return $this->averti;
    }
}
