<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Publication
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\PublicationRepository")
 */
class Publication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User")
    */
    protected $user;

     /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Events")
    */
    protected $event;

     /**
   * @ORM\OneToMany(targetEntity="M\CoreBundle\Entity\Commentaire", mappedBy="publication")
   */
  private $commentaires;

  /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateadd", type="datetime")
     */
    private $dateadd;

    public function __construct()
    {
        $this->dateadd = new \DateTime();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Publication
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return Publication
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param \M\CoreBundle\Entity\Events $event
     * @return Publication
     */
    public function setEvent(\M\CoreBundle\Entity\Events $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \M\CoreBundle\Entity\Events 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set dateadd
     *
     * @param \DateTime $dateadd
     * @return Publication
     */
    public function setDateadd($dateadd)
    {
        $this->dateadd = $dateadd;

        return $this;
    }

    /**
     * Get dateadd
     *
     * @return \DateTime 
     */
    public function getDateadd()
    {
        return $this->dateadd;
    }

    /**
     * Add commentaires
     *
     * @param \M\CoreBundle\Entity\Commentaire $commentaires
     * @return Publication
     */
    public function addCommentaire(\M\CoreBundle\Entity\Commentaire $commentaires)
    {
        $this->commentaires[] = $commentaires;

        return $this;
    }

    /**
     * Remove commentaires
     *
     * @param \M\CoreBundle\Entity\Commentaire $commentaires
     */
    public function removeCommentaire(\M\CoreBundle\Entity\Commentaire $commentaires)
    {
        $this->commentaires->removeElement($commentaires);
    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }
}
