<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="actus")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\ActusRepository")
 */
class Actus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    protected $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="datetime", type="string", length=255)
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    protected $source;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publication", type="datetime")
     */
    protected $publication;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Ville" , cascade={"persist"})
    */
    protected $ville;

    /**
    * @var string
    *
    * @ORM\Column(name="image", type="text", nullable=false)
    */

    protected $image;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /* affiché = 0 / non-affiché = 1 */
    /**
    * @var boolean
    *
    * @ORM\Column(name="status", type="boolean", nullable=true)
    */
    protected $status= 0;

    /* block dashbord events actu non-affiché = 0 / affiché = 1 */
    /**
    * @var boolean
    *
    * @ORM\Column(name="accueil", type="boolean", nullable=true)
    */
    protected $accueil= 0;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Events
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Events
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Events
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Events
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return Events
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;
    
        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Events
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set places
     *
     * @param integer $places
     * @return Events
     */
    public function setPlaces($places)
    {
        $this->places = $places;
    
        return $this;
    }

    /**
     * Get places
     *
     * @return integer 
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Set inscription
     *
     * @param boolean $inscription
     * @return Events
     */
    public function setInscription($inscription)
    {
        $this->inscription = $inscription;
    
        return $this;
    }

    /**
     * Get inscription
     *
     * @return boolean 
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Events
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Events
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    
        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Events
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    
        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Events
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set telcontact
     *
     * @param string $telcontact
     * @return Events
     */
    public function setTelcontact($telcontact)
    {
        $this->telcontact = $telcontact;
    
        return $this;
    }

    /**
     * Get telcontact
     *
     * @return string 
     */
    public function getTelcontact()
    {
        return $this->telcontact;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     * @return Events
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    
        return $this;
    }

    /**
     * Get prix
     *
     * @return integer 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Events
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Events
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set ville
     *
     * @param \M\CoreBundle\Entity\Ville $ville
     * @return Events
     */
    public function setVille(\M\CoreBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return \M\CoreBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return Events
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return Actus
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set publication
     *
     * @param \DateTime $publication
     * @return Actus
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
    
        return $this;
    }

    /**
     * Get publication
     *
     * @return \DateTime 
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set accueil
     *
     * @param boolean $accueil
     * @return Actus
     */
    public function setAccueil($accueil)
    {
        $this->accueil = $accueil;
    
        return $this;
    }

    /**
     * Get accueil
     *
     * @return boolean 
     */
    public function getAccueil()
    {
        return $this->accueil;
    }
}
