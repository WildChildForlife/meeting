<?php 
namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Pays
*
* @ORM\Table(name="images")
* @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\ImagesRepository")
*/
class Images
{
	/**
	* @var integer
	*
	* @ORM\Column(name="id", type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
    * @var string
    *
    * @ORM\Column(name="image", type="text", nullable=false)
    */
	protected $image;

        /**
    * @var string
    *
    * @ORM\Column(name="image_old", type="text", nullable=true)
    */
    protected $image_old;


    /**
    * @ORM\Column(name="date_upload", type="datetime" , nullable=true)
    */
    protected $date_upload;

	/**
	* @var boolean
	*
	* @ORM\Column(name="profile", type="boolean")
	*/
	protected $profile = 0;

    /**
    * @var boolean
    *
    * @ORM\Column(name="profile_default", type="boolean")
    */
    protected $profile_default = 0;

    /**
    * @var boolean
    *
    * @ORM\Column(name="moderer", type="boolean")
    */
    protected $moderer = 0;

	/**
	* @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User", inversedBy="images")
	*/
	protected $user;


    public function __construct()
    {
        $this->setDateUpload(new \DateTime());
    }


	/**
     * Set image
     *
     * @param UploadedFile $file
     */
    public function setImage($image)
    {
    	$this->image = $image;
        return $this;
    }

	/**
	* Get id
	*
	* @return integer
	*/
	public function getId()
	{
	return $this->id;
	}

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set profile
     *
     * @param boolean $profile
     * @return Images
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    
        return $this;
    }

    /**
     * Get profile
     *
     * @return boolean 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return Images
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set profile_default
     *
     * @param boolean $profileDefault
     * @return Images
     */
    public function setProfileDefault($profileDefault)
    {
        $this->profile_default = $profileDefault;
    
        return $this;
    }

    /**
     * Get profile_default
     *
     * @return boolean 
     */
    public function getProfileDefault()
    {
        return $this->profile_default;
    }

    /**
     * Set date_upload
     *
     * @param \DateTime $dateUpload
     * @return Images
     */
    public function setDateUpload($dateUpload)
    {
        $this->date_upload = $dateUpload;
    
        return $this;
    }

    /**
     * Get date_upload
     *
     * @return \DateTime 
     */
    public function getDateUpload()
    {
        return $this->date_upload;
    }

    /**
     * Set moderer
     *
     * @param boolean $moderer
     * @return Images
     */
    public function setModerer($moderer)
    {
        $this->moderer = $moderer;
    
        return $this;
    }

    /**
     * Get moderer
     *
     * @return boolean 
     */
    public function getModerer()
    {
        return $this->moderer;
    }

    /**
     * Set image_old
     *
     * @param string $imageOld
     * @return Images
     */
    public function setImageOld($imageOld)
    {
        $this->image_old = $imageOld;
    
        return $this;
    }

    /**
     * Get image_old
     *
     * @return string 
     */
    public function getImageOld()
    {
        return $this->image_old;
    }
}
