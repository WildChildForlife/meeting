<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Centresinteret
 *
 * @ORM\Table("f_centresinteret")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\CentresinteretRepository")
 */
class Centresinteret
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    protected $nom;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Categoriecentresinteret")
    */
    protected $categorie;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Centresinteret
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set categorie
     *
     * @param \M\CoreBundle\Entity\Categoriecentresinteret $categorie
     * @return Centresinteret
     */
    public function setCategorie(\M\CoreBundle\Entity\Categoriecentresinteret $categorie = null)
    {
        $this->categorie = $categorie;
    
        return $this;
    }

    /**
     * Get categorie
     *
     * @return \M\CoreBundle\Entity\Categoriecentresinteret 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
}
