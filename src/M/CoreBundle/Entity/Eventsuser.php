<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="eventsuser")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\EventsuserRepository")
 */
class Eventsuser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    protected $user;

    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\Events" , cascade={"persist"})
    */
    protected $event;

  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return Eventsuser
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param \M\CoreBundle\Entity\Events $event
     * @return Eventsuser
     */
    public function setEvent(\M\CoreBundle\Entity\Events $event = null)
    {
        $this->event = $event;
    
        return $this;
    }

    /**
     * Get event
     *
     * @return \M\CoreBundle\Entity\Events 
     */
    public function getEvent()
    {
        return $this->event;
    }
}
