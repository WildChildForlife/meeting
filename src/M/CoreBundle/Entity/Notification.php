<?php

namespace M\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table("notification")
 * @ORM\Entity(repositoryClass="M\CoreBundle\Entity\Repository\NotificationRepository")
 */
class notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_notification", type="datetime")
     */
    protected $dateNotification;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    protected $type;

   /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    protected $userNotifieur;
    /**
    * @ORM\ManyToOne(targetEntity="M\CoreBundle\Entity\User" , cascade={"persist"})
    */
    protected $user;

    /**
    * @ORM\Column(name="vue", type="boolean",nullable=true)
     */
    protected $vue = false;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateNotification
     *
     * @param \DateTime $dateNotification
     * @return notification
     */
    public function setDateNotification($dateNotification)
    {
        $this->dateNotification = $dateNotification;

        return $this;
    }

    /**
     * Get dateNotification
     *
     * @return \DateTime 
     */
    public function getDateNotification()
    {
        return $this->dateNotification;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set vue
     *
     * @param boolean $vue
     * @return notification
     */
    public function setVue($vue)
    {
        $this->vue = $vue;

        return $this;
    }

    /**
     * Get vue
     *
     * @return boolean 
     */
    public function getVue()
    {
        return $this->vue;
    }

    /**
     * Set userNotifieur
     *
     * @param \M\CoreBundle\Entity\User $userNotifieur
     * @return notification
     */
    public function setUserNotifieur(\M\CoreBundle\Entity\User $userNotifieur = null)
    {
        $this->userNotifieur = $userNotifieur;

        return $this;
    }

    /**
     * Get userNotifieur
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUserNotifieur()
    {
        return $this->userNotifieur;
    }

    /**
     * Set user
     *
     * @param \M\CoreBundle\Entity\User $user
     * @return notification
     */
    public function setUser(\M\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \M\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
