<?php

namespace M\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AbonnementCoordonneesUserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add('nom', 'text', array('required' => true))
            ->add('prenom', 'text', array('required' => true))
            ->add('adresse', 'text', array('required' => true))
            ->add('adresse_suite', 'text', array('required' => false))
            ->add('code_postal', 'text', array('required' => true))
            ->add('ville', 'entity', array(
                    'label'  => 'Ville Au Maroc :',
                    'attr'   => array('class' => 'ville'),
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Ville',
                    'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                    'property' => 'nom',
                    'required'  => true
                ))
            ->add('telephone', 'text', array('required' => true));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_abonnementcoordonneesusertype';
    }
}

