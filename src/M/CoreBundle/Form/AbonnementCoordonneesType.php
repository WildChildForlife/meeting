<?php

namespace M\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AbonnementCoordonneesType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add('codePromotionnel', 'text', array('required' => false))
            ->add('moyenPaiement', 'choice', array( 
                                'choices'   => array(
                                    '0' => "Carte Bancaire Marocaine / Etrangère", 
                                    '1' => "Chèque Bancaire Certifié ", 
                                    '2' => "Paiement en Espèces dans nos bureaux à Casablanca",
                                    '3' => "Paiement en Espèces via Wafacash & BINGA"
                                ),
                                'expanded'      => true,
                                'multiple'      => false,
                                'required'      => true
                ))
            ->add('offreAbonnement', 'choice', array( 
                                    'choices'   => array(
                                        '0' => "1 mois / 220 DHS TTC",
                                        '1' => "3 mois / 480 DHS TTC",
                                        '2' => "6 mois / 720 DHS TTC",
                                        '3' => "12 mois / 1080 DHS TTC"
                                    ),
                                'required'  => true,
                                'mapped'    => false
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Abonnement'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_abonnementcoordonneestype';
    }
}

