<?php

namespace M\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UnsubcribeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add('username', 'text', array(
                            'required' => true, 
                            'label'  => 'Pseudo ou E-mail :',
                            'mapped'  => false
                            )
            )
            ->add('password', 'password', array(
                  'label'  => 'Mot de Passe',
                   'required' => true,
                   'mapped'  => false))
            ->add('raison', 'choice', array( 
                                            'choices'   => array(
                                                '0' => "J'ai trouvé l'âme soeur grâce à `Rencontres & Mariages`", 
                                                '1' => 'Les rencontres sur Internet ne sont pas faites pour moi', 
                                                '2' => 'Ce site ne répond pas à mes attentes', 
                                                '3' => 'Je le garde pour moi', 
                                                '4' => 'Autre raison (laissez votre commentaire ci-dessous) :'
                                            ),
                                                'expanded'     => true,
                                                'multiple'     => false,
                                                'required'     => true,
                                                'mapped'  => false
                ))
            ->add('commentaire', 'textarea', array(
                    'label'     => 'Une description :',
                    'required'  => false,
                    'mapped'  => false,

                    
                ))
                   ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_unsubcribetype';
    }
}

