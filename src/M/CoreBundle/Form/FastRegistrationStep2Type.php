<?php

namespace M\CoreBundle\Form;

use Doctrine\ORM\EntityManager ;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FastRegistrationStep2Type extends AbstractType
{  
    private $sSexe;

    public function __construct($bSexe)
    {
        $this->sSexe = $bSexe;
    }  

    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $builder->add('description', 'textarea', array(
                    'label'     => 'Une description :',
                    'required'  => true
                ))
                ->add('qualites', 'entity', array(
                    'class' => 'MCoreBundle:Qualites',
                    'property'     => $this->sSexe,
                    'expanded' => true,
                    'multiple' => true,
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_usertype';
    }
}
