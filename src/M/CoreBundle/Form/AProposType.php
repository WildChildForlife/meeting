<?php

namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AProposType extends AbstractType
{
    private $sSexe;

    public function __construct($bSexe)
    {
        $this->sSexe = $bSexe;
    }  
    public function getSexe()
    {
        //ici nous verifirons le sexe pour retournee la liste de qualité
        return ($this->sSexe == 1) ? "femme" : "homme";
    }

    private function getTaille($iDeb = 120, $iFin = 229)
    {
        // génération d'un rang pour les tailles pour que les clés corresponde au choix
        $aTailletmp = range($iDeb, $iFin);
        foreach ( $aTailletmp as $key => $val )
        {
            $aTaille[$val] = $val;
        }
        return $aTaille;
    }

    public function getMot()
    {
         //ici nous verifirons le sexe pour retournee la liste de mot
        $aMot = array();
        if ($this->sSexe)
        {

            $aMot['divorce'] = 'Divorcée';
            $aMot['separe'] = 'Séparée';
            $aMot['veuf'] = 'Veuve';

        } 
        else
        {
            $aMot['inscription'] = 'Inscrits';
            $aMot['divorce'] = 'Divorcé';
            $aMot['separe'] = 'Séparé';
            $aMot['veuf'] = 'Veuf';

        }
        return $aMot;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $aMot = $this->getMot();

            $builder
            ->add('description', 'textarea', array(
                    'label'     => 'Une description :',
                    'required'  => true,
                    
                ))
            ->add('qualites', 'entity', array(
                    'class' => 'MCoreBundle:Qualites',
                    'property'     => $this->getSexe(),
                    'expanded' => true,
                    'multiple' => true,
                ))
            ->add('marital', 'choice', array( 
                    'label'  => 'Situation :',
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Célibataire' => 'Célibataire', 
                        'Divorcé'     => $aMot['divorce'], 
                        'Veuf'        => $aMot['veuf'] ,
                        'Séparé'      => $aMot['separe']
                        ),
                    'required'  => true
                ))
            ->add('nbEnfant', 'choice', array(
                    'label'  => "Nombre d'enfants :",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        '0'     => 'Aucun', 
                        '1'     => '1', 
                        '2'     => '2', 
                        '3'     => '3', 
                        '4'     => '3 et plus'
                        ),
                    'required'  => true
                ))
            ->add('nationalite', 'entity', array(
                    'label'  => 'Nationalité :',
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Nationalite',
                    'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                    'property' => 'nom',
                    'required'  => true
                ))
            ->add('taille', 'choice', array(
                'label'  => 'Taille :',
                'empty_value' => 'Sélectionnez',
                'choices'   => $this->getTaille(),
                'required' => true))
            ->add('silhouette', 'choice', array(
                    'label'  => 'Silhouette :',
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Mince'                     => 'Mince', 
                        'Normale'                   => 'Normale', 
                        'Quelques kilos en trop'    => 'Quelques kilos en trop', 
                        'Ronde'                     => 'Ronde', 
                        'Sportive'                  => 'Sportive'
                        ),
                    'required'  => true
                ))
            ->add('cheveux', 'choice', array(
                    'label'  => 'Couleur des cheveux :',
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Blancs'    =>  'Blancs', 
                        'Blonds'    =>  'Blonds', 
                        'Bruns'     =>  'Bruns', 
                        'Châtains'  =>  'Châtains', 
                        'Gris'      =>  'Gris', 
                        'Noir'      =>  'Noir', 
                        'Roux'      =>  'Roux'
                        ),
                    'required'  => true
                ))
            ->add('yeux', 'choice', array(
                    'label'  => 'Couleur des yeux :',
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Bleus'     =>  'Bleus', 
                        'Marrons'   =>  'Marrons', 
                        'Noirs'     =>  'Noirs', 
                        'Noisettes' =>  'Noisettes', 
                        'Verts'     =>  'Verts'
                        ),
                    'required'  => true
                ))
            ->add('ville', 'entity', array(
                    'label'  => 'Ville :',
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Ville',
                    'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                    'property' => 'nom',
                    'required'  => true
                  
                ))
            ->add('etudes', 'choice', array(
                    'label'  => "Niveau d'études :",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Je le garde pour moi'          =>  'Je le garde pour moi',
                        'Pas de diplôme'                 =>  'Pas de diplôme', 
                        'Brevet'                         =>  'Brevet', 
                        'BEP'                            =>  'BEP', 
                        'BAC Pro'                        =>  'BAC Pro', 
                        'BAC Général'                    =>  'BAC Général', 
                        'BAC + 2'                        =>  'BAC + 2', 
                        'BAC + 3'                        =>  'BAC + 3', 
                        'BAC + 4'                        =>  'BAC + 4', 
                        'BAC + 5'                        =>  'BAC + 5', 
                        'Doctorat'                       =>  'Doctorat',
                        ),
                    'required' => true
                ))
            ->add('profession', 'entity', array(
                    'label'  => 'Profession :',
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Profession',
                    'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom, p.id', 'ASC'); },
                    'property' => 'nom',
                    'required'  => true,
                   ))
            ->add('secteur', 'choice', array(
                    'label'  => "Secteur d'activité :",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Aéronautique - Marine - Espace - Armement' => 'Aéronautique - Marine - Espace - Armement', 
                        'Agroalimentaire et agriculture'            => 'Agroalimentaire et agriculture', 
                        'Arts et culture'                           => 'Arts et culture',
                        'Associations'                              => 'Associations', 
                        'Bâtiment Travaux publics'                  => 'Bâtiment Travaux publics', 
                        'Biens de consommation'                     => 'Biens de consommation', 
                        'Communication et médias'                   => 'Communication et médias', 
                        'Conseil et services'                       => 'Conseil et services', 
                        'Distribution'                              => 'Distribution', 
                        'Enseignement'                              => 'Enseignement', 
                        'Finance'                                   => 'Finance', 
                        'High-tech'                                 => 'High-tech', 
                        'Industrie'                                 => 'Industrie', 
                        'Pharmacie et santé'                        => 'Pharmacie et santé', 
                        'Services publics - administration'         => 'Services publics - administration', 
                        'Sport'                                     => 'Sport',
                        'Tourisme'                                  => 'Tourisme', 
                        'Transports'                                => 'Transports',
                        'Autres'                                    => 'Autres'
                        ),
                    'required'  => true
                ))
            ->add('vestimentaire', 'choice', array(
                    'label'  => "Style Vestimentaire",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Je le garde pour moi' => 'Je le garde pour moi', 
                        'BCBG'                 => 'BCBG', 
                        'Business'             => 'Business', 
                        'Branché'              => 'Branché', 
                        'Classique'            => 'Classique', 
                        'Décontracté'          => 'Décontracté', 
                        'Sportif'              => 'Sportif' 
                         ),
                    'required'  => true
                ))
            ->add('sport', 'choice', array(
                    'label'  => "Pratique du sport",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                                            'Jamais'        => 'Jamais', 
                                            'Rare'          => 'Rare', 
                                            'Occasionnelle' => 'Occasionnelle', 
                                            'Souvent'       => 'Souvent', 
                                            'Quotidienne'   => 'Quotidienne', 
                                                            ),
                  'required'  => true
                ))
            ->add('fumeur', 'choice', array(
                    'label'  => "Fumez-vous ?",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        '0' => 'Non', 
                        '1' => 'Oui', 
                        '2' => 'Occasionnel', 
                                        ),
                   'required'  => true
                    ))
            
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_usertype';
    }
}