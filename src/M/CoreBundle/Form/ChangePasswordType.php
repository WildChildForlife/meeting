<?php

namespace M\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ChangePasswordType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add('oldpassword', 'password', array(
                  'label'  => 'Mot de Passe actuel :',
                'required' => true
                ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Confirmation du mot de passe : Vos deux mot de passes ne sont pas identiques.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array(
                    'label' => 'Nouveau Mot de Passe :', 
                    'attr'  => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Nouveau Mot de passe'),
                    'label_attr' => array(
                        'class' => 'form-control')),
                'second_options' => array(
                    'label' => 'Confirmation :', 
                    'attr'  => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Confirmation'),
                    'label_attr' => array(
                        'class' => 'control-label')),
            ))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_usertype';
    }
}
