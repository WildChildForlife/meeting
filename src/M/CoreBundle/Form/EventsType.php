<?php

namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventsType extends AbstractType
{
    private function getHeures($iDeb = 00, $iFin = 23)
    {
        // génération d'un rang pour les tailles pour que les clés corresponde au choix
        $aHeuretmp = range($iDeb, $iFin);
        $aHeure = array();
        foreach ( $aHeuretmp as $key => $val )
        {
            if ( strlen($val) <= 1 ) $val = 0 . $val;
            $aHeure[$val] = $val;
        }
        return $aHeure;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

            $builder

            ->add('titre', 'text', array(
                    'label'     => 'Titre :',
                    'required'  => true
                    
                ))
            ->add('heure', 'choice', array(
                'label'  => 'Heure :',
                'choices'   => $this->getHeures(),
                'required' => true))
            ->add('dateEvent', 'text', array('label'  => 'Date : ',
                                            'required'   => true))
           /* ->add('dateEvent', 'date', array('label'  => 'Date : ',
                                            'input'  => 'datetime',
                                            'widget' => 'single_text',
                                            'required'   => true))*/
            ->add('lieu', 'text', array(
                    'label'     => 'Nom du lieu :',
                    'required'  => true,
                    
                ))
            ->add('adresse', 'text', array(
                    'label'     => 'Adresse :',
                    'required'  => true,
                    
                ))
            ->add('ville', 'entity', array(
                    'label'  => 'Ville au Maroc :',
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Ville',
                    'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                    'property' => 'nom',
                    'required'  => true
   
                ))
            ->add('places', 'text', array(
                    'label'     => 'Nombre de place :',
                    'required'  => true,
                    
                ))
            ->add('inscription', 'choice', array( 
                                'choices'   => array(
                                    '0' => 'Gratuite', 
                                    '1' => 'Avec Réservation payante', 
                                     ),
                                'data' => 0,
                                'expanded'     => true,
                                'multiple'     => false,
                                'required'     => false
                ))
            ->add('prix', 'text', array(
                    'label'     => 'Prix/Personne :',
                    'required'  => false,
                    
                ))
            ->add('description', 'textarea', array(
                    'label'     => 'Description :',
                    'required'  => true,
                    
                ))
            ->add('prenom', 'text', array(
                    'label'     => 'Prénom:',
                    'required'  => true,
                    
                ))
            ->add('mail', 'email', array('required' => true, 'label' => 'E-mail :'))
            ->add('tel', 'text', array('required' => true, 'label' => 'GSM :'))
            ->add('contact', 'text', array('required' => false, 'label' => 'Contact :'))
            ->add('telcontact', 'text', array('required' => false, 'label' => 'Tél / GSM :'))
            
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Events'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_usertype';
    }
}