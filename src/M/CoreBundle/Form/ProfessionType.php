<?php
// src/Acme/TaskBundle/Form/Type/CategoryType.php
namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager ;

class ProfessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('profession', 'entity', array(
                    'label'  => 'Profession',
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Profession',
                    'query_builder' => function(EntityRepository $er)
                                        { 
                                            return $er->createQueryBuilder('p');
                                        },
                    'property' => 'nom',
                    'required'  => true,
                     
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Profession',
        ));
    }

    public function getName()
    {
        return 'm_corebundle_professiontype';
    }
}