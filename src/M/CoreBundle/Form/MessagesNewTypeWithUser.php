<?php

namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessagesNewTypeWithUser extends AbstractType
{
    private $sUser;

    public function __construct($sUser)
    {
        $this->sUser = $sUser;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {     
        $builder
            ->add('to', 'choice', array(
                    'choices' => array($this->sUser => $this->sUser)
                    )
            )
            ->add('sujet', 'text')
            ->add('corps', 'textarea')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Messages'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_messagestype';
    }
}
