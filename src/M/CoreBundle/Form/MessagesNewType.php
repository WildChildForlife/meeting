<?php

namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MessagesNewType extends AbstractType
{
    private $oContainer;

    public function __construct(ContainerInterface $oContainer)
    {
        $this->oContainer = $oContainer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {     
        $builder
            ->add('to', 'entity', array(
                    'empty_value' => 'Envoyer à...',
                    'class' => 'MCoreBundle:User',
                    'query_builder' => function($oRepository) { 
                        /*return $oRepository->createQueryBuilder('p')
                                          ->where('p.username = :username')
                                          ->orderBy('p.username', 'ASC')
                                          ->setParameters(array('username' => $sActualUsername)); */
                        return $oRepository->fetchAllUserButActual();
                    },
                    'property' => 'username',
                ))
            ->add('sujet', 'text')
            ->add('corps', 'textarea')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Messages'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_messagestype';
    }
}
