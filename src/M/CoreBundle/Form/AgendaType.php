<?php

namespace M\CoreBundle\Form;

use Doctrine\ORM\EntityManager ;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AgendaType extends AbstractType
{  
    private function getInterval($iDeb = 18, $iFin = 80)
    {
        $aIntervaltmp = range($iDeb,$iFin);
        foreach ( $aIntervaltmp as $iVal )
        {
            $aInterval[$iVal] = $iVal;
        }
        return $aInterval;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {   

        $builder->add('titre','text',array('label'       => 'Titre : ',
                                             'required'  => true
                                            ))
                ->add('lieu','text',array('label'       => 'Lieu : ',
                                            'required'   => true
                                            ))
                ->add('date', 'time', array('label'  => 'Heure : ',
                                            'required'   => true))
                ->add('heurefin','time', array('label'  => ' ',
                                               'required'   => true,
                                               ))
                ->add('commentaires', 'textarea', 
                                        array(
                                            'label'     => 'Commentaires :',
                                            'required'   => false
                                             ));
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_Usertype';
    }
}
