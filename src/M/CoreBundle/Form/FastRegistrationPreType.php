<?php

namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FastRegistrationPreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder           
            ->add('sexe', 'choice', array( 
                    'label'  => 'Je suis :',
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array('Un homme', 'Une Femme'),
                    'required'  => true
                ))
            ->add('email', 'email', array('required' => true, 'label' => 'E-mail :'))
            ->add('datenaissance', 'birthday',  array(
                    'label'  => 'Né(e) le :',
                    'widget' => 'choice',
                    'input' => 'timestamp',
                    'format' => 'dd-MM-yyyy',
                    'years' => range(1933 ,date("Y") - 18),
                    'empty_value' => array('year' => 'Année', 'month' => 'Mois', 'day' => 'Jour'),
                    'required'  => true                    
                ))
            ->add('majeur', 'checkbox', array(
                'required' => true,
                'mapped' => false,
                'invalid_message' => "Vous devez certifié...",
                /*'label' => "Je certifie être majeur(e), avoir lu et accepté les CGUV ainsi que la politique sur la vie privée et la charte du site",*/
                ))
            ->add('captcha', 'captcha', array(
                'width'     => 70,
                'height'    => 22,
                'length'    => 3,
                'quality'   => 100,
                'font'      => null,
                'background_color' => [255, 255, 255],
                'interpolation'    => true,
                'charset'          => '0123456789',
                'invalid_message' => "Captcha : Vous n'avez pas bien saisis les caractères de l'image",
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_usertype';
    }
}
