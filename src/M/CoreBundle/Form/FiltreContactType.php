<?php

namespace M\CoreBundle\Form;

use Doctrine\ORM\EntityManager ;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FiltreContactType extends AbstractType
{  
    private function getInterval($iDeb = 18, $iFin = 80)
    {
        $aIntervaltmp = range($iDeb,$iFin);
        foreach ( $aIntervaltmp as $iVal )
        {
            $aInterval[$iVal] = $iVal;
        }
        return $aInterval;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {   

        $builder->add('ageDebut','choice',array('choices'     =>  $this->getInterval(), 
                                              'label'         =>  'Age entre :',
                                              'required'     => false,
                                            ))
                ->add('ageFin','choice',array('choices'     =>  $this->getInterval(), 
                                             'label'        => 'et',
                                             'label_attr'   => array('class' => 'width-auto second_label'),
                                             'required'     => false,
                                            ))
                ->add('ville', 'entity', array(
                    'label'  => 'Ville Au Maroc :',
                    'attr'   => array('class' => 'ville'),
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Ville',
                    'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                    'property' => 'nom',
                    'required'  => false
                  
                ))
                ->add('EnLigne', 'checkbox', array(
                    'required' => false,
                    'mapped' => false,
                    'label'  => 'En ligne',
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_Usertype';
    }
}
