<?php

namespace M\CoreBundle\Form;

use Doctrine\ORM\EntityManager ;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FastRegistrationStep3Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', 'hidden');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Images'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_usertype';
    }
}
