<?php

namespace M\CoreBundle\Form;

use Doctrine\ORM\EntityManager ;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class RechercheFavType extends AbstractType
{  
    private $id;
    
    public function __construct($iId)
    {
        $this->id = $iId;
    }  
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   

        $id = $this->id;
        $builder->add('recherche', 'entity', array(
                        'label'         => 'Mes recherches favorites :',
                        'empty_value'   => 'Sélectionnez',
                        'class'         => 'MCoreBundle:Recherche',
                        'query_builder' => function($repository) use ($id)
                            {  return $repository->createQueryBuilder('p')
                                                  ->orderBy('p.nom', 'ASC')
                                                  ->where('p.user = :userid')
                                                  ->setParameters(array('userid' => $id));
                            },
                        'property'      => 'nom',
                        'required'      => false,
                        'mapped'        => false,

                    ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Recherche'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_Recherchetype';
    }
}
