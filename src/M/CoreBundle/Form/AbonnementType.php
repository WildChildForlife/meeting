<?php

namespace M\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AbonnementType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add('typeAbonnement', 'choice', array( 
                                'choices'   => array(
                                    '0' => "Sélectionner", 
                                    '1' => "Sélectionner", 
                                    '2' => "Sélectionner", 
                                    '3' => "Sélectionner"
                                ),
                                'expanded'      => true,
                                'multiple'      => false,
                                'required'      => true,
                                'data'          => 1
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Abonnement'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_abonnementtype';
    }
}

