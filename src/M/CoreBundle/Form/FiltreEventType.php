<?php

namespace M\CoreBundle\Form;

use Doctrine\ORM\EntityManager ;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FiltreEventType extends AbstractType
{  

    public function buildForm(FormBuilderInterface $builder, array $options)
    {   

        $builder
                ->add('ville', 'entity', array(
                    'label'  => 'Ville :',
                    'attr'   => array('class' => 'ville'),
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Ville',
                    'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                    'property' => 'nom',
                    'required'  => false
                  
                ))
                /*->add('date', 'date', array('label'  => 'Date : ',
                                            'input'  => 'datetime',
                                            'widget' => 'single_text',
                                            'required'   => false));*/
                ->add('date', 'text', array('label'  => 'Date : ',
                                            'required'   => false));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Events'
        ));
    }

    public function getName()
    {
        return 'Events';
    }
}
