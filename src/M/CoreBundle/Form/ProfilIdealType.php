<?php

namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfilIdealType extends AbstractType
{
    private function getInterval($iDeb = 18, $iFin = 80)
    {
        $aIntervaltmp = range($iDeb,$iFin);
        foreach ( $aIntervaltmp as $iVal )
        {
            $aInterval[$iVal] = $iVal;
        }
        return $aInterval;
    }
     public function __construct($bSexe)
    {
        $this->sexe = $bSexe;
    }  
    public function getMot()
    {
         //ici nous verifirons le sexe pour retournee la liste de mot
        $aMot = array();
        if (!$this->sexe)
        {

            $aMot['divorce'] = 'Divorcée';
            $aMot['separe'] = 'Séparée';
            $aMot['veuf'] = 'Veuve';
            $aMot['fumeur'] = 'Fumeuse ?';
            $aMot['Occasionnel'] = 'Occasionnelle';

        } 
        else
        {

            $aMot['divorce'] = 'Divorcé';
            $aMot['separe'] = 'Séparé';
            $aMot['veuf'] = 'Veuf';
            $aMot['fumeur'] = 'Fumeur ?';
            $aMot['Occasionnel'] = 'Occasionnel';
        }
        return $aMot;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $aMot = $this->getMot();
        $builder->add('ageDebut','choice',array('choices'     =>  $this->getInterval(), 
                                              'label'         =>  'Age entre :',
                                              'required'     => false,
                                            ))
                ->add('ageFin','choice',array('choices'     =>  $this->getInterval(), 
                                             'label'        => 'et',
                                             'label_attr'   => array('class' => 'width-auto second_label'),
                                             'required'     => false,
                                            ))
                ->add('marital', 'choice', array( 
                    'label'  => 'Situation :',
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Célibataire' => 'Célibataire', 
                        $aMot['divorce']    => $aMot['divorce'], 
                        $aMot['veuf']       => $aMot['veuf'] ,
                        $aMot['separe']     => $aMot['separe']
                        
                        ),
                        'required'     => false,
                ))
                ->add('nbEnfant', 'choice', array(
                    'label'  => "Nombre d'enfants :",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        '0'     => 'Aucun', 
                        '1'     => '1', 
                        '2'     => '2', 
                        '3'     => '3', 
                        '4'     => '3 et plus'
                        ),
                    'required'  => false
                ))
                ->add('nationalite', 'entity', array(
                        'label'  => 'Nationalité :',
                        'empty_value' => 'Sélectionnez',
                        'class' => 'MCoreBundle:Nationalite',
                        'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                        'property' => 'nom',
                        'required'     => false,
                     
                    ))
                ->add('tailleDebut','choice',array('choices'     =>  $this->getInterval(120,229), 
                                              'label'         =>  'Taille entre :',
                                              'required'     => false,
                                            ))
                ->add('tailleFin','choice',array('choices'     =>  $this->getInterval(120,229), 
                                             'label'        => 'et',
                                             'label_attr'   => array('class' => 'width-auto second_label'),
                                             'required'     => false,
                                            ))
                ->add('silhouette', 'choice', array(
                        'label'  => 'Silhouette :',
                        'empty_value' => 'Sélectionnez',
                        'choices'   => array(
                            'Mince'                     => 'Mince', 
                            'Normale'                   => 'Normale', 
                            'Quelques kilos en trop'    => 'Quelques kilos en trop', 
                            'Ronde'                     => 'Ronde', 
                            'Sportive'                  => 'Sportive',
                            ),
                        'required'     => false,

                    ))
                ->add('cheveux', 'choice', array(
                        'label'  => 'Couleur des cheveux :',
                        'empty_value' => 'Sélectionnez',
                        'choices'   => array(
                            'Blancs'    =>  'Blancs', 
                            'Blonds'    =>  'Blonds', 
                            'Bruns'     =>  'Bruns', 
                            'Châtains'  =>  'Châtains', 
                            'Gris'      =>  'Gris', 
                            'Noir'      =>  'Noir', 
                            'Roux'      =>  'Roux',
                            ),
                        'required'     => false,

                    ))
                ->add('yeux', 'choice', array(
                        'label'  => 'Couleur des yeux :',
                        'empty_value' => 'Sélectionnez',
                        'choices'   => array(
                            'Bleus'     =>  'Bleus', 
                            'Marrons'   =>  'Marrons', 
                            'Noirs'     =>  'Noirs', 
                            'Noisettes' =>  'Noisettes', 
                            'Verts'     =>  'Verts',
                            ),
                        'required'     => false,

                    ))
                ->add('ville', 'entity', array(
                        'label'  => 'Ville :',
                        'empty_value' => 'Sélectionnez',
                        'class' => 'MCoreBundle:Ville',
                        'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                        'property' => 'nom',
                        'required'     => false,

                    ))
                ->add('etudes', 'choice', array(
                        'label'  => "Niveau d'études :",
                        'empty_value' => 'Sélectionnez',
                        'choices'   => array(
                            'Je le garde pour moi'          =>  'Je le garde pour moi',
                            'Pas de diplôme'                 =>  'Pas de diplôme', 
                            'Brevet'                         =>  'Brevet', 
                            'BEP'                            =>  'BEP', 
                            'BAC Pro'                        =>  'BAC Pro', 
                            'BAC Général'                    =>  'BAC Général', 
                            'BAC + 2'                        =>  'BAC + 2', 
                            'BAC + 3'                        =>  'BAC + 3', 
                            'BAC + 4'                        =>  'BAC + 4', 
                            'BAC + 5'                        =>  'BAC + 5', 
                            'Doctorat'                       =>  'Doctorat'
                            ),
                        'required'     => false,

                    ))
                ->add('vestimentaire', 'choice', array(
                    'label'  => "Style Vestimentaire",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        'Je le garde pour moi' => 'Je le garde pour moi', 
                        'BCBG'                 => 'BCBG', 
                        'Business'             => 'Business', 
                        'Branché'              => 'Branché', 
                        'Classique'            => 'Classique', 
                        'Décontracté'          => 'Décontracté', 
                        'Sportif'              => 'Sportif' 
                         ),
                    'required'  => false
                ))
                ->add('sport', 'choice', array(
                    'label'  => "Pratique du sport",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                                            'Jamais'        => 'Jamais', 
                                            'Rare'          => 'Rare', 
                                            'Occasionnelle' => 'Occasionnelle', 
                                            'Souvent'       => 'Souvent', 
                                            'Quotidienne'   => 'Quotidienne', 
                                                            ),
                  'required'  => false
                ))
                ->add('fumeur', 'choice', array(
                    'label'  => $aMot['fumeur'],
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        '0' => 'Non', 
                        '1' => 'Oui', 
                        '2' => $aMot['Occasionnel'], 
                                        ),
                   'required'  => false
                    ))
                ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\UserIdeal'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_useridealtype';
    }
}
