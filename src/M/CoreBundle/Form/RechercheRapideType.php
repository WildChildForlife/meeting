<?php

namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RechercheRapideType extends AbstractType
{
    private function getAdultAges($deb=18,$fin=80)
    {
        $aAgetmp = range($deb,$fin);
        foreach ( $aAgetmp as $iVal )
        {
            $aAge[$iVal] = $iVal;
        }
        return $aAge;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
 
        $builder->add('ageDebut','choice',array('choices'     =>  $this->getAdultAges(), 
                                              'label'         =>  'Age entre : ',
                                              'required'     => false,
                                            ))
                ->add('ageFin','choice',array('choices'     =>  $this->getAdultAges(19), 
                                             'label'        => 'et',
                                             'label_attr'   => array('class' => 'width-auto'),
                                             'required'     => false,
                                            ))
                ->add('ville', 'entity', array(
                                             'empty_value'   => 'Sélectionnez une ville',
                                             'class'         => 'MCoreBundle:Ville',
                                             'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                                             'property'      => 'nom',
                                             'label'         => 'Ville : ',
                                             'required'     => false,
                ))
                ->add('username','text',array('label'       => 'Pseudo : ',
                                             'required'     => false,
                                            ))
                ->add('EnLigne', 'choice', array( 
                                                'label'  => 'En ligne :',
                                                'choices'   => array(
                                                    '1' => 'Oui', 
                                                    '0' => 'Indifférent', 
                                                     ),
                                                'data' => 0,
                                                'expanded'     => true,
                                                'multiple'     => false,
                                                'required'     => false,
                ));
               
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_usertype';
    }
}
