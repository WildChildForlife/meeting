<?php

namespace M\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActusType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

            $builder

            ->add('titre', 'text', array(
                    'label'     => 'Titre :',
                    'required'  => true
                    
                ))
            ->add('publication', 'date', array('label'  => 'publication : ',
                                            'input'  => 'datetime',
                                            'widget' => 'single_text',
                                            'required'   => true))
            ->add('date', 'text', array(
                    'label'     => 'date :',
                    'required'  => true,
                    
                ))
            ->add('source', 'text', array(
                    'label'     => 'Source :',
                    'required'  => true,
                    
                ))
            ->add('ville', 'entity', array(
                    'label'  => 'Ville au Maroc :',
                    'empty_value' => 'Sélectionnez',
                    'class' => 'MCoreBundle:Ville',
                    'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.nom', 'ASC'); },
                    'property' => 'nom',
                    'required'  => false
   
                ))
            ->add('description', 'textarea', array(
                    'label'     => 'Texte :',
                    'required'  => true,
                    
                ))

            ->add('accueil', 'choice', array(
                    'label'  => "En Page d'accueil :",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        '1'         => 'oui', 
                        ),
                    'required'  => false,
                    'expanded'     => true,
                    'multiple'     => true,
                    'mapped'  => false
                ))
            ->add('status', 'choice', array(
                    'label'  => "Activer :",
                    'empty_value' => 'Sélectionnez',
                    'choices'   => array(
                        '1'         => 'oui',

                        ),
                    'required'  => false,
                    'expanded'     => true,
                    'multiple'     => true,
                    'mapped'  => false,
                    
                ))
           
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Actus'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_actu';
    }
}