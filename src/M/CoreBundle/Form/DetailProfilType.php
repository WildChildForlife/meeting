<?php

namespace M\CoreBundle\Form;

use Doctrine\ORM\EntityManager ;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

//use M\CoreBundle\Form\EventListener\AddImageFieldSubscriber;

class DetailProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $builder->add('image1', 'hidden', array('required' => false, 'mapped' => false, 'attr' => array('class' => 'ADD1')));
        $builder->add('image2', 'hidden', array('required' => false, 'mapped' => false, 'attr' => array('class' => 'ADD2')));
        $builder->add('image3', 'hidden', array('required' => false, 'mapped' => false, 'attr' => array('class' => 'ADD3')));
        $builder->add('image4', 'hidden', array('required' => false, 'mapped' => false, 'attr' => array('class' => 'ADD4')));
        //$builder->addEventSubscriber(new AddImageFieldSubscriber());
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'M\CoreBundle\Entity\Images'
        ));
    }

    public function getName()
    {
        return 'm_corebundle_imagetype';
    }
}
