<?php

namespace M\CoreBundle\DQL;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;


class TimeDIFF extends FunctionNode {

    /**
     * holds the timestamp of the UNIX_TIMESTAMP DQL statement
     * @var Datetime
     */
    protected $datedeb;
    /**
     * holds the timestamp of the UNIX_TIMESTAMP DQL statement
     * @var Datetime
     */
    protected $datefin;





    /**
     * getSql - allows ORM  to inject a UNIX_TIMESTAMP() statement into an SQL string being constructed
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return void 
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'TIMESTAMPDIFF(YEAR,'  .
                $sqlWalker->walkArithmeticExpression($this->datedeb) .  ','   .
                $sqlWalker->walkArithmeticExpression($this->datefin) .
                ')';

    }

    /**
     * parse - allows DQL to breakdown the DQL string into a processable structure
     * @param \Doctrine\ORM\Query\Parser $parser 
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {

        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_COMMA);

         $this->datedeb = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        

 
        $this->datefin = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);


    }

}


