<?php
namespace M\CoreBundle\Listener;

use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use M\CoreBundle\Entity\User;

class ActivityListener
{
   function __construct($oContainer)
    {
        $this->oEm = $oContainer->get("doctrine")->getManager();
        $this->oContext = $oContainer->get("security.context");
    }

    private $oEm;
    private $oContext;
    // source http://www.symfony-grenoble.fr/238/lister-les-utilisateurs-en-ligne/
    /**
    * Update the user "lastActivity" on each request
    * @param FilterControllerEvent $event
    */
    public function onCoreController(FilterControllerEvent $event)
    {
        // ici nous vérifions que la requête est une "MASTER_REQUEST" pour que les sous-requête soit ingoré (par exemple si vous faites un render() dans votre template)
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }
 
        // Nous vérifions qu'un token d'autentification est bien présent avant d'essayer manipuler l'utilisateur courant.
        if ($this->oContext->getToken()) {
            $oUser = $this->oContext->getToken()->getUser();
 
            // Nous utilisons un délais pendant lequel nous considèrerons que l'utilisateur est toujours actif et qu'il n'est pas nécessaire de refaire de mise à jour
            $delay = new \DateTime();
            $delay->setTimestamp(strtotime('10 minutes ago'));

            // Nous vérifions que l'utilisateur est bien du bon type pour ne pas appeler getLastActivity() sur un objet autre objet User
            if ($oUser instanceof User)
            {
                $dTempsConnexion = ( (int) $oUser->getTempsConnexion() === 0 || $oUser->getTempsConnexion() === NULL ) ? 
                        time() - $oUser->getDateInscription()->getTimestamp() : time() - $oUser->getLastActivity()->getTimestamp();

                if ( $oUser->getLastActivity() < $delay )
                {
                    $oUser->isActiveNow();
                }
                $oUser->setTempsConnexion((int)$dTempsConnexion + $oUser->getTempsConnexion());
                $this->oEm->persist($oUser);
                $this->oEm->flush();
            }
            
        }
    }

}