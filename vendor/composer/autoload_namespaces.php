<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_Extensions_' => array($vendorDir . '/twig/extensions/lib'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Symfony\\Bundle\\SwiftmailerBundle' => array($vendorDir . '/symfony/swiftmailer-bundle'),
    'Symfony\\Bundle\\MonologBundle' => array($vendorDir . '/symfony/monolog-bundle'),
    'Symfony\\Bundle\\AsseticBundle' => array($vendorDir . '/symfony/assetic-bundle'),
    'Symfony\\' => array($vendorDir . '/symfony/symfony/src'),
    'Sensio\\Bundle\\GeneratorBundle' => array($vendorDir . '/sensio/generator-bundle'),
    'Sensio\\Bundle\\FrameworkExtraBundle' => array($vendorDir . '/sensio/framework-extra-bundle'),
    'Sensio\\Bundle\\DistributionBundle' => array($vendorDir . '/sensio/distribution-bundle'),
    'RaulFraile\\Bundle\\LadybugBundle' => array($vendorDir . '/raulfraile/ladybug-bundle'),
    'Ratchet' => array($vendorDir . '/cboden/ratchet/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src'),
    'Metadata\\' => array($vendorDir . '/jms/metadata/src'),
    'Ladybug\\Theme' => array($vendorDir . '/raulfraile/ladybug-themes', $vendorDir . '/raulfraile/ladybug/data/themes'),
    'Ladybug\\Plugin\\Symfony2\\' => array($vendorDir . '/raulfraile/ladybug-themes'),
    'Ladybug\\Plugin\\Extra\\' => array($vendorDir . '/raulfraile/ladybug-themes'),
    'Ladybug\\Plugin' => array($vendorDir . '/raulfraile/ladybug-plugins', $vendorDir . '/raulfraile/ladybug/data/plugins'),
    'Ladybug' => array($vendorDir . '/raulfraile/ladybug-installer/src', $vendorDir . '/raulfraile/ladybug/src'),
    'JMS\\SecurityExtraBundle' => array($vendorDir . '/jms/security-extra-bundle'),
    'JMS\\DiExtraBundle' => array($vendorDir . '/jms/di-extra-bundle'),
    'JMS\\AopBundle' => array($vendorDir . '/jms/aop-bundle'),
    'JMS\\' => array($vendorDir . '/jms/parser-lib/src'),
    'Guzzle\\Stream' => array($vendorDir . '/guzzle/stream'),
    'Guzzle\\Parser' => array($vendorDir . '/guzzle/parser'),
    'Guzzle\\Http' => array($vendorDir . '/guzzle/http'),
    'Guzzle\\Common' => array($vendorDir . '/guzzle/common'),
    'Gregwar\\CaptchaBundle' => array($vendorDir . '/gregwar/captcha-bundle'),
    'Gregwar\\Captcha' => array($vendorDir . '/gregwar/captcha'),
    'Evenement' => array($vendorDir . '/evenement/evenement/src'),
    'Doctrine\\ORM\\' => array($vendorDir . '/doctrine/orm/lib'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\DataFixtures' => array($vendorDir . '/doctrine/data-fixtures/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib'),
    'Doctrine\\Bundle\\FixturesBundle' => array($vendorDir . '/doctrine/doctrine-fixtures-bundle'),
    'Doctrine\\Bundle\\DoctrineBundle' => array($vendorDir . '/doctrine/doctrine-bundle'),
    'CssEmbed' => array($vendorDir . '/ptachoire/cssembed/src'),
    'CoreSphere\\ConsoleBundle' => array($vendorDir . '/winzou/console-bundle'),
    'CG\\' => array($vendorDir . '/jms/cg/src'),
    'Assetic' => array($vendorDir . '/kriswallsmith/assetic/src'),
    '' => array($baseDir . '/src'),
);
